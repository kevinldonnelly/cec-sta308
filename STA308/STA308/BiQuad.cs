﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace STA308
{
    class BiQuad
    {
      
       double W;
       double alpha;

       public double CxHx0;//b2
       public double CxHx1;//b0/2
       public double CxHx2;//-a2
       public double CxHx3;//-a1/2
       public double CxHx4;//b1/2

       public string CxHx0Hex;//b2
       public string CxHx1Hex;//b0/2
       public string CxHx2Hex;//-a2
       public string CxHx3Hex;//-a1/2
       public string CxHx4Hex;//b1/2


       public byte[] CxHx0bytes;//b2
       public byte[] CxHx1bytes;//b0/2
       public byte[] CxHx2bytes;//-a2
       public byte[] CxHx3bytes;//-a1/2
       public byte[] CxHx4bytes;//b1/2

       public double a0 = 1.0;
       public double a1;
       public double a2;
       public double b0;
       public double b1;
       public double b2;
       public double Q = 0.71;
       public double fs = 192000;
       double thetac=0;
       double beta;

       public double fc=0;
       public double gF;


       public filtertype ft;

     public   enum filtertype
        {
            undefined=0,
            lowpass,
            highpass,
            parametric

        };


      public BiQuad(double fc)
       {
           this.fc = fc;
           thetac = 2.0 * Math.PI * fc / fs;
       }

      public BiQuad(double fc, double G)
      {
          this.fc = fc;
          thetac = 2.0 * Math.PI * fc / fs;
          gF = Math.Pow(10.0, G / 20.0);
          beta = (2.0 * thetac / Q) + thetac * thetac + 4;
          b0 = (2.0 * gF * thetac / Q + thetac * thetac + 4) / beta;
          b1 = (2.0 * thetac * thetac - 8.0) / beta;
          b2 = (4.0 - 2.0 * gF * thetac / Q + thetac * thetac) / beta;
          a0 = 1;
          a1 = (2.0 * thetac * thetac - 8.0) / beta;
          a2 = (4.0 - 2.0 * thetac / Q + thetac * thetac) / beta;
          ft = filtertype.parametric;
          GetHexValues();
      }

        public BiQuad(double a1,double a2,double b0, double b1, double b2)
       {
           this.a1 = a1;
           this.a2 = a2;
           this.b0 = b0;
           this.b1 = b1;
           this.b2 = b2;

           double difference = Math.Abs(b0 * .01);

           if (Math.Abs(b2 - b0) < difference)
           {
               
               double K = 0;

               alpha = 4.0 / (a2 - a1 + 1);
           
               Q=-Math.Sqrt(a2-a1+1)*Math.Sqrt(a2+a1+1)/(2*a2-2);

               Q = Math.Round(Q, 3);

               K = Math.Sqrt(a2 / (a2 - a1 + 1) + a1 / (a2 - a1 + 1) + 1 / (a2 - a1 + 1));

               W = K * K;

               thetac = 2.0 * Math.Atan(K);

               fc= Math.Round(thetac * fs /( 2.0 * Math.PI),1);

               if (Math.Abs(b0 - W / alpha) < difference) ft = filtertype.lowpass;
               else if (Math.Abs(b0 - 1.0 / alpha) < difference) ft = filtertype.highpass;
           }

       }


        public void SecondOrderFilter()
        {
         
           double K = Math.Tan(thetac/2.0);
      

           W = K * K;
           alpha = 1.0 + K / Q + W;
       
           a1 = 2.0 * (W - 1.0) / alpha;
           a2 = (1.0 - K / Q + W) / alpha;

       


         }


       
     public byte[] bit24(double v)
        {
            uint minus = 0x80000000;//1000 0000 0000 0000 0000 0000 8,388,608 -1
            uint plus =  0x7FFFFFFF; // 111 1111 1111 1111 1111 1111 8,388,607  1
            if (v<0) return BitConverter.GetBytes((uint)(minus*v));
            else return BitConverter.GetBytes((uint)(plus*v));
        }

   


        public void LowPass()
        {
            b0 = W / alpha;
            b1 = 2.0 * W / alpha;
            b2 = b0;
            ft = filtertype.lowpass;
            GetHexValues();

         


        }

        public void HighPass()
        {
            b0 = 1.0 / alpha;
            b1 = -2.0 / alpha;
            b2 = b0;
            ft = filtertype.highpass;
            GetHexValues();
        

        }



        void GetHexValues()
        {

            CxHx3 = -a1 / 2.0;
            CxHx2 = -a2;

            CxHx3bytes = new byte[4];
            bit24(CxHx3).CopyTo(CxHx3bytes, 0);
            Array.Reverse(CxHx3bytes);
            CxHx3Hex = BitConverter.ToString(CxHx3bytes).Remove(8);

            CxHx2bytes = new byte[4];
            bit24(CxHx2).CopyTo(CxHx2bytes, 0);
            Array.Reverse(CxHx2bytes);
            CxHx2Hex = BitConverter.ToString(CxHx2bytes).Remove(8);

            
            
            CxHx1 = b0 / 2.0;
            CxHx4 = b1 / 2.0;
            CxHx0 = b2;

            CxHx1bytes = new byte[4];
            bit24(CxHx1).CopyTo(CxHx1bytes, 0);
            Array.Reverse(CxHx1bytes);
            CxHx1Hex = BitConverter.ToString(CxHx1bytes).Remove(8);

            CxHx4bytes = new byte[4];
            bit24(CxHx4).CopyTo(CxHx4bytes, 0);
            Array.Reverse(CxHx4bytes);
            CxHx4Hex = BitConverter.ToString(CxHx4bytes).Remove(8);

            CxHx0bytes = new byte[4];
            bit24(CxHx0).CopyTo(CxHx0bytes, 0);
            Array.Reverse(CxHx0bytes);
            CxHx0Hex = BitConverter.ToString(CxHx0bytes).Remove(8);

        }


    }
}
