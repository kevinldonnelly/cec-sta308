﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Collections;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;


namespace STA308
{
    public partial class Form1 : Form
    {


    //    public int b;
        byte[] commandread = {0x97};
        byte[] sta308_writemultiplebytes = new byte[32];
        byte[] sta308_readmultiplebytes = { 0x55, 0x31, 0x00,0x01};
      
        byte[] sta308_readsinglebyte = { 0x53, 0x31 };

        byte[] SetPWMValue = { 0x94, 0x01 ,0x50};
        byte[] SetPWMFrequency = { 0x95,2 };
        string[] regs = { "ConfA", "ConfB", "ConfC", "ConfD", "ConfE", "ConfF", "Mmute", "Mvol", "Cmute", "C1Vol", "C2Vol", "C3Vol", "C4Vol", "C5Vol", "C6Vol", "C7Vol", "C8Vol", "C12im", "C34im", "C56im", "C78im", "C1234ls", "C5678ls", "L1ar", "L1atrt", "L2ar", "L2atrt", "Tone", "Cfaddr", "B2cf1", "B2cf2", "B2cf3", "B0cf1", "B0cf2", "B0cf3", "A2cf1", "A2cf2", "A2cf3", "A1cf1", "A1cf2", "A1cf3", "B1cf1", "B1cf2", "B1cf3", "Cfud" };

      
        ToolTip toolTip1 = new ToolTip();

        BitArray C1234ls = new BitArray(8);
        BitArray C5678ls = new BitArray(8);
        byte[] C1234lsbyte = new byte[1];
        byte[] C5678lsbyte = new byte[1];


        BitArray L1ar = new BitArray(8);
     

        BitArray L1atrt = new BitArray(8);
    


        BitArray L2ar = new BitArray(8);
    



        BitArray L2atrt = new BitArray(8);
   



        byte[] L1arbyte = new byte[1];
        byte[] L1atrtbyte = new byte[1];
        byte[] L2arbyte = new byte[1];
        byte[] L2atrtbyte = new byte[1];


     
        public Form1()
        {
            InitializeComponent();
       

            L1ar.SetAll(false);
            L1atrt.SetAll(false);
            L2ar.SetAll(false);
            L2atrt.SetAll(false);

            Mvol.Text = Properties.Settings.Default.Mvol.ToString();

            vol12.Text = Properties.Settings.Default.vol12.ToString();
            vol34.Text = Properties.Settings.Default.vol34.ToString();
            vol56.Text = Properties.Settings.Default.vol56.ToString();
            vol78.Text = Properties.Settings.Default.vol78.ToString();


            combofreq1.Text = Properties.Settings.Default.freq1.ToString();
            combofreq2.Text = Properties.Settings.Default.freq2.ToString();
            combofreq3.Text = Properties.Settings.Default.freq3.ToString();

            prescale12.Text = Properties.Settings.Default.prescale12.ToString();
            prescale34.Text = Properties.Settings.Default.prescale34.ToString();
            prescale56.Text = Properties.Settings.Default.prescale56.ToString();
            prescale78.Text = Properties.Settings.Default.prescale78.ToString();

            postscale12.Text = Properties.Settings.Default.postscale12.ToString();
            postscale34.Text = Properties.Settings.Default.postscale34.ToString();
            postscale56.Text = Properties.Settings.Default.postscale56.ToString();
            postscale78.Text = Properties.Settings.Default.postscale78.ToString();

            lim12.Text = Properties.Settings.Default.lim12.ToString();
            lim34.Text = Properties.Settings.Default.lim34.ToString();
            lim56.Text = Properties.Settings.Default.lim56.ToString();
            lim78.Text = Properties.Settings.Default.lim78.ToString();

            lim1ar.Text = Properties.Settings.Default.lim1ar.ToString();
            lim1rr.Text = Properties.Settings.Default.lim1rr.ToString();

            lim1at.Text = Properties.Settings.Default.lim1at.ToString();
            lim1rt.Text = Properties.Settings.Default.lim1rt.ToString();



            BitArray myBA = new BitArray(BitConverter.GetBytes(lim1ar.SelectedIndex).ToArray());

            L1ar.Set(0, myBA.Get(0));
            L1ar.Set(1, myBA.Get(1));
            L1ar.Set(2, myBA.Get(2));
            L1ar.Set(3, myBA.Get(3));

            myBA = new BitArray(BitConverter.GetBytes(lim1rr.SelectedIndex).ToArray());
            L1ar.Set(4, myBA.Get(0));
            L1ar.Set(5, myBA.Get(1));
            L1ar.Set(6, myBA.Get(2));
            L1ar.Set(7, myBA.Get(3));


            L1ar.CopyTo(L1arbyte, 0);

            myBA = new BitArray(BitConverter.GetBytes(lim1at.SelectedIndex).ToArray());
            L1atrt.Set(0, myBA.Get(0));
            L1atrt.Set(1, myBA.Get(1));
            L1atrt.Set(2, myBA.Get(2));
            L1atrt.Set(3, myBA.Get(3));

            myBA = new BitArray(BitConverter.GetBytes(lim1rt.SelectedIndex).ToArray());
            L1atrt.Set(4, myBA.Get(0));
            L1atrt.Set(5, myBA.Get(1));
            L1atrt.Set(6, myBA.Get(2));
            L1atrt.Set(7, myBA.Get(3));

            L1atrt.CopyTo(L1atrtbyte,0);


            lim2ar.Text = Properties.Settings.Default.lim2ar.ToString();
            lim2rr.Text = Properties.Settings.Default.lim2rr.ToString();

            lim2at.Text = Properties.Settings.Default.lim2at.ToString();
            lim2rt.Text = Properties.Settings.Default.lim2rt.ToString();

            myBA = new BitArray(BitConverter.GetBytes(lim2ar.SelectedIndex).ToArray());

            L2ar.Set(0, myBA.Get(0));
            L2ar.Set(1, myBA.Get(1));
            L2ar.Set(2, myBA.Get(2));
            L2ar.Set(3, myBA.Get(3));

            myBA = new BitArray(BitConverter.GetBytes(lim2rr.SelectedIndex).ToArray());
            L2ar.Set(4, myBA.Get(0));
            L2ar.Set(5, myBA.Get(1));
            L2ar.Set(6, myBA.Get(2));
            L2ar.Set(7, myBA.Get(3));


            L2ar.CopyTo(L2arbyte, 0);

            myBA = new BitArray(BitConverter.GetBytes(lim2at.SelectedIndex).ToArray());
            L2atrt.Set(0, myBA.Get(0));
            L2atrt.Set(1, myBA.Get(1));
            L2atrt.Set(2, myBA.Get(2));
            L2atrt.Set(3, myBA.Get(3));

            myBA = new BitArray(BitConverter.GetBytes(lim2rt.SelectedIndex).ToArray());
            L2atrt.Set(4, myBA.Get(0));
            L2atrt.Set(5, myBA.Get(1));
            L2atrt.Set(6, myBA.Get(2));
            L2atrt.Set(7, myBA.Get(3));

            L2atrt.CopyTo(L2atrtbyte, 0);
           

            InitPopulateCoeff();

        //    BiQuad bgr = new BiQuad(bq.a1, bq.a2, bq.b0, bq.b1, bq.b2);


            toolTip1.ShowAlways = true;
            // Set up the delays for the ToolTip.
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 1000;
            toolTip1.ReshowDelay = 500;
            listRegs.ShowItemToolTips = true;
            
           //   tBaudrate.Text = _serialPort.BaudRate.ToString();

            // Allow the user to set the appropriate properties.
           SetPortName(Properties.Settings.Default.Port);

			/*
            _serialPort.Open();

            Thread.Sleep(2);

          //  Channel Input Mapping Channels

        

           WriteSTA308Bytes(0x2C, 0);//WA
     
           WriteSTA308Bytes(0x08, 0x0);//no mute, Channel Mute

         */
          

           WritConfigByte(0x07,(byte)(0x30 - Properties.Settings.Default.Mvol * 2), listRegs);

           WritConfigByte(0x08, 0, listRegs);//no mute, Channel Mute

           WritConfigByte(0x09,(byte)(0x30 - Properties.Settings.Default.vol12 * 2), listRegs);

           WritConfigByte(0xA,(byte)(0x30 - Properties.Settings.Default.vol12 * 2), listRegs);

           WritConfigByte(0xB,(byte)(0x30 - Properties.Settings.Default.vol34 * 2), listRegs);

           WritConfigByte(0xC,(byte)(0x30 - Properties.Settings.Default.vol34 * 2), listRegs);

           WritConfigByte(0xD,(byte)(0x30 - Properties.Settings.Default.vol56 * 2), listRegs);

           WritConfigByte(0xE,(byte)(0x30 - Properties.Settings.Default.vol56 * 2), listRegs);

           WritConfigByte(0xF,(byte)(0x30 - Properties.Settings.Default.vol78 * 2), listRegs);

           WritConfigByte(0x10,(byte)(0x30 - Properties.Settings.Default.vol78 * 2), listRegs);



           WritConfigByte(0x11, 16,listRegs);//channel 3&4 from 1&2
           WritConfigByte(0x12, 16,listRegs);//channel 3&4 from 1&2
           WritConfigByte(0x13, 16,listRegs);//channel 5&6 from 1&2
           WritConfigByte(0x14, 16,listRegs);//channel 7&8 from 1&2

          C1234ls.SetAll(false);
          C5678ls.SetAll(false);

           if (lim12.Text == "1")
           {
               C1234ls.Set(0, true);
               C1234ls.Set(1, false);
               C1234ls.Set(2, true);
               C1234ls.Set(3, false);

           }
           else if (lim12.Text == "2")
           {
               C1234ls.Set(0, false);
               C1234ls.Set(1, true);
               C1234ls.Set(2, false);
               C1234ls.Set(3, true);
            }


           if (lim34.Text == "1")
           {
               C1234ls.Set(4, true);
               C1234ls.Set(5, false);
               C1234ls.Set(6, true);
               C1234ls.Set(7, false);

           }
           else if (lim34.Text == "2")
           {
               C1234ls.Set(4, false);
               C1234ls.Set(5, true);
               C1234ls.Set(6, false);
               C1234ls.Set(7, true);
           }

            //--------------------------

           if (lim56.Text == "1")
           {
               C5678ls.Set(0, true);
               C5678ls.Set(1, false);
               C5678ls.Set(2, true);
               C5678ls.Set(3, false);

           }
           else if (lim56.Text == "2")
           {
               C5678ls.Set(0, false);
               C5678ls.Set(1, true);
               C5678ls.Set(2, false);
               C5678ls.Set(3, true);
           }


           if (lim78.Text == "1")
           {
               C5678ls.Set(4, true);
               C5678ls.Set(5, false);
               C5678ls.Set(6, true);
               C5678ls.Set(7, false);

           }
           else if (lim78.Text == "2")
           {
               C5678ls.Set(4, false);
               C5678ls.Set(5, true);
               C5678ls.Set(6, false);
               C5678ls.Set(7, true);
           }

            C1234ls.CopyTo(C1234lsbyte,0);
      
            C5678ls.CopyTo(C5678lsbyte, 0);

            WritConfigByte(0x15, C1234lsbyte[0], listRegs);//Limiter Enable

            WritConfigByte(0x16, C5678lsbyte[0], listRegs);//Limiter Enable

            WritConfigByte(0x17, L1arbyte[0], listRegs);//

            WritConfigByte(0x18, L1atrtbyte[0], listRegs);//

            WritConfigByte(0x19, L2arbyte[0], listRegs);//

            WritConfigByte(0x1A, L2atrtbyte[0], listRegs);//



            BiQuad bq = new BiQuad(Properties.Settings.Default.freq1);
            bq.SecondOrderFilter();
            bq.LowPass();

            WriteBiquad(0, bq, listCoeff); //c1
            WriteBiquad(5, bq, listCoeff); //c1

            WriteBiquad(25, bq, listCoeff); //c2
            WriteBiquad(30, bq, listCoeff); //c2

            bq.HighPass();
            WriteBiquad(50, bq, listCoeff); //c3
            WriteBiquad(55, bq, listCoeff); //c3

            WriteBiquad(75, bq, listCoeff); //c4
            WriteBiquad(80, bq, listCoeff); //c4


            bq = new BiQuad(Properties.Settings.Default.freq2);
            bq.SecondOrderFilter();
            bq.LowPass();

            WriteBiquad(60, bq, listCoeff); //c3
            WriteBiquad(65, bq, listCoeff); //c3

            WriteBiquad(85, bq, listCoeff); //c4
            WriteBiquad(90, bq, listCoeff); //c4

            bq.HighPass();

            WriteBiquad(100, bq, listCoeff); //c5
            WriteBiquad(105, bq, listCoeff); //c5

            WriteBiquad(125, bq, listCoeff); //c6
            WriteBiquad(130, bq, listCoeff); //c6

            bq = new BiQuad(Properties.Settings.Default.freq3);
            bq.SecondOrderFilter();
            bq.LowPass();

            WriteBiquad(110, bq, listCoeff); //c5
            WriteBiquad(115, bq, listCoeff); //c5


            WriteBiquad(135, bq, listCoeff); //c6
            WriteBiquad(140, bq, listCoeff); //c6

            bq.HighPass();

            WriteBiquad(150, bq, listCoeff); //c7
            WriteBiquad(155, bq, listCoeff); //c7

            WriteBiquad(175, bq, listCoeff); //c8
            WriteBiquad(180, bq, listCoeff); //c8



            WritePreScale(0, Properties.Settings.Default.prescale12);

            WritePreScale(2, Properties.Settings.Default.prescale34);

            WritePreScale(4, Properties.Settings.Default.prescale56);

            WritePreScale(6, Properties.Settings.Default.prescale78);

            WritePostScale(0, Properties.Settings.Default.postscale12);

            WritePostScale(2, Properties.Settings.Default.postscale34);

            WritePostScale(4, Properties.Settings.Default.postscale56);

            WritePostScale(6, Properties.Settings.Default.postscale78);





         //   _serialPort.Close();
         
        }

        void PoulateSta308RegList()
        {
            listRegs.Items.Clear();

            listRegs.BeginUpdate();
            for (int i = 0; i < regs.Count(); i++)
            {
                string hex = Convert.ToString(i, 16);
                ListViewItem item=   listRegs.Items.Add(hex);
           
                item.SubItems.Add(regs[i]);

            }

            byte[] b = ReadSTA308Bytes(0, (byte)listRegs.Items.Count);

           for(int i = 0; i < listRegs.Items.Count; i++)
            {
              ListViewItem item=  listRegs.Items[i];
              byte address = Convert.ToByte(item.Text.TrimEnd('h'), 16);
          //   int data = ReadSTA308Byte(address);
             int data=b[i];

              BitArray myBA = new BitArray(BitConverter.GetBytes(data).ToArray());
          

              string bin = Convert.ToString(data, 16);
              item.SubItems.Add(bin);
              for (int ii = 7; ii >= 0; ii--)
              {

                  if (myBA.Get(ii)) item.SubItems.Add("1");
                  else item.SubItems.Add("0");
              }
            


             // Thread.Sleep(2);
            }
           listRegs.EndUpdate();
           listRegs.Items[0].SubItems[3].Tag = "MPC: Max Power Correction";
           listRegs.Items[0].SubItems[4].Tag = "HPE: DDX Headphone Enable";
           listRegs.Items[0].SubItems[5].Tag = "BME: Bass Management Enable";
           listRegs.Items[0].SubItems[6].Tag = "IR1: Interpolation Ratio Select";
           listRegs.Items[0].SubItems[7].Tag = "IR0: Interpolation Ratio Select";
           listRegs.Items[0].SubItems[8].Tag = "MCS2: Master Clock Select : Selects the ratio between the input I2S sample frequency and the input clock";
           listRegs.Items[0].SubItems[9].Tag = "MCS1: Master Clock Select : Selects the ratio between the input I2S sample frequency and the input clock";
           listRegs.Items[0].SubItems[10].Tag= "MCS0: Master Clock Select : Selects the ratio between the input I2S sample frequency and the input clock";

           listRegs.Items[1].SubItems[3].Tag = "DRC: Dynamic Range Compression/Anti-Clipping";
           listRegs.Items[1].SubItems[4].Tag = "ZCE: Zero-Crossing Volume Enable";
           listRegs.Items[1].SubItems[5].Tag = "SAIFB: Determines MSB or LSB first for all SAI formats";
           listRegs.Items[1].SubItems[6].Tag = "SAI2: Serial Audio Input Interface Format";
           listRegs.Items[1].SubItems[7].Tag = "SAI1: Serial Audio Input Interface Format";
           listRegs.Items[1].SubItems[8].Tag = "SAI0: Serial Audio Input Interface Format";
           listRegs.Items[1].SubItems[9].Tag = "ZDE: Zero-Detect Mute Enable";
           listRegs.Items[1].SubItems[10].Tag= "DSPB: DSP Bypass Bit";

           listRegs.Items[2].SubItems[3].Tag = "HPB: High-Pass Filter Bypass Bit";
           listRegs.Items[2].SubItems[4].Tag = "Reserved";
           listRegs.Items[2].SubItems[5].Tag = "Reserved";
           listRegs.Items[2].SubItems[6].Tag = "Reserved";
           listRegs.Items[2].SubItems[7].Tag = "Reserved";
           listRegs.Items[2].SubItems[8].Tag = "Reserved";
           listRegs.Items[2].SubItems[9].Tag = "OM1: DDX Power Output Mode";
           listRegs.Items[2].SubItems[10].Tag= "OMO: DDX Power Output Mode";

           listRegs.Items[3].SubItems[3].Tag = "BQL: Biquad Link :0 Each Channel uses coefficient value";
           listRegs.Items[3].SubItems[4].Tag = "PSL: Post-Scale Link :0 – Each Channel uses individual Post-Scale value";
           listRegs.Items[3].SubItems[5].Tag = "COS1: Clock Output Select";
           listRegs.Items[3].SubItems[6].Tag = "COS0: Clock Output Select";
           listRegs.Items[3].SubItems[7].Tag = "C78B0: Channels 7&8 Binary Output Mode Enable Bits";
           listRegs.Items[3].SubItems[8].Tag = "C56B0: Channels 5&6 Binary Output Mode Enable Bits";
           listRegs.Items[3].SubItems[9].Tag = "C34B0: Channels 3&4 Binary Output Mode Enable Bits";
           listRegs.Items[3].SubItems[10].Tag= "C12BO: Channels 1&2  Binary Output Mode Enable Bits";

           listRegs.Items[4].SubItems[3].Tag = "Reserved";
           listRegs.Items[4].SubItems[4].Tag = "SAOFB: Determines MSB or LSB first for all SAO formats";
           listRegs.Items[4].SubItems[5].Tag = "SAO2: Serial Audio Output Interface Format";
           listRegs.Items[4].SubItems[6].Tag = "SAO1: Serial Audio Output Interface Format";
           listRegs.Items[4].SubItems[7].Tag = "SAO0: Serial Audio Output Interface Format";
           listRegs.Items[4].SubItems[8].Tag = "DEMP: Deemphasis : 0 – No Deemphasis, 1- Deemphasis";
           listRegs.Items[4].SubItems[9].Tag = "VOLEN: Volume Enable: 0 – Volume Operation Bypassed";
           listRegs.Items[4].SubItems[10].Tag= "MIXE: Mix Enable: 0 – Normal Operation";

           listRegs.Items[5].SubItems[3].Tag = "EAPD: External Amplifier Power Down 0 – External Power Stage Power Down Active";
           listRegs.Items[5].SubItems[4].Tag = "";
           listRegs.Items[5].SubItems[5].Tag = "";
           listRegs.Items[5].SubItems[6].Tag = "";
           listRegs.Items[5].SubItems[7].Tag = "AME: AM Mode Enable : 0 – Normal DDX operation";
           listRegs.Items[5].SubItems[8].Tag = "COD: Disable: 0 – Clock Output Normal";
           listRegs.Items[5].SubItems[9].Tag = "SID: Serial Interface (I2S Out) Disable: 0 – I2S Output Normal";
           listRegs.Items[5].SubItems[10].Tag= "PWMD: PWM Output Disable: 0 – PWM Output Normal";

           listRegs.Items[6].SubItems[6].Tag = "MMUTE: Master Mute Register";

           listRegs.Items[7].SubItems[3].Tag = "MV7: Master Volume Offset";
           listRegs.Items[7].SubItems[4].Tag = "MV6: Master Volume Offset";
           listRegs.Items[7].SubItems[5].Tag = "MV5: Master Volume Offset";
           listRegs.Items[7].SubItems[6].Tag = "MV4: Master Volume Offset";
           listRegs.Items[7].SubItems[7].Tag = "MV3: Master Volume Offset";
           listRegs.Items[7].SubItems[8].Tag = "MV2: Master Volume Offset";
           listRegs.Items[7].SubItems[9].Tag = "MV1: Master Volume Offset";
           listRegs.Items[7].SubItems[10].Tag = "MV0: Master Volume Offset";

           listRegs.Items[8].SubItems[3].Tag = "C8M: Channel 8 Mute";
           listRegs.Items[8].SubItems[4].Tag = "C7M: Channel 7 Mute";
           listRegs.Items[8].SubItems[5].Tag = "C6M: Channel 6 Mute";
           listRegs.Items[8].SubItems[6].Tag = "C5M: Channel 5 Mute";
           listRegs.Items[8].SubItems[7].Tag = "C4M: Channel 4 Mute";
           listRegs.Items[8].SubItems[8].Tag = "C3M: Channel 3 Mute";
           listRegs.Items[8].SubItems[9].Tag = "C2M: Channel 2 Mute";
           listRegs.Items[8].SubItems[10].Tag = "C1M: Channel 1 Mute";

           listRegs.Items[9].SubItems[3].Tag = "C1V7: Channel 1 Volume";
           listRegs.Items[9].SubItems[4].Tag = "C1V6: Channel 1 Volume";
           listRegs.Items[9].SubItems[5].Tag = "C1V5: Channel 1 Volume";
           listRegs.Items[9].SubItems[6].Tag = "C1V4: Channel 1 Volume";
           listRegs.Items[9].SubItems[7].Tag = "C1V3: Channel 1 Volume";
           listRegs.Items[9].SubItems[8].Tag = "C1V2 Channel 1 Volume";
           listRegs.Items[9].SubItems[9].Tag = "C1V1: Channel 1 Volume";
           listRegs.Items[9].SubItems[10].Tag = "C1V0: Channel 1 Volume";

           listRegs.Items[10].SubItems[3].Tag = "C2V7: Channel 2 Volume";
           listRegs.Items[10].SubItems[4].Tag = "C2V6: Channel 2 Volume";
           listRegs.Items[10].SubItems[5].Tag = "C2V5: Channel 2 Volume";
           listRegs.Items[10].SubItems[6].Tag = "C2V4: Channel 2 Volume";
           listRegs.Items[10].SubItems[7].Tag = "C2V3: Channel 2 Volume";
           listRegs.Items[10].SubItems[8].Tag = "C2V2 Channel 2 Volume";
           listRegs.Items[10].SubItems[9].Tag = "C2V1: Channel 2 Volume";
           listRegs.Items[10].SubItems[10].Tag = "C2V0: Channel 2 Volume";

           listRegs.Items[11].SubItems[3].Tag = "C3V7: Channel 3 Volume";
           listRegs.Items[11].SubItems[4].Tag = "C3V6: Channel 3 Volume";
           listRegs.Items[11].SubItems[5].Tag = "C3V5: Channel 3 Volume";
           listRegs.Items[11].SubItems[6].Tag = "C3V4: Channel 3 Volume";
           listRegs.Items[11].SubItems[7].Tag = "C3V3: Channel 3 Volume";
           listRegs.Items[11].SubItems[8].Tag = "C3V2 Channel 3 Volume";
           listRegs.Items[11].SubItems[9].Tag = "C3V1: Channel 3 Volume";
           listRegs.Items[11].SubItems[10].Tag = "C3V0: Channel 3 Volume";

           listRegs.Items[12].SubItems[3].Tag = "C4V7: Channel 4 Volume";
           listRegs.Items[12].SubItems[4].Tag = "C4V6: Channel 4 Volume";
           listRegs.Items[12].SubItems[5].Tag = "C4V5: Channel 4 Volume";
           listRegs.Items[12].SubItems[6].Tag = "C4V4: Channel 4 Volume";
           listRegs.Items[12].SubItems[7].Tag = "C4V3: Channel 4 Volume";
           listRegs.Items[12].SubItems[8].Tag = "C4V2 Channel 4 Volume";
           listRegs.Items[12].SubItems[9].Tag = "C4V1: Channel 4 Volume";
           listRegs.Items[12].SubItems[10].Tag = "C4V0: Channel 4 Volume";

           listRegs.Items[13].SubItems[3].Tag = "C5V7: Channel 5 Volume";
           listRegs.Items[13].SubItems[4].Tag = "C5V6: Channel 5 Volume";
           listRegs.Items[13].SubItems[5].Tag = "C5V5: Channel 5 Volume";
           listRegs.Items[13].SubItems[6].Tag = "C5V4: Channel 5 Volume";
           listRegs.Items[13].SubItems[7].Tag = "C5V3: Channel 5 Volume";
           listRegs.Items[13].SubItems[8].Tag = "C5V2 Channel 5 Volume";
           listRegs.Items[13].SubItems[9].Tag = "C5V1: Channel 5 Volume";
           listRegs.Items[13].SubItems[10].Tag = "C5V0: Channel 5 Volume";

           listRegs.Items[14].SubItems[3].Tag = "C6V7: Channel 6 Volume";
           listRegs.Items[14].SubItems[4].Tag = "C6V6: Channel 6 Volume";
           listRegs.Items[14].SubItems[5].Tag = "C6V5: Channel 6 Volume";
           listRegs.Items[14].SubItems[6].Tag = "C6V4: Channel 6 Volume";
           listRegs.Items[14].SubItems[7].Tag = "C6V3: Channel 6 Volume";
           listRegs.Items[14].SubItems[8].Tag = "C6V2 Channel 6 Volume";
           listRegs.Items[14].SubItems[9].Tag = "C6V1: Channel 6 Volume";
           listRegs.Items[14].SubItems[10].Tag = "C6V0: Channel 6 Volume";

           listRegs.Items[15].SubItems[3].Tag = "C7V7: Channel 7 Volume";
           listRegs.Items[15].SubItems[4].Tag = "C7V6: Channel 7 Volume";
           listRegs.Items[15].SubItems[5].Tag = "C7V5: Channel 7 Volume";
           listRegs.Items[15].SubItems[6].Tag = "C7V4: Channel 7 Volume";
           listRegs.Items[15].SubItems[7].Tag = "C7V3: Channel 7 Volume";
           listRegs.Items[15].SubItems[8].Tag = "C7V2 Channel 7 Volume";
           listRegs.Items[15].SubItems[9].Tag = "C7V1: Channel 7 Volume";
           listRegs.Items[15].SubItems[10].Tag = "C7V0: Channel 7 Volume";

           listRegs.Items[16].SubItems[3].Tag = "C8V7: Channel 8 Volume";
           listRegs.Items[16].SubItems[4].Tag = "C8V6: Channel 8 Volume";
           listRegs.Items[16].SubItems[5].Tag = "C8V5: Channel 8 Volume";
           listRegs.Items[16].SubItems[6].Tag = "C8V4: Channel 8 Volume";
           listRegs.Items[16].SubItems[7].Tag = "C8V3: Channel 8 Volume";
           listRegs.Items[16].SubItems[8].Tag = "C8V2 Channel 8 Volume";
           listRegs.Items[16].SubItems[9].Tag = "C8V1: Channel 8 Volume";
           listRegs.Items[16].SubItems[10].Tag = "C8V0: Channel 8 Volume";

           listRegs.Items[17].SubItems[3].Tag = "";
           listRegs.Items[17].SubItems[4].Tag = "C2IM0: Channel Input Mapping Channels 2";
           listRegs.Items[17].SubItems[5].Tag = "C2IM0: Channel Input Mapping Channels 2";
           listRegs.Items[17].SubItems[6].Tag = "C2IM0: Channel Input Mapping Channels 2";
           listRegs.Items[17].SubItems[7].Tag = "";
           listRegs.Items[17].SubItems[8].Tag = "C1IM0 Channel Input Mapping Channels 1";
           listRegs.Items[17].SubItems[9].Tag = "C1IM0: Channel Input Mapping Channels 1";
           listRegs.Items[17].SubItems[10].Tag = "C1IM0: Channel Input Mapping Channels 1";

           listRegs.Items[18].SubItems[3].Tag = "";
           listRegs.Items[18].SubItems[4].Tag = "C4IM0: Channel Input Mapping Channels 4";
           listRegs.Items[18].SubItems[5].Tag = "C4IM0: Channel Input Mapping Channels 4";
           listRegs.Items[18].SubItems[6].Tag = "C4IM0: Channel Input Mapping Channels 4";
           listRegs.Items[18].SubItems[7].Tag = "";
           listRegs.Items[18].SubItems[8].Tag = "C3IM0: Channel Input Mapping Channels 3";
           listRegs.Items[18].SubItems[9].Tag = "C3IM0: Channel Input Mapping Channels 3";
           listRegs.Items[18].SubItems[10].Tag = "C3IM0: Channel Input Mapping Channels 3";


           listRegs.Items[19].SubItems[3].Tag = "";
           listRegs.Items[19].SubItems[4].Tag = "C6IM0: Channel Input Mapping Channels 6";
           listRegs.Items[19].SubItems[5].Tag = "C6IM0: Channel Input Mapping Channels 6";
           listRegs.Items[19].SubItems[6].Tag = "C6IM0: Channel Input Mapping Channels 6";
           listRegs.Items[19].SubItems[7].Tag = "";
           listRegs.Items[19].SubItems[8].Tag = "C5IM0: Channel Input Mapping Channels 5";
           listRegs.Items[19].SubItems[9].Tag = "C5IM0: Channel Input Mapping Channels 5";
           listRegs.Items[19].SubItems[10].Tag = "C5IM0: Channel Input Mapping Channels 5";

           listRegs.Items[20].SubItems[3].Tag = "";
           listRegs.Items[20].SubItems[4].Tag = "C8IM0: Channel Input Mapping Channels 8";
           listRegs.Items[20].SubItems[5].Tag = "C8IM0: Channel Input Mapping Channels 8";
           listRegs.Items[20].SubItems[6].Tag = "C8IM0: Channel Input Mapping Channels 8";
           listRegs.Items[20].SubItems[7].Tag = "";
           listRegs.Items[20].SubItems[8].Tag = "C7IM0: Channel Input Mapping Channels 7";
           listRegs.Items[20].SubItems[9].Tag = "C7IM0: Channel Input Mapping Channels 7";
           listRegs.Items[20].SubItems[10].Tag = "C7IM0: Channel Input Mapping Channels 7";

           listRegs.Items[21].SubItems[3].Tag = "C1LS1: Channel Limiter Select Channel 1";
           listRegs.Items[21].SubItems[4].Tag = "C1LS0: Channel Limiter Select Channel 1";
           listRegs.Items[21].SubItems[5].Tag = "C2LS1: Channel Limiter Select Channel 2";
           listRegs.Items[21].SubItems[6].Tag = "C2LS0: Channel Limiter Select Channel 2";
           listRegs.Items[21].SubItems[7].Tag = "C3LS1: Channel Limiter Select Channel 3";
           listRegs.Items[21].SubItems[8].Tag = "C3LS0: Channel Limiter Select Channels 3";
           listRegs.Items[21].SubItems[9].Tag = "C4LS1: Channel Limiter Select Channel 4";
           listRegs.Items[21].SubItems[10].Tag = "C4LS0: Channel Limiter Select Channel 4";



           //  PopulateCoeff(); listRegs.Items[20].SubItems[3].Tag = "";
           listRegs.Items[22].SubItems[3].Tag = "C8LS1: Channel Limiter Select Channel 8";
           listRegs.Items[22].SubItems[4].Tag = "C8LS0: Channel Limiter Select Channel 8";
           listRegs.Items[22].SubItems[5].Tag = "C7LS1: Channel Limiter Select Channel 7";
           listRegs.Items[22].SubItems[6].Tag = "C7LS0: Channel Limiter Select Channel 7";
           listRegs.Items[22].SubItems[7].Tag = "C6LS1: Channel Limiter Select Channel 6";
           listRegs.Items[22].SubItems[8].Tag = "C6LS0: Channel Limiter Select Channel 6";
           listRegs.Items[22].SubItems[9].Tag = "C5LS1: Channel Limiter Select Channel 5";
           listRegs.Items[22].SubItems[10].Tag = "C5LS0: Channel Limiter Select Channel 5";
        }

        void PopulateFromDevice()
        {
            byte Channel;
            byte index = 0;
            byte Biquad = 0;
            double b2=0;
            double b0=0;
            double b1=0;
            double a1=0;
            double a2=0;
            BiQuad bq;

            for (Channel = 1; Channel < 9; Channel++)
            {
                for (Biquad = 1; Biquad < 6; Biquad++)
                {
                    for (byte coeff = 0; coeff < 5; coeff++)
                    {
                        byte[] input = ReadCoefficient(index);

                        byte[] fourbyte = new byte[4];
                        fourbyte[0] = input[0];
                        fourbyte[1] = input[1];
                        fourbyte[2] = input[2];
                        fourbyte[3] = 0;
                        string hex = BitConverter.ToString(input);
                        Array.Reverse(fourbyte);
                        int c2 = BitConverter.ToInt32(fourbyte, 0);
                  

                        double k;
                        if (fourbyte[0] < 128) k = (float)c2 / (float)0x7FFFFFFF;
                        else k = -(float)c2 / (float)0x80000000;
                        //   ii.SubItems.Add(k.ToString());
                        //  ii.SubItems.Add(BitConverter.ToUInt32(fourbyte, 0).ToString());

                        if (coeff == 0) b2 = k;
                        else if (coeff == 1) b0 = k * 2.0;
                        else if (coeff == 2) a2 = -k;
                        else if (coeff == 3) a1 = -k * 2.0;
                        else if (coeff == 4)
                        {
                            b1 = k * 2.0;
                            string type = "undefined";
                            bq = new BiQuad(a1, a2, b0, b1, b2);
                            if (bq.ft == BiQuad.filtertype.highpass) type = "highpass";
                            if (bq.ft == BiQuad.filtertype.lowpass) type = "lowpass";

                         //   ii.SubItems.Add("fc=" + bq.fc.ToString() + " Q=" + bq.Q.ToString() + " " + type);
                        }

                    }

                }

            }
        }


        void InitPopulateCoeff()
        {
      
            byte Channel;
            byte index=0;
            byte Biquad = 0;
     
            string[] coeffdesc={"b2","b0/2","-a2","-a1/2","b1/2"};

            listCoeff.Items.Clear();
            listCoeff.BeginUpdate();

            ListViewItem ii;

            for (Channel = 1; Channel < 9; Channel++)
            {
                for (Biquad = 1; Biquad < 6; Biquad++)
                {
                    for (byte coeff = 0;coeff < 5; coeff++)
                    {
                        
                     
                        ii = listCoeff.Items.Add(index.ToString());
                        //  ii.SubItems.Add(hex);
                        ii.SubItems.Add("Ch " + Channel.ToString() + " - Biquad " + Biquad.ToString() + "- Coeff " + coeffdesc[coeff]);

                        if (coeff == 1)
                        {
                            ii.SubItems.Add("0.5");
                            ii.SubItems.Add("3F-FF-FF");
                        }
                        else
                        {
                            ii.SubItems.Add("0.0");
                            ii.SubItems.Add("00-00-00");

                        }
                        
                
                        index++;
               
                    }
                }
            }

            for (int d = 1; d < 9; d++)
            {
                ii = listCoeff.Items.Add(index.ToString());
                ii.SubItems.Add("Ch " + d.ToString() + " - Prescale");
                ii.SubItems.Add("0");
                ii.SubItems.Add("80-00-00");
                index++;
            }

            for (int d = 1; d < 9; d++)
            {
                ii = listCoeff.Items.Add(index.ToString());
                ii.SubItems.Add("Ch " + d.ToString() + " - BassM scale");
                ii.SubItems.Add("0");
                ii.SubItems.Add("00-00-00");
                index++;
            }

            for (int d = 1; d < 9; d++)
            {
                ii = listCoeff.Items.Add(index.ToString());
                ii.SubItems.Add("Ch " + d.ToString() + " - Post scale");
                ii.SubItems.Add("0");
                ii.SubItems.Add("80-00-00");
                index++;
            }

            listCoeff.EndUpdate();

        }



        byte[] ReadCoefficient(byte address)
        {

           WriteSTA308Bytes(0x1C, address);
       
           return ReadSTA308Bytes(0x1D, 3);

          }

        long WriteCoefficient(byte address, byte[] data)
        {
            byte[] buffer = new byte[4];

            buffer[0] = address;

            buffer[1] = data[0];
            buffer[2] = data[1];
            buffer[3] = data[2] ;


            WriteSTA308Bytes(0x1C, buffer, 4);

        //    WriteSTA308Bytes(0x1C,address);

        //    WriteSTA308Bytes(0x1D,topbyte);
         //   WriteSTA308Bytes(0x1E,middlebyte);
         //   WriteSTA308Bytes(0x1F,bottombyte);
            WriteSTA308Bytes(0x2C, 1);//W1
            return 1;

        }

        long WriteCoefficient(byte address, byte[] data,ListView lv)
        {
            
            uint v= BitConverter.ToUInt32(data, 0);
            double x = (double)v / 0x80000000;

       
            byte[] buffer = new byte[4];
            data.CopyTo(buffer, 0);
            Array.Reverse(buffer);

            lv.Items[address].SubItems[2].Text = x.ToString("e2");
            lv.Items[address].SubItems[3].Text = BitConverter.ToString(buffer).Remove(8);
            lv.Items[address].ForeColor = System.Drawing.Color.Blue;
            lv.Items[address].UseItemStyleForSubItems = false;

            return 1;

        }

      //  string[] coeffdesc = { "b2", "b0/2", "-a2", "-a1/2", "b1/2" };
        void WriteBiquad(byte start, BiQuad bq,ListView lv)
        {
        
          
           lv.Items[start].SubItems[2].Text= bq.CxHx0.ToString("e2");
           lv.Items[start].SubItems[3].Text=bq.CxHx0Hex;
           lv.Items[start].Tag = bq;
           lv.Items[start].ForeColor = System.Drawing.Color.Blue;
           lv.Items[start].UseItemStyleForSubItems = false;


           lv.Items[start+1].SubItems[2].Text=bq.CxHx1.ToString("e2");
           lv.Items[start+1].SubItems[3].Text=bq.CxHx1Hex;
           lv.Items[start+1].ForeColor = System.Drawing.Color.Blue;
           lv.Items[start+1].UseItemStyleForSubItems = false;


           lv.Items[start+2].SubItems[2].Text=bq.CxHx2.ToString("e2");
           lv.Items[start+2].SubItems[3].Text=bq.CxHx2Hex;
           lv.Items[start+2].ForeColor = System.Drawing.Color.Blue;
           lv.Items[start+2].UseItemStyleForSubItems = false;

           lv.Items[start+3].SubItems[2].Text=bq.CxHx3.ToString("e2");
           lv.Items[start+3].SubItems[3].Text=bq.CxHx3Hex;
           lv.Items[start+3].ForeColor = System.Drawing.Color.Blue;
           lv.Items[start+3].UseItemStyleForSubItems = false;

           lv.Items[start+4].SubItems[2].Text=bq.CxHx4.ToString("e2");
           lv.Items[start+4].SubItems[3].Text=bq.CxHx4Hex;
           lv.Items[start+4].ForeColor = System.Drawing.Color.Blue;
           lv.Items[start+4].UseItemStyleForSubItems = false;

        }


        void WritePreScale(byte channel, double dbvalue)
        {
    
            double scale= Math.Pow(10, dbvalue / 10.0);
            double v = 0x80000000 * scale;
            byte[] b = BitConverter.GetBytes((uint)v);

            WriteCoefficient((byte)(200 + channel), b, listCoeff);
            WriteCoefficient((byte)(201 + channel), b, listCoeff);

         
        }


        void WritePostScale(byte channel, double dbvalue)
        {

            double scale = Math.Pow(10, dbvalue / 10.0);
            double v = 0x80000000 * scale;
            byte[] b = BitConverter.GetBytes((uint)v);

            WriteCoefficient((byte)(216 + channel), b, listCoeff);
            WriteCoefficient((byte)(217 + channel), b, listCoeff);


        }

        void WritConfigByte(byte address, byte data, ListView lv)
        {
            lv.Items[address].SubItems[2].Text=Convert.ToString(data, 16);
            lv.Items[address].ForeColor = System.Drawing.Color.Blue;
            lv.Items[address].UseItemStyleForSubItems = false;


        }


        void WriteBiquad(byte start,BiQuad bq)
        {

        //    byte[] a1 = bq.bit24(-bq.a1/2.0);
        //    byte[] a2 = bq.bit24(-bq.a2);

        //    byte[] b0 = bq.bit24(bq.b0/2.0);
         //   byte[] b1 = bq.bit24(bq.b1/2.0);
         //   byte[] b2 = bq.bit24(bq.b2);

            byte[] buffer = new byte[17];

            buffer[0] = start;
        //    buffer[1] = b2[2];
         //   buffer[2] = b2[1];
         //   buffer[3] = b2[0];

            buffer[1] = bq.CxHx0bytes[0];
            buffer[2] = bq.CxHx0bytes[1];
            buffer[3] = bq.CxHx0bytes[2];


          //  buffer[4] = b0[2];
         //   buffer[5] = b0[1];
         //   buffer[6] = b0[0];

            buffer[4] = bq.CxHx1bytes[0];
            buffer[5] = bq.CxHx1bytes[1];
            buffer[6] = bq.CxHx1bytes[2];

       //     buffer[7] = a2[2];
         ////   buffer[8] = a2[1];
        //    buffer[9] = a2[0];

            buffer[7] = bq.CxHx2bytes[0];
            buffer[8] = bq.CxHx2bytes[1];
            buffer[9] = bq.CxHx2bytes[2];


      //      buffer[10] = a1[2];
        //    buffer[11] = a1[1];
        //    buffer[12] = a1[0];

            buffer[10] = bq.CxHx3bytes[0];
            buffer[11] = bq.CxHx3bytes[1];
            buffer[12] = bq.CxHx3bytes[2];

        //    buffer[13] = b1[2];
         //   buffer[14] = b1[1];
         //   buffer[15] = b1[0];

            buffer[13] = bq.CxHx4bytes[0];
            buffer[14] = bq.CxHx4bytes[1];
            buffer[15] = bq.CxHx4bytes[2];

            buffer[16] = 2;

            WriteSTA308Bytes(0x2C, 0);//WA

            WriteSTA308Bytes(0x1C, buffer, 17);
        

        }

       


       


        public void SetPortName(string defaultPortName)
        {
           

            string[] ports = SerialPort.GetPortNames();

            foreach (string port in ports) cPortNames.Items.Add(port);

           int index=cPortNames.FindString(defaultPortName);
           if (index > 0)
           {
               cPortNames.SelectedIndex = index;
               _serialPort.PortName = defaultPortName;
           }
        }


        private  void DataReceivedHandler(
                        object sender,
                        SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            int indata = sp.ReadByte();
            //lrecieved.Items.Add(indata);
            
        }

        private void bCommand_Click(object sender, EventArgs e)
        {

            lout.Text = "";
         //   _serialPort.Open();
   
           PoulateSta308RegList();
           InitPopulateCoeff();
        //    _serialPort.Close();

        }





      

        int ReadSTA308Byte()
        {
            _serialPort.DiscardInBuffer();
            _serialPort.Write(sta308_readsinglebyte, 0, 2);
            return _serialPort.ReadByte();
        }

        byte ReadSTA308Byte(byte address)
        {
            _serialPort.DiscardInBuffer();
            sta308_readmultiplebytes[2]=address;
            sta308_readmultiplebytes[3] = 1;

            _serialPort.Write(sta308_readmultiplebytes, 0, 4);
            return (byte)_serialPort.ReadByte();
        }

        byte[] ReadSTA308Bytes(byte address,byte numbytes)
        {
            byte[] buffer = new byte[numbytes];
            _serialPort.DiscardInBuffer();
            sta308_readmultiplebytes[2] = address;
            sta308_readmultiplebytes[3] = numbytes;

           _serialPort.ReceivedBytesThreshold = numbytes;

            _serialPort.Write(sta308_readmultiplebytes, 0, 4);
            Thread.Sleep(25);

            for (int i = 0; i < numbytes; i++)
            {
             //   _serialPort.Read(buffer, 0, (int)numbytes);
              buffer[i]=  (byte)_serialPort.ReadByte();
            }
         
            return buffer;
        }


        int WriteSTA308Bytes(byte address, byte data)
        {
            sta308_writemultiplebytes[0] = 0x55;
            sta308_writemultiplebytes[1] = 0x30; 
            sta308_writemultiplebytes[2] = address;
            sta308_writemultiplebytes[3] = 1;
            sta308_writemultiplebytes[4] = data;
            _serialPort.Write(sta308_writemultiplebytes, 0, 5);
            Thread.Sleep(20);
            return 1;
        }


        int WriteSTA308Bytes(byte address, byte[] data,byte length)
        {
            _serialPort.DiscardOutBuffer();
            sta308_writemultiplebytes[0] = 0x55; 
            sta308_writemultiplebytes[1] = 0x30; 
            sta308_writemultiplebytes[2] = address;
            sta308_writemultiplebytes[3] = length;

            data.CopyTo(sta308_writemultiplebytes, 4);

            _serialPort.Write(sta308_writemultiplebytes,0,4+length);
        
            Thread.Sleep(20);

            return 1;
        }
    
      

        private void button1_Click(object sender, EventArgs e)
        {
            lout.Text = "";
            lout.Text = Convert.ToString(ReadSTA308Byte(4), 2);
        }

        private void listRegs_MouseMove(object sender, MouseEventArgs e)
        {
          //  ListViewItem item = listRegs.GetItemAt(e.X, e.Y);
           ListViewHitTestInfo info = listRegs.HitTest(e.X, e.Y);
          
            if ((info.SubItem != null) && info.SubItem.Tag != null)
            {
            
                toolTip1.Show(info.SubItem.Tag.ToString(), listRegs, e.X, e.Y, 2000);
           
              
             
            }
            else
            {
              //  toolTip1.SetToolTip(listRegs, "");
              //  toolTip1.Hide(listRegs);
                toolTip1.RemoveAll();
            }

        }

        private void save_Click(object sender, EventArgs e)
        {


            XmlSerializer serializer = new XmlSerializer(typeof(Properties.Settings));
         //   STA308 sta308 = new STA308();
          //  sta308.ConfA = 0x83;
          //  sta308.ConfB = 0x08;
            Stream fs = new FileStream("save.xml", FileMode.Create);
            XmlWriter writer =new XmlTextWriter(fs, Encoding.Unicode);
            // Serialize using the XmlTextWriter.
            serializer.Serialize(writer, Properties.Settings.Default);
            writer.Close();
           

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxVol_SelectedIndexChanged(object sender, EventArgs e)
        {
          //  MessageBox.Show(vol12.SelectedIndex.ToString());
        }

        public void Settings()
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.freq1 = ushort.Parse(combofreq1.Text);
            Properties.Settings.Default.freq2 = ushort.Parse(combofreq2.Text);
            Properties.Settings.Default.freq3 = ushort.Parse(combofreq3.Text);

           Properties.Settings.Default.Mvol = float.Parse(Mvol.Text);
           Properties.Settings.Default.vol12 = float.Parse(vol12.Text);
           Properties.Settings.Default.vol34 = float.Parse(vol34.Text);
           Properties.Settings.Default.vol56 = float.Parse(vol56.Text);
           Properties.Settings.Default.vol78 = float.Parse(vol78.Text);


           Properties.Settings.Default.prescale12 = float.Parse(prescale12.Text);
           Properties.Settings.Default.prescale34 = float.Parse(prescale34.Text);
           Properties.Settings.Default.prescale56 = float.Parse(prescale56.Text);
           Properties.Settings.Default.prescale78 = float.Parse(prescale78.Text);

           Properties.Settings.Default.postscale12 = float.Parse(postscale12.Text);
           Properties.Settings.Default.postscale34 = float.Parse(postscale34.Text);
           Properties.Settings.Default.postscale56 = float.Parse(postscale56.Text);
           Properties.Settings.Default.postscale78 = float.Parse(postscale78.Text);

           Properties.Settings.Default.lim12 = byte.Parse(lim12.Text);
           Properties.Settings.Default.lim34 = byte.Parse(lim34.Text);
           Properties.Settings.Default.lim56 = byte.Parse(lim56.Text);
           Properties.Settings.Default.lim78 = byte.Parse(lim78.Text);

           Properties.Settings.Default.lim1ar = float.Parse(lim1ar.Text);
           Properties.Settings.Default.lim1at = float.Parse(lim1at.Text);
           Properties.Settings.Default.lim1rr = float.Parse(lim1rr.Text);
           Properties.Settings.Default.lim1rt = float.Parse(lim1rt.Text);

           Properties.Settings.Default.lim2ar = float.Parse(lim2ar.Text);
           Properties.Settings.Default.lim2at = float.Parse(lim2at.Text);
           Properties.Settings.Default.lim2rr = float.Parse(lim2rr.Text);
           Properties.Settings.Default.lim2rt = float.Parse(lim2rt.Text);

           Properties.Settings.Default.Port = cPortNames.Text;

           Properties.Settings.Default.Save();


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }


      

          
    }
}
