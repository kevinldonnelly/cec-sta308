﻿namespace STA308
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem50 = new System.Windows.Forms.ListViewItem(new string[] {
            "00h",
            "ConfA",
            "131"}, -1);
            System.Windows.Forms.ListViewItem listViewItem51 = new System.Windows.Forms.ListViewItem(new string[] {
            "01h",
            "ConfB",
            "66"}, -1);
            System.Windows.Forms.ListViewItem listViewItem52 = new System.Windows.Forms.ListViewItem(new string[] {
            "02h",
            "ConfC",
            "124"}, -1);
            System.Windows.Forms.ListViewItem listViewItem53 = new System.Windows.Forms.ListViewItem(new string[] {
            "03h",
            "ConfD",
            "32"}, -1);
            System.Windows.Forms.ListViewItem listViewItem54 = new System.Windows.Forms.ListViewItem(new string[] {
            "04h",
            "ConfE",
            "2"}, -1);
            System.Windows.Forms.ListViewItem listViewItem55 = new System.Windows.Forms.ListViewItem(new string[] {
            "05h",
            "ConfF",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem56 = new System.Windows.Forms.ListViewItem(new string[] {
            "06h",
            "Mmute",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem57 = new System.Windows.Forms.ListViewItem(new string[] {
            "07h",
            "Mvol",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem58 = new System.Windows.Forms.ListViewItem(new string[] {
            "08h",
            "Cmute",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem59 = new System.Windows.Forms.ListViewItem(new string[] {
            "09h",
            "C1Vol",
            "48"}, -1);
            System.Windows.Forms.ListViewItem listViewItem60 = new System.Windows.Forms.ListViewItem(new string[] {
            "0Ah",
            "C2Vol",
            "48"}, -1);
            System.Windows.Forms.ListViewItem listViewItem61 = new System.Windows.Forms.ListViewItem(new string[] {
            "0Bh",
            "C3Vol",
            "48"}, -1);
            System.Windows.Forms.ListViewItem listViewItem62 = new System.Windows.Forms.ListViewItem(new string[] {
            "0Ch",
            "C4Vol",
            "48"}, -1);
            System.Windows.Forms.ListViewItem listViewItem63 = new System.Windows.Forms.ListViewItem(new string[] {
            "0Dh",
            "C5Vol",
            "48"}, -1);
            System.Windows.Forms.ListViewItem listViewItem64 = new System.Windows.Forms.ListViewItem(new string[] {
            "0Eh",
            "C6Vol",
            "48"}, -1);
            System.Windows.Forms.ListViewItem listViewItem65 = new System.Windows.Forms.ListViewItem(new string[] {
            "0Fh",
            "C7Vol",
            "48"}, -1);
            System.Windows.Forms.ListViewItem listViewItem66 = new System.Windows.Forms.ListViewItem(new string[] {
            "10h",
            "C8Vol",
            "48"}, -1);
            System.Windows.Forms.ListViewItem listViewItem67 = new System.Windows.Forms.ListViewItem(new string[] {
            "11h",
            "C12im",
            "10"}, -1);
            System.Windows.Forms.ListViewItem listViewItem68 = new System.Windows.Forms.ListViewItem(new string[] {
            "12h",
            "C34im",
            "10"}, -1);
            System.Windows.Forms.ListViewItem listViewItem69 = new System.Windows.Forms.ListViewItem(new string[] {
            "13h",
            "C56im",
            "10"}, -1);
            System.Windows.Forms.ListViewItem listViewItem70 = new System.Windows.Forms.ListViewItem(new string[] {
            "14h",
            "C78im",
            "10"}, -1);
            System.Windows.Forms.ListViewItem listViewItem71 = new System.Windows.Forms.ListViewItem(new string[] {
            "15h",
            "C1234ls",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem72 = new System.Windows.Forms.ListViewItem(new string[] {
            "16h",
            "C5678ls",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem73 = new System.Windows.Forms.ListViewItem(new string[] {
            "17h",
            "L1ar",
            "A6"}, -1);
            System.Windows.Forms.ListViewItem listViewItem74 = new System.Windows.Forms.ListViewItem(new string[] {
            "18h",
            "L1atrt",
            "67"}, -1);
            System.Windows.Forms.ListViewItem listViewItem75 = new System.Windows.Forms.ListViewItem(new string[] {
            "19h",
            "L2ar",
            "A6"}, -1);
            System.Windows.Forms.ListViewItem listViewItem76 = new System.Windows.Forms.ListViewItem(new string[] {
            "1Ah",
            "L2atrt",
            "67"}, -1);
            System.Windows.Forms.ListViewItem listViewItem77 = new System.Windows.Forms.ListViewItem(new string[] {
            "1Bh",
            "Tone"}, -1);
            System.Windows.Forms.ListViewItem listViewItem78 = new System.Windows.Forms.ListViewItem(new string[] {
            "1Ch",
            "Cfaddr"}, -1);
            System.Windows.Forms.ListViewItem listViewItem79 = new System.Windows.Forms.ListViewItem(new string[] {
            "1Dh",
            "B2cf1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem80 = new System.Windows.Forms.ListViewItem(new string[] {
            "1Eh",
            "B2cf2"}, -1);
            System.Windows.Forms.ListViewItem listViewItem81 = new System.Windows.Forms.ListViewItem(new string[] {
            "1Fh",
            "B2cf3"}, -1);
            System.Windows.Forms.ListViewItem listViewItem82 = new System.Windows.Forms.ListViewItem(new string[] {
            "20h",
            "B0cf1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem83 = new System.Windows.Forms.ListViewItem(new string[] {
            "21h",
            "B0cf2"}, -1);
            System.Windows.Forms.ListViewItem listViewItem84 = new System.Windows.Forms.ListViewItem(new string[] {
            "22h",
            "B0cf3"}, -1);
            System.Windows.Forms.ListViewItem listViewItem85 = new System.Windows.Forms.ListViewItem(new string[] {
            "23h",
            "A2cf1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem86 = new System.Windows.Forms.ListViewItem(new string[] {
            "24h",
            "A2cf2"}, -1);
            System.Windows.Forms.ListViewItem listViewItem87 = new System.Windows.Forms.ListViewItem(new string[] {
            "25h",
            "A2cf3"}, -1);
            System.Windows.Forms.ListViewItem listViewItem88 = new System.Windows.Forms.ListViewItem(new string[] {
            "26h",
            "A1cf1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem89 = new System.Windows.Forms.ListViewItem(new string[] {
            "27h",
            "A1cf2"}, -1);
            System.Windows.Forms.ListViewItem listViewItem90 = new System.Windows.Forms.ListViewItem(new string[] {
            "28h",
            "A1cf3"}, -1);
            System.Windows.Forms.ListViewItem listViewItem91 = new System.Windows.Forms.ListViewItem(new string[] {
            "29h",
            "B1cf1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem92 = new System.Windows.Forms.ListViewItem(new string[] {
            "2Ah",
            "B1cf2"}, -1);
            System.Windows.Forms.ListViewItem listViewItem93 = new System.Windows.Forms.ListViewItem(new string[] {
            "2Bh",
            "B1cf3"}, -1);
            System.Windows.Forms.ListViewItem listViewItem94 = new System.Windows.Forms.ListViewItem(new string[] {
            "2Ch",
            "Cfud"}, -1);
            System.Windows.Forms.ListViewItem listViewItem95 = new System.Windows.Forms.ListViewItem(new string[] {
            "2Dh",
            "DC1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem96 = new System.Windows.Forms.ListViewItem(new string[] {
            "2Eh",
            "DC2"}, -1);
            System.Windows.Forms.ListViewItem listViewItem97 = new System.Windows.Forms.ListViewItem(new string[] {
            "2Fh",
            "BIST1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem98 = new System.Windows.Forms.ListViewItem(new string[] {
            "30h",
            "BIST2"}, -1);
            this.cPortNames = new System.Windows.Forms.ComboBox();
            this.tBaudrate = new System.Windows.Forms.TextBox();
            this.lrecieved = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bCommand = new System.Windows.Forms.Button();
            this.listRegs = new System.Windows.Forms.ListView();
            this.Address = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Name1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Data = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.D7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.D6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.D5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.D4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.D3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.D2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.D1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.D0 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lout = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listCoeff = new System.Windows.Forms.ListView();
            this.Index = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HexValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Description = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Float = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cparams = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabCrossover = new System.Windows.Forms.TabPage();
            this.lim2rt = new System.Windows.Forms.ComboBox();
            this.lim2at = new System.Windows.Forms.ComboBox();
            this.lim2rr = new System.Windows.Forms.ComboBox();
            this.lim2ar = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lim78 = new System.Windows.Forms.ComboBox();
            this.lim56 = new System.Windows.Forms.ComboBox();
            this.lim34 = new System.Windows.Forms.ComboBox();
            this.lim12 = new System.Windows.Forms.ComboBox();
            this.Mvol = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.postscale78 = new System.Windows.Forms.ComboBox();
            this.postscale56 = new System.Windows.Forms.ComboBox();
            this.postscale34 = new System.Windows.Forms.ComboBox();
            this.postscale12 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.prescale78 = new System.Windows.Forms.ComboBox();
            this.prescale56 = new System.Windows.Forms.ComboBox();
            this.prescale34 = new System.Windows.Forms.ComboBox();
            this.prescale12 = new System.Windows.Forms.ComboBox();
            this.lim1rt = new System.Windows.Forms.ComboBox();
            this.lim1at = new System.Windows.Forms.ComboBox();
            this.vol78 = new System.Windows.Forms.ComboBox();
            this.lim1rr = new System.Windows.Forms.ComboBox();
            this.vol56 = new System.Windows.Forms.ComboBox();
            this.lim1ar = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.combofreq3 = new System.Windows.Forms.ComboBox();
            this.combofreq2 = new System.Windows.Forms.ComboBox();
            this.combofreq1 = new System.Windows.Forms.ComboBox();
            this.vol12 = new System.Windows.Forms.ComboBox();
            this.vol34 = new System.Windows.Forms.ComboBox();
            this.save = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialogRegs = new System.Windows.Forms.OpenFileDialog();
            this._serialPort = new System.IO.Ports.SerialPort(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabCrossover.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // cPortNames
            // 
            this.cPortNames.FormattingEnabled = true;
            this.cPortNames.Location = new System.Drawing.Point(714, 126);
            this.cPortNames.Name = "cPortNames";
            this.cPortNames.Size = new System.Drawing.Size(133, 24);
            this.cPortNames.TabIndex = 0;
            // 
            // tBaudrate
            // 
            this.tBaudrate.Location = new System.Drawing.Point(728, 190);
            this.tBaudrate.Name = "tBaudrate";
            this.tBaudrate.Size = new System.Drawing.Size(109, 22);
            this.tBaudrate.TabIndex = 2;
            // 
            // lrecieved
            // 
            this.lrecieved.FormattingEnabled = true;
            this.lrecieved.ItemHeight = 16;
            this.lrecieved.Location = new System.Drawing.Point(728, 340);
            this.lrecieved.Name = "lrecieved";
            this.lrecieved.Size = new System.Drawing.Size(109, 36);
            this.lrecieved.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(714, 78);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 24);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bCommand
            // 
            this.bCommand.Location = new System.Drawing.Point(703, 37);
            this.bCommand.Name = "bCommand";
            this.bCommand.Size = new System.Drawing.Size(134, 23);
            this.bCommand.TabIndex = 6;
            this.bCommand.Text = "Command";
            this.bCommand.UseVisualStyleBackColor = true;
            this.bCommand.Click += new System.EventHandler(this.bCommand_Click);
            // 
            // listRegs
            // 
            this.listRegs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Address,
            this.Name1,
            this.Data,
            this.D7,
            this.D6,
            this.D5,
            this.D4,
            this.D3,
            this.D2,
            this.D1,
            this.D0});
            this.listRegs.GridLines = true;
            listViewItem50.Tag = "data";
            listViewItem50.ToolTipText = "CONFIGURATION REGISTER A";
            this.listRegs.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem50,
            listViewItem51,
            listViewItem52,
            listViewItem53,
            listViewItem54,
            listViewItem55,
            listViewItem56,
            listViewItem57,
            listViewItem58,
            listViewItem59,
            listViewItem60,
            listViewItem61,
            listViewItem62,
            listViewItem63,
            listViewItem64,
            listViewItem65,
            listViewItem66,
            listViewItem67,
            listViewItem68,
            listViewItem69,
            listViewItem70,
            listViewItem71,
            listViewItem72,
            listViewItem73,
            listViewItem74,
            listViewItem75,
            listViewItem76,
            listViewItem77,
            listViewItem78,
            listViewItem79,
            listViewItem80,
            listViewItem81,
            listViewItem82,
            listViewItem83,
            listViewItem84,
            listViewItem85,
            listViewItem86,
            listViewItem87,
            listViewItem88,
            listViewItem89,
            listViewItem90,
            listViewItem91,
            listViewItem92,
            listViewItem93,
            listViewItem94,
            listViewItem95,
            listViewItem96,
            listViewItem97,
            listViewItem98});
            this.listRegs.Location = new System.Drawing.Point(6, 6);
            this.listRegs.Name = "listRegs";
            this.listRegs.ShowItemToolTips = true;
            this.listRegs.Size = new System.Drawing.Size(623, 500);
            this.listRegs.TabIndex = 7;
            this.listRegs.UseCompatibleStateImageBehavior = false;
            this.listRegs.View = System.Windows.Forms.View.Details;
            this.listRegs.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listRegs_MouseMove);
            // 
            // Address
            // 
            this.Address.Text = "Address";
            this.Address.Width = 66;
            // 
            // Name1
            // 
            this.Name1.Text = "Name";
            this.Name1.Width = 51;
            // 
            // Data
            // 
            this.Data.Text = "Data";
            this.Data.Width = 50;
            // 
            // D7
            // 
            this.D7.Text = "D7";
            this.D7.Width = 50;
            // 
            // D6
            // 
            this.D6.Text = "D6";
            this.D6.Width = 44;
            // 
            // D5
            // 
            this.D5.Text = "D5";
            this.D5.Width = 43;
            // 
            // D4
            // 
            this.D4.Text = "D4";
            this.D4.Width = 38;
            // 
            // D3
            // 
            this.D3.Text = "D3";
            this.D3.Width = 35;
            // 
            // D2
            // 
            this.D2.Text = "D2";
            this.D2.Width = 50;
            // 
            // D1
            // 
            this.D1.Text = "D1";
            this.D1.Width = 49;
            // 
            // D0
            // 
            this.D0.Text = "D0";
            this.D0.Width = 51;
            // 
            // lout
            // 
            this.lout.AutoSize = true;
            this.lout.Location = new System.Drawing.Point(677, 247);
            this.lout.Name = "lout";
            this.lout.Size = new System.Drawing.Size(171, 17);
            this.lout.TabIndex = 8;
            this.lout.Text = "Output..............................";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabCrossover);
            this.tabControl1.Location = new System.Drawing.Point(12, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(643, 526);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listRegs);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(635, 497);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Registers";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listCoeff);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(635, 497);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Coeff";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listCoeff
            // 
            this.listCoeff.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Index,
            this.HexValue,
            this.Description,
            this.Float,
            this.cparams,
            this.cValue});
            this.listCoeff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listCoeff.Location = new System.Drawing.Point(3, 3);
            this.listCoeff.Name = "listCoeff";
            this.listCoeff.Size = new System.Drawing.Size(629, 491);
            this.listCoeff.TabIndex = 0;
            this.listCoeff.UseCompatibleStateImageBehavior = false;
            this.listCoeff.View = System.Windows.Forms.View.Details;
            // 
            // Index
            // 
            this.Index.Text = "Index";
            this.Index.Width = 45;
            // 
            // HexValue
            // 
            this.HexValue.Text = "HexValue";
            this.HexValue.Width = 71;
            // 
            // Description
            // 
            this.Description.Text = "Description";
            this.Description.Width = 215;
            // 
            // Float
            // 
            this.Float.Text = "Float";
            // 
            // cparams
            // 
            this.cparams.Text = "Params";
            this.cparams.Width = 186;
            // 
            // cValue
            // 
            this.cValue.Text = "Value";
            // 
            // tabCrossover
            // 
            this.tabCrossover.Controls.Add(this.groupBox5);
            this.tabCrossover.Controls.Add(this.label5);
            this.tabCrossover.Controls.Add(this.label4);
            this.tabCrossover.Controls.Add(this.lim2rt);
            this.tabCrossover.Controls.Add(this.lim2at);
            this.tabCrossover.Controls.Add(this.lim2rr);
            this.tabCrossover.Controls.Add(this.lim2ar);
            this.tabCrossover.Controls.Add(this.groupBox4);
            this.tabCrossover.Controls.Add(this.Mvol);
            this.tabCrossover.Controls.Add(this.groupBox3);
            this.tabCrossover.Controls.Add(this.groupBox2);
            this.tabCrossover.Controls.Add(this.lim1rt);
            this.tabCrossover.Controls.Add(this.lim1at);
            this.tabCrossover.Controls.Add(this.lim1rr);
            this.tabCrossover.Controls.Add(this.lim1ar);
            this.tabCrossover.Controls.Add(this.groupBox1);
            this.tabCrossover.Location = new System.Drawing.Point(4, 25);
            this.tabCrossover.Name = "tabCrossover";
            this.tabCrossover.Padding = new System.Windows.Forms.Padding(3);
            this.tabCrossover.Size = new System.Drawing.Size(635, 497);
            this.tabCrossover.TabIndex = 2;
            this.tabCrossover.Text = "Config";
            this.tabCrossover.UseVisualStyleBackColor = true;
            // 
            // lim2rt
            // 
            this.lim2rt.FormattingEnabled = true;
            this.lim2rt.Items.AddRange(new object[] {
            "-23",
            "-16.9",
            "-13.4",
            "-10.9",
            "-9.0",
            "-7.4",
            "-6.0",
            "-4.9",
            "-3.8",
            "-2.9",
            "-2.1",
            "-1.3",
            "-0.65",
            "0",
            "0.6"});
            this.lim2rt.Location = new System.Drawing.Point(245, 451);
            this.lim2rt.Name = "lim2rt";
            this.lim2rt.Size = new System.Drawing.Size(75, 24);
            this.lim2rt.TabIndex = 35;
            // 
            // lim2at
            // 
            this.lim2at.FormattingEnabled = true;
            this.lim2at.Items.AddRange(new object[] {
            "-12",
            "-11",
            "-10",
            "-9",
            "-8",
            "-7",
            "-6",
            "-5",
            "-4",
            "-3",
            "-2",
            "-1",
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.lim2at.Location = new System.Drawing.Point(169, 451);
            this.lim2at.Name = "lim2at";
            this.lim2at.Size = new System.Drawing.Size(75, 24);
            this.lim2at.TabIndex = 34;
            // 
            // lim2rr
            // 
            this.lim2rr.FormattingEnabled = true;
            this.lim2rr.Items.AddRange(new object[] {
            "0.5116",
            "0.1370",
            "0.0744",
            "0.0499",
            "0.0360",
            "0.0299",
            "0.0264",
            "0.0208",
            "0.0198",
            "0.0172",
            "0.0147",
            "0.0137",
            "0.0134",
            "0.0117",
            "0.0110",
            "0.0104"});
            this.lim2rr.Location = new System.Drawing.Point(93, 451);
            this.lim2rr.Name = "lim2rr";
            this.lim2rr.Size = new System.Drawing.Size(75, 24);
            this.lim2rr.TabIndex = 33;
            // 
            // lim2ar
            // 
            this.lim2ar.FormattingEnabled = true;
            this.lim2ar.Items.AddRange(new object[] {
            "3.1584",
            "2.7072",
            "2.2560",
            "1.8048",
            "1.3536",
            "0.9024",
            "0.4512",
            "0.2256",
            "0.1504",
            "0.1123",
            "0.0902",
            "0.0752",
            "0.0645",
            "0.0564",
            "0.0501",
            "0.0451"});
            this.lim2ar.Location = new System.Drawing.Point(17, 451);
            this.lim2ar.Name = "lim2ar";
            this.lim2ar.Size = new System.Drawing.Size(75, 24);
            this.lim2ar.TabIndex = 32;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lim78);
            this.groupBox4.Controls.Add(this.lim56);
            this.groupBox4.Controls.Add(this.lim34);
            this.groupBox4.Controls.Add(this.lim12);
            this.groupBox4.Location = new System.Drawing.Point(17, 265);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(567, 58);
            this.groupBox4.TabIndex = 31;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Limiter Select";
            // 
            // lim78
            // 
            this.lim78.FormattingEnabled = true;
            this.lim78.Items.AddRange(new object[] {
            "0",
            "1",
            "2"});
            this.lim78.Location = new System.Drawing.Point(501, 14);
            this.lim78.Name = "lim78";
            this.lim78.Size = new System.Drawing.Size(41, 24);
            this.lim78.TabIndex = 19;
            // 
            // lim56
            // 
            this.lim56.FormattingEnabled = true;
            this.lim56.Items.AddRange(new object[] {
            "0",
            "1",
            "2"});
            this.lim56.Location = new System.Drawing.Point(322, 14);
            this.lim56.Name = "lim56";
            this.lim56.Size = new System.Drawing.Size(42, 24);
            this.lim56.TabIndex = 18;
            // 
            // lim34
            // 
            this.lim34.FormattingEnabled = true;
            this.lim34.Items.AddRange(new object[] {
            "0",
            "1",
            "2"});
            this.lim34.Location = new System.Drawing.Point(143, 14);
            this.lim34.Name = "lim34";
            this.lim34.Size = new System.Drawing.Size(54, 24);
            this.lim34.TabIndex = 12;
            // 
            // lim12
            // 
            this.lim12.FormattingEnabled = true;
            this.lim12.Items.AddRange(new object[] {
            "0",
            "1",
            "2"});
            this.lim12.Location = new System.Drawing.Point(13, 14);
            this.lim12.Name = "lim12";
            this.lim12.Size = new System.Drawing.Size(54, 24);
            this.lim12.TabIndex = 11;
            // 
            // Mvol
            // 
            this.Mvol.FormattingEnabled = true;
            this.Mvol.Items.AddRange(new object[] {
            "24",
            "23.5",
            "23",
            "22.5",
            "22",
            "21.5",
            "21",
            "20.5",
            "20",
            "19.5",
            "19",
            "18.8",
            "18",
            "17.5",
            "17",
            "16.5",
            "16",
            "15.5",
            "15",
            "14.5",
            "14",
            "13.5",
            "13",
            "12.5",
            "12",
            "11.5",
            "11",
            "10.5",
            "10",
            "9.5",
            "9",
            "8.5",
            "8",
            "7.5",
            "7",
            "6.5",
            "6",
            "5.5",
            "5",
            "4.5",
            "4",
            "3.5",
            "3",
            "2.5",
            "2",
            "1.5",
            "1",
            "0.5",
            "0",
            "-0.5",
            "-1",
            "-1.5",
            "-2",
            "-2.5",
            "-3",
            "-3.5",
            "-4",
            "-4.5",
            "-5",
            "-5.5",
            "-6",
            "-6.5",
            "-7",
            "-7.5",
            "-8",
            "-8.5",
            "-9",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-18.5",
            "-19",
            "-19.5",
            "-20",
            "-20.5",
            "-21",
            "-21.5",
            "-22",
            "-22.5",
            "-23",
            "-23.5",
            "-24"});
            this.Mvol.Location = new System.Drawing.Point(506, 423);
            this.Mvol.Name = "Mvol";
            this.Mvol.Size = new System.Drawing.Size(53, 24);
            this.Mvol.TabIndex = 30;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.postscale78);
            this.groupBox3.Controls.Add(this.postscale56);
            this.groupBox3.Controls.Add(this.postscale34);
            this.groupBox3.Controls.Add(this.postscale12);
            this.groupBox3.Location = new System.Drawing.Point(17, 187);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(589, 64);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PostScale";
            // 
            // postscale78
            // 
            this.postscale78.FormattingEnabled = true;
            this.postscale78.Items.AddRange(new object[] {
            "0.0",
            "-0.5",
            "-1.0",
            "-1.5",
            "-2.0",
            "-2.5",
            "-3.0",
            "-3.5",
            "-4.0",
            "-4.5",
            "-5.0",
            "-5.5",
            "-6.0",
            "-6.5",
            "-7.0",
            "-7.5",
            "-8.0",
            "-8.5",
            "-9.0",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-17.5",
            "-18",
            "-18.5",
            "-19",
            "-19.5",
            "-20"});
            this.postscale78.Location = new System.Drawing.Point(489, 12);
            this.postscale78.Name = "postscale78";
            this.postscale78.Size = new System.Drawing.Size(53, 24);
            this.postscale78.TabIndex = 27;
            // 
            // postscale56
            // 
            this.postscale56.FormattingEnabled = true;
            this.postscale56.Items.AddRange(new object[] {
            "0.0",
            "-0.5",
            "-1.0",
            "-1.5",
            "-2.0",
            "-2.5",
            "-3.0",
            "-3.5",
            "-4.0",
            "-4.5",
            "-5.0",
            "-5.5",
            "-6.0",
            "-6.5",
            "-7.0",
            "-7.5",
            "-8.0",
            "-8.5",
            "-9.0",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-17.5",
            "-18",
            "-18.5",
            "-19",
            "-19.5",
            "-20"});
            this.postscale56.Location = new System.Drawing.Point(311, 12);
            this.postscale56.Name = "postscale56";
            this.postscale56.Size = new System.Drawing.Size(53, 24);
            this.postscale56.TabIndex = 26;
            // 
            // postscale34
            // 
            this.postscale34.FormattingEnabled = true;
            this.postscale34.Items.AddRange(new object[] {
            "0.0",
            "-0.5",
            "-1.0",
            "-1.5",
            "-2.0",
            "-2.5",
            "-3.0",
            "-3.5",
            "-4.0",
            "-4.5",
            "-5.0",
            "-5.5",
            "-6.0",
            "-6.5",
            "-7.0",
            "-7.5",
            "-8.0",
            "-8.5",
            "-9.0",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-17.5",
            "-18",
            "-18.5",
            "-19",
            "-19.5",
            "-20"});
            this.postscale34.Location = new System.Drawing.Point(144, 12);
            this.postscale34.Name = "postscale34";
            this.postscale34.Size = new System.Drawing.Size(53, 24);
            this.postscale34.TabIndex = 25;
            // 
            // postscale12
            // 
            this.postscale12.FormattingEnabled = true;
            this.postscale12.Items.AddRange(new object[] {
            "0",
            "-0.5",
            "-1.0",
            "-1.5",
            "-2.0",
            "-2.5",
            "-3.0",
            "-3.5",
            "-4.0",
            "-4.5",
            "-5.0",
            "-5.5",
            "-6.0",
            "-6.5",
            "-7.0",
            "-7.5",
            "-8.0",
            "-8.5",
            "-9.0",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-17.5",
            "-18",
            "-18.5",
            "-19",
            "-19.5",
            "-20"});
            this.postscale12.Location = new System.Drawing.Point(14, 12);
            this.postscale12.Name = "postscale12";
            this.postscale12.Size = new System.Drawing.Size(53, 24);
            this.postscale12.TabIndex = 24;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.prescale78);
            this.groupBox2.Controls.Add(this.prescale56);
            this.groupBox2.Controls.Add(this.prescale34);
            this.groupBox2.Controls.Add(this.prescale12);
            this.groupBox2.Location = new System.Drawing.Point(17, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(581, 51);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PreScale";
            // 
            // prescale78
            // 
            this.prescale78.FormattingEnabled = true;
            this.prescale78.Items.AddRange(new object[] {
            "0",
            "-0.5",
            "-1.0",
            "-1.5",
            "-2.0",
            "-2.5",
            "-3.0",
            "-3.5",
            "-4.0",
            "-4.5",
            "-5.0",
            "-5.5",
            "-6.0",
            "-6.5",
            "-7.0",
            "-7.5",
            "-8.0",
            "-8.5",
            "-9.0",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-17.5",
            "-18",
            "-18.5",
            "-19",
            "-19.5",
            "-20"});
            this.prescale78.Location = new System.Drawing.Point(489, 14);
            this.prescale78.Name = "prescale78";
            this.prescale78.Size = new System.Drawing.Size(53, 24);
            this.prescale78.TabIndex = 23;
            // 
            // prescale56
            // 
            this.prescale56.FormattingEnabled = true;
            this.prescale56.Items.AddRange(new object[] {
            "0",
            "-0.5",
            "-1.0",
            "-1.5",
            "-2.0",
            "-2.5",
            "-3.0",
            "-3.5",
            "-4.0",
            "-4.5",
            "-5.0",
            "-5.5",
            "-6.0",
            "-6.5",
            "-7.0",
            "-7.5",
            "-8.0",
            "-8.5",
            "-9.0",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-17.5",
            "-18",
            "-18.5",
            "-19",
            "-19.5",
            "-20"});
            this.prescale56.Location = new System.Drawing.Point(311, 14);
            this.prescale56.Name = "prescale56";
            this.prescale56.Size = new System.Drawing.Size(53, 24);
            this.prescale56.TabIndex = 22;
            // 
            // prescale34
            // 
            this.prescale34.FormattingEnabled = true;
            this.prescale34.Items.AddRange(new object[] {
            "0",
            "-0.5",
            "-1.0",
            "-1.5",
            "-2.0",
            "-2.5",
            "-3.0",
            "-3.5",
            "-4.0",
            "-4.5",
            "-5.0",
            "-5.5",
            "-6.0",
            "-6.5",
            "-7.0",
            "-7.5",
            "-8.0",
            "-8.5",
            "-9.0",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-17.5",
            "-18",
            "-18.5",
            "-19",
            "-19.5",
            "-20"});
            this.prescale34.Location = new System.Drawing.Point(144, 14);
            this.prescale34.Name = "prescale34";
            this.prescale34.Size = new System.Drawing.Size(53, 24);
            this.prescale34.TabIndex = 21;
            // 
            // prescale12
            // 
            this.prescale12.FormattingEnabled = true;
            this.prescale12.Items.AddRange(new object[] {
            "0",
            "-0.5",
            "-1.0",
            "-1.5",
            "-2.0",
            "-2.5",
            "-3.0",
            "-3.5",
            "-4.0",
            "-4.5",
            "-5.0",
            "-5.5",
            "-6.0",
            "-6.5",
            "-7.0",
            "-7.5",
            "-8.0",
            "-8.5",
            "-9.0",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-17.5",
            "-18",
            "-18.5",
            "-19",
            "-19.5",
            "-20"});
            this.prescale12.Location = new System.Drawing.Point(14, 14);
            this.prescale12.Name = "prescale12";
            this.prescale12.Size = new System.Drawing.Size(53, 24);
            this.prescale12.TabIndex = 20;
            // 
            // lim1rt
            // 
            this.lim1rt.FormattingEnabled = true;
            this.lim1rt.Items.AddRange(new object[] {
            "-23",
            "-16.9",
            "-13.4",
            "-10.9",
            "-9.0",
            "-7.4",
            "-6.0",
            "-4.9",
            "-3.8",
            "-2.9",
            "-2.1",
            "-1.3",
            "-0.65",
            "0",
            "0.6"});
            this.lim1rt.Location = new System.Drawing.Point(245, 399);
            this.lim1rt.Name = "lim1rt";
            this.lim1rt.Size = new System.Drawing.Size(75, 24);
            this.lim1rt.TabIndex = 17;
            // 
            // lim1at
            // 
            this.lim1at.FormattingEnabled = true;
            this.lim1at.Items.AddRange(new object[] {
            "-12",
            "-11",
            "-10",
            "-9",
            "-8",
            "-7",
            "-6",
            "-5",
            "-4",
            "-3",
            "-2",
            "-1",
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.lim1at.Location = new System.Drawing.Point(169, 399);
            this.lim1at.Name = "lim1at";
            this.lim1at.Size = new System.Drawing.Size(75, 24);
            this.lim1at.TabIndex = 16;
            // 
            // vol78
            // 
            this.vol78.FormattingEnabled = true;
            this.vol78.Items.AddRange(new object[] {
            "24",
            "23.5",
            "23",
            "22.5",
            "22",
            "21.5",
            "21",
            "20.5",
            "20",
            "19.5",
            "19",
            "18.8",
            "18",
            "17.5",
            "17",
            "16.5",
            "16",
            "15.5",
            "15",
            "14.5",
            "14",
            "13.5",
            "13",
            "12.5",
            "12",
            "11.5",
            "11",
            "10.5",
            "10",
            "9.5",
            "9",
            "8.5",
            "8",
            "7.5",
            "7",
            "6.5",
            "6",
            "5.5",
            "5",
            "4.5",
            "4",
            "3.5",
            "3",
            "2.5",
            "2",
            "1.5",
            "1",
            "0.5",
            "0",
            "-0.5",
            "-1",
            "-1.5",
            "-2",
            "-2.5",
            "-3",
            "-3.5",
            "-4",
            "-4.5",
            "-5",
            "-5.5",
            "-6",
            "-6.5",
            "-7",
            "-7.5",
            "-8",
            "-8.5",
            "-9",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-18.5",
            "-19",
            "-19.5",
            "-20",
            "-20.5",
            "-21",
            "-21.5",
            "-22",
            "-22.5",
            "-23",
            "-23.5",
            "-24"});
            this.vol78.Location = new System.Drawing.Point(500, 18);
            this.vol78.Name = "vol78";
            this.vol78.Size = new System.Drawing.Size(53, 24);
            this.vol78.TabIndex = 10;
            // 
            // lim1rr
            // 
            this.lim1rr.FormattingEnabled = true;
            this.lim1rr.Items.AddRange(new object[] {
            "0.5116",
            "0.1370",
            "0.0744",
            "0.0499",
            "0.0360",
            "0.0299",
            "0.0264",
            "0.0208",
            "0.0198",
            "0.0172",
            "0.0147",
            "0.0137",
            "0.0134",
            "0.0117",
            "0.0110",
            "0.0104"});
            this.lim1rr.Location = new System.Drawing.Point(93, 399);
            this.lim1rr.Name = "lim1rr";
            this.lim1rr.Size = new System.Drawing.Size(75, 24);
            this.lim1rr.TabIndex = 15;
            // 
            // vol56
            // 
            this.vol56.FormattingEnabled = true;
            this.vol56.Items.AddRange(new object[] {
            "24",
            "23.5",
            "23",
            "22.5",
            "22",
            "21.5",
            "21",
            "20.5",
            "20",
            "19.5",
            "19",
            "18.8",
            "18",
            "17.5",
            "17",
            "16.5",
            "16",
            "15.5",
            "15",
            "14.5",
            "14",
            "13.5",
            "13",
            "12.5",
            "12",
            "11.5",
            "11",
            "10.5",
            "10",
            "9.5",
            "9",
            "8.5",
            "8",
            "7.5",
            "7",
            "6.5",
            "6",
            "5.5",
            "5",
            "4.5",
            "4",
            "3.5",
            "3",
            "2.5",
            "2",
            "1.5",
            "1",
            "0.5",
            "0",
            "-0.5",
            "-1",
            "-1.5",
            "-2",
            "-2.5",
            "-3",
            "-3.5",
            "-4",
            "-4.5",
            "-5",
            "-5.5",
            "-6",
            "-6.5",
            "-7",
            "-7.5",
            "-8",
            "-8.5",
            "-9",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-18.5",
            "-19",
            "-19.5",
            "-20",
            "-20.5",
            "-21",
            "-21.5",
            "-22",
            "-22.5",
            "-23",
            "-23.5",
            "-24"});
            this.vol56.Location = new System.Drawing.Point(322, 18);
            this.vol56.Name = "vol56";
            this.vol56.Size = new System.Drawing.Size(57, 24);
            this.vol56.TabIndex = 9;
            // 
            // lim1ar
            // 
            this.lim1ar.FormattingEnabled = true;
            this.lim1ar.Items.AddRange(new object[] {
            "3.1584",
            "2.7072",
            "2.2560",
            "1.8048",
            "1.3536",
            "0.9024",
            "0.4512",
            "0.2256",
            "0.1504",
            "0.1123",
            "0.0902",
            "0.0752",
            "0.0645",
            "0.0564",
            "0.0501",
            "0.0451"});
            this.lim1ar.Location = new System.Drawing.Point(17, 399);
            this.lim1ar.Name = "lim1ar";
            this.lim1ar.Size = new System.Drawing.Size(75, 24);
            this.lim1ar.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.combofreq3);
            this.groupBox1.Controls.Add(this.combofreq2);
            this.groupBox1.Controls.Add(this.combofreq1);
            this.groupBox1.Location = new System.Drawing.Point(6, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(569, 107);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Crossover";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(406, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 34);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ch 5 , 6 lowpass\r\nCh 7 , 8 highpass";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(208, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 34);
            this.label2.TabIndex = 7;
            this.label2.Text = "Ch 3 , 4 lowpass\r\nCh 5 , 6 highpass";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 34);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ch 1 , 2 lowpass\r\nCh 3 , 4 highpass\r\n";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // combofreq3
            // 
            this.combofreq3.FormattingEnabled = true;
            this.combofreq3.Items.AddRange(new object[] {
            "720",
            "900",
            "1080",
            "1260",
            "1440",
            "1620",
            "1800",
            "1980",
            "2160",
            "2340",
            "2520",
            "2700",
            "2880",
            "3060",
            "3420",
            "3600",
            "3960"});
            this.combofreq3.Location = new System.Drawing.Point(448, 14);
            this.combofreq3.Name = "combofreq3";
            this.combofreq3.Size = new System.Drawing.Size(54, 24);
            this.combofreq3.TabIndex = 2;
            // 
            // combofreq2
            // 
            this.combofreq2.FormattingEnabled = true;
            this.combofreq2.Items.AddRange(new object[] {
            "240",
            "300",
            "360",
            "420",
            "480",
            "540",
            "600",
            "660",
            "720",
            "780",
            "840",
            "900",
            "960",
            "1020",
            "1140",
            "1200",
            "1320"});
            this.combofreq2.Location = new System.Drawing.Point(271, 14);
            this.combofreq2.Name = "combofreq2";
            this.combofreq2.Size = new System.Drawing.Size(55, 24);
            this.combofreq2.TabIndex = 1;
            // 
            // combofreq1
            // 
            this.combofreq1.FormattingEnabled = true;
            this.combofreq1.Items.AddRange(new object[] {
            "80",
            "100",
            "120",
            "140",
            "160",
            "180",
            "200",
            "220",
            "240",
            "260",
            "280",
            "300",
            "320",
            "340",
            "360",
            "380",
            "400",
            "420",
            "440"});
            this.combofreq1.Location = new System.Drawing.Point(73, 14);
            this.combofreq1.Name = "combofreq1";
            this.combofreq1.Size = new System.Drawing.Size(53, 24);
            this.combofreq1.TabIndex = 0;
            // 
            // vol12
            // 
            this.vol12.FormattingEnabled = true;
            this.vol12.Items.AddRange(new object[] {
            "24",
            "23.5",
            "23",
            "22.5",
            "22",
            "21.5",
            "21",
            "20.5",
            "20",
            "19.5",
            "19",
            "18.8",
            "18",
            "17.5",
            "17",
            "16.5",
            "16",
            "15.5",
            "15",
            "14.5",
            "14",
            "13.5",
            "13",
            "12.5",
            "12",
            "11.5",
            "11",
            "10.5",
            "10",
            "9.5",
            "9",
            "8.5",
            "8",
            "7.5",
            "7",
            "6.5",
            "6",
            "5.5",
            "5",
            "4.5",
            "4",
            "3.5",
            "3",
            "2.5",
            "2",
            "1.5",
            "1",
            "0.5",
            "0",
            "-0.5",
            "-1",
            "-1.5",
            "-2",
            "-2.5",
            "-3",
            "-3.5",
            "-4",
            "-4.5",
            "-5",
            "-5.5",
            "-6",
            "-6.5",
            "-7",
            "-7.5",
            "-8",
            "-8.5",
            "-9",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-18.5",
            "-19",
            "-19.5",
            "-20",
            "-20.5",
            "-21",
            "-21.5",
            "-22",
            "-22.5",
            "-23",
            "-23.5",
            "-24"});
            this.vol12.Location = new System.Drawing.Point(24, 18);
            this.vol12.Name = "vol12";
            this.vol12.Size = new System.Drawing.Size(54, 24);
            this.vol12.TabIndex = 4;
            this.vol12.SelectedIndexChanged += new System.EventHandler(this.comboBoxVol_SelectedIndexChanged);
            // 
            // vol34
            // 
            this.vol34.FormattingEnabled = true;
            this.vol34.Items.AddRange(new object[] {
            "24",
            "23.5",
            "23",
            "22.5",
            "22",
            "21.5",
            "21",
            "20.5",
            "20",
            "19.5",
            "19",
            "18.8",
            "18",
            "17.5",
            "17",
            "16.5",
            "16",
            "15.5",
            "15",
            "14.5",
            "14",
            "13.5",
            "13",
            "12.5",
            "12",
            "11.5",
            "11",
            "10.5",
            "10",
            "9.5",
            "9",
            "8.5",
            "8",
            "7.5",
            "7",
            "6.5",
            "6",
            "5.5",
            "5",
            "4.5",
            "4",
            "3.5",
            "3",
            "2.5",
            "2",
            "1.5",
            "1",
            "0.5",
            "0",
            "-0.5",
            "-1",
            "-1.5",
            "-2",
            "-2.5",
            "-3",
            "-3.5",
            "-4",
            "-4.5",
            "-5",
            "-5.5",
            "-6",
            "-6.5",
            "-7",
            "-7.5",
            "-8",
            "-8.5",
            "-9",
            "-9.5",
            "-10",
            "-10.5",
            "-11",
            "-11.5",
            "-12",
            "-12.5",
            "-13",
            "-13.5",
            "-14",
            "-14.5",
            "-15",
            "-15.5",
            "-16",
            "-16.5",
            "-17",
            "-18.5",
            "-19",
            "-19.5",
            "-20",
            "-20.5",
            "-21",
            "-21.5",
            "-22",
            "-22.5",
            "-23",
            "-23.5",
            "-24"});
            this.vol34.Location = new System.Drawing.Point(154, 18);
            this.vol34.Name = "vol34";
            this.vol34.Size = new System.Drawing.Size(51, 24);
            this.vol34.TabIndex = 5;
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(731, 425);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(105, 35);
            this.save.TabIndex = 10;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(849, 28);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // openFileDialogRegs
            // 
            this.openFileDialogRegs.DefaultExt = "lpd";
            this.openFileDialogRegs.FileName = "openFileDialogRegs";
            // 
            // _serialPort
            // 
            this._serialPort.PortName = "COM13";
            this._serialPort.ReadBufferSize = 512;
            this._serialPort.ReadTimeout = 500;
            this._serialPort.WriteTimeout = 500;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(351, 406);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 17);
            this.label4.TabIndex = 36;
            this.label4.Text = "Limiter 1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(351, 458);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 17);
            this.label5.TabIndex = 37;
            this.label5.Text = "Limiter 2";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.vol78);
            this.groupBox5.Controls.Add(this.vol56);
            this.groupBox5.Controls.Add(this.vol12);
            this.groupBox5.Controls.Add(this.vol34);
            this.groupBox5.Location = new System.Drawing.Point(6, 323);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(581, 74);
            this.groupBox5.TabIndex = 38;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Volume";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 565);
            this.Controls.Add(this.save);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lout);
            this.Controls.Add(this.bCommand);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lrecieved);
            this.Controls.Add(this.tBaudrate);
            this.Controls.Add(this.cPortNames);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabCrossover.ResumeLayout(false);
            this.tabCrossover.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cPortNames;
        private System.Windows.Forms.TextBox tBaudrate;
        private System.Windows.Forms.ListBox lrecieved;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bCommand;
        private System.Windows.Forms.ListView listRegs;
        private System.Windows.Forms.ColumnHeader Address;
        private System.Windows.Forms.ColumnHeader Name1;
        private System.Windows.Forms.ColumnHeader Data;
        private System.Windows.Forms.Label lout;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView listCoeff;
        private System.Windows.Forms.ColumnHeader Index;
        private System.Windows.Forms.ColumnHeader HexValue;
        private System.Windows.Forms.ColumnHeader D7;
        private System.Windows.Forms.ColumnHeader D6;
        private System.Windows.Forms.ColumnHeader D5;
        private System.Windows.Forms.ColumnHeader D4;
        private System.Windows.Forms.ColumnHeader D3;
        private System.Windows.Forms.ColumnHeader D2;
        private System.Windows.Forms.ColumnHeader D1;
        private System.Windows.Forms.ColumnHeader D0;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.TabPage tabCrossover;
        private System.Windows.Forms.ComboBox combofreq1;
        private System.Windows.Forms.ComboBox combofreq2;
        private System.Windows.Forms.ComboBox combofreq3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialogRegs;
        private System.Windows.Forms.ComboBox vol34;
        private System.Windows.Forms.ComboBox vol12;
        private System.IO.Ports.SerialPort _serialPort;
        private System.Windows.Forms.ColumnHeader Description;
        private System.Windows.Forms.ColumnHeader Float;
        private System.Windows.Forms.ColumnHeader cparams;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader cValue;
        private System.Windows.Forms.ComboBox vol56;
        private System.Windows.Forms.ComboBox vol78;
        private System.Windows.Forms.ComboBox lim34;
        private System.Windows.Forms.ComboBox lim12;
        private System.Windows.Forms.ComboBox lim1rt;
        private System.Windows.Forms.ComboBox lim1at;
        private System.Windows.Forms.ComboBox lim1rr;
        private System.Windows.Forms.ComboBox lim1ar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox lim78;
        private System.Windows.Forms.ComboBox lim56;
        private System.Windows.Forms.ComboBox postscale78;
        private System.Windows.Forms.ComboBox postscale56;
        private System.Windows.Forms.ComboBox postscale34;
        private System.Windows.Forms.ComboBox postscale12;
        private System.Windows.Forms.ComboBox prescale78;
        private System.Windows.Forms.ComboBox prescale56;
        private System.Windows.Forms.ComboBox prescale34;
        private System.Windows.Forms.ComboBox prescale12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox Mvol;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox lim2rt;
        private System.Windows.Forms.ComboBox lim2at;
        private System.Windows.Forms.ComboBox lim2rr;
        private System.Windows.Forms.ComboBox lim2ar;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
    }
}

