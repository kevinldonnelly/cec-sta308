﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Collections;

namespace STA308
{
    class PCF8574
    {
        byte[] PCF8574_writesinglebyte = { 0x53, 0x40, 0x20 };
        byte[] PCF8574_readsinglebyte = { 0x53, 0x41 };
        static SerialPort _serialPort;
        BitArray initBA4bit;

        int ReadPCF8574Byte()
        {
            _serialPort.DiscardInBuffer();
            _serialPort.Write(PCF8574_readsinglebyte, 0, 2);
            return _serialPort.ReadByte();
        }
        void WriteChar(byte cmd)
        {
            byte[] c = new byte[1];
            c[0] = cmd;

            BitArray wholebyte = new BitArray(c);

            BitArray uppernibble = new BitArray(8);
            uppernibble.SetAll(false);
            uppernibble[0] = wholebyte[4];
            uppernibble[1] = wholebyte[5];
            uppernibble[2] = wholebyte[6];
            uppernibble[3] = wholebyte[7];





            uppernibble.Set(5, false);//write
            uppernibble.Set(6, true);//character mode

            uppernibble.Set(7, false);

            uppernibble.Set(4, true);// Enable
            uppernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
        //    Thread.Sleep(1);
            uppernibble.Set(4, false);// Enable
            uppernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
         //  Thread.Sleep(1);
            uppernibble.Set(4, true);
            uppernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
         //   Thread.Sleep(1);

            BitArray lowernibble = new BitArray(8);
            lowernibble.SetAll(false);
            lowernibble[0] = wholebyte[0];
            lowernibble[1] = wholebyte[1];
            lowernibble[2] = wholebyte[2];
            lowernibble[3] = wholebyte[3];

            lowernibble.Set(5, false);//write
            lowernibble.Set(6, true);//character mode

            lowernibble.Set(7, false);

            lowernibble.Set(4, true);// Enable
            lowernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
         //   Thread.Sleep(1);
            lowernibble.Set(4, false);
            lowernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
          //  Thread.Sleep(1);
            lowernibble.Set(4, true);
            lowernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
          //  Thread.Sleep(1);



        }


        void InitLCD()
        {
            initBA4bit = new BitArray(8);
            initBA4bit.SetAll(true);
            for (int i = 0; i < 3; i++)
            {

                initBA4bit.Set(2, true);//8 bit
                initBA4bit.Set(3, true);//8 bit

                initBA4bit.Set(4, true);// Enable
                initBA4bit.CopyTo(PCF8574_writesinglebyte, 2);
                _serialPort.Write(PCF8574_writesinglebyte, 0, 3);//4bit mode
              //  Thread.Sleep(2);

                initBA4bit.Set(4, false);// Enable
                initBA4bit.CopyTo(PCF8574_writesinglebyte, 2);
                _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
           //     Thread.Sleep(2);
                initBA4bit.Set(4, true);// Enable
                initBA4bit.CopyTo(PCF8574_writesinglebyte, 2);
                _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
             //   Thread.Sleep(2);
            }




            initBA4bit.SetAll(false);



            initBA4bit.Set(1, true);//4 bit
            initBA4bit.Set(4, true);// Enable
            initBA4bit.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);//4bit mode
            //Thread.Sleep(1);

            initBA4bit.Set(4, false);// Enable
            initBA4bit.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
         //   Thread.Sleep(1);
            initBA4bit.Set(4, true);// Enable
            initBA4bit.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
          //  Thread.Sleep(1);




            WriteCmd(0x28);// 2 Line


            WriteCmd(0x01);//clear
            //Thread.Sleep(1000);


            WriteCmd(0xF); //Cursor on

            WriteCmd(0x80); //first line
        }

        /*
         InitLCD();

         for (byte x = 0x30; x < 0x40; x++) WriteChar(x);
         WriteCmd(0xC0);//second line
         for (byte x = 0x41; x < 0x51; x++) WriteChar(x);
         WriteCmd(0x80);
         WriteCmd(0x01);

         WriteString("Hello World!");
         */

        void WriteCmd(byte cmd)
        {
            byte[] c = new byte[1];
            c[0] = cmd;
            //      bool value=false;
            BitArray wholebyte = new BitArray(c);

            BitArray uppernibble = new BitArray(8);
            uppernibble.SetAll(false);
            uppernibble[0] = wholebyte[4];
            uppernibble[1] = wholebyte[5];
            uppernibble[2] = wholebyte[6];
            uppernibble[3] = wholebyte[7];



            BitArray lowernibble = new BitArray(8);
            lowernibble.SetAll(false);
            lowernibble[0] = wholebyte[0];
            lowernibble[1] = wholebyte[1];
            lowernibble[2] = wholebyte[2];
            lowernibble[3] = wholebyte[3];




            uppernibble.Set(5, false);//write
            uppernibble.Set(6, false);//command mode

            uppernibble.Set(7, false);

            uppernibble.Set(4, true);
            uppernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
         //   Thread.Sleep(1);
            uppernibble.Set(4, false);
            uppernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
        //    Thread.Sleep(1);
            uppernibble.Set(4, true);
            uppernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
         //   Thread.Sleep(1);


            lowernibble.Set(5, false);//write
            lowernibble.Set(6, false);//command mode

            lowernibble.Set(7, false);

            lowernibble.Set(4, true);
            lowernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
            //Thread.Sleep(1);
            lowernibble.Set(4, false);
            lowernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
        //    Thread.Sleep(1);
            lowernibble.Set(4, true);
            lowernibble.CopyTo(PCF8574_writesinglebyte, 2);
            _serialPort.Write(PCF8574_writesinglebyte, 0, 3);
          //  Thread.Sleep(1);



        }
        void WriteString(string s)
        {
            for (int index = 0; index < s.Length; index++)
            {
                WriteChar(Convert.ToByte(s.ElementAt(index)));
            }
        }
    }
}
