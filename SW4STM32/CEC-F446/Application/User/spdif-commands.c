/*
 * spdif-commands.c

 *
 *  Created on: Jan 29, 2019
 *      Author: kevin
 */
#include "main.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include "cmsis_os.h"
#include "fatfs.h"
#include "smr_glo.h"


typedef struct
{
	union {
  __IO uint32_t   SR;           /*!< Control register,                   Address offset: 0x00 */
  struct {
	  __IO uint32_t
	  RXNE:1,//Read data register not empty
	  CSRNE:1,//The Control Buffer register is not empty
	  PERR:1,//Parity error
	  OVR:1,//Overrun error
	  SBD:1,//Synchronization Block Detected
	  SYNCD:1,//Synchronization Done
	  FERR:1,//Framing error
	  SERR:1,//Synchronization error
	  TERR:1,//Time-out error
	  RES:7,
	  WIDTH5:16;
	};
	};
} SPDIFRX_SR;

static void prvSPDIFTask( void *pvParameters );

static portBASE_TYPE prvSPDIFReadStats( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvSPDIFRec( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvSPDIFChannel( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
#define REC_BUFF_SIZE       8192
extern SPDIFRX_HandleTypeDef hspdif;
extern SAI_HandleTypeDef hsai_BlockB1;
static xTaskHandle xSPDIFTask = NULL;
uint16_t   SpdifBuff[REC_BUFF_SIZE];
extern osSemaphoreId myBinarySemSPDIFHandle;
__IO int16_t  RecUpdatePointer = -1;
FRESULT fr=FR_INVALID_PARAMETER;
FIL xFile;
void *pSmrPersistentMem = NULL;
void *pSmrScratchMem = NULL;
static  smr_static_param_t smr_static_param;
static  smr_dynamic_param_t smr_dynamic_param;
extern const uint32_t smr_persistent_mem_size;
#define AUDIO_CFG_SMR_AVGTIME_DEFAULT              1
static  buffer_t BufferHandler;
static  buffer_t *pBufferHandler = &BufferHandler;

/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvSPDIFReadStatCommandDefinition =
{
	( const int8_t * const ) "spdif-stats", /* The command string to type. */
	( const int8_t * const ) "spdif-stats\r\n",
	prvSPDIFReadStats, /* The function to run. */
	0 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSPDIRecCommandDefinition =
{
	( const int8_t * const ) "spdif-rec", /* The command string to type. */
	( const int8_t * const ) "spdif-rec\r\n",
	prvSPDIFRec, /* The function to run. */
	1 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSPDIFChannelCommandDefinition =
{
	( const int8_t * const ) "spdif-channel", /* The command string to type. */
	( const int8_t * const ) "spdif-channel:<channel>\r\n",
	prvSPDIFChannel, /* The function to run. */
	1 /* No parameters are expected. */
};


void vSPDIFTaskStart( void )
{
	xTaskCreate( 	prvSPDIFTask,				/* The task that implements the command console. */
					( const int8_t * const ) "xSPDIFTask",		/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
					configUART_COMMAND_CONSOLE_STACK_SIZE,	/* The size of the stack allocated to the task. */
					NULL,									/* The parameter is not used, so NULL is passed. */
					configUART_COMMAND_CONSOLE_TASK_PRIORITY,/* The priority allocated to the task. */
					&xSPDIFTask );					/* Used to store the handle to the created task. */
}

static void prvSPDIFTask( void *pvParameters )
{

	/* Open the file afile.bin for writing. */


	pSmrPersistentMem = pvPortMalloc(smr_persistent_mem_size); /* smr_persistent_mem_size  0x188 */
	if(pSmrPersistentMem==NULL)printf("Alloc failed\n");
	pSmrScratchMem = pvPortMalloc(smr_scratch_mem_size);       /* smr_scratch_mem_size  0xF04 */
	if(pSmrScratchMem==NULL)printf("Alloc failed\n");

	int read_pos;
	int cycles=0;
	int32_t error = SMR_ERROR_NONE;


	 error = smr_reset(pSmrPersistentMem, pSmrScratchMem);

	 if(error<0)printf("error\n");

	 smr_static_param.sampling_rate = 48000;

	 error = smr_setParam(&smr_static_param, pSmrPersistentMem);
	 smr_dynamic_param.enable = 1;                        /* SMR module enabler */


	 smr_dynamic_param.averaging_time = AUDIO_CFG_SMR_AVGTIME_DEFAULT;
	 smr_dynamic_param.filter_type = SMR_PREFILTER_AWEIGHTING;
	 error = smr_setConfig(&smr_dynamic_param, pSmrPersistentMem);

	 if(error<0)printf("error\n");

	 BufferHandler.nb_bytes_per_Sample =2; /* 8 bits in 0ne byte */


	 BufferHandler.nb_channels = 2; /* stereo */

	 BufferHandler.buffer_size = 1024; /* just half buffer is process (size per channel) */


	 BufferHandler.mode = INTERLEAVED;


	 HAL_SPDIFRX_ReceiveDataFlow_DMA(&hspdif,SpdifBuff,REC_BUFF_SIZE/2);
	 HAL_SAI_Transmit_DMA(&hsai_BlockB1, (uint8_t *) SpdifBuff, REC_BUFF_SIZE);


	while(1)
	{

		osSemaphoreWait(myBinarySemSPDIFHandle, osWaitForever);
		read_pos=hsai_BlockB1.XferSize-hsai_BlockB1.hdmarx->Instance->NDTR;
		BufferHandler.data_ptr = SpdifBuff;


		error = smr_process(pBufferHandler, pBufferHandler, pSmrPersistentMem);

		if(cycles++%50==0)
		{

			if(error<0)printf("smr_process error=%d\n",error);

			error = smr_getConfig(&smr_dynamic_param, pSmrPersistentMem);
			if(error<0)printf("error\n");
			printf("rpos=%d\n",read_pos);
			printf("Left mean power:  %ld dB\n",smr_dynamic_param.mean_level_left);
			printf("Right mean power:  %ld dB\n",smr_dynamic_param.mean_level_right);
		}



		if( fr == FR_OK && RecUpdatePointer > -1)
		    {
		        /* Write three single characters to the opened file. */
		     //   fr = f_write(&xFile, &SpdifBuff[RecUpdatePointer], REC_BUFF_SIZE/2, &byteswritten);

		    //    if(fr!=FR_OK)f_close( &xFile );
		    }

	}
}

void HAL_SPDIFRX_ErrorCallback(SPDIFRX_HandleTypeDef *hspdif)
{
	printf("spdif error=%d\n",hspdif->ErrorCode);
}

void HAL_SPDIFRX_RxHalfCpltCallback(SPDIFRX_HandleTypeDef *hspdif)
{
	int txStatus = 0;
	 RecUpdatePointer = 0;
		BaseType_t xHigherPriorityTaskWoken;

		txStatus = xSemaphoreGiveFromISR(myBinarySemSPDIFHandle, &xHigherPriorityTaskWoken);

				if (pdPASS == txStatus) {
							      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
							    }
}

void HAL_SPDIFRX_RxCpltCallback(SPDIFRX_HandleTypeDef *hspdif)
{
	int txStatus = 0;
	RecUpdatePointer = REC_BUFF_SIZE/2;
		BaseType_t xHigherPriorityTaskWoken;

		txStatus = xSemaphoreGiveFromISR(myBinarySemSPDIFHandle, &xHigherPriorityTaskWoken);

				if (pdPASS == txStatus) {
							      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
							    }
}
static portBASE_TYPE prvSPDIFRec( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	int8_t *pcParameter1;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

			    /* Obtain the name of the source file, and the length of its name, from
			    the command string. The name of the source file is the first parameter. */
			    pcParameter1 = FreeRTOS_CLIGetParameter
			                        (
			                          /* The command string itself. */
			                          pcCommandString,
			                          /* Return the first parameter. */
			                          1,
			                          /* Store the parameter string length. */
			                          &xParameter1StringLength
			                        );

			  if(  strstr (pcParameter1,".wav") !=NULL )
			  {

				  fr= f_open(&xFile, pcParameter1,FA_CREATE_ALWAYS | FA_WRITE );
				  sprintf(pcWriteBuffer,"Recording stated %s",pcParameter1);
			  }
			  else
			  {
				  fr=FR_INVALID_PARAMETER;
				  f_close( &xFile );
				  strcpy(pcWriteBuffer,"Recording stopped\n");
			  }

}

static portBASE_TYPE prvSPDIFReadStats( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{

SPDIFRX_SR* SR;
int FS;
	/* Remove compile time warnings about unused parameters, and check the
	write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	write buffer length is adequate, so does not check for buffer overflows. */
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );



SR=(SPDIFRX_SR*)&hspdif.Instance->SR;

	if(SR->WIDTH5>0)FS=5*62400000/(SR->WIDTH5*64);

	sprintf(pcWriteBuffer,"WIDTH5=%x FS=%d Hz PERR=%d TERR=%d SERR=%d OVR=%d SYNCD=%d\n",SR->WIDTH5,FS,SR->PERR,SR->TERR,SR->SERR,SR->OVR,SR->SYNCD);

	if(SR->OVR) __HAL_SPDIFRX_CLEAR_IT(&hspdif, SPDIFRX_FLAG_OVR);

	return pdFALSE;

}

static portBASE_TYPE prvSPDIFChannel( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	return pdFALSE;
}
