/*
 * FF-Commands.c
 *
 *  Created on: Mar 22, 2019
 *      Author: kevin
 */

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "cmsis_os.h"
#include "FreeRTOS_CLI.h"
#include "fatfs.h"

static portBASE_TYPE prvFFListFiles( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );



/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvSFFListFilesCommandDefinition =
{
	( const int8_t * const ) "ls", /* The command string to type. */
	( const int8_t * const ) "ls:\r\n List Files\r\n\r\n",
	prvFFListFiles, /* The function to run. */
	0 /* No parameters are expected. */
};

typedef union {
 struct   {
	  	 unsigned int Day:5;
	     unsigned int Month:4;
	     unsigned int Year:7;
	  } fields;
	  WORD bits;
} mdate;


	  typedef struct   {
	  	  	 unsigned int Second :5;
	  	     unsigned int Minute :6;
	  	     unsigned int Hour :5;
	  	  } mtime;


static portBASE_TYPE prvFFListFiles( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{

	  FRESULT fr;     /* Return value */
	  DIR dj;         /* Directory search object */
	  FILINFO fno;    /* File information */
	  char line[64];
	  pcWriteBuffer[0]=0;
	  int i=0;
	  mdate d;
	  mtime t;


	    fr = f_findfirst(&dj, &fno, "", "*.*");  /* Start to search for photo files */

	//    strcpy( ( char * ) pcWriteBuffer,"file list\n" );

	    if(fr != FR_OK)
	    {
	    	  sprintf( line, "no file found err=%d\n",(int)fr);
	    	  strcat(pcWriteBuffer,line);
	    	  f_closedir(&dj);

	    	 return pdFALSE;
	    }

	    while (fr == FR_OK && fno.fname[0]) {         /* Repeat while an item is found */
	        /* Display the object name */
	    	d.bits=fno.fdate;
	    	memcpy(&t,&fno.ftime,sizeof(mtime));
	        sprintf(line,"%d %s %s %d bytes %d/%d/%d %dh : %dm : %ds\n",i++, fno.fname,fno.altname,fno.fsize,d.fields.Day,d.fields.Month,1980+d.fields.Year,t.Hour,t.Minute,t.Second);
	        strcat(pcWriteBuffer,line);
	        fr = f_findnext(&dj, &fno);  /* Search for next item */

	    }

	    f_closedir(&dj);

	    return pdFALSE;

}

