/*
 * wifi-commands.c
 *
 *  Created on: Apr 1, 2019
 *      Author: kevin
 */
#include "FreeRTOS.h"

#include "FreeRTOS_CLI.h"
#include "usart.h"

#include "cmsis_os.h"

static portBASE_TYPE prvWIFICommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

static void prvUART3ReadTask( void *pvParameters );
extern osSemaphoreId myBinarySemUART3Handle;
#define UART3_BUFFERSIZE 1024
uint8_t   UART3Buff[UART3_BUFFERSIZE];

static const CLI_Command_Definition_t prvWIFICommandDefinition =
{
	( const int8_t * const ) "wifi",
	( const int8_t * const ) "wifi ssid <ssid>: \n wifi connect <url> :\r\n",
	prvWIFICommand,
	2
};

//http://radionz-ice.streamguys.com/national.mp3

static portBASE_TYPE prvWIFICommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{

	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );




	return pdFALSE;
}

static void prvUART3ReadTask( void *pvParameters )
{




}
