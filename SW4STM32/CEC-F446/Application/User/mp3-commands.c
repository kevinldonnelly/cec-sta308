/*
 * mp3-commands.c
 *
 *  Created on: 10/04/2019
 *      Author: kevin
 */

#include "main.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include "cmsis_os.h"
#include "fatfs.h"
#include "mp3dec.h"
#include "player.h"
#include <stdio.h>
extern SAI_HandleTypeDef hsai_BlockB1;
extern osSemaphoreId myBinarySemSAIHandle;
//MP3FrameInfo mp3FrameInfo;
HMP3Decoder hMP3Decoder;
//#define MP3_SIZE       2*1152

#define DECODED_MP3_FRAME_SIZE	MAX_NGRAN * MAX_NCHAN * MAX_NSAMP
#define OUT_BUFFER_SIZE			2 * DECODED_MP3_FRAME_SIZE
#define LO_EMPTY 0x01  //low half of buffer empty
#define HI_EMPTY 0x02  //high half of buffer empty
#define READ_BUFFER_SIZE	2 * MAINBUF_SIZE + 216 //2*1940+216=4096=4K
static int bytes_left=0;
static int offset;
static int bytes_left_before_decoding=0;
static unsigned char *read_pointer;
static unsigned char read_buffer[READ_BUFFER_SIZE];
unsigned char out_buf_state = LO_EMPTY | HI_EMPTY;
static int underflows;
short out_buffer[OUT_BUFFER_SIZE];

static int result;
static xTaskHandle xPlayMP3Task = NULL;
FIL xFile;
//uint16_t   mp3_data[MP3_SIZE];
//static int16_t audio_buffer0[4096];
//static int16_t audio_buffer1[4096];
static portBASE_TYPE prvMP3PlayFile( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static void prvPlayMP3Task( void *pvParameters );

/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvmp3playFileCommandDefinition =
{
	( const int8_t * const ) "mp3play", /* The command string to type. */
	( const int8_t * const ) "mp3play:<filename>\r\n List Files\r\n\r\n",
	prvMP3PlayFile, /* The function to run. */
	1 /* No parameters are expected. */
};

static portBASE_TYPE prvMP3PlayFile( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	int8_t *pcParameter1;
	FRESULT fr=FR_INVALID_PARAMETER;
	BaseType_t r;
		BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

				    /* Obtain the name of the source file, and the length of its name, from
				    the command string. The name of the source file is the first parameter. */
				    pcParameter1 = FreeRTOS_CLIGetParameter
				                        (
				                          /* The command string itself. */
				                          pcCommandString,
				                          /* Return the first parameter. */
				                          1,
				                          /* Store the parameter string length. */
				                          &xParameter1StringLength
				                        );


				    if(xPlayMP3Task!=NULL){
				    	HAL_SAI_Abort(&hsai_BlockB1);
				    	vTaskDelay(100);
				    	f_close(&xFile);
				    	bytes_left = 0;
				    	read_pointer = NULL;
				    	MP3FreeDecoder(hMP3Decoder);
				    	vTaskDelete(xPlayMP3Task);
				    	xPlayMP3Task=NULL;
				    	return pdFALSE;
				    }



	if(f_open(&xFile, pcParameter1,FA_OPEN_EXISTING | FA_READ )==0)
	{

	r=xTaskCreate( prvPlayMP3Task,				/* The task that implements the command console. */
						( const int8_t * const ) "MP3Task",		/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
						configUART_COMMAND_CONSOLE_STACK_SIZE*2,	/* The size of the stack allocated to the task. */
						NULL,									/* The parameter is not used, so NULL is passed. */
						configUART_COMMAND_CONSOLE_TASK_PRIORITY+1,/* The priority allocated to the task. */
						&xPlayMP3Task );

	sprintf(pcWriteBuffer,"mp3 play task started %s xTaskCreate=%d\n",pcParameter1,r);

	}
	else
	{
		sprintf(pcWriteBuffer,"openfailed  %s\n",pcParameter1);
	}
	return pdFALSE;
}


static int try_get_header(unsigned char *buff, MP3FrameInfo *fi)
{
	int ret, offs1, offs = 0;

	while (1)
	{
		offs1 = MP3FindSyncWord(buff + offs, 2048 - offs);
		if (offs1 < 0) return -2;
		offs += offs1;
		if (2048 - offs < 4) return -3;

		// printf("trying header %08x\n", *(int *)(buff + offs));

		ret = MP3GetNextFrameInfo(hMP3Decoder, fi, buff + offs);
		if (ret == 0 && fi->bitrate != 0) break;
		offs++;
	}

	return ret;
}

int mp3_get_bitrate(FILE *f, int len)
{
	unsigned char buff[2048];
	MP3FrameInfo fi;
	int ret;

	memset(buff, 0, 2048);

	if (!hMP3Decoder) hMP3Decoder = MP3InitDecoder();

	fseek(f, 0, SEEK_SET);
	ret = fread(buff, 1, 2048, f);
	fseek(f, 0, SEEK_SET);
	if (ret <= 0) return -1;

	ret = try_get_header(buff, &fi);
	if (ret != 0 || fi.bitrate == 0) {
		// try to read somewhere around the middle
		fseek(f, len>>1, SEEK_SET);
		fread(buff, 1, 2048, f);
		fseek(f, 0, SEEK_SET);
		ret = try_get_header(buff, &fi);
	}
	if (ret != 0) return ret;

	// printf("bitrate: %i\n", fi.bitrate / 1000);

	return fi.bitrate / 1000;
}

int refill_inbuffer(FIL *in_file)
{
	unsigned int bytes_read;
	unsigned int bytes_to_read;
	FRESULT result;

	if (bytes_left > 0) {
		//copy remaining data to beginning of buffer
		memcpy(read_buffer, read_pointer, bytes_left);
	}

	bytes_to_read = READ_BUFFER_SIZE - bytes_left;

	result = f_read(in_file, (BYTE *)read_buffer + bytes_left, bytes_to_read, &bytes_read);
	if(result != FR_OK)
		return READ_ERROR;

	if (bytes_read == bytes_to_read){
		read_pointer = read_buffer;
		offset = 0;
		bytes_left = READ_BUFFER_SIZE;
		return 0;
	}
	else{
		return END_OF_FILE;
	}

	return 0;  //should never reach this point
}

volatile int mp3_proccess(FIL *mp3_file)
{
	MP3FrameInfo mp3FrameInfo;

	//while(!out_buf_state);

	if (read_pointer == NULL) {
		if(refill_inbuffer(mp3_file) != 0){
			return END_OF_FILE;
		}
	}

	offset = MP3FindSyncWord(read_pointer, bytes_left);
	while(offset < 0) {
		if(refill_inbuffer(mp3_file) != 0)
			return END_OF_FILE;
		if(bytes_left > 0){
			bytes_left -= 1;
			read_pointer += 1;
		}
		offset = MP3FindSyncWord(read_pointer, bytes_left);
	}
	read_pointer += offset;
	bytes_left -= offset;
	bytes_left_before_decoding = bytes_left;

	if (MP3GetNextFrameInfo(hMP3Decoder, &mp3FrameInfo, read_pointer) == 0 &&
			mp3FrameInfo.nChans == 2 &&
			mp3FrameInfo.version == 0) {
		//	printf("Found a frame at offset %x\n", read_pointer);
	} else {
		// advance data pointer
		// TODO: handle bytes_left == 0
		if(bytes_left > 0){
			bytes_left -= 1;
			read_pointer += 1;
		}
		return 0;
	}

	if (bytes_left < MAINBUF_SIZE) {
		if(refill_inbuffer(mp3_file) != 0)
			return END_OF_FILE;
	}

	if(out_buf_state == (LO_EMPTY | HI_EMPTY)){
			underflows++;
			//DAC_DMA_disable();
	}

	if(out_buf_state & (LO_EMPTY)){
		result = MP3Decode(hMP3Decoder, &read_pointer, &bytes_left, out_buffer, 0);
		out_buf_state &= ~(LO_EMPTY);
		osSemaphoreWait(myBinarySemSAIHandle, osWaitForever);
		HAL_SAI_Transmit_IT(&hsai_BlockB1,(uint8_t *) out_buffer,DECODED_MP3_FRAME_SIZE);
	}
	if(out_buf_state & (HI_EMPTY)){
		result = MP3Decode(hMP3Decoder, &read_pointer, &bytes_left, &out_buffer[DECODED_MP3_FRAME_SIZE], 0);
		out_buf_state &= ~(HI_EMPTY);
		osSemaphoreWait(myBinarySemSAIHandle, osWaitForever);
		HAL_SAI_Transmit_IT(&hsai_BlockB1,(uint8_t *) &out_buffer[DECODED_MP3_FRAME_SIZE],DECODED_MP3_FRAME_SIZE);
	}

	//DAC_DMA_enable();
	if(result != ERR_MP3_NONE){
		switch(result){
		case ERR_MP3_INDATA_UNDERFLOW:
			bytes_left = 0;
			if(refill_inbuffer(mp3_file) != 0)
				return END_OF_FILE;
			break;
		case ERR_MP3_MAINDATA_UNDERFLOW:
			//do nothing, next call to MP3Decode will provide more data
			break;
		default:
			return 0; //skip this frame if error
			//return END_OF_FILE; //skip this file if error
		}
	}
	return 0;
}
static void prvPlayMP3Task( void *pvParameters )
{
	hMP3Decoder = MP3InitDecoder();
	if(hMP3Decoder==0)
	{
		printf("MP3 player could not allocate buffers\n");
		return;
	}
	read_pointer=NULL;
	bytes_left=0;
//	HAL_SAI_Transmit_DMA(&hsai_BlockB1,  (uint8_t *)out_buffer,OUT_BUFFER_SIZE);
	HAL_SAI_Transmit_IT(&hsai_BlockB1,  (uint8_t *)out_buffer,OUT_BUFFER_SIZE);

	while(1)
	{


			if(mp3_proccess(&xFile)!=0){
					HAL_SAI_Abort(&hsai_BlockB1);
					vTaskDelay(100);
					f_close(&xFile);

					bytes_left = 0;
					read_pointer = NULL;
					MP3FreeDecoder(hMP3Decoder);
					printf("MP3 Task ending\n");
					xPlayMP3Task=NULL;
					vTaskDelete(NULL);
					vTaskDelay(1000);

			}
			HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);

			vTaskDelay(1);

	}




}
/*
void HAL_SAI_TxHalfCpltCallback(SAI_HandleTypeDef *hsai)
{
	int txStatus = 0;
		//	RecUpdatePointer = REC_BUFF_SIZE/2;
				BaseType_t xHigherPriorityTaskWoken;
				out_buf_state |= HI_EMPTY;
				txStatus = xSemaphoreGiveFromISR(myBinarySemSAIHandle, &xHigherPriorityTaskWoken);

						if (pdPASS == txStatus) {
									      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
									    }
}
void HAL_SAI_TxCpltCallback(SAI_HandleTypeDef *hsai)
{
	int txStatus = 0;
	//	RecUpdatePointer = REC_BUFF_SIZE/2;
			BaseType_t xHigherPriorityTaskWoken;
			out_buf_state |= LO_EMPTY;
			txStatus = xSemaphoreGiveFromISR(myBinarySemSAIHandle, &xHigherPriorityTaskWoken);

					if (pdPASS == txStatus) {
								      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
								    }
}
*/
