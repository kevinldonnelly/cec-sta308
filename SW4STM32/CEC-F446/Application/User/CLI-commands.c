/*
    FreeRTOS V7.3.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"
extern const CLI_Command_Definition_t prvSendCECPacketCommandDefinition;
extern const CLI_Command_Definition_t prvSendCECPacketKeyCommandDefinition;
extern const CLI_Command_Definition_t prvSendCECPacketSysAudioModeReqCommandDefinition;
extern const CLI_Command_Definition_t prvSendCECPacketSetSystemAudioModeCommandDefinition;
extern const CLI_Command_Definition_t prvSendCECPacketInitiateARCCommandDefinition;
extern const CLI_Command_Definition_t prvSendI2CPacketCommandDefinition;
extern const CLI_Command_Definition_t prvReceiveI2CPacketCommandDefinition;
extern const CLI_Command_Definition_t prvConfigCommandDefinition;
extern const CLI_Command_Definition_t prvSPDIFReadStatCommandDefinition;
extern const CLI_Command_Definition_t prvI2CMemWriteDatatCommandDefinition;
extern const CLI_Command_Definition_t prvI2CMemReadDatatCommandDefinition;
extern const CLI_Command_Definition_t prvI2CReadIntCommandDefinition;
extern const CLI_Command_Definition_t prvI2CWriteIntCommandDefinition;
extern const CLI_Command_Definition_t prvSFFListFilesCommandDefinition;
extern const CLI_Command_Definition_t prvHSITrimCommandDefinition;
extern const CLI_Command_Definition_t prvmp3playFileCommandDefinition;
/*
 *
 * Implements the run-time-stats command.
 */
static portBASE_TYPE prvTaskStatsCommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

/*
 * Implements the task-stats command.
 */
static portBASE_TYPE prvRunTimeStatsCommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );




/*
 * Implements the create-task command.
 */
static portBASE_TYPE prvCreateTaskCommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

/*
 * Implements the delete-task command.
 */
static portBASE_TYPE prvDeleteTaskCommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

/*
 * The task that is created by the create-task command.
 */
static void prvCreatedTask( void *pvParameters );

/*
 * Holds the handle of the task created by the create-task command.
 */
static xTaskHandle xCreatedTaskHandle = NULL;

/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */


static const CLI_Command_Definition_t prvRunTimeStatsCommandDefinition =
{
	( const int8_t * const ) "run-time-stats",
	( const int8_t * const ) "run-time-stats:\r\n Displays a table showing how much processing time each FreeRTOS task has used\r\n\r\n",
	prvRunTimeStatsCommand,
	0
};


/* Structure that defines the "task-stats" command line command.  This generates
a table that gives information on each task in the system. */

static const CLI_Command_Definition_t prvTaskStatsCommandDefinition =
{
	( const int8_t * const ) "task-stats",
	( const int8_t * const ) "task-stats:\r\n Displays a table showing the state of each FreeRTOS task\r\n\r\n",
	prvTaskStatsCommand,
	0
};







/*-----------------------------------------------------------*/

void vRegisterCLICommands( void )
{
	/* Register all the command line commands defined immediately above. */
	FreeRTOS_CLIRegisterCommand( &prvTaskStatsCommandDefinition );
	FreeRTOS_CLIRegisterCommand( &prvRunTimeStatsCommandDefinition );
//	FreeRTOS_CLIRegisterCommand( &prvThreeParameterEchoCommandDefinition );
//	FreeRTOS_CLIRegisterCommand( &prvMultiParameterEchoCommandDefinition );
//	FreeRTOS_CLIRegisterCommand( &prvCreateTaskCommandDefinition );
//	FreeRTOS_CLIRegisterCommand( &prvDeleteTaskCommandDefinition );
	FreeRTOS_CLIRegisterCommand( &prvSendCECPacketCommandDefinition );
	FreeRTOS_CLIRegisterCommand( &prvSendCECPacketKeyCommandDefinition );
	FreeRTOS_CLIRegisterCommand( &prvSendCECPacketSysAudioModeReqCommandDefinition );
	FreeRTOS_CLIRegisterCommand( &prvSendCECPacketSetSystemAudioModeCommandDefinition);
	FreeRTOS_CLIRegisterCommand( &prvSendCECPacketInitiateARCCommandDefinition);
	FreeRTOS_CLIRegisterCommand( &prvSendI2CPacketCommandDefinition);
	FreeRTOS_CLIRegisterCommand(&prvReceiveI2CPacketCommandDefinition);
//	FreeRTOS_CLIRegisterCommand(&prvConfigCommandDefinition);
	FreeRTOS_CLIRegisterCommand(&prvSPDIFReadStatCommandDefinition);
	FreeRTOS_CLIRegisterCommand(&prvI2CMemWriteDatatCommandDefinition);
	FreeRTOS_CLIRegisterCommand(&prvI2CMemReadDatatCommandDefinition);
	FreeRTOS_CLIRegisterCommand(&prvI2CReadIntCommandDefinition);
	FreeRTOS_CLIRegisterCommand(&prvI2CWriteIntCommandDefinition);
	FreeRTOS_CLIRegisterCommand(&prvSFFListFilesCommandDefinition);
	FreeRTOS_CLIRegisterCommand(&prvmp3playFileCommandDefinition);
	FreeRTOS_CLIRegisterCommand(&prvHSITrimCommandDefinition);

}
/*-----------------------------------------------------------*/

static portBASE_TYPE prvTaskStatsCommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
const int8_t *const pcTaskTableHeader = ( int8_t * ) "Task          State  Priority  Stack	#\r\n************************************************\r\n";

	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );
	//memset(pcWriteBuffer,0,xWriteBufferLen);
	strcpy( ( char * ) pcWriteBuffer, ( char * ) pcTaskTableHeader );
	vTaskList( pcWriteBuffer + strlen( ( char * ) pcTaskTableHeader ) );


	return pdFALSE;
}

/*-----------------------------------------------------------*/

static portBASE_TYPE prvRunTimeStatsCommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
const int8_t * const pcStatsTableHeader = ( int8_t * ) "Task            Abs Time      % Time\r\n****************************************\r\n";


	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );


	strcpy( ( char * ) pcWriteBuffer, ( char * ) pcStatsTableHeader );
	vTaskGetRunTimeStats( pcWriteBuffer + strlen( ( char * ) pcStatsTableHeader ) );



	return pdFALSE;
}


/*-----------------------------------------------------------*/


/*-----------------------------------------------------------*/


/*-----------------------------------------------------------*/

