/*
 * I2C-Commands.c
 *
 *  Created on: Dec 8, 2018
 *      Author: kevin
 */
/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "i2c.h"
#include "cmsis_os.h"
#include "FreeRTOS_CLI.h"

extern osSemaphoreId myBinarySemI2CHandle;
uint8_t I2C_Data[8];
static portBASE_TYPE prvSendI2CData( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

static portBASE_TYPE prvReceiveI2CData( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvI2CMemWriteData( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvI2CMemReadData( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );


/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvSendI2CPacketCommandDefinition =
{
	( const int8_t * const ) "tx-i2c", /* The command string to type. */
	( const int8_t * const ) "tx-i2c Address data1 data2 ...\r\n Transmit packet\r\n\r\n",
	prvSendI2CData, /* The function to run. */
	-1 /* No parameters are expected. */
};

/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvReceiveI2CPacketCommandDefinition =
{
	( const int8_t * const ) "rx-i2c", /* The command string to type. */
	( const int8_t * const ) "rx-i2c Address size.\r\n Receive packet\r\n\r\n",
	prvReceiveI2CData, /* The function to run. */
	2 /* No parameters are expected. */
};

/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvI2CMemWriteDatatCommandDefinition =
{
	( const int8_t * const ) "i2c-mem-write", /* The command string to type. */
	( const int8_t * const ) "i2c-mem-write DevAddress MemAddress MemAddressSize data1 data2 ...\r\n Receive packet\r\n\r\n",
	prvI2CMemWriteData, /* The function to run. */
	-1 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvI2CMemReadDatatCommandDefinition =
{
	( const int8_t * const ) "i2c-mem-read", /* The command string to type. */
	( const int8_t * const ) "i2c-mem-read DevAddress MemAddressSize MemAddress size.\r\n Receive packet\r\n\r\n",
	prvI2CMemReadData, /* The function to run. */
	4 /* No parameters are expected. */
};



static portBASE_TYPE prvI2CMemWriteData( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	return pdFALSE;
}
static portBASE_TYPE prvI2CMemReadData( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	uint16_t DevAddress=0;
	HAL_StatusTypeDef s=0;
	uint16_t size=0;
	uint8_t ret;
	char DevAddressParam[4],MemAddressParam[4],MemAddressSizeParam[2];

	static portBASE_TYPE xParameterNumber = 0;
	portBASE_TYPE xParameterStringLength, xReturn;
	int8_t *pcParameterString;
	__IO uint32_t   MemaddSize;
	__IO uint32_t   Memaddress;
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	while(xParameterNumber < 4)
		{
			pcParameterString = ( int8_t * ) FreeRTOS_CLIGetParameter
										(
											pcCommandString,		/* The command string itself. */
											xParameterNumber,		/* Return the next parameter. */
											&xParameterStringLength	/* Store the parameter string length. */
										);



			if(pcParameterString==NULL){

				if(xParameterNumber>0){



					break;
				}

			}
			if(xParameterStringLength==0){

					xParameterNumber++;
					continue;
				}
			if(xParameterNumber==1)
			{
			memset(DevAddressParam,0,4);
			strncpy(DevAddressParam,pcParameterString,2);
			ret=(uint8_t)strtol(DevAddressParam,NULL,16);
			DevAddress=ret;

			}
			else if(xParameterNumber==2)
			{
				memset(MemAddressParam,0,4);
				strncpy(MemAddressParam,pcParameterString,4);
				ret=(uint8_t)strtol(MemAddressParam,NULL,16);
				Memaddress=ret;
			}
			else if(xParameterNumber==3)
			{
				memset(MemAddressSizeParam,0,2);
				strncpy(MemAddressSizeParam,pcParameterString,2);
				ret=(uint8_t)strtol(MemAddressSizeParam,NULL,10);
				MemaddSize=ret;
			}

			xParameterNumber++;
		}
	xParameterNumber=0;

	s=HAL_I2C_Mem_Read_IT(&hi2c1,DevAddress,Memaddress,MemaddSize,I2C_Data,size);
	osSemaphoreWait(myBinarySemI2CHandle, 5000);
	vTaskDelay(10);

	return pdFALSE;
}

static portBASE_TYPE prvSendI2CData( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
int8_t *pcParameterString;
portBASE_TYPE xParameterStringLength, xReturn;
static portBASE_TYPE xParameterNumber = 0;
uint16_t DevAddress=0;
uint8_t ret;
char DataParam[2];
char AddressParam[4];
	/* Remove compile time warnings about unused parameters, and check the
	write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	write buffer length is adequate, so does not check for buffer overflows. */
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	int index=0;
	HAL_StatusTypeDef s=4;
	char Data[2];
	uint16_t size=0;
	osStatus status;
		/* Obtain the parameter string. */
	while(xParameterNumber<16)
	{
		pcParameterString = ( int8_t * ) FreeRTOS_CLIGetParameter
									(
										pcCommandString,		/* The command string itself. */
										xParameterNumber,		/* Return the next parameter. */
										&xParameterStringLength	/* Store the parameter string length. */
									);



		if(pcParameterString==NULL){

			if(xParameterNumber>0){
				size=index;
				s=HAL_I2C_Master_Transmit_IT(&hi2c1,DevAddress,I2C_Data,index);
				status=osSemaphoreWait(myBinarySemI2CHandle, 1000);

				break;
			}

		}
		if(xParameterStringLength==0){

				xParameterNumber++;
				continue;
			}
		if(xParameterNumber>1)
		{
		strncpy(DataParam,pcParameterString,2);
		ret=(uint8_t)strtol(DataParam,NULL,16);
		I2C_Data[index]=ret;
		index++;
		}
		else if(xParameterNumber==1)
		{
			strncpy(AddressParam,pcParameterString,4);
			ret=(uint8_t)strtol(AddressParam,NULL,16);
			DevAddress=ret;
		}

		xParameterNumber++;
	}
	xParameterNumber=0;
	sprintf( ( char * ) pcWriteBuffer, "H=%d address=%d,size=%d status=%d\n", s,DevAddress,index,status);

	for(index=0; index< size; index++)
		{
			sprintf(Data,"%x ",I2C_Data[index]);
			strcat(pcWriteBuffer,Data);
		}

	return pdFALSE;
}

static portBASE_TYPE prvReceiveI2CData( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
int8_t *pcParameterString;
portBASE_TYPE xParameterStringLength, xReturn;
static portBASE_TYPE xParameterNumber = 0;
uint16_t DevAddress=0;
uint8_t ret=0;
char SizeParam[2];
char AddressParam[4];
char Data[2];
uint16_t size=0;
int index=0;
memset(I2C_Data,0,8);
	/* Remove compile time warnings about unused parameters, and check the
	write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	write buffer length is adequate, so does not check for buffer overflows. */
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );


	HAL_StatusTypeDef s=0;
		/* Obtain the parameter string. */
	while(xParameterNumber < 3)
	{
		pcParameterString = ( int8_t * ) FreeRTOS_CLIGetParameter
									(
										pcCommandString,		/* The command string itself. */
										xParameterNumber,		/* Return the next parameter. */
										&xParameterStringLength	/* Store the parameter string length. */
									);



		if(pcParameterString==NULL){

			if(xParameterNumber>0){



				break;
			}

		}
		if(xParameterStringLength==0){

				xParameterNumber++;
				continue;
			}
		if(xParameterNumber==1)
		{
		memset(AddressParam,0,4);
		strncpy(AddressParam,pcParameterString,2);
		ret=(uint8_t)strtol(AddressParam,NULL,16);
		DevAddress=ret;

		}
		else if(xParameterNumber==2)
		{
			memset(SizeParam,0,2);
			strncpy(SizeParam,pcParameterString,2);
			ret=(uint8_t)strtol(SizeParam,NULL,16);
			size=ret;
		}

		xParameterNumber++;
	}
	xParameterNumber=0;

	s=HAL_I2C_Master_Receive_IT(&hi2c1,DevAddress,I2C_Data,size);
	osSemaphoreWait(myBinarySemI2CHandle, 5000);
	vTaskDelay(10);

	sprintf( ( char * ) pcWriteBuffer, "H=%d address=%d,size=%d,%x\n", s,DevAddress,size,I2C_Data[0]);

	if(size>7) return pdFALSE;

	for(index=0; index< size; index++)
			{
				sprintf(Data,"%x ",I2C_Data[index]);
				strcat(pcWriteBuffer,Data);
			}


	return pdFALSE;
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{

	int txStatus = 0;
					  BaseType_t xHigherPriorityTaskWoken;
					  printf(">> \n");

					txStatus = xSemaphoreGiveFromISR(myBinarySemI2CHandle, &xHigherPriorityTaskWoken);


					    if (pdPASS == txStatus) {
					      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
					    }


	printf("i2c err code=%x\n",hi2c->ErrorCode);

}

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	int txStatus = 0;
				  BaseType_t xHigherPriorityTaskWoken;


				txStatus = xSemaphoreGiveFromISR(myBinarySemI2CHandle, &xHigherPriorityTaskWoken);


				    if (pdPASS == txStatus) {
				      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
				    }
				//    printf(">> \n");
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	int txStatus = 0;
			  BaseType_t xHigherPriorityTaskWoken;
			  printf("<< %x\n",I2C_Data[0]);

			txStatus = xSemaphoreGiveFromISR(myBinarySemI2CHandle, &xHigherPriorityTaskWoken);


			    if (pdPASS == txStatus) {
			      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
			    }
}
