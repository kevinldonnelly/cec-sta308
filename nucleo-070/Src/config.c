
/*
   $Author: PWQ $
 */

#include "main.h"
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include "cmsis_os.h"
#include "gpio.h"
//#define CONFIG_START_ADR 0x0807D000
//#define CONFIG_END_ADR   0x0807E000
#define CONFIG_ERASED 0xFFFFFFFF
#define FLASHKEY1 0x45670123
#define FLASHKEY2 0xCDEF89AB

//#define CONFIG_BLOCKS (int)((globals.configsize + FLASH_BLOCK_SIZE-1) / FLASH_BLOCK_SIZE)
char flashname[16];
char flashdata[16];//192.168.1.1

extern uint32_t configstart;
extern uint32_t configend;
extern osSemaphoreId myBinarySemFlashHandle;

static portBASE_TYPE prvConfigCommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
//#define PDATA(x) ( x + PARAM_START + CONFIG_START->offset )
//#define CFG(x) (CONFIG_START->x )

/* Structure that defines the "task-stats" command line command.  This generates
a table that gives information on each task in the system. */
const CLI_Command_Definition_t prvConfigCommandDefinition =
{
	( const int8_t * const ) "config", /* The command string to type. */
	( const int8_t * const ) "config:\r\n add,del,find,list\r\n\r\n",
	prvConfigCommand, /* The function to run. */
	-1 /* No parameters are expected. */
};




enum {
	CFG_NONE =0xffff,
	CFG_DEL  =0x0000,
	CFG_OK   =0xff00
};


int cmpstr(uint8_t *s1,uint8_t *s2) {
	while( *s2) {
		if (tolower(*s1++) != tolower(*s2++) ) return 0;
	}
	return 1;
}


cfgItem* cfgSrch(uint8_t *searchname,int8_t *pcWriteBuffer) {
	cfgItem *item;

	item = configstart;

//	sprintf(pcWriteBuffer,"state=%x state==CFG_NONE=%d\n",item->state,item->state == CFG_NONE);

	//return NULL;

	while( item->state != CFG_NONE) {
		if(item->len>100)item->len=0;
		if( item->state == CFG_OK && strncasecmp(searchname,item->name,16) == 0  ) {
			return item;
		}
		item = (uint32_t)item + sizeof(cfgItem) + (((item->len+1)/2)*2);
	}
	return NULL;
}


int cfgDelete(uint8_t *name,int8_t *pcWriteBuffer) {
	uint16_t state = CFG_DEL;
	cfgItem *item;
	HAL_StatusTypeDef res = HAL_OK;
	item = cfgSrch(name,pcWriteBuffer);
	if( item) {
		HAL_FLASH_Unlock();
		res=HAL_FLASH_Program_IT(FLASH_TYPEPROGRAM_HALFWORD,(unsigned)&(item->state),state);
		 osSemaphoreWait(myBinarySemFlashHandle, 1000);
		HAL_FLASH_Lock();
		return res;
	}
	return -1;
}

void cfgAdd(uint8_t *name,int len,uint16_t *data, int8_t *pcWriteBuffer) {
	cfgItem *item,*new;


	item =(cfgItem*)configstart;


	while( item->state != CFG_NONE) {
		item = (uint32_t)item + sizeof(cfgItem) + (((item->len+1)/2)*2);
	}
	if(item > configend) {
		strcpy(pcWriteBuffer,"ERROR: Configuration space is full, item not written\n");
		return;
	}

	cfgDelete(name,pcWriteBuffer);

	new =pcWriteBuffer;

	new->state = CFG_OK;
	new->len = len;

	strncpy(new->name,name,15);

	flash_write_halfwords(item,new,sizeof(*new));
	flash_write_halfwords(item->data,data,len);
//	sprintf(pcWriteBuffer,"Add %s at %x len %d dwords\n",name,item,len);
	pcWriteBuffer[0]=0;

	sprintf(pcWriteBuffer,"Add %s len %d dwords %x %x ",name,len,item,item->data);
	strncat(pcWriteBuffer,data,len);
	strcat(pcWriteBuffer,"\n");

}


int cfgErase(uint32_t key1,uint32_t key2) {
	int16_t state = CFG_NONE;

	if(key1 != FLASHKEY1 && key2 != FLASHKEY2) {
		printf("ERROR cfgErase called with wrong unlock key\n");
		return 0;
	}
	HAL_FLASH_Program_IT(FLASH_TYPEPROGRAM_HALFWORD,configstart,state);
	 osSemaphoreWait(myBinarySemFlashHandle, 1000);
	return 1;
}

void cfgList(int8_t *pattern) {
	cfgItem *item;
	item = configstart;
	char buffer[32];
	while( item->state != CFG_NONE) {
		if( item->state == CFG_OK  ) {
			if((pattern == NULL || *pattern == 0 || cmpstr(item->name,pattern)))
				memset(buffer,0,32);
				strncpy(buffer,item->data,item->len-1);
				sprintf("%-16s %4d  0x%p [%16s]\n",item->name,item->len,item->data,buffer);
				taskYIELD();
		}
		item = (uint32_t)item + sizeof(cfgItem) + (((item->len+1)/2)*2);
	}
}


static portBASE_TYPE prvConfigCommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	int8_t *args[3],i,len;
	static	portBASE_TYPE xParameterStringLength[3], xReturn;
	portBASE_TYPE argc = 1;
	cfgItem *item=NULL;



	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );


	args[0] = ( int8_t * ) FreeRTOS_CLIGetParameter
									(
										pcCommandString,		/* The command string itself. */
										1,		/* Return the next parameter. */
										&xParameterStringLength[0]	/* Store the parameter string length. */
									);



	if(args[0]==NULL)return pdFALSE;

	argc++;

	args[1] = ( int8_t * ) FreeRTOS_CLIGetParameter
										(
											pcCommandString,		/* The command string itself. */
											2,		/* Return the next parameter. */
											&xParameterStringLength[1]	/* Store the parameter string length. */
										);



	if(args[1]!=NULL)
	{
	argc++;
	args[2] = ( int8_t * ) FreeRTOS_CLIGetParameter
										(
											pcCommandString,		/* The command string itself. */
											3,		/* Return the next parameter. */
											&xParameterStringLength[2]	/* Store the parameter string length. */
										);
	if(args[2]==NULL)argc--;
	}



	pcWriteBuffer[0]=0;

	switch (args[0][0]) {
			default:
				sprintf(pcWriteBuffer,"Unknown config command args=%d,",argc);

				strcat(( char * ) pcWriteBuffer,args[0]);

				strcat(( char * ) pcWriteBuffer,"\n");
				break;
			case 'd':
				 if( argc < 2 || args[1]==NULL) {
					 strcpy(pcWriteBuffer,"Enter config name to delete\n");
				  break;
				 }
				 sprintf(pcWriteBuffer,"to delete %s\n",args[1]);

				 cfgDelete(args[1],pcWriteBuffer);
				 break;
			case 'l':
				 cfgList(argc > 1 ? args[1] : NULL);
				 break;
			case 'f':
				 if( argc != 2) {
					 sprintf(pcWriteBuffer,"Enter config name to find argc=%d\n",argc);
				  break;
				 }


				 item = cfgSrch(args[1],pcWriteBuffer);
				 if(!item)
				 {
					 	sprintf(pcWriteBuffer,"[%s] not found\n",args[1]);
				 }
				 else
				 {
					 sprintf(pcWriteBuffer,"[%s] len=%d data =0x%p name=",args[1],item->len,item->data);
				 	 strncat(pcWriteBuffer,item->name,16);
				 	 strcat(pcWriteBuffer," data=");
				 	 strncat(pcWriteBuffer,item->data,item->len);
				 	 strcat(pcWriteBuffer,"\n");
				 }
				 break;
			case 'a':
				 if( argc != 3)
					 sprintf(pcWriteBuffer,"Enter config name and new value argc=%d\n",argc);
				 else {


					 len=xParameterStringLength[2];

					 strncpy(flashname,args[1],xParameterStringLength[1]);
					 strncpy(flashdata,args[2],xParameterStringLength[2]);
					// sprintf(pcWriteBuffer,"name %s and new value %s argc=%d len=%d\n",buffer,args[2],argc,len);
					// args[1][xParameterStringLength[1]]=0;
					// sprintf(pcWriteBuffer,"argc=%d\n",argc);

					 cfgAdd(flashname,len,flashdata,pcWriteBuffer);
				 }
				 break;
			case 'e':
			//	cfgErase(FLASHKEY1,FLASHKEY2);
				strcpy(pcWriteBuffer,"Config Erase requested\n");
				break;
		}

	argc=0;

	return pdFALSE;
}
