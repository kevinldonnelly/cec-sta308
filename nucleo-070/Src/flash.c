#include "stm32f0xx_hal.h"
#include "flash.h"
//#include "Precept.h"

#include <stdint.h>
#include "cmsis_os.h"

extern osSemaphoreId myBinarySemFlashHandle;

void  HAL_FLASH_EndOfOperationCallback(uint32_t ReturnValue)
{
	 int txStatus = 0;
		  BaseType_t xHigherPriorityTaskWoken;
		  printf(">> %x\n",ReturnValue);

		txStatus = xSemaphoreGiveFromISR(myBinarySemFlashHandle, &xHigherPriorityTaskWoken);


		    if (pdPASS == txStatus) {
		      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
		    }
}


void  HAL_FLASH_OperationErrorCallback(uint32_t ReturnValue)
{
	  printf("flash error \n");
}

int flash_write_halfwords(unsigned address, void const* data, unsigned sz)
{
	HAL_StatusTypeDef res = HAL_OK;
//	uint32_t PageError=0;

	HAL_FLASH_Unlock();

	if (((address & (FLASH_PAGE_SIZE-1)) == 0) ) {

		FLASH_EraseInitTypeDef EraseInit;
		EraseInit.TypeErase=FLASH_TYPEERASE_PAGES;
		EraseInit.NbPages=1;
		EraseInit.PageAddress=address;
		 // Clear pending flags (if any)
		 __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP  | FLASH_FLAG_WRPERR);
		 HAL_FLASHEx_Erase_IT(&EraseInit);
		 osSemaphoreWait(myBinarySemFlashHandle, 1000);
		 vTaskDelay(1000);
	}




	for (; sz >= 2; sz -= 2, address += 2) {
		res = HAL_FLASH_Program_IT(FLASH_TYPEPROGRAM_HALFWORD, address, *(uint16_t const*)data);
		osSemaphoreWait(myBinarySemFlashHandle, 1000);
		vTaskDelay(100);
		data = (uint16_t const*)data + 1;
		if (res != HAL_OK)
			goto done;
	}
done:
	HAL_FLASH_Lock();


	return res == HAL_OK ? 0 : -1;
}


