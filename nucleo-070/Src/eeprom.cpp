/*
 * eeprom.cpp

 *
 *  Created on: Dec 28, 2018
 *      Author: kevin
 */

#include "OSFS.h"
#include "i2c.h"
#include "cmsis_os.h"

uint16_t OSFS::startOfEEPROM = 1;
uint16_t OSFS::endOfEEPROM = 1024;

extern I2C_HandleTypeDef hi2c1;
extern osSemaphoreId myBinarySemI2CHandle;

void  OSFS::readNBytes(uint16_t DevAddress, unsigned int num, byte* output)
{

		HAL_I2C_Mem_Read_IT(&hi2c1,0x57,DevAddress,I2C_MEMADD_SIZE_16BIT,output,num);
		osSemaphoreWait(myBinarySemI2CHandle, 5000);
}



int ReadInt(char* name)
{
	printf("Overly Simplified File System (OSFS) : read-test");

	  // Var to hold the result of actions
	  OSFS::result r;

	  // Useful consts
	  const OSFS::result noerr = OSFS::result::NO_ERROR;
	  const OSFS::result notfound = OSFS::result::FILE_NOT_FOUND;

	  ////////////////////////////

	 printf("Looking for testInt...");

	  int testInt;
	  r = OSFS::getFile(name, testInt);


	  if (r == noerr) return testInt;
	  else return (int)r;
}

extern "C" int CReadInt(char* name)
{
return ReadInt(name);
}




