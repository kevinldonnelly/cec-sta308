################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/CLI-commands.c \
../Src/FreeRTOS_CLI.c \
../Src/I2C-Commands.c \
../Src/UART-poll-driven-command-console.c \
../Src/config.c \
../Src/flash.c \
../Src/freertos.c \
../Src/gpio.c \
../Src/i2c.c \
../Src/main.c \
../Src/spi.c \
../Src/stm32f0xx_hal_msp.c \
../Src/stm32f0xx_hal_timebase_tim.c \
../Src/stm32f0xx_it.c \
../Src/syscalls.c \
../Src/system_stm32f0xx.c \
../Src/usart.c 

CPP_SRCS += \
../Src/OSFS.cpp \
../Src/eeprom.cpp 

OBJS += \
./Src/CLI-commands.o \
./Src/FreeRTOS_CLI.o \
./Src/I2C-Commands.o \
./Src/OSFS.o \
./Src/UART-poll-driven-command-console.o \
./Src/config.o \
./Src/eeprom.o \
./Src/flash.o \
./Src/freertos.o \
./Src/gpio.o \
./Src/i2c.o \
./Src/main.o \
./Src/spi.o \
./Src/stm32f0xx_hal_msp.o \
./Src/stm32f0xx_hal_timebase_tim.o \
./Src/stm32f0xx_it.o \
./Src/syscalls.o \
./Src/system_stm32f0xx.o \
./Src/usart.o 

C_DEPS += \
./Src/CLI-commands.d \
./Src/FreeRTOS_CLI.d \
./Src/I2C-Commands.d \
./Src/UART-poll-driven-command-console.d \
./Src/config.d \
./Src/flash.d \
./Src/freertos.d \
./Src/gpio.d \
./Src/i2c.d \
./Src/main.d \
./Src/spi.d \
./Src/stm32f0xx_hal_msp.d \
./Src/stm32f0xx_hal_timebase_tim.d \
./Src/stm32f0xx_it.d \
./Src/syscalls.d \
./Src/system_stm32f0xx.d \
./Src/usart.d 

CPP_DEPS += \
./Src/OSFS.d \
./Src/eeprom.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' -DSTM32F0xx '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F070xB -DUSE_FULL_LL_DRIVER -I"D:/project/workspace/CEC-sta308/nucleo-070/Inc" -I"D:/project/workspace/CEC-sta308/nucleo-070/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/project/workspace/CEC-sta308/nucleo-070/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/project/workspace/CEC-sta308/nucleo-070/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0" -I"D:/project/workspace/CEC-sta308/nucleo-070/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/project/workspace/CEC-sta308/nucleo-070/Middlewares/Third_Party/FreeRTOS/Source/include" -I"D:/project/workspace/CEC-sta308/nucleo-070/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"D:/project/workspace/CEC-sta308/nucleo-070/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Src/%.o: ../Src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU G++ Compiler'
	@echo $(PWD)
	arm-none-eabi-g++ -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' -DSTM32F0xx '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F070xB -DUSE_FULL_LL_DRIVER -I"D:/project/workspace/CEC-sta308/nucleo-070/Inc" -I"C:/users/kevin/Documents/ArduinoData/packages/STM32/hardware/stm32/1.4.0/variants/NUCLEO_F030R8" -I"C:/users/kevin/Documents/ArduinoData/packages/STM32/hardware/stm32/1.4.0/cores/arduino" -I"C:/users/kevin/Documents/ArduinoData/packages/STM32/hardware/stm32/1.4.0/cores/arduino/stm32" -I"D:/project/workspace/CEC-sta308/nucleo-070/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/project/workspace/CEC-sta308/nucleo-070/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/project/workspace/CEC-sta308/nucleo-070/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0" -I"D:/project/workspace/CEC-sta308/nucleo-070/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/project/workspace/CEC-sta308/nucleo-070/Middlewares/Third_Party/FreeRTOS/Source/include" -I"D:/project/workspace/CEC-sta308/nucleo-070/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"D:/project/workspace/CEC-sta308/nucleo-070/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fno-exceptions -fno-rtti -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


