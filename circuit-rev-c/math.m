clc;
clear;
close all;

Vin = 30;
Vout = 5;
Vfb = 2.5;
Rfb1 = 2e3;
Rfb2 = (Vout*Rfb1/Vfb)-Rfb1

Fmax = 263e3;
Rt = Vout/(1.385e-10*Fmax)

Ior=182e-3;
L1 = Vout*(Vin-Vout)/(Ior*Fmax*Vin);