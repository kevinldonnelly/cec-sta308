EESchema Schematic File Version 2
LIBS:_ams-reg
LIBS:_cirrus-logic
LIBS:conn
LIBS:device
LIBS:_maxim
LIBS:power
LIBS:_ptc
LIBS:regul
LIBS:_st-audio
LIBS:_switchcraft
LIBS:_ti-reg
LIBS:_wolfson
LIBS:_wurth
LIBS:_toshiba
LIBS:sta308a-ev-rev-c-cache
EELAYER 24 0
EELAYER END
$Descr USLegal 14000 8500
encoding utf-8
Sheet 3 7
Title "STA516B"
Date ""
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L STA516B U13
U 1 1 53D003D0
P 5100 3950
F 0 "U13" H 5100 6250 60  0000 C CNN
F 1 "STA516B" H 4600 1650 60  0000 C CNN
F 2 "_st:powerso36" H 5100 3950 60  0001 C CNN
F 3 "" H 5100 3950 60  0000 C CNN
F 4 "STMicroelectronics" H 5100 3950 60  0001 C CNN "Manufacturer"
F 5 "STA516B13TR" H 5100 3950 60  0001 C CNN "Manufacturer Part Number"
F 6 "497-11459-1-ND" H 5100 3950 60  0001 C CNN "Digi-Key Part Number"
F 7 "IC QUAD HALF BRIDGE AMP PWRS036" H 5100 3950 60  0001 C CNN "Description"
	1    5100 3950
	1    0    0    -1  
$EndComp
Text HLabel 3700 1700 0    60   Input ~ 0
IN1A
Text HLabel 3700 1800 0    60   Input ~ 0
IN1B
Wire Wire Line
	3700 5200 4100 5200
Wire Wire Line
	3900 5200 3900 5100
Wire Wire Line
	3900 5100 4100 5100
Wire Wire Line
	3700 5100 3700 5200
Connection ~ 3900 5200
Wire Wire Line
	4100 4700 3900 4700
Wire Wire Line
	3900 4700 3900 4600
Wire Wire Line
	3300 4600 4100 4600
Wire Wire Line
	3700 4600 3700 4700
Connection ~ 3900 4600
Wire Wire Line
	3300 4600 3300 4700
Connection ~ 3700 4600
$Comp
L GND #PWR053
U 1 1 53D00804
P 3300 5200
F 0 "#PWR053" H 3300 5200 30  0001 C CNN
F 1 "GND" H 3300 5130 30  0001 C CNN
F 2 "" H 3300 5200 60  0000 C CNN
F 3 "" H 3300 5200 60  0000 C CNN
	1    3300 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4400 3900 4400
Wire Wire Line
	3900 4400 3900 4300
Wire Wire Line
	2900 4300 4100 4300
Wire Wire Line
	2900 4300 2900 4700
Connection ~ 3900 4300
Wire Wire Line
	3300 5100 3300 5200
Wire Wire Line
	4100 5500 2900 5500
Wire Wire Line
	2900 5500 2900 5100
$Comp
L GND #PWR054
U 1 1 53D0095F
P 3900 5750
F 0 "#PWR054" H 3900 5750 30  0001 C CNN
F 1 "GND" H 3900 5680 30  0001 C CNN
F 2 "" H 3900 5750 60  0000 C CNN
F 3 "" H 3900 5750 60  0000 C CNN
	1    3900 5750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR055
U 1 1 53D00973
P 3900 5950
F 0 "#PWR055" H 3900 5950 30  0001 C CNN
F 1 "GND" H 3900 5880 30  0001 C CNN
F 2 "" H 3900 5950 60  0000 C CNN
F 3 "" H 3900 5950 60  0000 C CNN
	1    3900 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 5700 3900 5700
Wire Wire Line
	3900 5700 3900 5750
Wire Wire Line
	4100 5900 3900 5900
Wire Wire Line
	3900 5900 3900 5950
$Comp
L GND #PWR056
U 1 1 53D00A33
P 3900 2450
F 0 "#PWR056" H 3900 2450 30  0001 C CNN
F 1 "GND" H 3900 2380 30  0001 C CNN
F 2 "" H 3900 2450 60  0000 C CNN
F 3 "" H 3900 2450 60  0000 C CNN
	1    3900 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2400 3900 2400
Wire Wire Line
	3900 2400 3900 2450
Text HLabel 3600 2600 0    60   Input ~ 0
PWD_516a
Wire Wire Line
	3600 2600 4100 2600
Wire Wire Line
	3700 2700 3700 2600
Connection ~ 3700 2600
Wire Wire Line
	4100 3300 3700 3300
Wire Wire Line
	3700 3200 3700 3400
Connection ~ 3700 3300
Wire Wire Line
	4100 3200 3900 3200
Wire Wire Line
	3900 3200 3900 3300
Connection ~ 3900 3300
$Comp
L GND #PWR057
U 1 1 53D00D26
P 3700 3900
F 0 "#PWR057" H 3700 3900 30  0001 C CNN
F 1 "GND" H 3700 3830 30  0001 C CNN
F 2 "" H 3700 3900 60  0000 C CNN
F 3 "" H 3700 3900 60  0000 C CNN
	1    3700 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3800 3700 3900
Wire Wire Line
	2900 2200 4100 2200
Wire Wire Line
	2900 4100 2900 3400
Wire Wire Line
	2900 2100 2900 2900
Wire Wire Line
	2800 4100 4100 4100
$Comp
L +3.3V #PWR058
U 1 1 53D0106B
P 2900 2100
F 0 "#PWR058" H 2900 2060 30  0001 C CNN
F 1 "+3.3V" H 2900 2210 30  0000 C CNN
F 2 "" H 2900 2100 60  0000 C CNN
F 3 "" H 2900 2100 60  0000 C CNN
	1    2900 2100
	1    0    0    -1  
$EndComp
Connection ~ 2900 2200
Text HLabel 2800 4100 0    60   Output ~ 0
TH_WARa
Connection ~ 2900 4100
$Comp
L C C51
U 1 1 53D01CD9
P 3700 3600
F 0 "C51" H 3550 3700 40  0000 L CNN
F 1 "100n" H 3500 3500 40  0000 L CNN
F 2 "smd_packages:c_0603" H 3738 3450 30  0001 C CNN
F 3 "" H 3700 3600 60  0000 C CNN
F 4 "TDK Corporation" H 3700 3600 60  0001 C CNN "Manufacturer"
F 5 "C1608X7R1E104K080AA" H 3700 3600 60  0001 C CNN "Manufacturer Part Number"
F 6 "445-1316-1-ND" H 3700 3600 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 25V 10% X7R 0603" H 3700 3600 60  0001 C CNN "Description"
	1    3700 3600
	1    0    0    -1  
$EndComp
$Comp
L C C52
U 1 1 53D01E27
P 3700 4900
F 0 "C52" H 3550 5000 40  0000 L CNN
F 1 "100n" H 3500 4800 40  0000 L CNN
F 2 "smd_packages:c_0603" H 3738 4750 30  0001 C CNN
F 3 "" H 3700 4900 60  0000 C CNN
F 4 "TDK Corporation" H 3700 4900 60  0001 C CNN "Manufacturer"
F 5 "C1608X7R1E104K080AA" H 3700 4900 60  0001 C CNN "Manufacturer Part Number"
F 6 "445-1316-1-ND" H 3700 4900 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 25V 10% X7R 0603" H 3700 4900 60  0001 C CNN "Description"
	1    3700 4900
	1    0    0    -1  
$EndComp
$Comp
L C C50
U 1 1 53D01E76
P 3300 4900
F 0 "C50" H 3150 5000 40  0000 L CNN
F 1 "100n" H 3100 4800 40  0000 L CNN
F 2 "smd_packages:c_0603" H 3338 4750 30  0001 C CNN
F 3 "" H 3300 4900 60  0000 C CNN
F 4 "TDK Corporation" H 3300 4900 60  0001 C CNN "Manufacturer"
F 5 "C1608X7R1E104K080AA" H 3300 4900 60  0001 C CNN "Manufacturer Part Number"
F 6 "445-1316-1-ND" H 3300 4900 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 25V 10% X7R 0603" H 3300 4900 60  0001 C CNN "Description"
	1    3300 4900
	1    0    0    -1  
$EndComp
$Comp
L C C49
U 1 1 53D01EA4
P 2900 4900
F 0 "C49" H 2750 5000 40  0000 L CNN
F 1 "100n" H 2700 4800 40  0000 L CNN
F 2 "smd_packages:c_0603" H 2938 4750 30  0001 C CNN
F 3 "" H 2900 4900 60  0000 C CNN
F 4 "TDK Corporation" H 2900 4900 60  0001 C CNN "Manufacturer"
F 5 "C1608X7R1E104K080AA" H 2900 4900 60  0001 C CNN "Manufacturer Part Number"
F 6 "445-1316-1-ND" H 2900 4900 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 25V 10% X7R 0603" H 2900 4900 60  0001 C CNN "Description"
	1    2900 4900
	1    0    0    -1  
$EndComp
$Comp
L R R15
U 1 1 53D020A9
P 2900 3150
F 0 "R15" V 2980 3150 40  0000 C CNN
F 1 "10K" V 2907 3151 40  0000 C CNN
F 2 "smd_packages:r_0603" V 2830 3150 30  0001 C CNN
F 3 "" H 2900 3150 30  0000 C CNN
F 4 "Vishay Dale" V 2900 3150 60  0001 C CNN "Manufacturer"
F 5 "CRCW060310K0FKEA" V 2900 3150 60  0001 C CNN "Manufacturer Part Number"
F 6 "541-10.0KHCT-ND" V 2900 3150 60  0001 C CNN "Digi-Key Part Number"
F 7 "RES 10.0K OHM 1/10W 1% 0603 SMD" V 2900 3150 60  0001 C CNN "Description"
	1    2900 3150
	-1   0    0    1   
$EndComp
$Comp
L R R16
U 1 1 53D021BE
P 3700 2950
F 0 "R16" V 3780 2950 40  0000 C CNN
F 1 "10K" V 3707 2951 40  0000 C CNN
F 2 "smd_packages:r_0603" V 3630 2950 30  0001 C CNN
F 3 "" H 3700 2950 30  0000 C CNN
F 4 "Vishay Dale" V 3700 2950 60  0001 C CNN "Manufacturer"
F 5 "CRCW060310K0FKEA" V 3700 2950 60  0001 C CNN "Manufacturer Part Number"
F 6 "541-10.0KHCT-ND" V 3700 2950 60  0001 C CNN "Digi-Key Part Number"
F 7 "RES 10.0K OHM 1/10W 1% 0603 SMD" V 3700 2950 60  0001 C CNN "Description"
	1    3700 2950
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR059
U 1 1 53D024DF
P 5100 6700
F 0 "#PWR059" H 5100 6700 30  0001 C CNN
F 1 "GND" H 5100 6630 30  0001 C CNN
F 2 "" H 5100 6700 60  0000 C CNN
F 3 "" H 5100 6700 60  0000 C CNN
	1    5100 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2100 7200 2100
Wire Wire Line
	6300 2100 6300 2200
Wire Wire Line
	6300 2200 6100 2200
Wire Wire Line
	6100 3200 6300 3200
Wire Wire Line
	6300 3200 6300 3300
Wire Wire Line
	6100 3300 7200 3300
$Comp
L INDUCTOR L15
U 1 1 53D31F45
P 7900 1600
F 0 "L15" V 7850 1600 40  0000 C CNN
F 1 "22uH" V 8000 1600 40  0000 C CNN
F 2 "_wurth:7447017" H 7900 1600 60  0001 C CNN
F 3 "" H 7900 1600 60  0000 C CNN
F 4 "Wurth Electronics Inc" V 7900 1600 60  0001 C CNN "Manufacturer"
F 5 "7447017" V 7900 1600 60  0001 C CNN "Manufacturer Part Number"
F 6 "732-1418-ND" V 7900 1600 60  0001 C CNN "Digi-Key Part Number"
F 7 "CHOKE TOROID 22UH 5.0A VERT" V 7900 1600 60  0001 C CNN "Description"
	1    7900 1600
	0    -1   -1   0   
$EndComp
$Comp
L R R19
U 1 1 53D3208F
P 8400 2400
F 0 "R19" V 8480 2400 40  0000 C CNN
F 1 "6R" V 8407 2401 40  0000 C CNN
F 2 "_ohmite:tww-series-5w" V 8330 2400 30  0001 C CNN
F 3 "" H 8400 2400 30  0000 C CNN
F 4 "Ohmite" V 8400 2400 60  0001 C CNN "Manufacturer"
F 5 "TWW5J5R6E" V 8400 2400 60  0001 C CNN "Manufacturer Part Number"
F 6 "TWW5J5R6E-ND" V 8400 2400 60  0001 C CNN "Digi-Key Part Number"
F 7 "RES 5.6 OHM 5W 5% RADIAL" V 8400 2400 60  0001 C CNN "Description"
	1    8400 2400
	-1   0    0    1   
$EndComp
$Comp
L R R20
U 1 1 53D32448
P 8400 3000
F 0 "R20" V 8480 3000 40  0000 C CNN
F 1 "6R" V 8407 3001 40  0000 C CNN
F 2 "_ohmite:tww-series-5w" V 8330 3000 30  0001 C CNN
F 3 "" H 8400 3000 30  0000 C CNN
F 4 "Ohmite" V 8400 3000 60  0001 C CNN "Manufacturer"
F 5 "TWW5J5R6E" V 8400 3000 60  0001 C CNN "Manufacturer Part Number"
F 6 "TWW5J5R6E-ND" V 8400 3000 60  0001 C CNN "Digi-Key Part Number"
F 7 "RES 5.6 OHM 5W 5% RADIAL" V 8400 3000 60  0001 C CNN "Description"
	1    8400 3000
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR060
U 1 1 53D32524
P 8200 2800
F 0 "#PWR060" H 8200 2800 30  0001 C CNN
F 1 "GND" H 8200 2730 30  0001 C CNN
F 2 "" H 8200 2800 60  0000 C CNN
F 3 "" H 8200 2800 60  0000 C CNN
	1    8200 2800
	1    0    0    -1  
$EndComp
$Comp
L C C59
U 1 1 53D32800
P 8400 1850
F 0 "C59" H 8400 1950 40  0000 L CNN
F 1 "100n" H 8406 1765 40  0000 L CNN
F 2 "_epcos:b32620" H 8438 1700 30  0001 C CNN
F 3 "" H 8400 1850 60  0000 C CNN
F 4 "EPCOS Inc" H 8400 1850 60  0001 C CNN "Manufacturer"
F 5 "B32620A3104J" H 8400 1850 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1294-ND" H 8400 1850 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.1UF 250VDC RADIAL" H 8400 1850 60  0001 C CNN "Description"
	1    8400 1850
	1    0    0    -1  
$EndComp
$Comp
L C C67
U 1 1 53D32A16
P 9100 2700
F 0 "C67" H 9100 2800 40  0000 L CNN
F 1 "470n" H 9106 2615 40  0000 L CNN
F 2 "_epcos:b32652" H 9138 2550 30  0001 C CNN
F 3 "" H 9100 2700 60  0000 C CNN
F 4 "EPCOS Inc" H 9100 2700 60  0001 C CNN "Manufacturer"
F 5 "B32652A3474J" H 9100 2700 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1298-ND" H 9100 2700 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.47UF 250VDC RADIAL" H 9100 2700 60  0001 C CNN "Description"
	1    9100 2700
	1    0    0    -1  
$EndComp
$Comp
L C C57
U 1 1 53D32E7C
P 7400 2300
F 0 "C57" H 7400 2400 40  0000 L CNN
F 1 "330p" H 7406 2215 40  0000 L CNN
F 2 "_murata:dex-series" H 7438 2150 30  0001 C CNN
F 3 "" H 7400 2300 60  0000 C CNN
F 4 "Murata Electronics North America" H 7400 2300 60  0001 C CNN "Manufacturer"
F 5 "DEA1X3A331JA2B" H 7400 2300 60  0001 C CNN "Manufacturer Part Number"
F 6 "490-4158-ND" H 7400 2300 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 330PF 1KV 5% RADIAL" H 7400 2300 60  0001 C CNN "Description"
	1    7400 2300
	1    0    0    -1  
$EndComp
$Comp
L R R17
U 1 1 53D32EFB
P 7400 3150
F 0 "R17" V 7480 3150 40  0000 C CNN
F 1 "20R" V 7407 3151 40  0000 C CNN
F 2 "_ohmite:tww-series-5w" V 7330 3150 30  0001 C CNN
F 3 "" H 7400 3150 30  0000 C CNN
F 4 "Ohmite" V 7400 3150 60  0001 C CNN "Manufacturer"
F 5 "TWW5J20RE" V 7400 3150 60  0001 C CNN "Manufacturer Part Number"
F 6 "TWW5J20RE-ND" V 7400 3150 60  0001 C CNN "Digi-Key Part Number"
F 7 "RES 20 OHM 5W 5% RADIAL" V 7400 3150 60  0001 C CNN "Description"
	1    7400 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	8200 2800 8200 2700
Wire Wire Line
	8200 2700 8700 2700
Wire Wire Line
	8700 2600 8700 2800
Connection ~ 8700 2700
Wire Wire Line
	8400 2650 8400 2750
Connection ~ 8400 2700
Wire Wire Line
	8400 2150 8400 2050
Wire Wire Line
	8400 2100 8700 2100
Wire Wire Line
	8700 2100 8700 2200
Connection ~ 8400 2100
Wire Wire Line
	8400 3250 8400 3350
Wire Wire Line
	8400 3300 8700 3300
Wire Wire Line
	8700 3300 8700 3200
Connection ~ 8400 3300
Wire Wire Line
	8200 3800 9400 3800
Wire Wire Line
	8400 3800 8400 3750
Wire Wire Line
	9100 3800 9100 2900
Connection ~ 8400 3800
Wire Wire Line
	9100 2500 9100 1600
Wire Wire Line
	8200 1600 9400 1600
Wire Wire Line
	8400 1650 8400 1600
Connection ~ 8400 1600
Wire Wire Line
	9500 2600 9400 2600
Wire Wire Line
	9400 2600 9400 1600
Connection ~ 9100 1600
Wire Wire Line
	9500 2800 9400 2800
Wire Wire Line
	9400 2800 9400 3800
Connection ~ 9100 3800
Wire Wire Line
	7200 1600 7600 1600
Wire Wire Line
	7400 1600 7400 2100
Wire Wire Line
	7400 2500 7400 2900
Wire Wire Line
	7400 3400 7400 3800
Wire Wire Line
	7200 3800 7600 3800
$Comp
L GND #PWR061
U 1 1 53D33FC8
P 6900 2800
F 0 "#PWR061" H 6900 2800 30  0001 C CNN
F 1 "GND" H 6900 2730 30  0001 C CNN
F 2 "" H 6900 2800 60  0000 C CNN
F 3 "" H 6900 2800 60  0000 C CNN
	1    6900 2800
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR062
U 1 1 53D33FE9
P 6900 1700
F 0 "#PWR062" H 6900 1700 30  0001 C CNN
F 1 "GND" H 6900 1630 30  0001 C CNN
F 2 "" H 6900 1700 60  0000 C CNN
F 3 "" H 6900 1700 60  0000 C CNN
	1    6900 1700
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR063
U 1 1 53D3400A
P 6300 2600
F 0 "#PWR063" H 6300 2600 30  0001 C CNN
F 1 "GND" H 6300 2530 30  0001 C CNN
F 2 "" H 6300 2600 60  0000 C CNN
F 3 "" H 6300 2600 60  0000 C CNN
	1    6300 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR064
U 1 1 53D3402B
P 6300 3700
F 0 "#PWR064" H 6300 3700 30  0001 C CNN
F 1 "GND" H 6300 3630 30  0001 C CNN
F 2 "" H 6300 3700 60  0000 C CNN
F 3 "" H 6300 3700 60  0000 C CNN
	1    6300 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2100 7200 1600
Connection ~ 7400 1600
Connection ~ 6300 2100
Wire Wire Line
	7200 3300 7200 3800
Connection ~ 7400 3800
Connection ~ 6300 3300
Wire Wire Line
	6100 3600 6300 3600
Wire Wire Line
	6300 3600 6300 3700
Wire Wire Line
	6100 1800 6300 1800
Wire Wire Line
	6700 1800 6900 1800
Wire Wire Line
	6900 1800 6900 1700
Wire Wire Line
	6700 2900 6900 2900
Wire Wire Line
	6900 2900 6900 2800
Wire Wire Line
	6300 2900 6100 2900
$Comp
L CP1 C54
U 1 1 53D36051
P 6500 2900
F 0 "C54" V 6600 2650 50  0000 L CNN
F 1 "1000u" V 6400 2500 50  0000 L CNN
F 2 "_nichicon:he_series_7dot5" H 6500 2900 60  0001 C CNN
F 3 "" H 6500 2900 60  0000 C CNN
F 4 "Nichicon" V 6500 2900 60  0001 C CNN "Manufacturer"
F 5 "UHE1J102MHD" V 6500 2900 60  0001 C CNN "Manufacturer Part Number"
F 6 "493-1654-ND" V 6500 2900 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP ALUM 1000UF 63V 20% RADIAL" V 6500 2900 60  0001 C CNN "Description"
	1    6500 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 2500 6300 2500
Wire Wire Line
	6300 2500 6300 2600
$Comp
L CP1 C53
U 1 1 53D395EC
P 6500 1800
F 0 "C53" V 6600 1550 50  0000 L CNN
F 1 "1000u" V 6400 1400 50  0000 L CNN
F 2 "_nichicon:he_series_7dot5" H 6500 1800 60  0001 C CNN
F 3 "" H 6500 1800 60  0000 C CNN
F 4 "Nichicon" V 6500 1800 60  0001 C CNN "Manufacturer"
F 5 "UHE1J102MHD" V 6500 1800 60  0001 C CNN "Manufacturer Part Number"
F 6 "493-1654-ND" V 6500 1800 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP ALUM 1000UF 63V 20% RADIAL" V 6500 1800 60  0001 C CNN "Description"
	1    6500 1800
	0    -1   -1   0   
$EndComp
$Comp
L INDUCTOR L16
U 1 1 53D397B5
P 7900 3800
F 0 "L16" V 7850 3800 40  0000 C CNN
F 1 "22uH" V 8000 3800 40  0000 C CNN
F 2 "_wurth:7447017" H 7900 3800 60  0001 C CNN
F 3 "" H 7900 3800 60  0000 C CNN
F 4 "Wurth Electronics Inc" V 7900 3800 60  0001 C CNN "Manufacturer"
F 5 "7447017" V 7900 3800 60  0001 C CNN "Manufacturer Part Number"
F 6 "732-1418-ND" V 7900 3800 60  0001 C CNN "Digi-Key Part Number"
F 7 "CHOKE TOROID 22UH 5.0A VERT" V 7900 3800 60  0001 C CNN "Description"
	1    7900 3800
	0    -1   -1   0   
$EndComp
$Comp
L C C63
U 1 1 53D3B63E
P 8700 2400
F 0 "C63" H 8700 2500 40  0000 L CNN
F 1 "100n" H 8706 2315 40  0000 L CNN
F 2 "_epcos:b32620" H 8738 2250 30  0001 C CNN
F 3 "" H 8700 2400 60  0000 C CNN
F 4 "EPCOS Inc" H 8700 2400 60  0001 C CNN "Manufacturer"
F 5 "B32620A3104J" H 8700 2400 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1294-ND" H 8700 2400 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.1UF 250VDC RADIAL" H 8700 2400 60  0001 C CNN "Description"
	1    8700 2400
	1    0    0    -1  
$EndComp
$Comp
L C C64
U 1 1 53D3B6A3
P 8700 3000
F 0 "C64" H 8700 3100 40  0000 L CNN
F 1 "100n" H 8706 2915 40  0000 L CNN
F 2 "_epcos:b32620" H 8738 2850 30  0001 C CNN
F 3 "" H 8700 3000 60  0000 C CNN
F 4 "EPCOS Inc" H 8700 3000 60  0001 C CNN "Manufacturer"
F 5 "B32620A3104J" H 8700 3000 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1294-ND" H 8700 3000 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.1UF 250VDC RADIAL" H 8700 3000 60  0001 C CNN "Description"
	1    8700 3000
	1    0    0    -1  
$EndComp
$Comp
L C C60
U 1 1 53D3B6F3
P 8400 3550
F 0 "C60" H 8400 3650 40  0000 L CNN
F 1 "100n" H 8406 3465 40  0000 L CNN
F 2 "_epcos:b32620" H 8438 3400 30  0001 C CNN
F 3 "" H 8400 3550 60  0000 C CNN
F 4 "EPCOS Inc" H 8400 3550 60  0001 C CNN "Manufacturer"
F 5 "B32620A3104J" H 8400 3550 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1294-ND" H 8400 3550 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.1UF 250VDC RADIAL" H 8400 3550 60  0001 C CNN "Description"
	1    8400 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4600 7200 4600
Wire Wire Line
	6300 4600 6300 4700
Wire Wire Line
	6300 4700 6100 4700
Wire Wire Line
	6100 5700 6300 5700
Wire Wire Line
	6300 5700 6300 5800
Wire Wire Line
	6100 5800 7200 5800
$Comp
L INDUCTOR L17
U 1 1 53D3D925
P 7900 4100
F 0 "L17" V 7850 4100 40  0000 C CNN
F 1 "22uH" V 8000 4100 40  0000 C CNN
F 2 "_wurth:7447017" H 7900 4100 60  0001 C CNN
F 3 "" H 7900 4100 60  0000 C CNN
F 4 "Wurth Electronics Inc" V 7900 4100 60  0001 C CNN "Manufacturer"
F 5 "7447017" V 7900 4100 60  0001 C CNN "Manufacturer Part Number"
F 6 "732-1418-ND" V 7900 4100 60  0001 C CNN "Digi-Key Part Number"
F 7 "CHOKE TOROID 22UH 5.0A VERT" V 7900 4100 60  0001 C CNN "Description"
	1    7900 4100
	0    -1   -1   0   
$EndComp
$Comp
L R R21
U 1 1 53D3D92F
P 8400 4900
F 0 "R21" V 8480 4900 40  0000 C CNN
F 1 "6R" V 8407 4901 40  0000 C CNN
F 2 "_ohmite:tww-series-5w" V 8330 4900 30  0001 C CNN
F 3 "" H 8400 4900 30  0000 C CNN
F 4 "Ohmite" V 8400 4900 60  0001 C CNN "Manufacturer"
F 5 "TWW5J5R6E" V 8400 4900 60  0001 C CNN "Manufacturer Part Number"
F 6 "TWW5J5R6E-ND" V 8400 4900 60  0001 C CNN "Digi-Key Part Number"
F 7 "RES 5.6 OHM 5W 5% RADIAL" V 8400 4900 60  0001 C CNN "Description"
	1    8400 4900
	-1   0    0    1   
$EndComp
$Comp
L R R22
U 1 1 53D3D939
P 8400 5500
F 0 "R22" V 8480 5500 40  0000 C CNN
F 1 "6R" V 8407 5501 40  0000 C CNN
F 2 "_ohmite:tww-series-5w" V 8330 5500 30  0001 C CNN
F 3 "" H 8400 5500 30  0000 C CNN
F 4 "Ohmite" V 8400 5500 60  0001 C CNN "Manufacturer"
F 5 "TWW5J5R6E" V 8400 5500 60  0001 C CNN "Manufacturer Part Number"
F 6 "TWW5J5R6E-ND" V 8400 5500 60  0001 C CNN "Digi-Key Part Number"
F 7 "RES 5.6 OHM 5W 5% RADIAL" V 8400 5500 60  0001 C CNN "Description"
	1    8400 5500
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR065
U 1 1 53D3D93F
P 8200 5300
F 0 "#PWR065" H 8200 5300 30  0001 C CNN
F 1 "GND" H 8200 5230 30  0001 C CNN
F 2 "" H 8200 5300 60  0000 C CNN
F 3 "" H 8200 5300 60  0000 C CNN
	1    8200 5300
	1    0    0    -1  
$EndComp
$Comp
L C C61
U 1 1 53D3D949
P 8400 4350
F 0 "C61" H 8400 4450 40  0000 L CNN
F 1 "100n" H 8406 4265 40  0000 L CNN
F 2 "_epcos:b32620" H 8438 4200 30  0001 C CNN
F 3 "" H 8400 4350 60  0000 C CNN
F 4 "EPCOS Inc" H 8400 4350 60  0001 C CNN "Manufacturer"
F 5 "B32620A3104J" H 8400 4350 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1294-ND" H 8400 4350 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.1UF 250VDC RADIAL" H 8400 4350 60  0001 C CNN "Description"
	1    8400 4350
	1    0    0    -1  
$EndComp
$Comp
L C C68
U 1 1 53D3D953
P 9100 5200
F 0 "C68" H 9100 5300 40  0000 L CNN
F 1 "470n" H 9106 5115 40  0000 L CNN
F 2 "_epcos:b32652" H 9138 5050 30  0001 C CNN
F 3 "" H 9100 5200 60  0000 C CNN
F 4 "EPCOS Inc" H 9100 5200 60  0001 C CNN "Manufacturer"
F 5 "B32652A3474J" H 9100 5200 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1298-ND" H 9100 5200 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.47UF 250VDC RADIAL" H 9100 5200 60  0001 C CNN "Description"
	1    9100 5200
	1    0    0    -1  
$EndComp
$Comp
L C C58
U 1 1 53D3D967
P 7400 4800
F 0 "C58" H 7400 4900 40  0000 L CNN
F 1 "330p" H 7406 4715 40  0000 L CNN
F 2 "_murata:dex-series" H 7438 4650 30  0001 C CNN
F 3 "" H 7400 4800 60  0000 C CNN
F 4 "Murata Electronics North America" H 7400 4800 60  0001 C CNN "Manufacturer"
F 5 "DEA1X3A331JA2B" H 7400 4800 60  0001 C CNN "Manufacturer Part Number"
F 6 "490-4158-ND" H 7400 4800 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 330PF 1KV 5% RADIAL" H 7400 4800 60  0001 C CNN "Description"
	1    7400 4800
	1    0    0    -1  
$EndComp
$Comp
L R R18
U 1 1 53D3D971
P 7400 5650
F 0 "R18" V 7480 5650 40  0000 C CNN
F 1 "20R" V 7407 5651 40  0000 C CNN
F 2 "_ohmite:tww-series-5w" V 7330 5650 30  0001 C CNN
F 3 "" H 7400 5650 30  0000 C CNN
F 4 "Ohmite" V 7400 5650 60  0001 C CNN "Manufacturer"
F 5 "TWW5J20RE" V 7400 5650 60  0001 C CNN "Manufacturer Part Number"
F 6 "TWW5J20RE-ND" V 7400 5650 60  0001 C CNN "Digi-Key Part Number"
F 7 "RES 20 OHM 5W 5% RADIAL" V 7400 5650 60  0001 C CNN "Description"
	1    7400 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	8200 5300 8200 5200
Wire Wire Line
	8200 5200 8700 5200
Wire Wire Line
	8700 5100 8700 5300
Connection ~ 8700 5200
Wire Wire Line
	8400 5150 8400 5250
Connection ~ 8400 5200
Wire Wire Line
	8400 4650 8400 4550
Wire Wire Line
	8400 4600 8700 4600
Wire Wire Line
	8700 4600 8700 4700
Connection ~ 8400 4600
Wire Wire Line
	8400 5750 8400 5850
Wire Wire Line
	8400 5800 8700 5800
Wire Wire Line
	8700 5800 8700 5700
Connection ~ 8400 5800
Wire Wire Line
	8200 6300 9400 6300
Wire Wire Line
	8400 6300 8400 6250
Wire Wire Line
	9100 6300 9100 5400
Connection ~ 8400 6300
Wire Wire Line
	9100 5000 9100 4100
Wire Wire Line
	8200 4100 9400 4100
Wire Wire Line
	8400 4150 8400 4100
Connection ~ 8400 4100
Wire Wire Line
	9500 5100 9400 5100
Wire Wire Line
	9400 5100 9400 4100
Connection ~ 9100 4100
Wire Wire Line
	9500 5300 9400 5300
Wire Wire Line
	9400 5300 9400 6300
Connection ~ 9100 6300
Wire Wire Line
	7200 4100 7600 4100
Wire Wire Line
	7400 4100 7400 4600
Wire Wire Line
	7400 5000 7400 5400
Wire Wire Line
	7400 5900 7400 6300
Wire Wire Line
	7200 6300 7600 6300
$Comp
L GND #PWR066
U 1 1 53D3D998
P 6900 5300
F 0 "#PWR066" H 6900 5300 30  0001 C CNN
F 1 "GND" H 6900 5230 30  0001 C CNN
F 2 "" H 6900 5300 60  0000 C CNN
F 3 "" H 6900 5300 60  0000 C CNN
	1    6900 5300
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR067
U 1 1 53D3D99E
P 6900 4200
F 0 "#PWR067" H 6900 4200 30  0001 C CNN
F 1 "GND" H 6900 4130 30  0001 C CNN
F 2 "" H 6900 4200 60  0000 C CNN
F 3 "" H 6900 4200 60  0000 C CNN
	1    6900 4200
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR068
U 1 1 53D3D9A4
P 6300 5100
F 0 "#PWR068" H 6300 5100 30  0001 C CNN
F 1 "GND" H 6300 5030 30  0001 C CNN
F 2 "" H 6300 5100 60  0000 C CNN
F 3 "" H 6300 5100 60  0000 C CNN
	1    6300 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR069
U 1 1 53D3D9AA
P 6300 6200
F 0 "#PWR069" H 6300 6200 30  0001 C CNN
F 1 "GND" H 6300 6130 30  0001 C CNN
F 2 "" H 6300 6200 60  0000 C CNN
F 3 "" H 6300 6200 60  0000 C CNN
	1    6300 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4600 7200 4100
Connection ~ 7400 4100
Connection ~ 6300 4600
Wire Wire Line
	7200 5800 7200 6300
Connection ~ 7400 6300
Connection ~ 6300 5800
Wire Wire Line
	6100 6100 6300 6100
Wire Wire Line
	6300 6100 6300 6200
Wire Wire Line
	6100 4300 6300 4300
Wire Wire Line
	6700 4300 6900 4300
Wire Wire Line
	6900 4300 6900 4200
Wire Wire Line
	6700 5400 6900 5400
Wire Wire Line
	6900 5400 6900 5300
Wire Wire Line
	6300 5400 6100 5400
$Comp
L CP1 C56
U 1 1 53D3D9C2
P 6500 5400
F 0 "C56" V 6600 5150 50  0000 L CNN
F 1 "1000u" V 6400 5000 50  0000 L CNN
F 2 "_nichicon:he_series_7dot5" H 6500 5400 60  0001 C CNN
F 3 "" H 6500 5400 60  0000 C CNN
F 4 "Nichicon" V 6500 5400 60  0001 C CNN "Manufacturer"
F 5 "UHE1J102MHD" V 6500 5400 60  0001 C CNN "Manufacturer Part Number"
F 6 "493-1654-ND" V 6500 5400 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP ALUM 1000UF 63V 20% RADIAL" V 6500 5400 60  0001 C CNN "Description"
	1    6500 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 5000 6300 5000
Wire Wire Line
	6300 5000 6300 5100
$Comp
L CP1 C55
U 1 1 53D3D9CE
P 6500 4300
F 0 "C55" V 6600 4050 50  0000 L CNN
F 1 "1000u" V 6400 3900 50  0000 L CNN
F 2 "_nichicon:he_series_7dot5" H 6500 4300 60  0001 C CNN
F 3 "" H 6500 4300 60  0000 C CNN
F 4 "Nichicon" V 6500 4300 60  0001 C CNN "Manufacturer"
F 5 "UHE1J102MHD" V 6500 4300 60  0001 C CNN "Manufacturer Part Number"
F 6 "493-1654-ND" V 6500 4300 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP ALUM 1000UF 63V 20% RADIAL" V 6500 4300 60  0001 C CNN "Description"
	1    6500 4300
	0    -1   -1   0   
$EndComp
$Comp
L INDUCTOR L18
U 1 1 53D3D9D8
P 7900 6300
F 0 "L18" V 7850 6300 40  0000 C CNN
F 1 "22uH" V 8000 6300 40  0000 C CNN
F 2 "_wurth:7447017" H 7900 6300 60  0001 C CNN
F 3 "" H 7900 6300 60  0000 C CNN
F 4 "Wurth Electronics Inc" V 7900 6300 60  0001 C CNN "Manufacturer"
F 5 "7447017" V 7900 6300 60  0001 C CNN "Manufacturer Part Number"
F 6 "732-1418-ND" V 7900 6300 60  0001 C CNN "Digi-Key Part Number"
F 7 "CHOKE TOROID 22UH 5.0A VERT" V 7900 6300 60  0001 C CNN "Description"
	1    7900 6300
	0    -1   -1   0   
$EndComp
$Comp
L C C65
U 1 1 53D3D9E2
P 8700 4900
F 0 "C65" H 8700 5000 40  0000 L CNN
F 1 "100n" H 8706 4815 40  0000 L CNN
F 2 "_epcos:b32620" H 8738 4750 30  0001 C CNN
F 3 "" H 8700 4900 60  0000 C CNN
F 4 "EPCOS Inc" H 8700 4900 60  0001 C CNN "Manufacturer"
F 5 "B32620A3104J" H 8700 4900 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1294-ND" H 8700 4900 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.1UF 250VDC RADIAL" H 8700 4900 60  0001 C CNN "Description"
	1    8700 4900
	1    0    0    -1  
$EndComp
$Comp
L C C66
U 1 1 53D3D9EC
P 8700 5500
F 0 "C66" H 8700 5600 40  0000 L CNN
F 1 "100n" H 8706 5415 40  0000 L CNN
F 2 "_epcos:b32620" H 8738 5350 30  0001 C CNN
F 3 "" H 8700 5500 60  0000 C CNN
F 4 "EPCOS Inc" H 8700 5500 60  0001 C CNN "Manufacturer"
F 5 "B32620A3104J" H 8700 5500 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1294-ND" H 8700 5500 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.1UF 250VDC RADIAL" H 8700 5500 60  0001 C CNN "Description"
	1    8700 5500
	1    0    0    -1  
$EndComp
$Comp
L C C62
U 1 1 53D3D9F6
P 8400 6050
F 0 "C62" H 8400 6150 40  0000 L CNN
F 1 "100n" H 8406 5965 40  0000 L CNN
F 2 "_epcos:b32620" H 8438 5900 30  0001 C CNN
F 3 "" H 8400 6050 60  0000 C CNN
F 4 "EPCOS Inc" H 8400 6050 60  0001 C CNN "Manufacturer"
F 5 "B32620A3104J" H 8400 6050 60  0001 C CNN "Manufacturer Part Number"
F 6 "495-1294-ND" H 8400 6050 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP FILM 0.1UF 250VDC RADIAL" H 8400 6050 60  0001 C CNN "Description"
	1    8400 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 6600 5100 6700
Text HLabel 3700 1900 0    60   Input ~ 0
IN2A
Text HLabel 3700 2000 0    60   Input ~ 0
IN2B
Wire Wire Line
	4100 1700 3700 1700
Wire Wire Line
	3700 1800 4100 1800
Wire Wire Line
	4100 1900 3700 1900
Wire Wire Line
	3700 2000 4100 2000
Text HLabel 6400 1400 2    60   Input ~ 0
PWR
Wire Wire Line
	6400 1400 6200 1400
Wire Wire Line
	6200 1400 6200 5400
Connection ~ 6200 1800
Connection ~ 6200 2900
$Comp
L DS18B20Z U14
U 1 1 53D7D6F7
P 4400 6800
F 0 "U14" H 4400 7075 60  0000 C CNN
F 1 "DS18B20Z" V 4775 6800 40  0000 C CNN
F 2 "smd_packages:so8n" H 4275 6625 60  0001 C CNN
F 3 "" H 4400 6800 60  0000 C CNN
F 4 "Maxim Integrated" H 4400 6800 60  0001 C CNN "Manufacturer"
F 5 "DS18B20Z+" H 4400 6800 60  0001 C CNN "Manufacturer Part Number"
F 6 "DS18B20Z+-ND" H 4400 6800 60  0001 C CNN "Digi-Key Part Number"
F 7 "IC THERM MICROLAN PROG-RES 8SOIC" H 4400 6800 60  0001 C CNN "Description"
	1    4400 6800
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR070
U 1 1 53D7DA1A
P 3900 6600
F 0 "#PWR070" H 3900 6560 30  0001 C CNN
F 1 "+3.3V" H 3900 6710 30  0000 C CNN
F 2 "" H 3900 6600 60  0000 C CNN
F 3 "" H 3900 6600 60  0000 C CNN
	1    3900 6600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR071
U 1 1 53D7DBB1
P 3900 7000
F 0 "#PWR071" H 3900 7000 30  0001 C CNN
F 1 "GND" H 3900 6930 30  0001 C CNN
F 2 "" H 3900 7000 60  0000 C CNN
F 3 "" H 3900 7000 60  0000 C CNN
	1    3900 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 6700 3900 6700
Wire Wire Line
	3900 6700 3900 6600
Wire Wire Line
	4100 6900 3900 6900
Wire Wire Line
	3900 6900 3900 7000
Text HLabel 3700 6800 0    60   BiDi ~ 0
DQa
Wire Wire Line
	3700 6800 4100 6800
Text Notes 4600 6500 0    30   ~ 0
near to...
Connection ~ 6200 4300
Connection ~ 6200 5400
$Comp
L CONN_2 P14
U 1 1 53F4A368
P 9850 2700
F 0 "P14" V 9800 2700 40  0000 C CNN
F 1 "CONN_2" V 9900 2700 40  0000 C CNN
F 2 "connect:bornier2" H 9850 2700 60  0001 C CNN
F 3 "" H 9850 2700 60  0000 C CNN
F 4 "Weidmuller" V 9850 2700 60  0001 C CNN "Manufacturer"
F 5 "1760510000" V 9850 2700 60  0001 C CNN "Manufacturer Part Number"
F 6 "281-1882-ND" V 9850 2700 60  0001 C CNN "Digi-Key Part Number"
F 7 "TERM BLOCK PCB 2POS 5.08MM BLACK" V 9850 2700 60  0001 C CNN "Description"
	1    9850 2700
	1    0    0    1   
$EndComp
$Comp
L CONN_2 P15
U 1 1 53F4A808
P 9850 5200
F 0 "P15" V 9800 5200 40  0000 C CNN
F 1 "CONN_2" V 9900 5200 40  0000 C CNN
F 2 "connect:bornier2" H 9850 5200 60  0001 C CNN
F 3 "" H 9850 5200 60  0000 C CNN
F 4 "Weidmuller" V 9850 5200 60  0001 C CNN "Manufacturer"
F 5 "1760510000" V 9850 5200 60  0001 C CNN "Manufacturer Part Number"
F 6 "281-1882-ND" V 9850 5200 60  0001 C CNN "Digi-Key Part Number"
F 7 "TERM BLOCK PCB 2POS 5.08MM BLACK" V 9850 5200 60  0001 C CNN "Description"
	1    9850 5200
	1    0    0    1   
$EndComp
$EndSCHEMATC
