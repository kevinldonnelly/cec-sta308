EESchema Schematic File Version 2
LIBS:_ams-reg
LIBS:_cirrus-logic
LIBS:conn
LIBS:device
LIBS:_maxim
LIBS:power
LIBS:_ptc
LIBS:regul
LIBS:_st-audio
LIBS:_switchcraft
LIBS:_ti-reg
LIBS:_wolfson
LIBS:_wurth
LIBS:_toshiba
LIBS:sta308a-ev-rev-c-cache
EELAYER 24 0
EELAYER END
$Descr USLegal 14000 8500
encoding utf-8
Sheet 5 7
Title "24-bit, 192kHz 8-Channel DAC"
Date ""
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4600 5300 0    60   Input ~ 0
BCLK
Text HLabel 4600 5400 0    60   Input ~ 0
LRCLK
Text HLabel 4600 5600 0    60   Input ~ 0
DIN1
Text HLabel 4600 5700 0    60   Input ~ 0
DIN2
Text HLabel 4600 5800 0    60   Input ~ 0
DIN3
Text HLabel 4600 5900 0    60   Input ~ 0
DIN4
Text HLabel 7100 3200 2    60   Output ~ 0
AGND
Text HLabel 6600 3400 2    60   Output ~ 0
OUT1L
Text HLabel 6600 3600 2    60   Output ~ 0
OUT1R
Text HLabel 6600 3800 2    60   Output ~ 0
OUT2L
Text HLabel 6600 4000 2    60   Output ~ 0
OUT2R
Text HLabel 6600 4200 2    60   Output ~ 0
OUT3L
Text HLabel 6600 4400 2    60   Output ~ 0
OUT3R
Text HLabel 6600 4600 2    60   Output ~ 0
OUT4L
Text HLabel 6600 4800 2    60   Output ~ 0
OUT4R
$Comp
L GND #PWR085
U 1 1 53DBCC54
P 4300 3000
F 0 "#PWR085" H 4300 3000 30  0001 C CNN
F 1 "GND" H 4300 2930 30  0001 C CNN
F 2 "" H 4300 3000 100 0000 C CNN
F 3 "" H 4300 3000 100 0000 C CNN
	1    4300 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2900 4300 2900
Wire Wire Line
	4300 2800 4300 3000
Connection ~ 4300 2900
Wire Wire Line
	4600 2800 4600 2900
Connection ~ 4600 2900
Wire Wire Line
	4300 2200 4300 2400
Wire Wire Line
	4300 2300 4800 2300
Wire Wire Line
	4600 2400 4600 2300
Connection ~ 4600 2300
Wire Wire Line
	6400 3100 6500 3100
Wire Wire Line
	6500 3100 6500 3000
Wire Wire Line
	6400 3000 8300 3000
Wire Wire Line
	6700 3000 6700 2900
Connection ~ 6500 3000
Wire Wire Line
	7000 2900 7000 3200
Connection ~ 6700 3000
$Comp
L R R4
U 1 1 53DBD088
P 7650 2400
F 0 "R4" V 7730 2400 40  0000 C CNN
F 1 "33" V 7657 2401 40  0000 C CNN
F 2 "smd_packages:r_1206" V 7580 2400 30  0001 C CNN
F 3 "" H 7650 2400 30  0000 C CNN
F 4 "Yageo" V 7650 2400 60  0001 C CNN "Manufacturer"
F 5 "RC1206JR-0733RL" V 7650 2400 60  0001 C CNN "Manufacturer Part Number"
F 6 "311-33ERCT-ND" V 7650 2400 60  0001 C CNN "Digi-Key Part Number"
F 7 "RES 33 OHM 1/4W 5% 1206 SMD" V 7650 2400 60  0001 C CNN "Description"
	1    7650 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	8000 3000 8000 2900
Connection ~ 7000 3000
Wire Wire Line
	7300 2900 7300 3000
Connection ~ 7300 3000
Wire Wire Line
	7650 3000 7650 3100
Connection ~ 7650 3000
Wire Wire Line
	8000 2300 8000 2500
Wire Wire Line
	7900 2400 8300 2400
Wire Wire Line
	6400 2400 7400 2400
Wire Wire Line
	7300 2400 7300 2500
Wire Wire Line
	7000 2500 7000 2300
Wire Wire Line
	6400 2300 8000 2300
Wire Wire Line
	6700 2400 6700 2500
Connection ~ 7300 2400
Connection ~ 6700 2400
Connection ~ 8000 2400
Connection ~ 7000 2300
$Comp
L AGND #PWR086
U 1 1 53DBD30F
P 7650 3100
F 0 "#PWR086" H 7650 3100 40  0001 C CNN
F 1 "AGND" H 7650 3030 50  0000 C CNN
F 2 "" H 7650 3100 100 0000 C CNN
F 3 "" H 7650 3100 100 0000 C CNN
	1    7650 3100
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR087
U 1 1 53DBD752
P 6700 5700
F 0 "#PWR087" H 6700 5700 40  0001 C CNN
F 1 "AGND" H 6700 5630 50  0000 C CNN
F 2 "" H 6700 5700 100 0000 C CNN
F 3 "" H 6700 5700 100 0000 C CNN
	1    6700 5700
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR088
U 1 1 53DBD794
P 7100 5700
F 0 "#PWR088" H 7100 5700 40  0001 C CNN
F 1 "AGND" H 7100 5630 50  0000 C CNN
F 2 "" H 7100 5700 100 0000 C CNN
F 3 "" H 7100 5700 100 0000 C CNN
	1    7100 5700
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR089
U 1 1 53DBD7D6
P 7100 6500
F 0 "#PWR089" H 7100 6500 40  0001 C CNN
F 1 "AGND" H 7100 6430 50  0000 C CNN
F 2 "" H 7100 6500 100 0000 C CNN
F 3 "" H 7100 6500 100 0000 C CNN
	1    7100 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 5900 7100 5900
Wire Wire Line
	7100 5900 7100 6000
Wire Wire Line
	6400 5100 7100 5100
Wire Wire Line
	7100 5100 7100 5200
Wire Wire Line
	6700 5200 6700 5100
Connection ~ 6700 5100
Wire Wire Line
	7100 6400 7100 6500
Wire Wire Line
	6600 4800 6400 4800
Wire Wire Line
	6400 4600 6600 4600
Wire Wire Line
	6600 4400 6400 4400
Wire Wire Line
	6400 4200 6600 4200
Wire Wire Line
	6600 4000 6400 4000
Wire Wire Line
	6400 3800 6600 3800
Wire Wire Line
	6600 3600 6400 3600
Wire Wire Line
	6400 3400 6600 3400
NoConn ~ 4800 3900
NoConn ~ 4800 3800
NoConn ~ 4800 3700
NoConn ~ 4800 3600
NoConn ~ 4800 3500
Wire Wire Line
	4600 5300 4800 5300
Wire Wire Line
	4800 5400 4600 5400
Wire Wire Line
	4600 5600 4800 5600
Wire Wire Line
	4800 5700 4600 5700
Wire Wire Line
	4600 5800 4800 5800
Wire Wire Line
	4800 5900 4600 5900
$Comp
L WM8768EDS U5
U 1 1 53DBEE9C
P 5600 4100
F 0 "U5" H 5600 6100 100 0000 C CNN
F 1 "WM8768EDS" H 5600 2100 100 0000 C CNN
F 2 "smd_packages:ssop-28" H 5600 4800 100 0001 C CNN
F 3 "" H 5600 4800 100 0000 C CNN
F 4 "Wolfson Microelectronics" H 5600 4100 60  0001 C CNN "Manufacturer"
F 5 "WM8768GEDS" H 5600 4100 60  0001 C CNN "Manufacturer Part Number"
	1    5600 4100
	1    0    0    -1  
$EndComp
$Comp
L C C26
U 1 1 53DBFDC2
P 4600 2600
F 0 "C26" H 4500 2500 40  0000 L CNN
F 1 "100n" H 4400 2700 40  0000 L CNN
F 2 "smd_packages:c_0603" H 4638 2450 30  0001 C CNN
F 3 "" H 4600 2600 60  0000 C CNN
F 4 "Yageo" V 4600 2600 60  0001 C CNN "Manufacturer"
F 5 "CC0603ZRY5V9BB104" V 4600 2600 60  0001 C CNN "Manufacturer Part Number"
F 6 "311-1343-1-ND" V 4600 2600 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 50V Y5V 0603" V 4600 2600 60  0001 C CNN "Description"
	1    4600 2600
	-1   0    0    1   
$EndComp
$Comp
L C C27
U 1 1 53DBFF2C
P 6700 2700
F 0 "C27" H 6600 2600 40  0000 L CNN
F 1 "100n" H 6500 2800 40  0000 L CNN
F 2 "smd_packages:c_0603" H 6738 2550 30  0001 C CNN
F 3 "" H 6700 2700 60  0000 C CNN
F 4 "Yageo" V 6700 2700 60  0001 C CNN "Manufacturer"
F 5 "CC0603ZRY5V9BB104" V 6700 2700 60  0001 C CNN "Manufacturer Part Number"
F 6 "311-1343-1-ND" V 6700 2700 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 50V Y5V 0603" V 6700 2700 60  0001 C CNN "Description"
	1    6700 2700
	-1   0    0    1   
$EndComp
$Comp
L C C29
U 1 1 53DBFFAE
P 7000 2700
F 0 "C29" H 6900 2600 40  0000 L CNN
F 1 "100n" H 6800 2800 40  0000 L CNN
F 2 "smd_packages:c_0603" H 7038 2550 30  0001 C CNN
F 3 "" H 7000 2700 60  0000 C CNN
F 4 "Yageo" V 7000 2700 60  0001 C CNN "Manufacturer"
F 5 "CC0603ZRY5V9BB104" V 7000 2700 60  0001 C CNN "Manufacturer Part Number"
F 6 "311-1343-1-ND" V 7000 2700 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 50V Y5V 0603" V 7000 2700 60  0001 C CNN "Description"
	1    7000 2700
	-1   0    0    1   
$EndComp
$Comp
L C C28
U 1 1 53DC014C
P 6700 5400
F 0 "C28" H 6600 5300 40  0000 L CNN
F 1 "100n" H 6500 5500 40  0000 L CNN
F 2 "smd_packages:c_0603" H 6738 5250 30  0001 C CNN
F 3 "" H 6700 5400 60  0000 C CNN
F 4 "Yageo" V 6700 5400 60  0001 C CNN "Manufacturer"
F 5 "CC0603ZRY5V9BB104" V 6700 5400 60  0001 C CNN "Manufacturer Part Number"
F 6 "311-1343-1-ND" V 6700 5400 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 50V Y5V 0603" V 6700 5400 60  0001 C CNN "Description"
	1    6700 5400
	-1   0    0    1   
$EndComp
$Comp
L C C31
U 1 1 53DC0262
P 7100 6200
F 0 "C31" H 7000 6100 40  0000 L CNN
F 1 "100n" H 6900 6300 40  0000 L CNN
F 2 "smd_packages:c_0603" H 7138 6050 30  0001 C CNN
F 3 "" H 7100 6200 60  0000 C CNN
F 4 "Yageo" V 7100 6200 60  0001 C CNN "Manufacturer"
F 5 "CC0603ZRY5V9BB104" V 7100 6200 60  0001 C CNN "Manufacturer Part Number"
F 6 "311-1343-1-ND" V 7100 6200 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 50V Y5V 0603" V 7100 6200 60  0001 C CNN "Description"
	1    7100 6200
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 5600 6700 5700
Wire Wire Line
	7100 5700 7100 5600
$Comp
L CP1 C32
U 1 1 53DC0FE8
P 7300 2700
F 0 "C32" H 7350 2800 50  0000 L CNN
F 1 "10u" H 7350 2600 50  0000 L CNN
F 2 "smd_packages:tantalc_sizeb_eia-3528" H 7300 2700 60  0001 C CNN
F 3 "" H 7300 2700 60  0000 C CNN
F 4 "Kemet" H 7300 2700 60  0001 C CNN "Manufacturer"
F 5 "T491B106K006AT" H 7300 2700 60  0001 C CNN "Manufacturer Part Number"
F 6 "399-3704-1-ND" H 7300 2700 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP TANT 10UF 6.3V 10% 1411" H 7300 2700 60  0001 C CNN "Description"
	1    7300 2700
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR_SMALL L7
U 1 1 53DC0FF3
P 8550 2400
F 0 "L7" H 8550 2500 50  0000 C CNN
F 1 "600ohm @ 100MHz" H 8550 2350 50  0000 C CNN
F 2 "smd_packages:l_1206" H 8550 2400 60  0001 C CNN
F 3 "" H 8550 2400 60  0000 C CNN
F 4 "Wurth Electronics Inc" H 8550 2400 60  0001 C CNN "Manufacturer"
F 5 "7427920415" H 8550 2400 60  0001 C CNN "Manufacturer Part Number"
F 6 "732-1605-1-ND" H 8550 2400 60  0001 C CNN "Digi-Key Part Number"
F 7 "FERRITE BEAD 600 OHM .50A 0805" H 8550 2400 60  0001 C CNN "Description"
	1    8550 2400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR090
U 1 1 53DC1001
P 9100 2300
F 0 "#PWR090" H 9100 2260 30  0001 C CNN
F 1 "+3.3V" H 9100 2410 30  0000 C CNN
F 2 "" H 9100 2300 60  0000 C CNN
F 3 "" H 9100 2300 60  0000 C CNN
	1    9100 2300
	1    0    0    -1  
$EndComp
$Comp
L C C70
U 1 1 53DC1017
P 9100 2700
F 0 "C70" H 9000 2600 40  0000 L CNN
F 1 "100n" H 8900 2800 40  0000 L CNN
F 2 "smd_packages:c_0603" H 9138 2550 30  0001 C CNN
F 3 "" H 9100 2700 60  0000 C CNN
F 4 "Yageo" V 9100 2700 60  0001 C CNN "Manufacturer"
F 5 "CC0603ZRY5V9BB104" V 9100 2700 60  0001 C CNN "Manufacturer Part Number"
F 6 "311-1343-1-ND" V 9100 2700 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP CER 0.1UF 50V Y5V 0603" V 9100 2700 60  0001 C CNN "Description"
	1    9100 2700
	-1   0    0    1   
$EndComp
$Comp
L INDUCTOR_SMALL L8
U 1 1 53DC1022
P 8550 3000
F 0 "L8" H 8550 3100 50  0000 C CNN
F 1 "600ohm @ 100MHz" H 8550 2950 50  0000 C CNN
F 2 "smd_packages:l_1206" H 8550 3000 60  0001 C CNN
F 3 "" H 8550 3000 60  0000 C CNN
F 4 "Wurth Electronics Inc" H 8550 3000 60  0001 C CNN "Manufacturer"
F 5 "7427920415" H 8550 3000 60  0001 C CNN "Manufacturer Part Number"
F 6 "732-1605-1-ND" H 8550 3000 60  0001 C CNN "Digi-Key Part Number"
F 7 "FERRITE BEAD 600 OHM .50A 0805" H 8550 3000 60  0001 C CNN "Description"
	1    8550 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 2400 9100 2400
Wire Wire Line
	9100 2300 9100 2500
Connection ~ 9100 2400
Connection ~ 9100 3000
Wire Wire Line
	9100 2900 9100 3100
Wire Wire Line
	8800 3000 9100 3000
$Comp
L GND #PWR091
U 1 1 53DC0FFA
P 9100 3100
F 0 "#PWR091" H 9100 3100 30  0001 C CNN
F 1 "GND" H 9100 3030 30  0001 C CNN
F 2 "" H 9100 3100 60  0000 C CNN
F 3 "" H 9100 3100 60  0000 C CNN
	1    9100 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3200 7100 3200
Connection ~ 8000 3000
$Comp
L CP1 C25
U 1 1 53DC1B8D
P 4300 2600
F 0 "C25" H 4350 2700 50  0000 L CNN
F 1 "10u" H 4350 2500 50  0000 L CNN
F 2 "smd_packages:tantalc_sizeb_eia-3528" H 4300 2600 60  0001 C CNN
F 3 "" H 4300 2600 60  0000 C CNN
F 4 "Kemet" H 4300 2600 60  0001 C CNN "Manufacturer"
F 5 "T491B106K006AT" H 4300 2600 60  0001 C CNN "Manufacturer Part Number"
F 6 "399-3704-1-ND" H 4300 2600 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP TANT 10UF 6.3V 10% 1411" H 4300 2600 60  0001 C CNN "Description"
	1    4300 2600
	1    0    0    -1  
$EndComp
$Comp
L CP1 C69
U 1 1 53DC2042
P 8000 2700
F 0 "C69" H 8050 2800 50  0000 L CNN
F 1 "10u" H 8050 2600 50  0000 L CNN
F 2 "smd_packages:tantalc_sizeb_eia-3528" H 8000 2700 60  0001 C CNN
F 3 "" H 8000 2700 60  0000 C CNN
F 4 "Kemet" H 8000 2700 60  0001 C CNN "Manufacturer"
F 5 "T491B107K006AT" H 8000 2700 60  0001 C CNN "Manufacturer Part Number"
F 6 "399-8301-1-ND" H 8000 2700 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP TANT 100UF 6.3V 10% 1411" H 8000 2700 60  0001 C CNN "Description"
	1    8000 2700
	1    0    0    -1  
$EndComp
$Comp
L CP1 C30
U 1 1 53DC2215
P 7100 5400
F 0 "C30" H 7150 5500 50  0000 L CNN
F 1 "10u" H 7150 5300 50  0000 L CNN
F 2 "smd_packages:tantalc_sizeb_eia-3528" H 7100 5400 60  0001 C CNN
F 3 "" H 7100 5400 60  0000 C CNN
F 4 "Kemet" H 7100 5400 60  0001 C CNN "Manufacturer"
F 5 "T491B106K006AT" H 7100 5400 60  0001 C CNN "Manufacturer Part Number"
F 6 "399-3704-1-ND" H 7100 5400 60  0001 C CNN "Digi-Key Part Number"
F 7 "CAP TANT 10UF 6.3V 10% 1411" H 7100 5400 60  0001 C CNN "Description"
	1    7100 5400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR092
U 1 1 53DEF71E
P 4300 2200
F 0 "#PWR092" H 4300 2160 30  0001 C CNN
F 1 "+3.3V" H 4300 2310 30  0000 C CNN
F 2 "" H 4300 2200 60  0000 C CNN
F 3 "" H 4300 2200 60  0000 C CNN
	1    4300 2200
	1    0    0    -1  
$EndComp
Connection ~ 4300 2300
Text HLabel 4600 5200 0    60   Input ~ 0
XTI
Wire Wire Line
	4600 5200 4800 5200
$EndSCHEMATC
