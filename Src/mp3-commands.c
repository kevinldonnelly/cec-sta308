/*
 * mp3-commands.c
 *
 *  Created on: 10/04/2019
 *      Author: kevin
 */

#include "main.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include "cmsis_os.h"
#include "fatfs.h"

#include "player.h"
#include <stdio.h>
extern SAI_HandleTypeDef hsai_BlockB1;
extern osSemaphoreId myBinarySemSAIHandle;
//MP3FrameInfo mp3FrameInfo;
HMP3Decoder hMP3Decoder;
static uint32_t BlockReadAdd=60000000;


//#define MP3_SIZE       2*1152

//#define DECODED_MP3_FRAME_SIZE	MAX_NGRAN * MAX_NCHAN * MAX_NSAMP
//#define OUT_BUFFER_SIZE			2 * DECODED_MP3_FRAME_SIZE
#define DEFAULT_AUDIO_IN_FREQ 48000


#define READ_BUFFER_SIZE	2 * MAINBUF_SIZE + 216 //2*1940+216=4096=4K
static int bytes_left=0;
static int offset;
static int bytes_left_before_decoding=0;
static unsigned char *read_pointer;
static unsigned char read_buffer[READ_BUFFER_SIZE];

extern unsigned char out_buf_state;
static int underflows;
static short out_buffer[OUT_BUFFER_SIZE];

static int result;
static xTaskHandle xPlayMP3Task = NULL;
static FIL xFile,xwFile;
static UINT br, bw;
//uint16_t   mp3_data[MP3_SIZE];
//static int16_t audio_buffer0[4096];
//static int16_t audio_buffer1[4096];
static portBASE_TYPE prvMP3PlayFile( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvMP3DecFile( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

static void prvPlayMP3Task( void *pvParameters );

/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvmp3playFileCommandDefinition =
{
		( const int8_t * const ) "mp3play", /* The command string to type. */
		( const int8_t * const ) "mp3play:<filename>\r\n",
		prvMP3PlayFile, /* The function to run. */
		1 /* No parameters are expected. */
};


const CLI_Command_Definition_t prvmp3decFileCommandDefinition =
{
		( const int8_t * const ) "mp3dec", /* The command string to type. */
		( const int8_t * const ) "mp3dec:<filename>\r\n",
		prvMP3DecFile, /* The function to run. */
		1 /* No parameters are expected. */
};

static portBASE_TYPE prvMP3DecFile( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{

	uint8_t *pcParameter1;
	char wav[16];
	//	FRESULT fr=FR_INVALID_PARAMETER;
	BaseType_t r;
	BaseType_t xParameter1StringLength;

	/* Obtain the name of the source file, and the length of its name, from
					    the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					1,
					/* Store the parameter string length. */
					&xParameter1StringLength
			);


	if(xPlayMP3Task!=NULL){

		vTaskDelay(100);
		f_close(&xFile);


		vTaskDelete(xPlayMP3Task);
		xPlayMP3Task=NULL;
		return pdFALSE;
	}


	//    char * pch;
	memset(wav,0,16);
	//    pch=strrchr(pcParameter1,'.');
	strncpy(wav,pcParameter1,xParameter1StringLength-3);
	strcat(wav,"wav");



	if( f_open(&xFile, pcParameter1,FA_OPEN_EXISTING | FA_READ )==0 && f_open(&xwFile, wav, FA_WRITE | FA_CREATE_ALWAYS )==0)
	{

		r=xTaskCreate( prvPlayMP3Task,( const int8_t * const ) "MP3Task",
				configUART_COMMAND_CONSOLE_STACK_SIZE,
				NULL,
				configUART_COMMAND_CONSOLE_TASK_PRIORITY,
				&xPlayMP3Task );

		sprintf(pcWriteBuffer,"mp3 decode task started %s %s xTaskCreate=%d fptr=%d\n",pcParameter1,wav,r,xwFile.flag);

	}
	else
	{
		sprintf(pcWriteBuffer,"openfailed  %s %s\n",pcParameter1,wav);
	}
	return pdFALSE;
}

static portBASE_TYPE prvMP3PlayFile( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	int8_t *pcParameter1;
	FRESULT fr=FR_INVALID_PARAMETER;
	BaseType_t r;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

	/* Obtain the name of the source file, and the length of its name, from
				    the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					1,
					/* Store the parameter string length. */
					&xParameter1StringLength
			);


	if(xPlayMP3Task!=NULL){
		HAL_SAI_Abort(&hsai_BlockB1);
		vTaskDelay(100);
		f_close(&xFile);
		bytes_left = 0;
		read_pointer = NULL;
		MP3FreeDecoder(hMP3Decoder);
		vTaskDelete(xPlayMP3Task);
		xPlayMP3Task=NULL;
		return pdFALSE;
	}

	BlockReadAdd=atol(pcParameter1);

	if(BlockReadAdd>50000000)
	{
		xFile.flag=0;
		r=xTaskCreate( prvPlayMP3Task,				/* The task that implements the command console. */
				( const int8_t * const ) "MP3Task",		/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
				configUART_COMMAND_CONSOLE_STACK_SIZE,	/* The size of the stack allocated to the task. */
				NULL,									/* The parameter is not used, so NULL is passed. */
				configUART_COMMAND_CONSOLE_TASK_PRIORITY+1,/* The priority allocated to the task. */
				&xPlayMP3Task );
		sprintf(pcWriteBuffer,"sector read task started %s\n",pcParameter1);
		return pdFALSE;
	}


	if(f_open(&xFile, pcParameter1,FA_OPEN_EXISTING | FA_READ )==0)
	{
		//ulHighFrequencyTimerTicks=0;
		r=xTaskCreate( prvPlayMP3Task,				/* The task that implements the command console. */
				( const int8_t * const ) "MP3Task",		/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
				configUART_COMMAND_CONSOLE_STACK_SIZE,	/* The size of the stack allocated to the task. */
				NULL,									/* The parameter is not used, so NULL is passed. */
				configUART_COMMAND_CONSOLE_TASK_PRIORITY+1,/* The priority allocated to the task. */
				&xPlayMP3Task );

		sprintf(pcWriteBuffer,"mp3 play task started %s\n",pcParameter1);

	}
	else
	{
		sprintf(pcWriteBuffer,"openfailed  %s\n",pcParameter1);
	}
	return pdFALSE;
}


static int try_get_header(unsigned char *buff, MP3FrameInfo *fi)
{
	int ret, offs1, offs = 0;

	while (1)
	{
		offs1 = MP3FindSyncWord(buff + offs, 2048 - offs);
		if (offs1 < 0) return -2;
		offs += offs1;
		if (2048 - offs < 4) return -3;

		// printf("trying header %08x\n", *(int *)(buff + offs));

		ret = MP3GetNextFrameInfo(hMP3Decoder, fi, buff + offs);
		if (ret == 0 && fi->bitrate != 0) break;
		offs++;
	}

	return ret;
}

int mp3_get_bitrate(FILE *f, int len)
{
	unsigned char buff[2048];
	MP3FrameInfo fi;
	int ret;

	memset(buff, 0, 2048);

	if (!hMP3Decoder) hMP3Decoder = MP3InitDecoder();

	fseek(f, 0, SEEK_SET);
	ret = fread(buff, 1, 2048, f);
	fseek(f, 0, SEEK_SET);
	if (ret <= 0) return -1;

	ret = try_get_header(buff, &fi);
	if (ret != 0 || fi.bitrate == 0) {
		// try to read somewhere around the middle
		fseek(f, len>>1, SEEK_SET);
		fread(buff, 1, 2048, f);
		fseek(f, 0, SEEK_SET);
		ret = try_get_header(buff, &fi);
	}
	if (ret != 0) return ret;

	// printf("bitrate: %i\n", fi.bitrate / 1000);

	return fi.bitrate / 1000;
}

int refill_inbuffer(FIL *in_file)
{
	unsigned int bytes_read;
	unsigned int bytes_to_read;
	FRESULT result;

	if (bytes_left > 0) {
		//copy remaining data to beginning of buffer
		memcpy(read_buffer, read_pointer, bytes_left);
	}

	bytes_to_read = READ_BUFFER_SIZE - bytes_left;

	if(in_file->flag!=0)
	{
		result = f_read(in_file, (BYTE *)read_buffer + bytes_left, bytes_to_read, &bytes_read);
	}
	else
	{
		result=SD_read(0,(BYTE *)read_buffer + bytes_left,BlockReadAdd,4);
		if(result==0)bytes_read=bytes_to_read;
		else bytes_read=0;
		BlockReadAdd=BlockReadAdd+4;
		if(BlockReadAdd%500==0)printf("blkr=%d bl=%d\n",BlockReadAdd,bytes_left);
	}

	if(result != FR_OK)
		return READ_ERROR;

	if (bytes_read == bytes_to_read){
		read_pointer = read_buffer;
		offset = 0;

		if(in_file->flag!=0)bytes_left = READ_BUFFER_SIZE;
		else bytes_left = 4*512 + bytes_left;

		return 0;
	}
	else{
		return result;
	}

	return 0;  //should never reach this point
}

volatile int mp3_proccess(FIL *mp3_file)
{
	MP3FrameInfo mp3FrameInfo;
	FRESULT fr;
	//while(!out_buf_state);

	if (read_pointer == NULL) {
		if(refill_inbuffer(mp3_file) != 0){
			return END_OF_FILE;
		}
	}

	offset = MP3FindSyncWord(read_pointer, bytes_left);
	while(offset < 0) { //Sync not found,
		if(refill_inbuffer(mp3_file) != 0)
			return END_OF_FILE;
		if(bytes_left > 0){		//decrement bytes left and increment read_pointer
			bytes_left -= 1;
			read_pointer += 1;
		}
		offset = MP3FindSyncWord(read_pointer, bytes_left);
	}
	read_pointer += offset;
	bytes_left -= offset;
	bytes_left_before_decoding = bytes_left;

	if (MP3GetNextFrameInfo(hMP3Decoder, &mp3FrameInfo, read_pointer) == 0 &&
			mp3FrameInfo.nChans == 2 &&
			mp3FrameInfo.version == 0) {
		//	printf("Found a frame at offset %x\n", read_pointer);
	} else {
		// advance data pointer
		// TODO: handle bytes_left == 0
		if(bytes_left > 0){	//Frame not found, decrement bytes left and increment read_pointer
			bytes_left -= 1;
			read_pointer += 1;
		}
		return 0;
	}

	if (bytes_left < MAINBUF_SIZE) {
		if(refill_inbuffer(mp3_file) != 0)
			return END_OF_FILE;
	}

	if(out_buf_state == (LO_EMPTY | HI_EMPTY)){
		underflows++;
		//DAC_DMA_disable();
	}

	if(xwFile.flag != 0)
	{

		result = MP3Decode(hMP3Decoder, &read_pointer, &bytes_left, out_buffer, 0);
		fr = f_write(&xwFile, out_buffer, DECODED_MP3_FRAME_SIZE*2, &bw);
		//	printf("fr=%d result=%d bytes=%d\n",fr,result,mp3FrameInfo.outputSamps);

	}
	else
	{
		osSemaphoreWait(myBinarySemSAIHandle, osWaitForever);

		if(out_buf_state & (LO_EMPTY)){


			result = MP3Decode(hMP3Decoder, &read_pointer, &bytes_left, out_buffer, 0);
			out_buf_state &= ~(LO_EMPTY);


		}else if(out_buf_state & (HI_EMPTY)){


			result = MP3Decode(hMP3Decoder, &read_pointer, &bytes_left, &out_buffer[DECODED_MP3_FRAME_SIZE], 0);
			out_buf_state &= ~(HI_EMPTY);

		}

	}

	//DAC_DMA_enable();
	if(result != ERR_MP3_NONE){
		switch(result){
		case ERR_MP3_INDATA_UNDERFLOW:
			bytes_left = 0;
			if(refill_inbuffer(mp3_file) != 0)
				return END_OF_FILE;
			break;
		case ERR_MP3_MAINDATA_UNDERFLOW:
			//do nothing, next call to MP3Decode will provide more data
			break;
		default:
			return 0; //skip this frame if error
			//return END_OF_FILE; //skip this file if error
		}
	}
	return 0;
}

WAVE_FormatTypeDef WaveFormat;
uint8_t pHeaderBuff[44];
extern uint32_t WavProcess_EncInit(uint32_t Freq, uint8_t* pHeader);

static void prvPlayMP3Task( void *pvParameters )
{

	int result;
	hMP3Decoder = MP3InitDecoder();
	if(hMP3Decoder==0)
	{
		printf("MP3 player could not allocate buffers\n");
		return;
	}
	read_pointer=NULL;
	bytes_left=0;
	HAL_SAI_Transmit_DMA(&hsai_BlockB1,  (uint8_t *)out_buffer,OUT_BUFFER_SIZE);
	//	HAL_SAI_Transmit_IT(&hsai_BlockB1,  (uint8_t *)out_buffer,OUT_BUFFER_SIZE);

	//	WavProcess_EncInit(DEFAULT_AUDIO_IN_FREQ, pHeaderBuff);

	while(1)
	{

		result=mp3_proccess(&xFile);

		if(result!=0){
			HAL_SAI_Abort(&hsai_BlockB1);
			printf("MP3 Task ending=%d\n",result);
			vTaskDelay(100);
			f_close(&xFile);
			if(xwFile.flag != 0){
				f_close(&xwFile);
				xwFile.flag=0;
			}
			bytes_left = 0;
			read_pointer = NULL;
			MP3FreeDecoder(hMP3Decoder);

			xPlayMP3Task=NULL;
			vTaskDelete(NULL);
			vTaskDelay(1000);

		}

		//	HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);

		vTaskDelay(1);

	}




}



