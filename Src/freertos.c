/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "tim.h"
#include "tm_stm32_delay.h"
#include "tm_stm32_onewire.h"
#include "smr_glo.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern TM_OneWire_t OW;
extern float temp;
extern smr_dynamic_param_t smr_dynamic_param;

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osMessageQId myQueueUARTHandle;
osMutexId myMutexUART2Handle;
osMutexId myMutexUART1Handle;
osSemaphoreId myBinarySemUART2Handle;
osSemaphoreId myBinarySemI2CHandle;
osSemaphoreId myBinarySemFlashHandle;
osSemaphoreId myBinarySemCECHandle;
osSemaphoreId myBinarySemSAIHandle;
osSemaphoreId myBinarySemSPDIFHandle;
osSemaphoreId myBinarySemUART3Handle;
osSemaphoreId myBinarySemUART2IdleHandle;
osSemaphoreId myBinarySemSPDIFSyncHandle;
osSemaphoreId myBinarySemI2CRecHandle;
osSemaphoreId myBinarySemTim7Handle;
osSemaphoreId myBinarySemTim5Handle;
osSemaphoreId myBinarySemSPIWifiHandle;
osSemaphoreId myBinarySemSDIOHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
extern IWDG_HandleTypeDef hiwdg;
volatile unsigned long ulHighFrequencyTimerTicks = 0;
/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 1 */

/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
void configureTimerForRunTimeStats(void) {
	HAL_TIM_Base_Start_IT(&htim14);
}

unsigned long getRunTimeCounterValue(void) {
	return ulHighFrequencyTimerTicks;
}
/* USER CODE END 1 */

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName) {
	printf("App stack overflowed %s\n", pcTaskName);

}

/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
void vApplicationMallocFailedHook(void) {
	printf("Alloc failed");
}

/* USER CODE END 5 */

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer,
		StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize) {
	*ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
	*ppxIdleTaskStackBuffer = &xIdleStack[0];
	*pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
	/* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* definition and creation of myMutexUART2 */
  osMutexDef(myMutexUART2);
  myMutexUART2Handle = osMutexCreate(osMutex(myMutexUART2));

  /* definition and creation of myMutexUART1 */
  osMutexDef(myMutexUART1);
  myMutexUART1Handle = osMutexCreate(osMutex(myMutexUART1));

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of myBinarySemUART2 */
  osSemaphoreDef(myBinarySemUART2);
  myBinarySemUART2Handle = osSemaphoreCreate(osSemaphore(myBinarySemUART2), 1);

  /* definition and creation of myBinarySemI2C */
  osSemaphoreDef(myBinarySemI2C);
  myBinarySemI2CHandle = osSemaphoreCreate(osSemaphore(myBinarySemI2C), 1);

  /* definition and creation of myBinarySemFlash */
  osSemaphoreDef(myBinarySemFlash);
  myBinarySemFlashHandle = osSemaphoreCreate(osSemaphore(myBinarySemFlash), 1);

  /* definition and creation of myBinarySemCEC */
  osSemaphoreDef(myBinarySemCEC);
  myBinarySemCECHandle = osSemaphoreCreate(osSemaphore(myBinarySemCEC), 1);

  /* definition and creation of myBinarySemSAI */
  osSemaphoreDef(myBinarySemSAI);
  myBinarySemSAIHandle = osSemaphoreCreate(osSemaphore(myBinarySemSAI), 1);

  /* definition and creation of myBinarySemSPDIF */
  osSemaphoreDef(myBinarySemSPDIF);
  myBinarySemSPDIFHandle = osSemaphoreCreate(osSemaphore(myBinarySemSPDIF), 1);

  /* definition and creation of myBinarySemUART3 */
  osSemaphoreDef(myBinarySemUART3);
  myBinarySemUART3Handle = osSemaphoreCreate(osSemaphore(myBinarySemUART3), 1);

  /* definition and creation of myBinarySemUART2Idle */
  osSemaphoreDef(myBinarySemUART2Idle);
  myBinarySemUART2IdleHandle = osSemaphoreCreate(osSemaphore(myBinarySemUART2Idle), 1);

  /* definition and creation of myBinarySemSPDIFSync */
  osSemaphoreDef(myBinarySemSPDIFSync);
  myBinarySemSPDIFSyncHandle = osSemaphoreCreate(osSemaphore(myBinarySemSPDIFSync), 1);

  /* definition and creation of myBinarySemI2CRec */
  osSemaphoreDef(myBinarySemI2CRec);
  myBinarySemI2CRecHandle = osSemaphoreCreate(osSemaphore(myBinarySemI2CRec), 1);

  /* definition and creation of myBinarySemTim7 */
  osSemaphoreDef(myBinarySemTim7);
  myBinarySemTim7Handle = osSemaphoreCreate(osSemaphore(myBinarySemTim7), 1);

  /* definition and creation of myBinarySemTim5 */
  osSemaphoreDef(myBinarySemTim5);
  myBinarySemTim5Handle = osSemaphoreCreate(osSemaphore(myBinarySemTim5), 1);

  /* definition and creation of myBinarySemSPIWifi */
  osSemaphoreDef(myBinarySemSPIWifi);
  myBinarySemSPIWifiHandle = osSemaphoreCreate(osSemaphore(myBinarySemSPIWifi), 1);

  /* definition and creation of myBinarySemSDIO */
  osSemaphoreDef(myBinarySemSDIO);
  myBinarySemSDIOHandle = osSemaphoreCreate(osSemaphore(myBinarySemSDIO), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of myQueueUART */
  osMessageQDef(myQueueUART, 16, uint16_t);
  myQueueUARTHandle = osMessageCreate(osMessageQ(myQueueUART), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 512);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
 * @brief  Function implementing the defaultTask thread.
 * @param  argument: Not used xTaskHandle xOneWireTask;
 * @retval None
 */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN StartDefaultTask */
	MX_FATFS_Init();
	setTextSize(2);
	i2c_detect();
	int i = 0;
	int ii = 0;
	int iii = 0;
	float old_temp = 0;
	int r;
	int d;
	int32_t Left_rms, Right_rms;
	int32_t Left_rms_line, Right_rms_line, Old_Left_rms_line,
	Old_Right_rms_line;
	char buffer[8];
	char left_buffer[8];
	char right_buffer[8];
	vUARTCommandConsoleStart();


	vOneWireTaskStart();
	vTaskDelay(200);
	//vSPDIFTaskStart();
	// HAL_DAC_Start(&hdac,DAC_CHANNEL_2);
	HAL_TIM_Base_Start(&htim13);

	//  HAL_TIM_Base_Start_IT(&htim5);
	HAL_TIM_PWM_Start(&htim13, TIM_CHANNEL_1);
	//  HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_2);
	LL_GPIO_TogglePin(GPIOC,LL_GPIO_PIN_10);
	//  ITM->LAR = 0xC5ACCE55;
	/* Infinite loop */
	for (;;) {
		HAL_IWDG_Refresh(&hiwdg);

		//  LL_GPIO_SetOutputPin(LCD_DC_GPIO_Port,LCD_DC_Pin);
		//  TaskDelay(400);

		//  LL_GPIO_ResetOutputPin(LCD_DC_GPIO_Port,LCD_DC_Pin);
		vTaskDelay(100);

		Left_rms = (float) smr_dynamic_param.mean_level_left * 0.25;
		Right_rms = (float) smr_dynamic_param.mean_level_right * 0.25;
		Left_rms_line = -2 * Left_rms;
		Right_rms_line = -2 * Right_rms;

		if (Left_rms_line != Old_Left_rms_line) {
			drawLine(90, Old_Left_rms_line, 120, Old_Left_rms_line, 0);
			drawtext(left_buffer, 50, Old_Left_rms_line, 0x000);
			drawLine(90, Left_rms_line, 120, Left_rms_line, 0x07E0);
			sprintf(left_buffer, "%ld", Left_rms);
			drawtext(left_buffer, 50, Left_rms_line, 0xF800);
		}

		if (Right_rms_line != Old_Right_rms_line) {
			drawLine(150, Old_Right_rms_line, 180, Old_Right_rms_line, 0);
			drawtext(right_buffer, 125, Old_Right_rms_line, 0x0000);
			drawLine(150, Right_rms_line, 180, Right_rms_line, 0x07E0);
			sprintf(right_buffer, "%ld", Right_rms);
			drawtext(right_buffer, 125, Right_rms_line, 0xF800);
		}

		// else if(Left_rms_line > Old_Left_rms_line)
		// {
		// 			  drawLine(50,Left_rms_line,80,Left_rms_line,0x07E0);
		// }

		Old_Left_rms_line = Left_rms_line;
		Old_Right_rms_line = Right_rms_line;

		if (old_temp != temp) {
			old_temp = temp;
			drawtext(buffer, 50, 200, 0x000);
			r = temp;
			d = (temp - r) * 10;
			sprintf(buffer, "%d.%d C", r, d);
			drawtext(buffer, 50, 200, 0xF800);
			//  strcat(buffer,"\n");
			//  printf(buffer);
		}

		//  drawLine(10,i,40,i,0);
		//  if(i++ > 240)i=0;
		//  drawLine(10,i,40,i,0x07E0);

		//  HAL_DAC_SetValue(&hdac,DAC_CHANNEL_2,DAC_ALIGN_8B_R,c++);

	}
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
