/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "adc.h"
#include "crc.h"
#include "dma.h"
#include "fatfs.h"
#include "hdmi_cec.h"
#include "i2c.h"
#include "iwdg.h"
#include "rtc.h"
#include "sai.h"
#include "sdio.h"
#include "spdifrx.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdarg.h>
#include "tm_stm32_gpio.h"
#include "tm_stm32_onewire.h"


/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
uint16_t c=0;
//char printbuf[48];

extern int InitLCD(uint16_t color);
extern void Intpcm512x();
ssize_t usart_write_dma(USART_TypeDef *usart, DMA_TypeDef *dma, uint32_t stream, const char* buf __attribute__((unused)), size_t nbyte __attribute__((unused)));

extern TM_OneWire_t OW;


extern osMutexId myMutexUART2Handle;
extern osMutexId myMutexUART1Handle;
uint32_t dma1_stream6_transmitting = 0;
uint32_t dma1_stream7_transmitting = 0;
extern void initialise_monitor_handles(void);
volatile unsigned long ulHighFrequencyTimerTicks;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint32_t GetCurrentMilli(void)
{
	return HAL_GetTick();
}

//#ifdef __GNUC__
//#ifdef 0
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
// #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
//#else
//  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
//#endif /* __GNUC__ */
/**
 * @brief  Retargets the C library printf function to the USART.
 * @param  None
 * @retval None
 */

/*
int _write(int fd,const char *ptr, size_t len)
{

ssize_t ret = len;

int DataIdx;
int timeout=0;

for(DataIdx=0; DataIdx<len; DataIdx++)
{
timeout=0;
//while (!LL_USART_IsActiveFlag_TXE(USART2))
//{
//we can put some timeout here
//	if (timeout++ > 10000)return len;
//}

//LL_USART_TransmitData8(USART2,*ptr++);

ITM_SendChar( *ptr++ );


}




return ret;
}
 */
/*
PUTCHAR_PROTOTYPE
{

//	osSemaphoreWait( myMutexPrintHandle, 1000 );
	 LL_USART_TransmitData8(USART2, ch);
	 while (!LL_USART_IsActiveFlag_TXE(USART2));

	 while (!LL_USART_IsActiveFlag_TC(USART2));
	 return ch;
}
 */

ssize_t usart_write_dma(USART_TypeDef *usart, DMA_TypeDef *dma, uint32_t stream, const char* buf __attribute__((unused)), size_t nbyte __attribute__((unused)))
{


	dma1_stream6_transmitting = 1;

	LL_DMA_DisableStream(dma, stream);
	LL_USART_DisableDMAReq_TX(usart);

	LL_DMA_EnableIT_TC(dma, stream);
	//	LL_DMA_EnableIT_HT(dma, stream);
	LL_DMA_ConfigAddresses(dma, stream, (uint32_t)buf, LL_USART_DMA_GetRegAddr(usart), LL_DMA_GetDataTransferDirection(dma, stream));
	LL_DMA_SetDataLength(dma, stream, nbyte);
	LL_USART_EnableDMAReq_TX(usart);
	LL_DMA_EnableStream(dma, stream);

	return nbyte;
}

ssize_t usart_write_dma_str_rtos_UART2(char* buf)
{
	osMutexWait( myMutexUART2Handle, osWaitForever );
	dma1_stream6_transmitting=1;
	ssize_t len= strlen(buf);
	usart_write_dma(USART2,DMA1, LL_DMA_STREAM_6,buf,len);
	while (dma1_stream6_transmitting!=0 &&  !LL_DMA_IsActiveFlag_TC6(DMA1) &&  LL_DMA_GetDataLength(DMA1, LL_DMA_STREAM_6) && dma1_stream6_transmitting++ < 1000000)
	{
		osThreadYield();
	}
	osMutexRelease( myMutexUART2Handle );
	//	osSemaphoreWait(myBinarySemUART2Handle,1);
	return len;
}

ssize_t usart_write_dma_str_rtos_UART1(char* buf,int len)
{
	osMutexWait( myMutexUART1Handle, osWaitForever );
	dma1_stream7_transmitting=1;
	usart_write_dma(USART1,DMA2, LL_DMA_STREAM_7,buf,len);
	while (dma1_stream7_transmitting!=0 &&  !LL_DMA_IsActiveFlag_TC6(DMA2) &&  LL_DMA_GetDataLength(DMA2, LL_DMA_STREAM_7) && dma1_stream7_transmitting++ < 1000000)
	{
		osThreadYield();
	}
	osMutexRelease( myMutexUART1Handle );
	//	osSemaphoreWait(myBinarySemUART2Handle,1);
	return len;
}

ssize_t usart_write_dma_str(char* buf)
{
	ssize_t len= strlen(buf);
	dma1_stream6_transmitting=1;
	usart_write_dma(USART2,DMA1, LL_DMA_STREAM_6,buf,len);
	while (dma1_stream6_transmitting!=0 &&  !LL_DMA_IsActiveFlag_TC6(DMA1) &&  LL_DMA_GetDataLength(DMA1, LL_DMA_STREAM_6) && dma1_stream6_transmitting++ < 1000000);

	return len;
}

ssize_t usart_write_dma_str_len_uart2(char* buf, int len)
{
	//ssize_t len= strlen(buf);
	usart_write_dma(USART2,DMA1, LL_DMA_STREAM_6,buf,len);
	while (dma1_stream6_transmitting!=0 &&  !LL_DMA_IsActiveFlag_TC6(DMA1) &&  LL_DMA_GetDataLength(DMA1, LL_DMA_STREAM_6) && dma1_stream6_transmitting++ < 1000000);

	return len;
}

ssize_t usart_write_dma_str_len_uart1(char* buf, int len)
{
	//ssize_t len= strlen(buf);
	usart_write_dma(USART1,DMA2, LL_DMA_STREAM_7,buf,len);
	while (dma1_stream7_transmitting!=0 &&  !LL_DMA_IsActiveFlag_TC6(DMA2) &&  LL_DMA_GetDataLength(DMA2, LL_DMA_STREAM_7) && dma1_stream7_transmitting++ < 1000000);

	return len;
}

int _write(int file, char *ptr, int len)
{
	int l=usart_write_dma_str_rtos_UART1(ptr,len);
	return l;
}


void i2c_detect(void)
{
	uint8_t devices = 0u;
	printf("Searching for I2C devices on the bus...\n");
	// usart_write_dma_str(printbuf);


	/* Values outside 0x03 and 0x77 are invalid. */
	for (uint8_t i = 0x03u; i < 0x78u; i++)
	{
		uint8_t address = i << 1u ;
		/* In case there is a positive feedback, print it out. */
		if (HAL_OK == HAL_I2C_IsDeviceReady(&hi2c3, address, 3u, 10u))
		{
			printf("Device found: 0x%02X\n", address);
			//  usart_write_dma_str(printbuf);

			devices++;
		}
	}
	/* Feedback of the total number of devices. */
	if (0u == devices)
	{
		printf("No device found.\n");
	}
	else
	{
		printf("Total found devices: %d\n", devices);
		//	  usart_write_dma_str(printbuf);

	}
}
/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */
	initialise_monitor_handles(); /* initialize handles include rdimon in library */
	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_SPDIFRX_Init();
	MX_HDMI_CEC_Init();
	MX_ADC1_Init();
	MX_SAI1_Init();
	MX_SDIO_SD_Init();
	MX_SPI1_Init();
	MX_USART2_UART_Init();
	MX_IWDG_Init();
	MX_I2C3_Init();
	MX_TIM5_Init();
	MX_SPI2_Init();
	MX_TIM7_Init();
	MX_TIM11_Init();
	MX_TIM13_Init();
	MX_RTC_Init();
	MX_TIM1_Init();
	MX_TIM2_Init();
	MX_USART1_UART_Init();
	MX_TIM14_Init();
	MX_CRC_Init();
	MX_TIM3_Init();
	//MX_FATFS_Init();
	MX_TIM4_Init();
	/* USER CODE BEGIN 2 */
	setbuf(stdout, NULL);
	setvbuf(stdout, NULL, _IOLBF,0);
	InitLCD(0);
	vRegisterCLICommands();

	// HAL_TIM_Base_Start_IT(&htim7);
	/* USER CODE END 2 */

	/* Call init function for freertos objects (in freertos.c) */
	MX_FREERTOS_Init();
	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */
	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_4);
	while(LL_FLASH_GetLatency()!= LL_FLASH_LATENCY_4)
	{
	}
	LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE2);
	LL_PWR_DisableOverDriveMode();
	LL_RCC_HSE_Enable();

	/* Wait till HSE is ready */
	while(LL_RCC_HSE_IsReady() != 1)
	{

	}
	LL_RCC_HSI_SetCalibTrimming(16);
	LL_RCC_HSI_Enable();

	/* Wait till HSI is ready */
	while(LL_RCC_HSI_IsReady() != 1)
	{

	}
	LL_RCC_LSI_Enable();

	/* Wait till LSI is ready */
	while(LL_RCC_LSI_IsReady() != 1)
	{

	}
	LL_PWR_EnableBkUpAccess();
	LL_RCC_ForceBackupDomainReset();
	LL_RCC_ReleaseBackupDomainReset();
	LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSI);
	LL_RCC_EnableRTC();
	LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_8, 250, LL_RCC_PLLR_DIV_3);
	LL_RCC_PLL_ConfigDomain_48M(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_8, 250, LL_RCC_PLLQ_DIV_8);
	LL_RCC_PLLSAI_ConfigDomain_SAI(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLSAIM_DIV_12, 397, LL_RCC_PLLSAIQ_DIV_9, LL_RCC_PLLSAIDIVQ_DIV_1);
	LL_RCC_SetSAIClockSource(LL_RCC_SAI1_CLKSOURCE_PLLSAI);
	LL_RCC_PLL_ConfigDomain_SPDIFRX(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_8, 250, LL_RCC_PLLR_DIV_3);
	LL_RCC_SetSPDIFRXClockSource(LL_RCC_SPDIFRX1_CLKSOURCE_PLL);
	LL_RCC_PLL_Enable();

	/* Wait till PLL is ready */
	while(LL_RCC_PLL_IsReady() != 1)
	{

	}
	LL_RCC_PLLSAI_Enable();

	/* Wait till PLL is ready */
	while(LL_RCC_PLLSAI_IsReady() != 1)
	{

	}
	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_4);
	LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_2);
	LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLLR);

	/* Wait till System clock is ready */
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLLR)
	{

	}
	LL_SetSystemCoreClock(128000000);

	/* Update the time base */
	if (HAL_InitTick (TICK_INT_PRIORITY) != HAL_OK)
	{
		Error_Handler();
	}
	LL_RCC_SetSDIOClockSource(LL_RCC_SDIO_CLKSOURCE_PLL48CLK);
	LL_RCC_SetCK48MClockSource(LL_RCC_CK48M_CLKSOURCE_PLL);
	LL_RCC_SetUSBClockSource(LL_RCC_USB_CLKSOURCE_PLL);
	LL_RCC_SetCECClockSource(LL_RCC_CEC_CLKSOURCE_HSI_DIV488);
	LL_RCC_SetTIMPrescaler(LL_RCC_TIM_PRESCALER_TWICE);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  Period elapsed callback in non blocking mode
 * @note   This function is called  when TIM10 interrupt took place, inside
 * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
 * a global variable "uwTick" used as application time base.
 * @param  htim : TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	/* USER CODE BEGIN Callback 0 */

	/* USER CODE END Callback 0 */
	if (htim->Instance == TIM10) {
		HAL_IncTick();
	}
	/* USER CODE BEGIN Callback 1 */
	else if(htim->Instance==TIM7)
	{
		//  ONEWIRE_HIGH(&OW);

		HAL_TIM_Base_Stop_IT(htim);
		OW.Counter = __HAL_TIM_GET_COUNTER(htim);
		OW.GPIO_Pin_value=TM_GPIO_GetInputPinValue(OW.GPIOx, OW.GPIO_Pin);
		if(OW.Task_suspeneded)osThreadResume(OW.xOneWireTask);
		else osSignalSet (OW.xOneWireTask, 0x01);

	}
	else if(htim->Instance==TIM5)
	{

		OW.Counter = __HAL_TIM_GET_COUNTER(htim);


		if(OW.Counter<500)
		{
			if(OW.Counter>OW.MaxCounter)OW.MaxCounter=OW.Counter;
			OW.GPIO_Pin_value=TM_GPIO_GetInputPinValue(OW.GPIOx, OW.GPIO_Pin);
			HAL_TIM_PWM_Stop(htim,TIM_CHANNEL_2);
			HAL_TIM_Base_Stop_IT(htim);
			if(OW.Task_suspeneded)osThreadResume(OW.xOneWireTask);
			else  osSignalSet(OW.xOneWireTask, 0x02);
		}

	}
	else if(htim->Instance==TIM14)
	{
		ulHighFrequencyTimerTicks++;
		//  if(ulHighFrequencyTimerTicks%100)printf("hft = %d\n",ulHighFrequencyTimerTicks);
	}

	/* USER CODE END Callback 1 */
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
