/**	
 * |----------------------------------------------------------------------
 * | Copyright (c) 2016 Tilen Majerle
 * |  
 * | Permission is hereby granted, free of charge, to any person
 * | obtaining a copy of this software and associated documentation
 * | files (the "Software"), to deal in the Software without restriction,
 * | including without limitation the rights to use, copy, modify, merge,
 * | publish, distribute, sublicense, and/or sell copies of the Software, 
 * | and to permit persons to whom the Software is furnished to do so, 
 * | subject to the following conditions:
 * | 
 * | The above copyright notice and this permission notice shall be
 * | included in all copies or substantial portions of the Software.
 * | 
 * | THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * | EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * | OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * | AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * | HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * | WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * | FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * | OTHER DEALINGS IN THE SOFTWARE.
 * |----------------------------------------------------------------------
 */
#include "tm_stm32_onewire.h"
#include "main.h"
#include "tim.h"
#include "cmsis_os.h"




extern TM_OneWire_t OW;
void TaskDelay(int delay_micro);

void pwm_setvalue(uint16_t period,uint16_t value)
{
    TIM_OC_InitTypeDef sConfigOC;

    __HAL_TIM_SET_COUNTER(&htim5,0);
    __HAL_TIM_SET_AUTORELOAD(&htim5,period);

    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = value;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    HAL_TIM_MspPostInit(&htim5);
 if( HAL_TIM_PWM_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_2)==HAL_OK)
 {

	htim5.Instance->CCMR1 &= ~TIM_CCMR1_OC2PE;
    HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_2);
    HAL_TIM_Base_Start_IT(&htim5);
 }

}

void Task_OneWireWriteBit(TM_OneWire_t* OneWireStruct,uint8_t mbit)
{

	OneWireStruct->Task_suspeneded=0;

	if(mbit)
	{

		pwm_setvalue(60*64,59*64);

	}
	else
	{

		pwm_setvalue(60*64,24*64);

	}

	osSignalWait(0x02, osWaitForever);
//	TaskDelay(40);
}

uint8_t Task_OneWireReadBit(TM_OneWire_t* OneWireStruct)
{
	uint8_t i;
	OneWireStruct->Task_suspeneded=0;
	htim5.Instance->CR1 &= ~TIM_OPMODE_SINGLE;//single pulse
	pwm_setvalue(1000,900);
	osSignalWait(0x02, osWaitForever);
	i=OneWireStruct->GPIO_Pin_value;
	TaskDelay(60);
	return i;
}


void TaskDelay(int delay_micro)
{

	if(delay_micro<11)
	{
	//	taskENTER_CRITICAL();
		Delay(delay_micro);
		OW.GPIO_Pin_value=TM_GPIO_GetInputPinValue(OW.GPIOx, OW.GPIO_Pin);
		//taskEXIT_CRITICAL();
	}
	else
	{
		__HAL_TIM_SET_AUTORELOAD(&htim7,delay_micro*64);
		__HAL_TIM_SET_COUNTER(&htim7,0);
		HAL_TIM_Base_Start_IT(&htim7);
		if(delay_micro>10)
		{
			OW.Task_suspeneded=0;
			osSignalWait(0x01,1000);
		}
		else
		{
			OW.Task_suspeneded=1;
			osThreadSuspend(NULL);
		}
	}

}

//void PulseOneWire(int length_micro)//timer 5




void TM_OneWire_Init(TM_OneWire_t* OneWireStruct, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
	/* Initialize delay if it was not already */
	TM_DELAY_Init();

	/* Init GPIO pin */
	TM_GPIO_Init(GPIOx, GPIO_Pin, TM_GPIO_Mode_OUT, TM_GPIO_OType_OD, TM_GPIO_PuPd_UP, TM_GPIO_Speed_Low);// kevin 21 June 2019
	
	/* Save settings */
	OneWireStruct->GPIOx = GPIOx;
	OneWireStruct->GPIO_Pin = GPIO_Pin;
	ONEWIRE_HIGH(OneWireStruct);//kevin 21 June 1019
}

uint8_t TM_OneWire_Reset(TM_OneWire_t* OneWireStruct) {
	uint8_t i;
	
	/* Line low, and wait 480us */
	ONEWIRE_LOW(OneWireStruct);
	ONEWIRE_OUTPUT(OneWireStruct);
	ONEWIRE_DELAY(480);
	
	/* Release line and wait for 70us */
//	ONEWIRE_INPUT(OneWireStruct);
	ONEWIRE_HIGH(OneWireStruct);//kevin 21 June 1019
	ONEWIRE_DELAY(70);
	
	/* Check bit value */
//	i = TM_GPIO_GetInputPinValue(OneWireStruct->GPIOx, OneWireStruct->GPIO_Pin);

	i = OneWireStruct->GPIO_Pin_value;
	
	/* Delay for 410 us */
	ONEWIRE_DELAY(410);
	
	/* Return value of presence pulse, 0 = OK, 1 = ERROR */
	return i;
}

void TM_OneWire_WriteBit(TM_OneWire_t* OneWireStruct, uint8_t bit) {
	if (bit) {
		/* Set line low */
		ONEWIRE_OUTPUT(OneWireStruct);
		ONEWIRE_LOW(OneWireStruct);
		ONEWIRE_DELAY(11);// 1-15 us
		
		/* Bit high */
	//	ONEWIRE_INPUT(OneWireStruct);
		ONEWIRE_HIGH(OneWireStruct);//kevin 21 June 1019
		
		/* Wait for 55 us and release the line */
		ONEWIRE_DELAY(60);
	//	ONEWIRE_INPUT(OneWireStruct);
	} else {
		/* Set line low */
		ONEWIRE_OUTPUT(OneWireStruct);
		ONEWIRE_LOW(OneWireStruct);
		ONEWIRE_DELAY(65);
		
		/* Bit high */
	//	ONEWIRE_INPUT(OneWireStruct);
		ONEWIRE_HIGH(OneWireStruct);//kevin 21 June 1019
		
		/* Wait for 5 us and release the line */
		ONEWIRE_DELAY(5);
		//ONEWIRE_INPUT(OneWireStruct);
	}
}

uint8_t TM_OneWire_ReadBit(TM_OneWire_t* OneWireStruct) {
	uint8_t bit = 0;
	
	OneWireStruct->GPIO_Pin_value=0;
	return Task_OneWireReadBit(OneWireStruct);

	ONEWIRE_OUTPUT(OneWireStruct);
	/* Line low */
	ONEWIRE_LOW(OneWireStruct);
//	ONEWIRE_OUTPUT(OneWireStruct);
	ONEWIRE_DELAY(3);//1-15 us
	
	/* Release line */
//	ONEWIRE_INPUT(OneWireStruct);
	ONEWIRE_HIGH(OneWireStruct);//kevin 21 June 1019
	ONEWIRE_DELAY(11);//1-60 us
	
	/* Read line value */
	if (TM_GPIO_GetInputPinValue(OneWireStruct->GPIOx, OneWireStruct->GPIO_Pin)) {
		/* Bit is HIGH */
	//	bit = 1;
		bit = OneWireStruct->GPIO_Pin_value;
	}
	
	/* Wait 50us to complete 60us period */
	ONEWIRE_DELAY(50);// > 50 us
	
	/* Return bit value */
	return bit;
}

void TM_OneWire_WriteByte(TM_OneWire_t* OneWireStruct, uint8_t byte) {
	uint8_t i = 8;
	/* Write 8 bits */
	while (i--) {
		/* LSB bit is first */
	//	TM_OneWire_WriteBit(OneWireStruct, byte & 0x01);
		Task_OneWireWriteBit(OneWireStruct, byte & 0x01);
		byte >>= 1;
	}
}

uint8_t TM_OneWire_ReadByte(TM_OneWire_t* OneWireStruct) {
	uint8_t i = 8, byte = 0;
	while (i--) {
		byte >>= 1;
		byte |= (TM_OneWire_ReadBit(OneWireStruct) << 7);
	}
	
	return byte;
}

uint8_t TM_OneWire_First(TM_OneWire_t* OneWireStruct) {
	/* Reset search values */
	TM_OneWire_ResetSearch(OneWireStruct);

	/* Start with searching */
	return TM_OneWire_Search(OneWireStruct, ONEWIRE_CMD_SEARCHROM);
}

uint8_t TM_OneWire_Next(TM_OneWire_t* OneWireStruct) {
   /* Leave the search state alone */
   return TM_OneWire_Search(OneWireStruct, ONEWIRE_CMD_SEARCHROM);
}

void TM_OneWire_ResetSearch(TM_OneWire_t* OneWireStruct) {
	/* Reset the search state */
	OneWireStruct->LastDiscrepancy = 0;
	OneWireStruct->LastDeviceFlag = 0;
	OneWireStruct->LastFamilyDiscrepancy = 0;
}

uint8_t TM_OneWire_Search(TM_OneWire_t* OneWireStruct, uint8_t command) {
	uint8_t id_bit_number;
	uint8_t last_zero, rom_byte_number, search_result;
	uint8_t id_bit, cmp_id_bit;
	uint8_t rom_byte_mask, search_direction;

	/* Initialize for search */
	id_bit_number = 1;
	last_zero = 0;
	rom_byte_number = 0;
	rom_byte_mask = 1;
	search_result = 0;

	/* Check if any devices */
	if (!OneWireStruct->LastDeviceFlag) {
		/* 1-Wire reset */
		if (TM_OneWire_Reset(OneWireStruct)) {
			/* Reset the search */
			OneWireStruct->LastDiscrepancy = 0;
			OneWireStruct->LastDeviceFlag = 0;
			OneWireStruct->LastFamilyDiscrepancy = 0;
			return 0;
		}
		vTaskDelay(1);
		/* Issue the search command */
		TM_OneWire_WriteByte(OneWireStruct, command);  

		/* Loop to do the search */
		do {
			/* Read a bit and its complement */
			//printf("readbits in 1 seconds\n");
		//	vTaskDelay(1);
			id_bit = TM_OneWire_ReadBit(OneWireStruct);
			cmp_id_bit = TM_OneWire_ReadBit(OneWireStruct);

			/* Check for no devices on 1-wire */
			if ((id_bit == 1) && (cmp_id_bit == 1)) {


				break;
			} else {
				/* All devices coupled have 0 or 1 */
				if (id_bit != cmp_id_bit) {
					/* Bit write value for search */
					search_direction = id_bit;
				} else {
					/* If this discrepancy is before the Last Discrepancy on a previous next then pick the same as last time */
					if (id_bit_number < OneWireStruct->LastDiscrepancy) {
						search_direction = ((OneWireStruct->ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
					} else {
						/* If equal to last pick 1, if not then pick 0 */
						search_direction = (id_bit_number == OneWireStruct->LastDiscrepancy);
					}
					
					/* If 0 was picked then record its position in LastZero */
					if (search_direction == 0) {
						last_zero = id_bit_number;

						/* Check for Last discrepancy in family */
						if (last_zero < 9) {
							OneWireStruct->LastFamilyDiscrepancy = last_zero;
						}
					}
				}

				/* Set or clear the bit in the ROM byte rom_byte_number with mask rom_byte_mask */
				if (search_direction == 1) {
					OneWireStruct->ROM_NO[rom_byte_number] |= rom_byte_mask;
				} else {
					OneWireStruct->ROM_NO[rom_byte_number] &= ~rom_byte_mask;
				}
				
				/* Serial number search direction write bit */
			//	TM_OneWire_WriteBit(OneWireStruct, search_direction);
				Task_OneWireWriteBit(OneWireStruct, search_direction);
				/* Increment the byte counter id_bit_number and shift the mask rom_byte_mask */
				id_bit_number++;
				rom_byte_mask <<= 1;

				/* If the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask */
				if (rom_byte_mask == 0) {
					rom_byte_number++;
					rom_byte_mask = 1;
				}
			}
		/* Loop until through all ROM bytes 0-7 */
		} while (rom_byte_number < 8);

		/* If the search was successful then */
		if (!(id_bit_number < 65)) {
			/* Search successful so set LastDiscrepancy, LastDeviceFlag, search_result */
			OneWireStruct->LastDiscrepancy = last_zero;

			/* Check for last device */
			if (OneWireStruct->LastDiscrepancy == 0) {
				OneWireStruct->LastDeviceFlag = 1;
			}

			search_result = 1;
		}
	}

	/* If no device found then reset counters so next 'search' will be like a first */
	if (!search_result || !OneWireStruct->ROM_NO[0]) {
		OneWireStruct->LastDiscrepancy = 0;
		OneWireStruct->LastDeviceFlag = 0;
		OneWireStruct->LastFamilyDiscrepancy = 0;
		search_result = 0;
	}

	return search_result;
}

int TM_OneWire_Verify(TM_OneWire_t* OneWireStruct) {
	unsigned char rom_backup[8];
	int i,rslt,ld_backup,ldf_backup,lfd_backup;

	/* Keep a backup copy of the current state */
	for (i = 0; i < 8; i++)
	rom_backup[i] = OneWireStruct->ROM_NO[i];
	ld_backup = OneWireStruct->LastDiscrepancy;
	ldf_backup = OneWireStruct->LastDeviceFlag;
	lfd_backup = OneWireStruct->LastFamilyDiscrepancy;

	/* Set search to find the same device */
	OneWireStruct->LastDiscrepancy = 64;
	OneWireStruct->LastDeviceFlag = 0;

	if (TM_OneWire_Search(OneWireStruct, ONEWIRE_CMD_SEARCHROM)) {
		/* Check if same device found */
		rslt = 1;
		for (i = 0; i < 8; i++) {
			if (rom_backup[i] != OneWireStruct->ROM_NO[i]) {
				rslt = 1;
				break;
			}
		}
	} else {
		rslt = 0;
	}

	/* Restore the search state */
	for (i = 0; i < 8; i++) {
		OneWireStruct->ROM_NO[i] = rom_backup[i];
	}
	OneWireStruct->LastDiscrepancy = ld_backup;
	OneWireStruct->LastDeviceFlag = ldf_backup;
	OneWireStruct->LastFamilyDiscrepancy = lfd_backup;

	/* Return the result of the verify */
	return rslt;
}

void TM_OneWire_TargetSetup(TM_OneWire_t* OneWireStruct, uint8_t family_code) {
   uint8_t i;

	/* Set the search state to find SearchFamily type devices */
	OneWireStruct->ROM_NO[0] = family_code;
	for (i = 1; i < 8; i++) {
		OneWireStruct->ROM_NO[i] = 0;
	}
	
	OneWireStruct->LastDiscrepancy = 64;
	OneWireStruct->LastFamilyDiscrepancy = 0;
	OneWireStruct->LastDeviceFlag = 0;
}

void TM_OneWire_FamilySkipSetup(TM_OneWire_t* OneWireStruct) {
	/* Set the Last discrepancy to last family discrepancy */
	OneWireStruct->LastDiscrepancy = OneWireStruct->LastFamilyDiscrepancy;
	OneWireStruct->LastFamilyDiscrepancy = 0;

	/* Check for end of list */
	if (OneWireStruct->LastDiscrepancy == 0) {
		OneWireStruct->LastDeviceFlag = 1;
	}
}

uint8_t TM_OneWire_GetROM(TM_OneWire_t* OneWireStruct, uint8_t index) {
	return OneWireStruct->ROM_NO[index];
}

void TM_OneWire_Select(TM_OneWire_t* OneWireStruct, uint8_t* addr) {
	uint8_t i;
	TM_OneWire_WriteByte(OneWireStruct, ONEWIRE_CMD_MATCHROM);
	
	for (i = 0; i < 8; i++) {
		TM_OneWire_WriteByte(OneWireStruct, *(addr + i));
	}
}

void TM_OneWire_SelectWithPointer(TM_OneWire_t* OneWireStruct, uint8_t *ROM) {
	uint8_t i;
	TM_OneWire_WriteByte(OneWireStruct, ONEWIRE_CMD_MATCHROM);
	
	for (i = 0; i < 8; i++) {
		TM_OneWire_WriteByte(OneWireStruct, *(ROM + i));
	}	
}

void TM_OneWire_GetFullROM(TM_OneWire_t* OneWireStruct, uint8_t *firstIndex) {
	uint8_t i;
	for (i = 0; i < 8; i++) {
		*(firstIndex + i) = OneWireStruct->ROM_NO[i];
	}
}

uint8_t TM_OneWire_CRC8(uint8_t *addr, uint8_t len) {
	uint8_t crc = 0, inbyte, i, mix;
	
	while (len--) {
		inbyte = *addr++;
		for (i = 8; i; i--) {
			mix = (crc ^ inbyte) & 0x01;
			crc >>= 1;
			if (mix) {
				crc ^= 0x8C;
			}
			inbyte >>= 1;
		}
	}
	
	/* Return calculated CRC */
	return crc;
}
