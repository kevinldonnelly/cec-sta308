/*
 * pcm512x_commands.c

 *
 *  Created on: 8/08/2019
 *      Author: kevin
 */
#include "i2c.h"
#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
#include "cmsis_os.h"

static portBASE_TYPE  prvSetVolume( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

const CLI_Command_Definition_t prvVolumeCommandDefinition =
{
	( const int8_t * const ) "SetVolume", /* The command string to type. */
	( const int8_t * const ) "SetVolume Value r\n",
	prvSetVolume, /* The function to run. */
	1 /* No parameters are expected. */
};

static portBASE_TYPE  prvSetVolume( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	int8_t *pcParameter1;

				BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

				    /* Obtain the name of the source file, and the length of its name, from
				    the command string. The name of the source file is the first parameter. */
				    pcParameter1 = FreeRTOS_CLIGetParameter
				                        (
				                          /* The command string itself. */
				                          pcCommandString,
				                          /* Return the first parameter. */
				                          1,
				                          /* Store the parameter string length. */
				                          &xParameter1StringLength
				                        );


				    /* Terminate both file names. */
				  pcParameter1[ xParameter1StringLength ] = 0x00;
				  int  v =  strtol(pcParameter1);



				  sprintf( ( char * ) pcWriteBuffer, "Volume %d\n",v);
				  return pdFALSE;
}


void Intpcm512x()
{
	uint8_t data;
	data=0x08;
	HAL_I2C_Mem_Write(&hi2c3,0x9B,0x08,I2C_MEMADD_SIZE_8BIT,&data,1,1000);
	HAL_I2C_Mem_Write(&hi2c3,0x9B,0x25,I2C_MEMADD_SIZE_8BIT,&data,1,1000);
	data=0x10;
	HAL_I2C_Mem_Write(&hi2c3,0x9B,0x0D,I2C_MEMADD_SIZE_8BIT,&data,1,1000);
	data=0x03;
	HAL_I2C_Mem_Write(&hi2c3,0x9B,0x53,I2C_MEMADD_SIZE_8BIT,&data,1,1000);

}

