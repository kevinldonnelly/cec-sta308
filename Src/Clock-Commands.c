/*
 * Clock-Commands.c

 *
 *  Created on: Mar 26, 2019
 *      Author: kevin
 */
#include "main.h"
/* FreeRTOS includes. */
#include "FreeRTOS.h"

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* FreeRTOS+CLI includes. */

#include "FreeRTOS_CLI.h"

#include "cmsis_os.h"

static portBASE_TYPE prvHSITrim( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

static portBASE_TYPE prvSAIClk( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );


/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvHSITrimCommandDefinition =
{
		( const int8_t * const ) "hsi-trim", /* The command string to type. */
		( const int8_t * const ) "hsi-trim value\r\n",
		prvHSITrim, /* The function to run. */
		1 /* No parameters are expected. */
};


/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvSAIClkCommandDefinition =
{
		( const int8_t * const ) "sai-clk", /* The command string to type. */
		( const int8_t * const ) "sai-clk value\r\n",
		prvSAIClk, /* The function to run. */
		1 /* No parameters are expected. */
};

static portBASE_TYPE prvSAIClk( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	int8_t *pcParameter1;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

	/* Obtain the name of the source file, and the length of its name, from
			    the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					1,
					/* Store the parameter string length. */
					&xParameter1StringLength
			);


	int8_t  ret=(uint8_t)strtol(pcParameter1,NULL,10);
	LL_RCC_PLLSAI_Disable();

	if(ret==1)
	{
		LL_RCC_PLLSAI_ConfigDomain_SAI(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLSAIM_DIV_12, 397, LL_RCC_PLLSAIQ_DIV_9, LL_RCC_PLLSAIDIVQ_DIV_1);
	}
	else if(ret==0)
	{
		LL_RCC_PLLSAI_ConfigDomain_SAI(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLSAIM_DIV_6, 72, LL_RCC_PLLSAIQ_DIV_3, LL_RCC_PLLSAIDIVQ_DIV_1);
	}

	LL_RCC_PLLSAI_Enable();


	sprintf( ( char * ) pcWriteBuffer, "valid clk=%d\n", ret);

	return pdFALSE;
}


static portBASE_TYPE prvHSITrim( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	int8_t *pcParameter1;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

	/* Obtain the name of the source file, and the length of its name, from
		    the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					1,
					/* Store the parameter string length. */
					&xParameter1StringLength
			);


	int8_t  ret=(uint8_t)strtol(pcParameter1,NULL,10);

	if(ret > (RCC_HSICALIBRATION_DEFAULT-6) && ret < (RCC_HSICALIBRATION_DEFAULT+6)) {
		__HAL_RCC_HSI_CALIBRATIONVALUE_ADJUST(ret);


		sprintf( ( char * ) pcWriteBuffer, "valid trim=%d\n", ret);
	}
	else sprintf( ( char * ) pcWriteBuffer, "invalid trim=%d\n", ret);

	return pdFALSE;
}

