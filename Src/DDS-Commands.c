/*
 * DDS-Commands.c

 *
 *  Created on: 14/08/2020
 *      Author: kevin
 */
#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
#include "main.h"
#include "cmsis_os.h"

#include "dds.h"
#define DMATRMBUF_SIZE 2046

extern unsigned char out_buf_state;
extern SAI_HandleTypeDef hsai_BlockB1;
extern osSemaphoreId myBinarySemSAIHandle;

extern uint16_t SpdifBuff[REC_BUFF_SIZE];

static uint32_t phase_accumulatorL = 0;
static uint32_t phase_accumulatorR = 0;

static portBASE_TYPE dds_set(int8_t *pcWriteBuffer, size_t xWriteBufferLen,
		const int8_t *pcCommandString);

//void DDS_calculate(uint16_t * buffer, uint16_t buffer_size, float frequency, uint32_t * phase_accumulator);

static void prvDDSTask(void *pvParameters);

const CLI_Command_Definition_t prvDDSommandDefinition =
		{ (const int8_t * const ) "dds_set", /* The command string to type. */
				(const int8_t * const ) "\r\ndds_set:\r\ndds_set <param> <value>\r\n\r\n",
				dds_set, /* The function to run. */
				2 /* No parameters are expected. */
		};


typedef enum {
	SAI_MODE_DMA, SAI_MODE_IT
} SAI_ModeTypedef;

static xTaskHandle xDDSTask = NULL;
static float freqL = 500;
static float freqR = 500;
static uint16_t* high_buffer = NULL;
static uint8_t chan = 0;
static uint8_t mode = SAI_MODE_DMA;


static uint32_t SAI_InterruptFlag(SAI_HandleTypeDef *hsai, uint32_t mode) {
	uint32_t tmpIT = SAI_IT_OVRUDR;

	if (mode == SAI_MODE_IT) {
		tmpIT |= SAI_IT_FREQ;
	}

	if ((hsai->Init.Protocol == SAI_AC97_PROTOCOL)
			&& ((hsai->Init.AudioMode == SAI_MODESLAVE_RX)
					|| (hsai->Init.AudioMode == SAI_MODEMASTER_RX))) {
		tmpIT |= SAI_IT_CNRDY;
	}

	if ((hsai->Init.AudioMode == SAI_MODESLAVE_RX)
			|| (hsai->Init.AudioMode == SAI_MODESLAVE_TX)) {
		tmpIT |= SAI_IT_AFSDET | SAI_IT_LFSDET;
	} else {
		/* hsai has been configured in master mode */
		tmpIT |= SAI_IT_WCKCFG;
	}
	return tmpIT;
}

static portBASE_TYPE dds_set(int8_t *pcWriteBuffer, size_t xWriteBufferLen,
		const int8_t *pcCommandString) {

	uint8_t *pcParameter1, *pcParameter2;
	int value = 0;

	memset(pcWriteBuffer, 0, 100);

	BaseType_t xParameter1StringLength, xParameter2StringLength;

	/* Obtain the name of the source file, and the length of its name, from
	 the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter(
	/* The command string itself. */
	pcCommandString,
	/* Return the first parameter. */
	1,
	/* Store the parameter string length. */
	&xParameter1StringLength);

	/* Obtain the name of the source file, and the length of its name, from
	 the command string. The name of the source file is the first parameter. */
	pcParameter2 = FreeRTOS_CLIGetParameter(
	/* The command string itself. */
	pcCommandString,
	/* Return the first parameter. */
	2,
	/* Store the parameter string length. */
	&xParameter2StringLength);

	if (strncmp(pcParameter1, "freq", 4) == 0) {

		int value = atol(pcParameter2);

		if (chan == 0)
			freqL = freqR = value;
		else if (chan == 1)
			freqL = value;
		else if (chan == 2)
			freqR = value;

		sprintf(pcWriteBuffer, "%s value = %d \n", pcParameter1, value);

	} else if (strncmp(pcParameter1, "chan", 4) == 0) {
		chan = atol(pcParameter2);
	} else if (strncmp(pcParameter1, "mode", 4) == 0) {
		mode = atol(pcParameter2);
	}


	if(mode==SAI_MODE_IT){
		HAL_SAI_Abort(&hsai_BlockB1);
		HAL_SAI_Transmit_IT(&hsai_BlockB1, (uint8_t*) SpdifBuff,
					DMATRMBUF_SIZE);
	}



	if (high_buffer == NULL) {

		high_buffer = &SpdifBuff[DMATRMBUF_SIZE];
		DDS_Init();

		if(mode==SAI_MODE_DMA)HAL_SAI_Transmit_DMA(&hsai_BlockB1, (uint8_t*) SpdifBuff,DMATRMBUF_SIZE * 2);

	}

	phase_accumulatorL = 0;
	phase_accumulatorR = 0;

	if (xDDSTask == NULL) {

		xTaskCreate(prvDDSTask, (const int8_t * const ) "DDSTask",
		configMINIMAL_STACK_SIZE * 2,
		NULL,
		configUART_COMMAND_CONSOLE_TASK_PRIORITY, &xDDSTask);
	}

	return pdFALSE;

}

void prvDDSTask(void *pvParameters) {

	static short i = 0;

	static uint8_t I_Buffer = 0;

	while (1) {

		osSemaphoreWait(myBinarySemSAIHandle, 5000);
		I_Buffer = !I_Buffer;

		if (out_buf_state & (LO_EMPTY)) {


			DDS_calculate((dmabuf_t*) SpdifBuff, DMATRMBUF_SIZE / 2,
					&phase_accumulatorL, freqL, &phase_accumulatorR, freqR);
			out_buf_state &= ~(LO_EMPTY);



		} else if (out_buf_state & (HI_EMPTY)) {

			//	LL_GPIO_TogglePin(GPIOB,TST_1_Pin);
			if (mode == SAI_MODE_IT) {
				if (I_Buffer) {

					HAL_SAI_Transmit_IT(&hsai_BlockB1, (uint8_t*) high_buffer,
					DMATRMBUF_SIZE);
					DDS_calculate((dmabuf_t*) SpdifBuff, DMATRMBUF_SIZE / 2,
										&phase_accumulatorL, freqL, &phase_accumulatorR, freqR);

				} else {

					HAL_SAI_Transmit_IT(&hsai_BlockB1, (uint8_t*) SpdifBuff,
					DMATRMBUF_SIZE);
					DDS_calculate(high_buffer, DMATRMBUF_SIZE / 2, &phase_accumulatorL,
										freqL, &phase_accumulatorR, freqR);

				}
			}
			else{

				DDS_calculate(high_buffer, DMATRMBUF_SIZE / 2, &phase_accumulatorL,
						freqL, &phase_accumulatorR, freqR);
			}

			out_buf_state &= ~(HI_EMPTY);
		}

	}

}
