/*
 * ST7789.cpp



 *
 *  Created on: 23/05/2019
 *      Author: kevin
 */

#include "Adafruit_ST7789.h"
//#include "SPI.h"

#define TFT_CS        	PB0 //
#define TFT_RST        	PB1//
#define TFT_DC         	PC5 //
#define TFT_MOSI 		PA7
#define TFT_CLK 		PA5
#define TFT_BL			PA6

//6 A6 PA7 SPI1_MOSI
//7 A5 PA6 ADC_IN6 || I2C1_SCL
//8 A4 PA5 SPI1_SCL

//11 A1 PA1 DC
//9 A3 PA4 CS
//10 A2 PA3 RST
/*
 * BL
 * RST
 * DC
 * CS
 * CLK
 * DIN
 *
 */

Adafruit_ST7789 *tft;

int CPPInitLCD(uint16_t color)
{

	tft = new Adafruit_ST7789(TFT_CS, TFT_DC,TFT_MOSI,TFT_CLK, TFT_RST);
	tft->init(240, 240);
	tft->fillScreen(color);
	return 1;
}

extern "C" int InitLCD(uint16_t color)
{
	/*
	PinName	dc=digitalPinToPinName(TFT_DC);
	PinName	rst=digitalPinToPinName(TFT_RST);
	PinName	cs=digitalPinToPinName(TFT_CS);

	printf("dc %d rst %d cs %d\n",dc,rst,cs);
*/

	CPPInitLCD(color);
	return 1;
}

extern "C" int DrawPixel(int16_t x, int16_t y, uint16_t color)
{
	tft->drawPixel(x, y, color);
	return 1;
}

extern "C" int drawtext(char *text,int16_t x, int16_t y, uint16_t color) {
  tft->setCursor(x, y);
  tft->setTextColor(color);
  tft->setTextWrap(true);
  tft->print(text);
  return 1;
}

extern "C" void  setCursor(int16_t x, int16_t y)
{
	tft->setCursor(x, y);
}

extern "C" void drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color)
{
	tft->drawCircle(x0,y0,r,color);
}

extern "C" void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color)
{
	tft->drawLine( x0,  y0,  x1,  y1,  color);
}

extern "C" void  drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
	tft->drawRect( x,  y,  w,  h,  color);
}

extern "C" void setTextSize(uint8_t s)
{
	tft->setTextSize(s);
}
