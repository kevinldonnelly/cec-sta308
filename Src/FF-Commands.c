/*
 * FF-Commands.c
 *
 *  Created on: Mar 22, 2019
 *      Author: kevin
 */

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "cmsis_os.h"
#include "FreeRTOS_CLI.h"
#include "fatfs.h"

static portBASE_TYPE prvFFListFiles( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvFFDiskStats( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvFFSectorRead( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvFFSectorWrite( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvFFSectormkfs( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
extern Disk_drvTypeDef  disk;
extern SD_HandleTypeDef hsd;
uint8_t   SDIOBuff[512];

/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvSFFListFilesCommandDefinition =
{
		( const int8_t * const ) "ls", /* The command string to type. */
		( const int8_t * const ) "ls:\r\n List Files\r\n",
		prvFFListFiles, /* The function to run. */
		0 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSFFDiskStatsCommandDefinition =
{
		( const int8_t * const ) "disk", /* The command string to type. */
		( const int8_t * const ) "disk:\r\n",
		prvFFDiskStats, /* The function to run. */
		0 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSFFSectorReadCommandDefinition =
{
		( const int8_t * const ) "sector_read", /* The command string to type. */
		( const int8_t * const ) "sector_read :  <num>\r\n",
		prvFFSectorRead, /* The function to run. */
		1 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSFFSectorWriteCommandDefinition =
{
		( const int8_t * const ) "sector_write", /* The command string to type. */
		( const int8_t * const ) "sector_write :  <num> <str>\r\n",
		prvFFSectorWrite, /* The function to run. */
		2 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSFFSectormkfsCommandDefinition =
{
		( const int8_t * const ) "mkfs", /* The command string to type. */
		( const int8_t * const ) "mkfs\r\n",
		prvFFSectormkfs, /* The function to run. */
		0 /* No parameters are expected. */
};

typedef union {
	struct   {
		unsigned int Day:5;
		unsigned int Month:4;
		unsigned int Year:7;
	} fields;
	WORD bits;
} mdate;


typedef struct   {
	unsigned int Second :5;
	unsigned int Minute :6;
	unsigned int Hour :5;
} mtime;

/*
extern osSemaphoreId myBinarySemSDIOHandle;;

void SD_CardCallback(SD_HandleTypeDef* hsd)
{
	osSemaphoreRelease(myBinarySemSDIOHandle);
}
 */

static portBASE_TYPE prvFFSectormkfs( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{


	memset(pcWriteBuffer,0,128);
	FRESULT res=f_mkfs("",FM_FAT32,0,SDIOBuff,512);

	sprintf(pcWriteBuffer,"f_mkfs r=%d\n",res);

	return pdFALSE;
}


static portBASE_TYPE prvFFSectorWrite( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	uint8_t *pcParameter1,*pcParameter2;
	uint32_t BlockAdd=1000000;
	char buf[16];
	DRESULT s;
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );
	memset(pcWriteBuffer,0,128);
	memset(SDIOBuff,0,512);


	BaseType_t xParameter1StringLength,xParameter2StringLength ;

	/* Obtain the name of the source file, and the length of its name, from
								    the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					1,
					/* Store the parameter string length. */
					&xParameter1StringLength
			);


	/* Obtain the name of the source file, and the length of its name, from
								    the command string. The name of the source file is the first parameter. */
	pcParameter2 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					2,
					/* Store the parameter string length. */
					&xParameter2StringLength
			);

	BlockAdd=atol(pcParameter1);

	strncpy(SDIOBuff,pcParameter2,xParameter2StringLength);

	if(BlockAdd>5000000)s=  SD_write(0,SDIOBuff,BlockAdd,1);
	sprintf(pcWriteBuffer,"sector = %d string=%s r=%d\n",BlockAdd,SDIOBuff,s);

	//    strcpy(pcWriteBuffer,buf);



	return pdFALSE;
}

static portBASE_TYPE prvFFSectorRead( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{


	uint8_t *pcParameter1;
	uint32_t BlockAdd=1000000;
	DRESULT res=0;
	char buf[32];
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );
	memset(pcWriteBuffer,0,128);
	memset(SDIOBuff,0,512);


	BaseType_t xParameter1StringLength ;

	/* Obtain the name of the source file, and the length of its name, from
							    the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					1,
					/* Store the parameter string length. */
					&xParameter1StringLength
			);



	BlockAdd=atol(pcParameter1);

	DRESULT s=  SD_read(0,SDIOBuff,BlockAdd,1);
	DWORD sector_count=0;

	res=disk.drv[0]->disk_status(0);

	res = disk.drv[0]->disk_ioctl(0, GET_SECTOR_SIZE, &sector_count);

	sprintf(buf,"sector = %d r=%d count=%d res=%d\n",BlockAdd,s,sector_count,res);
	strcpy(pcWriteBuffer,buf);

	for(int i=0; i< 512; i++)
	{
		if(SDIOBuff[i]>32 && SDIOBuff[i]<127)sprintf(buf,"%c ",SDIOBuff[i]);
		else sprintf(buf,"%x ",SDIOBuff[i]);
		strcat(pcWriteBuffer,buf);
	}


	return pdFALSE;

}

static portBASE_TYPE prvFFListFiles( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{

	FRESULT res;     /* Return value */
	DIR dj;         /* Directory search object */
	FILINFO fno;    /* File information */

	pcWriteBuffer[0]=0;
	int i=0;
	mdate d;
	mtime t;
	FATFS *fs;

	res=disk_status(0);

	if(res==FR_OK)res = f_findfirst(&dj, &fno, "", "*.*");  /* Start to search for photo files */

	if(res != FR_OK)
	{
		sprintf( SDIOBuff, "no file found err=%d\n",(int)res);
		strcat(pcWriteBuffer,SDIOBuff);
		f_closedir(&dj);

		return pdFALSE;
	}
	sprintf( pcWriteBuffer, "files found err=%d\n",(int)res);

	//	return pdFALSE;

	while (res == FR_OK && fno.fname[0]) {         /* Repeat while an item is found */
		/* Display the object name */
		d.bits=fno.fdate;
		memcpy(&t,&fno.ftime,sizeof(mtime));
		sprintf(SDIOBuff,"%d %s %s %d bytes %d/%d/%d %dh : %dm : %ds\n",i++, fno.fname,fno.altname,fno.fsize,d.fields.Day,d.fields.Month,1980+d.fields.Year,t.Hour,t.Minute,t.Second);
		strcat(pcWriteBuffer,SDIOBuff);
		res = f_findnext(&dj, &fno);  /* Search for next item */

	}

	f_closedir(&dj);

	return pdFALSE;

}

static portBASE_TYPE prvFFDiskStats( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{

	sprintf(pcWriteBuffer,"%d Sectors size=%d Block Size %d\n",hsd.SdCard.LogBlockNbr,hsd.SdCard.LogBlockSize,hsd.SdCard.LogBlockSize/512);
	return pdFALSE;
}
