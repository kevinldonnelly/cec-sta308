/*
 * wifi-commands.c
 *
 *  Created on: Apr 1, 2019
 *      Author: kevin
 */
#include "FreeRTOS.h"

#include "FreeRTOS_CLI.h"
#include "usart.h"
#include "spi.h"
#include "cmsis_os.h"
#include "fatfs.h"

static portBASE_TYPE prvWIFICommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );


static void prvSPIReceiveToFileTask( void *pvParameters );
static void prvSPIReceiveToSectorsTask( void *pvParameters );

//#define UART3_BUFFERSIZE 1024
//uint8_t   UART3Buff[UART3_BUFFERSIZE];

const CLI_Command_Definition_t prvWIFICommandDefinition =
{
	( const int8_t * const ) "wifi",
	( const int8_t * const ) "wifi file\r\n",
	prvWIFICommand,
	1
};

//http://radionz-ice.streamguys.com/national.mp3
static xTaskHandle xWifiTask = NULL;
static FIL xwFile;
static uint32_t BlockWriteAdd=0;
static portBASE_TYPE prvWIFICommand( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	uint8_t *pcParameter1;

	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

		//	FRESULT fr=FR_INVALID_PARAMETER;
			BaseType_t r;
			BaseType_t xParameter1StringLength;

						    /* Obtain the name of the source file, and the length of its name, from
						    the command string. The name of the source file is the first parameter. */
						    pcParameter1 = FreeRTOS_CLIGetParameter
						                        (
						                          /* The command string itself. */
						                          pcCommandString,
						                          /* Return the first parameter. */
						                          1,
						                          /* Store the parameter string length. */
						                          &xParameter1StringLength
						                        );


						    if(xWifiTask!=NULL){

						    	vTaskDelay(100);
						    	f_close(&xwFile);

						    	vTaskDelete(xWifiTask);
						    	xWifiTask=NULL;
						    	return pdFALSE;
						    }

						    BlockWriteAdd=atol(pcParameter1);

						    if(BlockWriteAdd>1000000)
						    {
						    	startSectorWriteTask(BlockWriteAdd);
						    	sprintf(pcWriteBuffer,"sector write task started %s\n",pcParameter1);
						    	return pdFALSE;
						    }



			if( f_open(&xwFile, pcParameter1, FA_WRITE | FA_CREATE_ALWAYS )==0)
			{

			r=xTaskCreate( prvSPIReceiveToFileTask,( const int8_t * const ) "WifiSPITask",
								configMINIMAL_STACK_SIZE*2,
								NULL,
								configUART_COMMAND_CONSOLE_TASK_PRIORITY,
								&xWifiTask );

			sprintf(pcWriteBuffer,"wifi stream receive task started %s\n",pcParameter1);

			}
			else
			{
				sprintf(pcWriteBuffer,"openfailed  %s \n",pcParameter1);
			}
		return pdFALSE;



	return pdFALSE;
}

static short out_buffer[OUT_BUFFER_SIZE];//2*2*2*576

extern osSemaphoreId myBinarySemSPIWifiHandle;
unsigned char buf_state = LO_EMPTY | HI_EMPTY;

void SD_CardWriteCallback(SD_HandleTypeDef* hsd)
{

}

extern SD_HandleTypeDef hsd;



static void prvSPIReceiveToFileTask( void *pvParameters )
{
	UINT br, bw;
	uint16_t loops=0;
	FRESULT res=0;
	HAL_SPI_Receive_DMA(&hspi2, out_buffer, OUT_BUFFER_SIZE*2);//2*2*2*2*576 16*576 bytes
//	HAL_SD_RegisterCallback(&hsd,HAL_SD_TX_CPLT_CB_ID,SD_CardWriteCallback);
	uint8_t * 	pData = &out_buffer;

	while(loops++ < 500)
	{
		osSemaphoreWait(myBinarySemSPIWifiHandle, osWaitForever);
		if(buf_state & (LO_EMPTY)){


					res=f_write(&xwFile, pData,OUT_BUFFER_SIZE, &bw);//2*2*2*576
					buf_state &= ~(LO_EMPTY);
					if(loops%10==0)printf("lo-empty %d res=%d\n",loops,res);
					if(res!=0)break;


		}else if(buf_state & (HI_EMPTY)){


			res=f_write(&xwFile, &pData[OUT_BUFFER_SIZE],OUT_BUFFER_SIZE, &bw);//
					buf_state &= ~(HI_EMPTY);
					if(loops%10==0)printf("hi-empty %d\n",loops);

		}

	}
	printf("Wifi Task ending\n");
	f_close(&xwFile);
	vTaskDelay(100);
	HAL_SPI_Abort_IT(&hspi2);
	vTaskDelay(1000);
	xWifiTask=NULL;
	vTaskDelete(NULL);
	vTaskDelay(1000);


}

void startSectorWriteTask(uint32_t BlockAdd)
{
	BlockWriteAdd=BlockAdd;

	xTaskCreate( prvSPIReceiveToSectorsTask,( const int8_t * const ) "WifiSPITask",
									configMINIMAL_STACK_SIZE*2,
									NULL,
									configUART_COMMAND_CONSOLE_TASK_PRIORITY,
									&xWifiTask );
}

static void prvSPIReceiveToSectorsTask( void *pvParameters )
{
	UINT br, bw;

	DRESULT res=0;
	HAL_SPI_Receive_DMA(&hspi2, out_buffer, 512*16);
//	HAL_SD_RegisterCallback(&hsd,HAL_SD_TX_CPLT_CB_ID,SD_CardWriteCallback);
	long StartBlock;
	if(BlockWriteAdd==0)BlockWriteAdd=hsd.SdCard.LogBlockNbr -1000000;
	StartBlock=BlockWriteAdd;
	uint8_t * 	pData = &out_buffer;

	while(1)
	{
		osSemaphoreWait(myBinarySemSPIWifiHandle, osWaitForever);
		if(buf_state & (LO_EMPTY)){


				//	res=f_write(&xwFile, out_buffer,OUT_BUFFER_SIZE, &bw);
					res=SD_write(0,pData,BlockWriteAdd,8);
					buf_state &= ~(LO_EMPTY);
					if(BlockWriteAdd%100==0)printf("blk=%d\n",BlockWriteAdd);
					if(res!=0)break;


		}else if(buf_state & (HI_EMPTY)){


					res=SD_write(0, &pData[ 512*8],BlockWriteAdd, 8);
					buf_state &= ~(HI_EMPTY);


		}
		BlockWriteAdd=BlockWriteAdd+8;

		if(BlockWriteAdd > hsd.SdCard.LogBlockNbr -1000)BlockWriteAdd=StartBlock;

	}
	printf("Wifi Task ending\n");

	vTaskDelay(100);
	HAL_SPI_Abort_IT(&hspi2);
	vTaskDelay(1000);
	xWifiTask=NULL;
	vTaskDelete(NULL);
	vTaskDelay(1000);


}

void HAL_SPI_RxHalfCpltCallback(SPI_HandleTypeDef *hspi)
{
	buf_state = LO_EMPTY;
	osSemaphoreRelease(myBinarySemSPIWifiHandle);
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
	buf_state = HI_EMPTY;
	osSemaphoreRelease(myBinarySemSPIWifiHandle);
}


