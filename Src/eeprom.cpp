/*
 * eeprom.cpp

 *
 *  Created on: Dec 28, 2018
 *      Author: kevin
 */

#include "OSFS.h"
#include "i2c.h"
#include "cmsis_os.h"

uint16_t OSFS::startOfEEPROM = 1;
uint16_t OSFS::endOfEEPROM = 1024;


extern osSemaphoreId myBinarySemI2CHandle;

void  OSFS::readNBytes(uint16_t MemAddress, unsigned int num, byte* output)
{

	HAL_I2C_Mem_Read_IT(&hi2c3,0xA0,MemAddress,I2C_MEMADD_SIZE_16BIT,output,num);
	osSemaphoreWait(myBinarySemI2CHandle, 5000);
	vTaskDelay(10);
}

// 4) How to I write to the medium?
void OSFS::writeNBytes(uint16_t MemAddress, unsigned int num, const byte* input) {

	HAL_I2C_Mem_Write_IT(&hi2c3,0xA1,MemAddress,I2C_MEMADD_SIZE_16BIT,(uint8_t*)input,num);
	osSemaphoreWait(myBinarySemI2CHandle, 5000);
	vTaskDelay(10);
}


int ReadInt(char* name)
{


	  // Var to hold the result of actions
	  OSFS::result r;

	  // Useful consts
	  const OSFS::result noerr = OSFS::result::NO_ERROR;
	  const OSFS::result notfound = OSFS::result::FILE_NOT_FOUND;

	  ////////////////////////////


	  int testInt;
	  r = OSFS::getFile(name, testInt);


	  if (r == noerr) return testInt;
	  else if(r==OSFS::result::UNFORMATTED)
	  {
		  OSFS::format();
		  return (int)r;
	  }
	  else return (int)r;
}

int WriteInt(char* name,int value)
{
	 OSFS::result r;
	 r=OSFS::newFile(name, value,true);

	return (int)r;

}


extern "C" int CReadInt(char* name)
{
	return ReadInt(name);
}

extern "C" int CWriteInt(char* name,int v)
{
	return WriteInt(name,v);
}


