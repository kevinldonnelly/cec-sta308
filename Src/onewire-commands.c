/*
 * onewire-commands.c


 *
 *  Created on: 7/06/2019
 *      Author: kevin
 */
#include "tm_stm32_ds18b20.h"
#include "tm_stm32_onewire.h"
#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
//#include "semphr.h"
//#include "task.h"
#include "tim.h"
#include "cmsis_os.h"
#include <stdio.h>
TM_OneWire_t OW;
/* Array for DS18B20 ROM number */
static uint8_t DS_ROM[8];

/* Temperature variable */
float temp;
int us_delay = 3;
extern xTaskHandle xCommandConsoleTask;
xTaskHandle xOneWireTask = NULL;
static xTaskHandle xDelayTask = NULL;
static void prvOneWireTask(void *pvParameters);
void vDelayTaskStart(void);
static portBASE_TYPE pvrPulse(int8_t *pcWriteBuffer, size_t xWriteBufferLen,
		const int8_t *pcCommandString);
static portBASE_TYPE pvrOneWireReset(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString);
static portBASE_TYPE pvrOneWireWriteByte(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString);
static portBASE_TYPE pvrOneWireReadByte(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString);
static portBASE_TYPE pvrOneWireSearch(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString);
static portBASE_TYPE pvrOneWireWriteBit(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString);
static portBASE_TYPE pvrOneWireReadBit(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString);

void InitOneWire();

const CLI_Command_Definition_t prvOneWireSearchCommandDefinition = {
		(const int8_t * const ) "OneWireSearch", /* The command string to type. */
		(const int8_t * const ) "OneWireSearch\r\n", pvrOneWireSearch, /* The function to run. */
		0 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvOneWireWriteByteCommandDefinition = {
		(const int8_t * const ) "OneWireWriteByte", /* The command string to type. */
		(const int8_t * const ) "OneWireWriteByte\r\n", pvrOneWireWriteByte, /* The function to run. */
		1 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvOneWireReadByteCommandDefinition = {
		(const int8_t * const ) "OneWireReadByte", /* The command string to type. */
		(const int8_t * const ) "OneWireReadByte\r\n", pvrOneWireReadByte, /* The function to run. */
		0 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvOneWireWriteBitCommandDefinition = {
		(const int8_t * const ) "OneWireWriteBit", /* The command string to type. */
		(const int8_t * const ) "OneWireWriteBit\r\n", pvrOneWireWriteBit, /* The function to run. */
		1 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvOneWireReadBitCommandDefinition = {
		(const int8_t * const ) "OneWireReadBit", /* The command string to type. */
		(const int8_t * const ) "OneWireReadBit\r\n", pvrOneWireReadBit, /* The function to run. */
		1 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvOneWireResetCommandDefinition = {
		(const int8_t * const ) "OneWireReset", /* The command string to type. */
		(const int8_t * const ) "OneWireReset\r\n", pvrOneWireReset, /* The function to run. */
		0 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvPulseTaskStartCommandDefinition = {
		(const int8_t * const ) "pulse", /* The command string to type. */
		(const int8_t * const ) "pulse\r\n", pvrPulse, /* The function to run. */
		1 /* No parameters are expected. */
};

static portBASE_TYPE pvrOneWireSearch(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString) {
	OW.xOneWireTask = xCommandConsoleTask;
	TM_OneWire_Search(&OW, ONEWIRE_CMD_SEARCHROM);
	sprintf((char *) pcWriteBuffer, "%02X %02X %02X %02X %02X %02X %02X %02X\n",
			OW.ROM_NO[0], OW.ROM_NO[1], OW.ROM_NO[2], OW.ROM_NO[3],
			OW.ROM_NO[4], OW.ROM_NO[5], OW.ROM_NO[6], OW.ROM_NO[7]);
	return pdFALSE;

}

static portBASE_TYPE pvrOneWireReadByte(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString) {
	OW.xOneWireTask = xCommandConsoleTask;
	int8_t byte = TM_OneWire_ReadByte(&OW);
	sprintf((char *) pcWriteBuffer, "OneWireReadByte %x\n", byte);
	return pdFALSE;
}

static portBASE_TYPE pvrOneWireReadBit(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString) {

	int8_t *pcParameter1;
	int8_t bit;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

	/* Obtain the name of the source file, and the length of its name, from
	 the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter(
	/* The command string itself. */
	pcCommandString,
	/* Return the first parameter. */
	1,
	/* Store the parameter string length. */
	&xParameter1StringLength);

	OW.xOneWireTask = xCommandConsoleTask;
	if (strcmp("PWM", pcParameter1)) {
		bit = Task_OneWireReadBit(&OW);
	} else
		bit = TM_OneWire_ReadBit(&OW);

	sprintf((char *) pcWriteBuffer, "OneWireReadBit %s %d\n", pcParameter1,
			bit);
	return pdFALSE;
}

static portBASE_TYPE pvrOneWireWriteByte(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString) {
	int8_t *pcParameter1;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

	/* Obtain the name of the source file, and the length of its name, from
	 the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter(
	/* The command string itself. */
	pcCommandString,
	/* Return the first parameter. */
	1,
	/* Store the parameter string length. */
	&xParameter1StringLength);

	int8_t byte = (uint8_t) strtol(pcParameter1, NULL, 16);
	OW.xOneWireTask = xCommandConsoleTask;
	TM_OneWire_WriteByte(&OW, byte);

	sprintf((char *) pcWriteBuffer, "OneWireWriteByte %x\n", byte);

	return pdFALSE;
}

static portBASE_TYPE pvrOneWireWriteBit(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString) {
	int8_t *pcParameter1;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

	/* Obtain the name of the source file, and the length of its name, from
	 the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter(
	/* The command string itself. */
	pcCommandString,
	/* Return the first parameter. */
	1,
	/* Store the parameter string length. */
	&xParameter1StringLength);

	int8_t bit = (uint8_t) strtol(pcParameter1, NULL, 16);
	OW.xOneWireTask = xCommandConsoleTask;
	//    TM_OneWire_WriteBit(&OW,bit);
	Task_OneWireWriteBit(&OW, bit);
	sprintf((char *) pcWriteBuffer, "OneWireWriteBit %d\n", bit);

	return pdFALSE;
}

static portBASE_TYPE pvrPulse(int8_t *pcWriteBuffer, size_t xWriteBufferLen,
		const int8_t *pcCommandString) {
	int8_t *pcParameter1;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

	/* Obtain the name of the source file, and the length of its name, from
	 the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter(
	/* The command string itself. */
	pcCommandString,
	/* Return the first parameter. */
	1,
	/* Store the parameter string length. */
	&xParameter1StringLength);

	int16_t period = (uint16_t) strtol(pcParameter1, NULL, 10);

	OW.xOneWireTask = xCommandConsoleTask;
	__HAL_TIM_SET_COUNTER(&htim5, 0);
	HAL_TIM_PWM_Start_IT(&htim5, TIM_CHANNEL_2);
	HAL_TIM_Base_Start_IT(&htim5);
	OW.Task_suspeneded = 1;
	osThreadSuspend(NULL);
	int cc2 = __HAL_TIM_GET_COUNTER(&htim5);
	HAL_TIM_Base_Stop_IT(&htim5);
	HAL_TIM_PWM_Stop_IT(&htim5, TIM_CHANNEL_2);

	sprintf((char *) pcWriteBuffer,
			"pulse %d count %d countaft=%d pinstate %d\n", period, OW.Counter,
			cc2, OW.GPIO_Pin_value);
	return pdFALSE;

}

static portBASE_TYPE pvrOneWireReset(int8_t *pcWriteBuffer,
		size_t xWriteBufferLen, const int8_t *pcCommandString) {
	OW.xOneWireTask = xCommandConsoleTask;
	int pres = TM_OneWire_Reset(&OW);
	sprintf((char *) pcWriteBuffer, "present %d\n", pres);
	return pdFALSE;
}

void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM5) {
		//	 osSemaphoreRelease (myBinarySemTim5Handle);
	}
}

void vPulseTaskStart(void) {
	PulseOneWire(10);
}

void vDelayTaskStart(void) {
	xTaskCreate(prvOneWireTask, /* The task that implements the command console. */
	(const int8_t * const ) "xDelayTask", /* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
	configUART_COMMAND_CONSOLE_STACK_SIZE, /* The size of the stack allocated to the task. */
	NULL, /* The parameter is not used, so NULL is passed. */
	configUART_COMMAND_CONSOLE_TASK_PRIORITY,/* The priority allocated to the task. */
	&xDelayTask); /* Used to store the handle to the created task. */
}

void vOneWireTaskStart(void) {
	xTaskCreate(prvOneWireTask, /* The task that implements the command console. */
	(const int8_t * const ) "OneWireTask", /* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
	configMINIMAL_STACK_SIZE * 2, /* The size of the stack allocated to the task. */
	NULL, /* The parameter is not used, so NULL is passed. */
	configUART_COMMAND_CONSOLE_TASK_PRIORITY - 1,/* The priority allocated to the task. */
	&xOneWireTask); /* Used to store the handle to the created task. */
}

extern void Task_OneWireWriteBit(TM_OneWire_t* OneWireStruct, uint8_t mbit);

static void prvOneWireTask(void *pvParameters) {

	TM_OneWire_Init(&OW, GPIOA, GPIO_PIN_1);
	OW.xOneWireTask = xOneWireTask;
	TaskDelay(100);
	int i = 0;
	int ii = 0;
	int iii = 0;

	while (1) {

		//	Task_OneWireWriteBit(&OW,1);
		//	Task_OneWireReadBit(&OW);
		//	TaskDelay(12);

		//	Task_OneWireWriteBit(&OW,0);
		//vTaskDelay(100);
		//	continue;

		i = TM_OneWire_Reset(&OW);

		TM_OneWire_WriteByte(&OW, 0xF0);
		//  vTaskDelay(1);
		printf("readbit in 0.5 seconds\n");
		vTaskDelay(500);

		ii = Task_OneWireReadBit(&OW);
		// vTaskDelay(1);
		iii = Task_OneWireReadBit(&OW);
		printf("present=%d %d %d\n", i, ii, iii);
		if (i == 0 && ii == 0 && iii == 1)
			break;

	}

	int loops = 0;

	vTaskDelay(100);
	OW.MaxCounter = 0;
	InitOneWire();

	printf("%02X %02X %02X %02X %02X %02X %02X %02X\n", OW.ROM_NO[0],
			OW.ROM_NO[1], OW.ROM_NO[2], OW.ROM_NO[3], OW.ROM_NO[4],
			OW.ROM_NO[5], OW.ROM_NO[6], OW.ROM_NO[7]);

	while (1) {

		vTaskDelay(5000);
		OW.MaxCounter = 0;

		/* Check if connected device is DS18B20 */
		if (TM_DS18B20_Is(DS_ROM)) {
			/* Everything is done */
			if (TM_DS18B20_AllDone(&OW)) {
				/* Read temperature from device */
				if (TM_DS18B20_Read(&OW, DS_ROM, &temp)) {
					/* Temp read OK, CRC is OK */

					/* Start again on all sensors */
					TM_DS18B20_StartAll(&OW);

					int r = temp;
					int d = (temp - r) * 10;

					/* Check temperature */
					if (loops++ % 20 == 0)
						printf("temp=%d.%d MC=%d\n", r, d, OW.MaxCounter);

				} else {
					/* CRC failed, hardware problems on data line */
				}
			}
		}
	}
}

void InitOneWire() {
	/* Init ONEWIRE port on PB4 pin */
	//  TM_OneWire_Init(&OW, GPIOA, GPIO_PIN_1);
	/* Check if any device is connected */

	if (TM_OneWire_First(&OW)) {
		/* Set LED GREEN */

		/* Search for next devices */
		do {
			/* Read ROM from device */
			TM_OneWire_GetFullROM(&OW, DS_ROM);
		} while (TM_OneWire_Next(&OW));

		vTaskDelay(5);

	}

	if (TM_DS18B20_Is(DS_ROM)) {
		/* Set resolution */
		TM_DS18B20_SetResolution(&OW, DS_ROM, TM_DS18B20_Resolution_12bits);

		/* Set high and low alarms */
		TM_DS18B20_SetAlarmHighTemperature(&OW, DS_ROM, 30);
		TM_DS18B20_SetAlarmLowTemperature(&OW, DS_ROM, 10);

		/* Start conversion on all sensors */
		TM_DS18B20_StartAll(&OW);
	}
}
