/*
 * pwm-commands.c
 *
 *  Created on: 11/08/2020
 *      Author: kevin
 */

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
#include "tim.h"

extern TIM_HandleTypeDef htim14;
extern TIM_HandleTypeDef htim1;
static portBASE_TYPE pwm_set( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

const CLI_Command_Definition_t prvPWMCommandDefinition =
{
		( const int8_t * const ) "pwm_set", /* The command string to type. */
		( const int8_t * const ) "\r\npwm_set:\r\npwm_set <param> <value>\r\n\r\n",
		pwm_set, /* The function to run. */
		2/* No parameters are expected. */
};


static portBASE_TYPE pwm_set( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString ){


	uint8_t *pcParameter1,*pcParameter2;

	memset(pcWriteBuffer,0,100);

	long value=0;

	BaseType_t xParameter1StringLength,xParameter2StringLength ;

	/* Obtain the name of the source file, and the length of its name, from
								    the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					1,
					/* Store the parameter string length. */
					&xParameter1StringLength
			);


	/* Obtain the name of the source file, and the length of its name, from
								    the command string. The name of the source file is the first parameter. */
	pcParameter2 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					2,
					/* Store the parameter string length. */
					&xParameter2StringLength
			);


		//	HAL_TIM_PWM_DeInit(&htim1);

		if(strncmp(pcParameter1,"period",xParameter1StringLength)==0){


			value=atol(pcParameter2);
			htim14.Instance->ARR = value;
			sprintf(pcWriteBuffer,"command = period value=%d \n",value);

		}
		else if(strncmp(pcParameter1,"pulse",xParameter1StringLength)==0){

		}




	//	HAL_TIM_PWM_Init(&htim14);

	return pdFALSE;


	//	htim1->Instance->
}

