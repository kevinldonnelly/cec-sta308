/*
 * SAI-Commands.c
 *

 *
 *  Created on: Mar 25, 2019
 *      Author: kevin
 */
//#include "core_cm4.h"
#include "FreeRTOS.h"

#include "semphr.h"
#include "sai.h"
#include "cmsis_os.h"
#include "fatfs.h"
#define PLAY_BUFF_SIZE       1024
#define PLAY_HEADER          0x2C
#define AUDIO_FILE_ADDRESS   0x08020000
#define AUDIO_FILE_SIZE      (180*2024)
uint16_t                      PlayBuff[PLAY_BUFF_SIZE];//PLAY_BUFF_SIZE *2 bytes

int16_t  UpdatePointer = -1;
extern osSemaphoreId myBinarySemSPDIFHandle;
static void prvSAIPlayFlashTask( void *pvParameters );
static void prvSAIPlayFileTask( void *pvParameters );

extern SAI_HandleTypeDef hsai_BlockB1;
extern osSemaphoreId myBinarySemSAIHandle;
/* Holds the handle of the task that implements the UART command console. */
static xTaskHandle xSAITask = NULL;
FIL xreadFile;
/*-----------------------------------------------------------*/

void vSAITaskPlayFlashStart( void )
{
	xTaskCreate( 	prvSAIPlayFlashTask,				/* The task that implements the command console. */
					( const int8_t * const ) "SAIPlayFlashTask",		/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
					configUART_COMMAND_CONSOLE_STACK_SIZE,	/* The size of the stack allocated to the task. */
					NULL,									/* The parameter is not used, so NULL is passed. */
					configUART_COMMAND_CONSOLE_TASK_PRIORITY,/* The priority allocated to the task. */
					&xSAITask );					/* Used to store the handle to the created task. */
}

void vSAITaskPlayFileStart( void )
{
	xTaskCreate( 	prvSAIPlayFileTask,				/* The task that implements the command console. */
					( const int8_t * const ) "SAIPlayFileTask",		/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
					configUART_COMMAND_CONSOLE_STACK_SIZE,	/* The size of the stack allocated to the task. */
					NULL,									/* The parameter is not used, so NULL is passed. */
					configUART_COMMAND_CONSOLE_TASK_PRIORITY+6,/* The priority allocated to the task. */
					&xSAITask );					/* Used to store the handle to the created task. */
}

/*-----------------------------------------------------------*/

static void prvSAIPlayFlashTask( void *pvParameters )
{
	 uint32_t PlaybackPosition   = PLAY_BUFF_SIZE + PLAY_HEADER;
	 for(int i=0; i < PLAY_BUFF_SIZE; i+=2)
	   {
	     PlayBuff[i]=*((__IO uint16_t *)(AUDIO_FILE_ADDRESS + PLAY_HEADER + i));
	   }
	  HAL_SAI_Transmit_DMA(&hsai_BlockB1, (uint8_t *) PlayBuff, PLAY_BUFF_SIZE);
		//	osSemaphoreWait(myBinarySemSAIHandle, osWaitForever);
	/* Start loopback */
	  while(1)
	  {
	  //  BSP_LED_Toggle(LED3);

	    /* Wait a callback event */
	 //   while(UpdatePointer==-1);
		osSemaphoreWait(myBinarySemSAIHandle, osWaitForever);
	    int position = UpdatePointer;
	    UpdatePointer = -1;

	    /* Upate the first or the second part of the buffer */
	    for(int i = 0; i < PLAY_BUFF_SIZE/2; i++)
	    {
	      PlayBuff[i+position] = *(uint16_t *)(AUDIO_FILE_ADDRESS + PlaybackPosition);
	      PlaybackPosition+=2;
	    }

	    /* check the end of the file */
	    if((PlaybackPosition+PLAY_BUFF_SIZE/2) > AUDIO_FILE_SIZE)
	    {
	      PlaybackPosition = PLAY_HEADER;
	    }

	    if(UpdatePointer != -1)
	    {
	      /* Buffer update time is too long compare to the data transfer time */
	      Error_Handler();
	    }

	  }
}

static void prvSAIPlayFileTask( void *pvParameters )
{


	UINT  bytesread;
	UINT blockalign=0;
	UINT format=0;
	short sample[2];
	FRESULT  fr= f_open(&xreadFile, "spdif.wav",FA_OPEN_EXISTING | FA_READ );
	f_lseek(&xreadFile,20);
	f_read(&xreadFile,&format,2,&bytesread);
	f_lseek(&xreadFile,32);
	f_read(&xreadFile,&blockalign,2,&bytesread);
	printf("format=%d BlockAlign = %d\n",format,blockalign);
	f_lseek(&xreadFile,44);
	f_read(&xreadFile,&sample[0],4,&bytesread);

	printf("first stereo sample = %d %d\n",sample[0],sample[1]);
	f_lseek(&xreadFile,44+65536);
	f_read(&xreadFile,PlayBuff,PLAY_BUFF_SIZE*2,&bytesread);

	//  HAL_SAI_Transmit_DMA(&hsai_BlockB1, (uint8_t *) PlayBuff, PLAY_BUFF_SIZE);
	HAL_SAI_Transmit_IT(&hsai_BlockB1, (uint8_t *) PlayBuff, PLAY_BUFF_SIZE);
	/* Start loopback */
	  while(1)
	  {
	  //  BSP_LED_Toggle(LED3);


	    /* Wait a callback event */

		osSemaphoreWait(myBinarySemSAIHandle, osWaitForever);
	//	vTaskDelay(1);

	//	HAL_SAI_Transmit_IT(&hsai_BlockB1, (uint8_t *) PlayBuff, PLAY_BUFF_SIZE);

	    UpdatePointer = -1;

	    /* Upate the first or the second part of the buffer */

	    if(fr==FR_OK)
	    {
	  //  f_lseek(&xreadFile,44+65536);

	    f_read(&xreadFile,PlayBuff,PLAY_BUFF_SIZE*2,&bytesread);
	    printf("bytes read %d \n",bytesread);
	    if(UpdatePointer != -1)
	     {
	    	      /* Buffer update time is too long compare to the data transfer time */
	    	      Error_Handler();
	    	      break;
	    }

	    /* check the end of the file */
	    if(bytesread < PLAY_BUFF_SIZE*2)
	    {
	    	f_lseek(&xreadFile,44);
	    }


	    }

	  }

}
/**
  * @brief Tx Transfer completed callbacks.
  * @param  hsai : pointer to a SAI_HandleTypeDef structure that contains
  *                the configuration information for SAI module.
  * @retval None
  */
/*
void HAL_SAI_TxCpltCallback(SAI_HandleTypeDef *hsai)
{

	HAL_SAI_Transmit_IT(&hsai_BlockB1, (uint8_t *) PlayBuff, PLAY_BUFF_SIZE);
	UpdatePointer = PLAY_BUFF_SIZE/2;



	int txStatus = 0;
	BaseType_t xHigherPriorityTaskWoken;

	txStatus = xSemaphoreGiveFromISR(myBinarySemSAIHandle, &xHigherPriorityTaskWoken);

			if (pdPASS == txStatus) {
						      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
						    }
}
*/
void HAL_SAI_RxHalfCpltCallback(SAI_HandleTypeDef *hsai)
{
	 osSemaphoreRelease(myBinarySemSPDIFHandle);
}

void HAL_SAI_RxCpltCallback(SAI_HandleTypeDef *hsai)
{
	 osSemaphoreRelease(myBinarySemSPDIFHandle);
}
/**
  * @brief Tx Transfer Half completed callbacks
  * @param  hsai : pointer to a SAI_HandleTypeDef structure that contains
  *                the configuration information for SAI module.
  * @retval None
  */
/*
void HAL_SAI_TxHalfCpltCallback(SAI_HandleTypeDef *hsai)
{

  UpdatePointer = 0;
  int txStatus = 0;

  BaseType_t xHigherPriorityTaskWoken;

  	txStatus = xSemaphoreGiveFromISR(myBinarySemSAIHandle, &xHigherPriorityTaskWoken);

  			if (pdPASS == txStatus) {
  						      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
  						    }

}
*/
