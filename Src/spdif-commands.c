/*
 * spdif-commands.c

 *
 *  Created on: Jan 29, 2019
 *      Author: kevin
 */
#include "main.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include "cmsis_os.h"
#include "fatfs.h"
#include "smr_glo.h"


typedef struct
{
	union {
		__IO uint32_t   SR;           /*!< Control register,                   Address offset: 0x00 */
		struct {
			__IO uint32_t
			RXNE:1,//Read data register not empty
			CSRNE:1,//The Control Buffer register is not empty
			PERR:1,//Parity error
			OVR:1,//Overrun error
			SBD:1,//Synchronization Block Detected
			SYNCD:1,//Synchronization Done
			FERR:1,//Framing error
			SERR:1,//Synchronization error
			TERR:1,//Time-out error
			RES:7,
			WIDTH5:16;
		};
	};
} SPDIFRX_SR;

typedef struct
{
	union {
		__IO uint32_t   CR;           /*!< Control register,                   Address offset: 0x00 */
		struct {
			__IO uint32_t
			SPDIFRXEN:2,//Peripheral block enable
			RXDMAEN:1,//Receiver DMA enable for data flow
			RXSTEO:1,//Stereo mode
			DRFMT:2,//RX data format
			PMSK:1,//Mask parity error bit
			VMSK:1,//Mask of validity bit
			CUMSK:1,//Mask of channel status and user bits
			PTMSK:1,//Mask of preamble type bits
			CBDMAEN:1,//Control buffer DMA enable for control flow
			CHSEL:1,//Channel selection
			NBTR:2,//Maximum allowed re-tries during synchronization phase
			WFA:1,// Wait for activity
			RES1:1,
			INSEL:3,//SPDIFRX input selection
			RES2:13;
		};
	};
} SPDIFRX_CR;

static void prvSPDIFTask( void *pvParameters );

static portBASE_TYPE prvSPDIFReadStats( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvSPDIFRec( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvSPDIFChannel( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

extern SPDIFRX_HandleTypeDef hspdif;
extern SAI_HandleTypeDef hsai_BlockB1;
extern SAI_HandleTypeDef hsai_BlockA1;
int8_t spdif=1;

static xTaskHandle xSPDIFTask = NULL;
uint16_t   SpdifBuff[REC_BUFF_SIZE];
extern osSemaphoreId myBinarySemSPDIFHandle;
__IO int16_t  RecUpdatePointer = -1;
FRESULT fr=FR_INVALID_PARAMETER;
FIL xFile;
void *pSmrPersistentMem = NULL;
void *pSmrScratchMem = NULL;
static  smr_static_param_t smr_static_param;
smr_dynamic_param_t smr_dynamic_param;
extern const uint32_t smr_persistent_mem_size;
#define AUDIO_CFG_SMR_AVGTIME_DEFAULT              10
static  buffer_t BufferHandler;
static  buffer_t *pBufferHandler = &BufferHandler;

/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvSPDIFReadStatCommandDefinition =
{
		( const int8_t * const ) "spdif-stats", /* The command string to type. */
		( const int8_t * const ) "spdif-stats\r\n",
		prvSPDIFReadStats, /* The function to run. */
		0 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSPDIRecCommandDefinition =
{
		( const int8_t * const ) "spdif-rec", /* The command string to type. */
		( const int8_t * const ) "spdif-rec\r\n",
		prvSPDIFRec, /* The function to run. */
		1 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSPDIFChannelCommandDefinition =
{
		( const int8_t * const ) "input", /* The command string to type. */
		( const int8_t * const ) "input:<channel>\r\nINO IN1 IN2 ADC\r\n",
		prvSPDIFChannel, /* The function to run. */
		1 /* No parameters are expected. */
};


void vSPDIFTaskStart( void )
{
	xTaskCreate( 	prvSPDIFTask,				/* The task that implements the command console. */
			( const int8_t * const ) "xSPDIFTask",		/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
			configUART_COMMAND_CONSOLE_STACK_SIZE,	/* The size of the stack allocated to the task. */
			NULL,									/* The parameter is not used, so NULL is passed. */
			configUART_COMMAND_CONSOLE_TASK_PRIORITY,/* The priority allocated to the task. */
			&xSPDIFTask );					/* Used to store the handle to the created task. */
}

static void prvSPDIFTask( void *pvParameters )
{

	/* Open the file afile.bin for writing. */


	pSmrPersistentMem = pvPortMalloc(smr_persistent_mem_size); /* smr_persistent_mem_size  0x188 */
	if(pSmrPersistentMem==NULL)printf("Alloc failed\n");
	pSmrScratchMem = pvPortMalloc(smr_scratch_mem_size);       /* smr_scratch_mem_size  0xF04 */
	if(pSmrScratchMem==NULL)printf("Alloc failed\n");

	int read_pos;
	int cycles=0;
	int32_t error = SMR_ERROR_NONE;


	error = smr_reset(pSmrPersistentMem, pSmrScratchMem);

	if(error<0)printf("error\n");

	smr_static_param.sampling_rate = 48000;

	error = smr_setParam(&smr_static_param, pSmrPersistentMem);
	smr_dynamic_param.enable = 1;                        /* SMR module enabler */


	smr_dynamic_param.averaging_time = AUDIO_CFG_SMR_AVGTIME_DEFAULT;
	smr_dynamic_param.filter_type = SMR_PREFILTER_NONE;
	error = smr_setConfig(&smr_dynamic_param, pSmrPersistentMem);

	if(error<0)printf("error\n");

	BufferHandler.nb_bytes_per_Sample =2; /* 8 bits in 0ne byte */


	BufferHandler.nb_channels = 2; /* stereo */

	BufferHandler.buffer_size = 1024; /* just half buffer is process (size per channel) */


	BufferHandler.mode = INTERLEAVED;


	//	 if(spdif)HAL_SPDIFRX_ReceiveDataFlow_DMA(&hspdif,SpdifBuff,REC_BUFF_SIZE/2);
	//	 else HAL_SAI_Receive_DMA(&hsai_BlockA1, (uint8_t *) SpdifBuff, REC_BUFF_SIZE);
	HAL_SAI_Transmit_DMA(&hsai_BlockB1, (uint8_t *) SpdifBuff, REC_BUFF_SIZE);

	/*
	 uint8_t input =  CReadInt("input");

	 if(input<3)
	 {
		 SPDIFRX_CR* CR=(SPDIFRX_CR*)&hspdif.Instance->CR;
		 CR->INSEL=input;
	 }
	 else
	 {
		  HAL_SAI_Receive_DMA(&hsai_BlockA1, (uint8_t *) SpdifBuff, REC_BUFF_SIZE);
	 }
	 */

	while(1)
	{

		if(osSemaphoreWait(myBinarySemSPDIFHandle, 200) != osOK)
		{
			HAL_SPDIFRX_DeInit(&hspdif);
			HAL_SAI_DMAStop(&hsai_BlockA1);
			//	HAL_SAI_DMAStop(&hsai_BlockB1);
			memset(SpdifBuff,0,REC_BUFF_SIZE*2);
			vTaskDelay(2000);
			if(HAL_SPDIFRX_Init(&hspdif)==HAL_OK)
			{
				if(HAL_SPDIFRX_ReceiveDataFlow_DMA(&hspdif,(uint32_t *)SpdifBuff,REC_BUFF_SIZE/2)==HAL_OK)
				{
					//	HAL_SAI_Transmit_DMA(&hsai_BlockB1, (uint8_t *) SpdifBuff, REC_BUFF_SIZE);
				}
			}
			continue;
		}

		read_pos=hsai_BlockB1.XferSize-hsai_BlockB1.hdmarx->Instance->NDTR;

		BufferHandler.data_ptr = SpdifBuff;


		if(cycles++%2)error = smr_process(pBufferHandler, pBufferHandler, pSmrPersistentMem);

		if(cycles%10==0)
		{

			if(error<0)printf("smr_process error=%d\n",error);

			error = smr_getConfig(&smr_dynamic_param, pSmrPersistentMem);
			if(error<0)printf("error\n");
			//	printf("rpos=%d  %ld dB  %ld dB\n",read_pos,(int32_t)((float)smr_dynamic_param.mean_level_left*0.25),(int32_t)((float)smr_dynamic_param.mean_level_right*0.25));
			//	printf("Left mean power:  %ld dB\n",(int32_t)((float)smr_dynamic_param.mean_level_left*0.25));
			//	printf("Right mean power:  %ld dB\n",(int32_t)((float)smr_dynamic_param.mean_level_right*0.25));
		}



		if( fr == FR_OK && RecUpdatePointer > -1)
		{
			/* Write three single characters to the opened file. */
			//   fr = f_write(&xFile, &SpdifBuff[RecUpdatePointer], REC_BUFF_SIZE/2, &byteswritten);

			//    if(fr!=FR_OK)f_close( &xFile );
		}

	}
}

void HAL_SPDIFRX_ErrorCallback(SPDIFRX_HandleTypeDef *hspdif)
{
	printf("spdif error=%d\n",hspdif->ErrorCode);
}
/*
void HAL_SPDIFRX_RxHalfCpltCallback(SPDIFRX_HandleTypeDef *hspdif)
{
	 RecUpdatePointer = 0;
	 osSemaphoreRelease(myBinarySemSPDIFHandle);
}

void HAL_SPDIFRX_RxCpltCallback(SPDIFRX_HandleTypeDef *hspdif)
{

	RecUpdatePointer = REC_BUFF_SIZE/2;
	 osSemaphoreRelease(myBinarySemSPDIFHandle);
}
 */
static portBASE_TYPE prvSPDIFRec( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	int8_t *pcParameter1;
	BaseType_t xParameter1StringLength, xParameter2StringLength, xResult;

	/* Obtain the name of the source file, and the length of its name, from
			    the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					1,
					/* Store the parameter string length. */
					&xParameter1StringLength
			);

	if(  strstr (pcParameter1,".wav") !=NULL )
	{

		fr= f_open(&xFile, pcParameter1,FA_CREATE_ALWAYS | FA_WRITE );
		sprintf(pcWriteBuffer,"Recording stated %s",pcParameter1);
	}
	else
	{
		fr=FR_INVALID_PARAMETER;
		f_close( &xFile );
		strcpy(pcWriteBuffer,"Recording stopped\n");
	}

}

static portBASE_TYPE prvSPDIFReadStats( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{

	SPDIFRX_SR* SR;
	int FS;
	/* Remove compile time warnings about unused parameters, and check the
	write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	write buffer length is adequate, so does not check for buffer overflows. */
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );



	SR=(SPDIFRX_SR*)&hspdif.Instance->SR;

	if(SR->WIDTH5>0)FS=5*128000000/(SR->WIDTH5*64);

	sprintf(pcWriteBuffer,"WIDTH5=%x FS=%d Hz PERR=%d TERR=%d SERR=%d OVR=%d SYNCD=%d\n",SR->WIDTH5,FS,SR->PERR,SR->TERR,SR->SERR,SR->OVR,SR->SYNCD);

	if(SR->OVR) __HAL_SPDIFRX_CLEAR_IT(&hspdif, SPDIFRX_FLAG_OVR);

	return pdFALSE;

}

static portBASE_TYPE prvSPDIFChannel( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{

	int8_t *pcParameter1;
	BaseType_t xParameter1StringLength;
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );
	memset(pcWriteBuffer,0,50);
	if(xSPDIFTask==NULL)vSPDIFTaskStart();
	/* Obtain the name of the source file, and the length of its name, from
				    the command string. The name of the source file is the first parameter. */
	pcParameter1 = FreeRTOS_CLIGetParameter
			(
					/* The command string itself. */
					pcCommandString,
					/* Return the first parameter. */
					1,
					/* Store the parameter string length. */
					&xParameter1StringLength
			);
	HAL_StatusTypeDef hr=HAL_OK;
	//  HAL_SPDIFRX_DMAStop(&hspdif);
	HAL_SPDIFRX_DeInit(&hspdif);
	HAL_SAI_DMAStop(&hsai_BlockA1);
	vTaskDelay(100);

	if(strstr (pcParameter1,"IN0") !=NULL )
	{

		hspdif.Init.InputSelection = SPDIFRX_INPUT_IN0;
		HAL_SPDIFRX_Init(&hspdif);
		hr=HAL_SPDIFRX_ReceiveDataFlow_DMA(&hspdif,(uint32_t *)SpdifBuff,REC_BUFF_SIZE/2);
		sprintf(pcWriteBuffer,"IN0 %d\n",hr);
	}
	else  if(strstr (pcParameter1,"IN1") !=NULL )
	{

		hspdif.Init.InputSelection = SPDIFRX_INPUT_IN1;
		HAL_SPDIFRX_Init(&hspdif);
		hr=HAL_SPDIFRX_ReceiveDataFlow_DMA(&hspdif,(uint32_t *)SpdifBuff,REC_BUFF_SIZE/2);
		sprintf(pcWriteBuffer,"IN1 %d\n",hr);
	}
	else  if(strstr (pcParameter1,"IN2") !=NULL )
	{

		hspdif.Init.InputSelection = SPDIFRX_INPUT_IN2;
		HAL_SPDIFRX_Init(&hspdif);
		hr=HAL_SPDIFRX_ReceiveDataFlow_DMA(&hspdif,(uint32_t *)SpdifBuff,REC_BUFF_SIZE/2);
		sprintf(pcWriteBuffer,"IN2 %d\n",hr);
	}
	else  if(strstr (pcParameter1,"ADC") !=NULL )
	{
		//  HAL_SAI_Init(&hsai_BlockA1);
		vTaskDelay(100);
		hr=HAL_SAI_Receive_DMA(&hsai_BlockA1, (uint8_t *) SpdifBuff, REC_BUFF_SIZE);
		sprintf(pcWriteBuffer,"ADC %d\n",hr);
	}
	return pdFALSE;
}
