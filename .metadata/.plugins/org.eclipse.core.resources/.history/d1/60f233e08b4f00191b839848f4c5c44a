/*
 * SAI-Commands.c
 *

 *
 *  Created on: Mar 25, 2019
 *      Author: kevin
 */
//#include "core_cm4.h"
#include "FreeRTOS.h"

#include "semphr.h"
#include "sai.h"
#include "cmsis_os.h"
#define PLAY_BUFF_SIZE       4096
#define PLAY_HEADER          0x2C
#define AUDIO_FILE_ADDRESS   0x08010000
#define AUDIO_FILE_SIZE      (180*1024)
uint16_t                      PlayBuff[PLAY_BUFF_SIZE];
__IO int16_t  UpdatePointer = -1;
static void prvSAITask( void *pvParameters );
extern SAI_HandleTypeDef hsai_BlockB1;
extern osSemaphoreId myBinarySemSAIHandle;
/* Holds the handle of the task that implements the UART command console. */
static xTaskHandle xSAITask = NULL;
/*-----------------------------------------------------------*/

void vSAITaskStart( void )
{
	xTaskCreate( 	prvSAITask,				/* The task that implements the command console. */
					( const int8_t * const ) "SAITask",		/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
					configUART_COMMAND_CONSOLE_STACK_SIZE,	/* The size of the stack allocated to the task. */
					NULL,									/* The parameter is not used, so NULL is passed. */
					configUART_COMMAND_CONSOLE_TASK_PRIORITY,/* The priority allocated to the task. */
					&xSAITask );					/* Used to store the handle to the created task. */
}
/*-----------------------------------------------------------*/

static void prvSAITask( void *pvParameters )
{
	 uint32_t PlaybackPosition   = PLAY_BUFF_SIZE + PLAY_HEADER;
	 for(int i=0; i < PLAY_BUFF_SIZE; i+=2)
	   {
	     PlayBuff[i]=*((__IO uint16_t *)(AUDIO_FILE_ADDRESS + PLAY_HEADER + i));
	   }
	  HAL_SAI_Transmit_DMA(&hsai_BlockB1, (uint8_t *) PlayBuff, PLAY_BUFF_SIZE);
		//	osSemaphoreWait(myBinarySemSAIHandle, osWaitForever);
	/* Start loopback */
	  while(1)
	  {
	  //  BSP_LED_Toggle(LED3);

	    /* Wait a callback event */
	 //   while(UpdatePointer==-1);
		osSemaphoreWait(myBinarySemSAIHandle, osWaitForever);
	    int position = UpdatePointer;
	    UpdatePointer = -1;

	    /* Upate the first or the second part of the buffer */
	    for(int i = 0; i < PLAY_BUFF_SIZE/2; i++)
	    {
	      PlayBuff[i+position] = *(uint16_t *)(AUDIO_FILE_ADDRESS + PlaybackPosition);
	      PlaybackPosition+=2;
	    }

	    /* check the end of the file */
	    if((PlaybackPosition+PLAY_BUFF_SIZE/2) > AUDIO_FILE_SIZE)
	    {
	      PlaybackPosition = PLAY_HEADER;
	    }

	    if(UpdatePointer != -1)
	    {
	      /* Buffer update time is too long compare to the data transfer time */
	      Error_Handler();
	    }

	  }
}

/**
  * @brief Tx Transfer completed callbacks.
  * @param  hsai : pointer to a SAI_HandleTypeDef structure that contains
  *                the configuration information for SAI module.
  * @retval None
  */
void HAL_SAI_TxCpltCallback(SAI_HandleTypeDef *hsai)
{
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_SAI_TxCpltCallback could be implemented in the user file
   */
	UpdatePointer = PLAY_BUFF_SIZE/2;

	int txStatus = 0;
	BaseType_t xHigherPriorityTaskWoken;

	txStatus = xSemaphoreGiveFromISR(myBinarySemSAIHandle, &xHigherPriorityTaskWoken);

			if (pdPASS == txStatus) {
						      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
						    }
}

/**
  * @brief Tx Transfer Half completed callbacks
  * @param  hsai : pointer to a SAI_HandleTypeDef structure that contains
  *                the configuration information for SAI module.
  * @retval None
  */
void HAL_SAI_TxHalfCpltCallback(SAI_HandleTypeDef *hsai)
{
  /* NOTE : This function Should not be modified, when the callback is needed,
            the HAL_SAI_TxHalfCpltCallback could be implenetd in the user file
   */
  UpdatePointer = 0;
  int txStatus = 0;
  BaseType_t xHigherPriorityTaskWoken;

  	txStatus = xSemaphoreGiveFromISR(myBinarySemSAIHandle, &xHigherPriorityTaskWoken);

  			if (pdPASS == txStatus) {
  						      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
  						    }

}
