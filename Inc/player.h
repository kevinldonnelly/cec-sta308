#ifndef PLAYER_H_
#define PLAYER_H_

#define END_OF_FILE	-1
#define READ_ERROR	-2

typedef enum {
	MP3,
	WAV,
	UNSUPPORTED
} Tfile_type;

typedef enum {
	PLAYING,
	STOPPED,
	PAUSED
} Tplayer_state;

typedef enum {
	PLAY,
	NEXT,
	STOP,
	PREV,
	EXIT
} Tplayer_action;

//void play(Tfile_list* file_list);


#endif /* PLAYER_H_ */
