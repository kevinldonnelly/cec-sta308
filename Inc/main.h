/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "mp3dec.h"
#define RX_BUFFER_SIZE 1024
#define REC_BUFF_SIZE       8192
#define DECODED_MP3_FRAME_SIZE	MAX_NGRAN * MAX_NCHAN * MAX_NSAMP
#define OUT_BUFFER_SIZE			2 * DECODED_MP3_FRAME_SIZE
#define LO_EMPTY 0x01  //low half of buffer empty
#define HI_EMPTY 0x02  //high half of buffer empty
typedef struct UARTReceiveBuffer
{
uint8_t cRxedChar[RX_BUFFER_SIZE];
uint8_t nBytes;
} URB;

typedef struct ONEWIRE_DEF {
int Task_suspeneded;
int PinInput;
}ONEWIRE;

typedef struct {
  uint32_t ChunkID;       /* 0 */
  uint32_t FileSize;      /* 4 */
  uint32_t FileFormat;    /* 8 */
  uint32_t SubChunk1ID;   /* 12 */
  uint32_t SubChunk1Size; /* 16*/
  uint16_t AudioFormat;   /* 20 */
  uint16_t NbrChannels;   /* 22 */
  uint32_t SampleRate;    /* 24 */

  uint32_t ByteRate;      /* 28 */
  uint16_t BlockAlign;    /* 32 */
  uint16_t BitPerSample;  /* 34 */
  uint32_t SubChunk2ID;   /* 36 */
  uint32_t SubChunk2Size; /* 40 */
}WAVE_FormatTypeDef;



/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
//void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin LL_GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define WIFI_SPI_MOSI_Pin LL_GPIO_PIN_3
#define WIFI_SPI_MOSI_GPIO_Port GPIOC
#define IR_CAP_TIM2_Pin LL_GPIO_PIN_0
#define IR_CAP_TIM2_GPIO_Port GPIOA
#define ONEWIRE_TIM5_CH2_Pin LL_GPIO_PIN_1
#define ONEWIRE_TIM5_CH2_GPIO_Port GPIOA
#define LCD_SPI_SCK_Pin LL_GPIO_PIN_5
#define LCD_SPI_SCK_GPIO_Port GPIOA
#define LCD_SPI_MOSI_Pin LL_GPIO_PIN_7
#define LCD_SPI_MOSI_GPIO_Port GPIOA
#define LCD_DC_Pin LL_GPIO_PIN_5
#define LCD_DC_GPIO_Port GPIOC
#define LDC_CS_Pin LL_GPIO_PIN_0
#define LDC_CS_GPIO_Port GPIOB
#define LCD_RST_Pin LL_GPIO_PIN_1
#define LCD_RST_GPIO_Port GPIOB
#define SCCARD_SDIO_CK_Pin LL_GPIO_PIN_2
#define SCCARD_SDIO_CK_GPIO_Port GPIOB
#define WIFI_SPI_SCK_Pin LL_GPIO_PIN_10
#define WIFI_SPI_SCK_GPIO_Port GPIOB
#define SDCARD_SDIO_D0_Pin LL_GPIO_PIN_8
#define SDCARD_SDIO_D0_GPIO_Port GPIOC
#define WFI_USART_RX_Pin LL_GPIO_PIN_10
#define WFI_USART_RX_GPIO_Port GPIOA
#define TMS_Pin LL_GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin LL_GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define CEC_D10_PB6_Pin LL_GPIO_PIN_15
#define CEC_D10_PB6_GPIO_Port GPIOA
#define TST_2_Pin LL_GPIO_PIN_10
#define TST_2_GPIO_Port GPIOC
#define SCCARD_SDIO_CMD_Pin LL_GPIO_PIN_2
#define SCCARD_SDIO_CMD_GPIO_Port GPIOD
#define TST_1_Pin LL_GPIO_PIN_3
#define TST_1_GPIO_Port GPIOB
#define WIFI_USART_TX_Pin LL_GPIO_PIN_6
#define WIFI_USART_TX_GPIO_Port GPIOB
#define PWM_SUBWOOFER_Pin LL_GPIO_PIN_8
#define PWM_SUBWOOFER_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
