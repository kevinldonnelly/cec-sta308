#ifndef	_TM1640_H
#define	_TM1640_H

#include	<avr/io.h>
#include	<util/delay.h>
#include	<avr/pgmspace.h>

#define	DATA_COMMAND	0X40
#define	DISP_COMMAND	0x80
#define	ADDR_COMMAND	0XC0
#define Default_light	0x8f

#define SET_BIT(PORT,BIT)	PORT|=(1<<BIT)
#define	CLR_BIT(PORT,BIT)	PORT&=~(1<<BIT)
#define	BIT_IN(DDR,BIT)		DDR&=~(1<<BIT)
#define	BIT_OUT(DDR,BIT)	DDR|=(1<<BIT)

#define	DIN		PD6
#define	SCLK	PD7

#define DIN_high			SET_BIT(PORTD,DIN)
#define DIN_low				CLR_BIT(PORTD,DIN)
#define SCLK_high			SET_BIT(PORTD,SCLK)
#define SCLK_low			CLR_BIT(PORTD,SCLK)

unsigned char tab[]={0,0,0,0,0,0,0,0,0x18,0x3C,0x7E,0xFF,0x18,0x18,0x18,0x18,};
unsigned char tab0[]={0x1C,0x22,0x42,0x84,0x84,0x42,0x22,0x1C};//心形外框
unsigned char tab1[]={0x1C,0x3E,0x7E,0xFC,0xFC,0x7E,0x3E,0x1C};//心形
unsigned char tab2[]={0x18,0x24,0x42,0x81};
void Start(void)
{
	SCLK_high;
	_delay_us(5);
	DIN_high;
	_delay_us(5);
	DIN_low;
	_delay_us(5);
//	SCLK_low;
//	_delay_us(5);
}
void Stop(void)
{
	SCLK_high;
	_delay_us(5);
	DIN_low;
	_delay_us(5);
	DIN_high;
	_delay_us(5);
}
void TM1640_Write(unsigned char	DATA)			//写数据函数
{
	unsigned char i;
	for(i=0;i<8;i++)
	{
		SCLK_low;
		if(DATA&0X01)
			DIN_high;
		else
			DIN_low;
		_delay_us(5);
		DATA>>=1;
		_delay_us(5);
		SCLK_high;
		_delay_us(5);
	}
}
void Write_COM(unsigned char cmd)		//发送命令字
{
	Start();
	TM1640_Write(cmd);
	Stop();
}
void Write_DATA(unsigned char add,unsigned char DATA)		//指定地址写入数据
{
	Write_COM(0x44);
	Start();
	TM1640_Write(0xc0|add);
	TM1640_Write(DATA);
	Stop();
}
void cls_TM1640(void)
{
	unsigned char i;	
	Write_COM(0x40);//连续地址模式
	Start();
	TM1640_Write(0xc0);
	for(i=0;i<16;i++)
		TM1640_Write(0x00);
	Stop();
}
void init_TM1640(void)
{
	cls_TM1640();
	Write_COM(Default_light);//亮度
}
#endif
