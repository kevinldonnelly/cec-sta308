/**********************************************************************
* � 2005 Microchip Technology Inc.
*
* FileName:        i2sDrv.c
* Dependencies:    Header (.h) files if applicable, see below
* Processor:       dsPIC33Fxxxx/PIC24Hxxxx
* Compiler:        MPLAB� C30 v3.00 or higher
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip's
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP'S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
* REVISION HISTORY:
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Author            Date      Comments on this revision
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Veena K.          03/23/06  First release of source file
*
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* ADDITIONAL NOTES:
*
**********************************************************************/

#if defined(__dsPIC33F__)
#include "p33Fxxxx.h"
#elif defined(__PIC24H__)
#include "p24Hxxxx.h"
#endif

#include "i2sDrv.h"
#include "tglPin.h"

void ProcessDciRxSamples(unsigned int * DciRxBuffer);

unsigned int dci1RxBuffA[16] __attribute__((space(dma)));
unsigned int dci1RxBuffB[16] __attribute__((space(dma)));
unsigned int dci1TxBuffA[16] __attribute__((space(dma)));
unsigned int dci1TxBuffB[16] __attribute__((space(dma)));


/*=============================================================================
i2sInit(): Initialise DCI peripheral for I2S Interface
=============================================================================*/

void i2sInit (void)
{

/*
In this section we will set up the DCI module for I2S operation to interface 
with a stereo audio codec sampling data at 48 KHz. The timing diagram is 
provided in Fig 1 below:
                                  FIGURE 1
       
					   _______________________________
      |		 	    	          |	                          |
COFS: |___________________________________| 	     	                  |
       _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _    
CSCK:_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_|
          |<--------Left Channel Data---->|<----Right Channel Data------->|
          |<---------32 bits------------->|<---------32 bits------------->|
          |<---TXBUF0---->|<----TXBUF1--->|<---TXBUF2---->|<----TXBUF3--->|
          |<---RXBUF0---->|<----RXBUF1--->|<---RXBUF2---->|<----RXBUF3--->|
          |<--TimeSlot0-->|<--TimeSlot1-->|<--TimeSlot0-->|<--TimeSlot1-->|
          |<--------------------1/Fs = 20.8 microseconds----------------->|
*/

//DCI Control Register DCICON1 Initialization
	DCICON1bits.CSCKD = 0;	// Serial Bit Clock (CSCK pin) is output	
	DCICON1bits.CSCKE = 1;	// Data changes on rising edge sampled on rising edge of CSCK
	DCICON1bits.COFSD = 0;	// Frame Sync Signal is output	
	DCICON1bits.UNFM = 0;	// Transmit �0�s on a transmit underflow
	DCICON1bits.CSDOM = 0;	// CSDO pin drives �0�s during disabled transmit time slots
	DCICON1bits.DJST = 0;	// Data TX/RX is begun one serial clock cycle after frame sync pulse
	DCICON1bits.COFSM = 1;	// Frame Sync Signal set up for I2S mode 

#if	LOOPBACKMODE
	DCICON1bits.DLOOP = 1;	
#endif


// DCI Control Register DCICON2 Initialization
	DCICON2bits.BLEN = 0;	// One data word will be buffered between interrupts	
	DCICON2bits.COFSG = 1;	// Data frame has 2 words  viz., LEFT & RIGHT samples
	DCICON2bits.WS = 15;	// Data word size is 16 bits


// DCI Control Register DCICON3 Initialization
	DCICON3 = BCG_VAL;	    // Set up CSCK Bit Clock Frequency

// Transmit Slot Control Register Initialization
	TSCONbits.TSE0 = 1;	    // Transmit on Time Slot 0	
	TSCONbits.TSE1 = 1;	    // Transmit on Time Slot 1	

//Receiver Slot Control Register Initialization
	RSCONbits.RSE0 = 1;	    // Receive on Time Slot 0	
	RSCONbits.RSE1 = 1;	    // Receive on Time Slot 1	

// Force First two words to fill-in buffer/shift register
    DMA0REQbits.FORCE=1;
    while(DMA0REQbits.FORCE==1);
  

// Disable DCI Interrupt and Enable DCI module
	IFS3bits.DCIIF=0;
    IEC3bits.DCIIE=0; 

	DCICON1bits.DCIEN = 1; 

    tglPinInit();

}

/*=============================================================================
_DMA0Init(): Initialise DMA0 for DCI Data Transmission 
=============================================================================*/
// DMA0 configuration
// Direction: Read from DMA RAM and write to peripheral address 0x298 (TXBUF0 register)
// AMODE: Register Indirect with Post-Increment mode
// MODE: Continuous, Ping-Pong Enabled
// IRQ: DCI
void dma0Init(void)
{

	DMA0CON = 0x2002;					
	DMA0CNT = 15;						
	DMA0REQ = 0x003C;					

	DMA0PAD = (volatile unsigned int) &TXBUF0;
	DMA0STA= __builtin_dmaoffset(dci1TxBuffA);
	DMA0STB= __builtin_dmaoffset(dci1TxBuffB);
	
	
	IFS0bits.DMA0IF  = 0;			// Clear DMA interrupt
	IEC0bits.DMA0IE  = 1;			// Enable DMA interrupt
	DMA0CONbits.CHEN = 1;			// Enable DMA Channel	
}

/*=============================================================================
DMA1Init(): Initialise DMA1 to receive DCI data
==============================================================================*/
// DMA1 configuration
// Direction: Read from peripheral address 0-x290 (RXBUF0) and write to DMA RAM 
// AMODE: Register Indirect with Post-Increment mode
// MODE: Continuous, Ping-Pong Enabled
// IRQ: DCI Reception
void dma1Init(void)
{
	
	DMA1CON = 0x0002;				
	DMA1CNT = 15;						
	DMA1REQ = 0x003C;					

	DMA1PAD = (volatile unsigned int) &RXBUF0;
	DMA1STA= __builtin_dmaoffset(dci1RxBuffA);
	DMA1STB= __builtin_dmaoffset(dci1RxBuffB);
	
	
	IFS0bits.DMA1IF  = 0;			// Clear DMA interrupt
	IEC0bits.DMA1IE  = 1;			// Enable DMA interrupt
	DMA1CONbits.CHEN = 1;			// Enable DMA Channel		

}





/*=============================================================================
_DMA0Interrupt(): DCI Transmit Interrupt Handler
=============================================================================*/
unsigned int RxDmaBuffer = 0;

void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void)
{
		tglPin();					// Toggle PORTA, BIT0	   		

        IFS0bits.DMA0IF = 0;		//Clear the DMA0 Interrupt Flag
}

/*=============================================================================
DMA1Interrupt(): DCI Receive Interrupt Handler
==============================================================================*/
void __attribute__((interrupt, no_auto_psv)) _DMA1Interrupt(void)
{
	if(RxDmaBuffer == 0)
	{
		ProcessDciRxSamples(&dci1RxBuffA[0]);

	}
	else
	{
		ProcessDciRxSamples(&dci1RxBuffB[0]);
	}

	RxDmaBuffer ^= 1;

	tglPin();					// Toggle RA6	
    IFS0bits.DMA1IF = 0;		// Clear the DMA0 Interrupt Flag
}

void ProcessDciRxSamples(unsigned int * DciRxBuffer)
{
	/* Do something with DCI Samples */
}



/*=============================================================================
initI2sBuff(): Initialise I2S Transmit and Receive Buffer
==============================================================================*/
void initI2sBuff(void)
{
unsigned int i;
    for(i=0;i<16;i++)
        dci1TxBuffA[i]=i;
    for(i=0;i<16;i++)
        dci1TxBuffB[i]=16+i;        
        
   	for(i=0;i<16;i++){
        dci1RxBuffA[i]=0xDEED;
        dci1RxBuffB[i]=0xDEED;
		}	
}


