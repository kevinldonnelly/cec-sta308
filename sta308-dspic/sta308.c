
#include <p33Fxxxx.h>
#define FCY  6144000
#define BAUDRATE 9600
#define BRGVAL ((FCY/BAUDRATE)/16)-1

#define REGMAX	28
#define COEFFMAX 200

#include <dci.h>
#include <uart.h>
#include <dsp.h>
#include <delay.h>
#include <xlcd.h>
#include <stdio.h>
#include <timer.h>
#include <outcompare.h>
#include <comparator.h>
#include <spi.h>
#include <string.h>
#include <i2c.h>
#include <pps.h>
#include <adc.h>
#include "libpic30.h"
#include "GenericTypeDefs.h"
#include "tm1640.h"

#include "DEE Emulation 16-bit.h"
#define INPUT 1
#define OUTPUT 0

void InitUart();
void InitSPI();
void SaveSTA308ToEEPROM();
void LoadSTA308FromEEPROM();
unsigned char Buf[32];
long uart_timeout;
int i;
int temp;
unsigned char firsttime;
char printbuffer[64];

int cmp;
const  char regtab[]={0x03,0x06,0x07,0x08,0x09,0x0A,0x0B,'\0'};

const unsigned char tab3[]={1,2,4,8,16,32,64,128};
const unsigned char tab4[]={1,3,7,15,31,63,127,255};

unsigned char i2cbuffer[64];

void __attribute__((interrupt,shadow, no_auto_psv)) _U1RXInterrupt(void)
{
_U1RXIF = 0; // clear UART Rx interrupt flag
i++;
uart_timeout=0;
}

void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt(void)
{
    _U1TXIF = 0; // clear UART Tx interrupt flag
}

void __attribute__((interrupt, no_auto_psv))_CMPInterrupt (void)
{
  	_CMIF = 0; //Comparator Interrupt Flag Status bit 

}


void Initi2c()
{
 
// 	DataEEInit();

unsigned int config2, config1;
 
  /* Baud rate is set for 100 Khz */
  config2 = 0x3B;
  /* Configure I2C for 7 bit address mode */
  config1 =I2C1_ON & I2C1_IDLE_CON & I2C1_CLK_HLD & I2C1_IPMI_DIS & I2C1_7BIT_ADD &I2C1_SLW_DIS & I2C1_SM_DIS &I2C1_GCALL_DIS & I2C1_STR_DIS & I2C1_NACK & I2C1_ACK_DIS & I2C1_RCV_DIS & I2C1_STOP_DIS & I2C1_RESTART_DIS &I2C1_START_DIS;
  OpenI2C1(config1,config2);
  IdleI2C1();


}

void InitADC()
{

unsigned int config1;//AD1CON1 = config1;
unsigned int config2;//AD1CON2 = config2;
unsigned int config3;//AD1CON3 = config3;
unsigned int config4;//AD1CON4 = config4;	

unsigned int configport_h;//AD1PCFGH = configport_h;
unsigned int configport_l;//AD1PCFGL = configport_l;
unsigned int configscan_h;//AD1CSSH = configscan_h;
unsigned int configscan_l;//AD1CSSL = configscan_l;


config1 = ADC_MODULE_ON & ADC_IDLE_STOP & ADC_ADDMABM_ORDER & ADC_AD12B_12BIT & ADC_FORMAT_SIGN_INT & ADC_CLK_TMR & ADC_AUTO_SAMPLING_ON  & ADC_MULTIPLE;
 
config2 = ADC_VREF_AVDD_AVSS & ADC_SCAN_ON & ADC_SELECT_CHAN_0123 & ADC_DMA_ADD_INC_1;
 
config3 =  ADC_CONV_CLK_SYSTEM & ADC_SAMPLE_TIME_3 & ADC_CONV_CLK_2Tcy;
 
config4 = ADC_DMA_BUF_LOC_32;
 
configport_h = ENABLE_ALL_DIG_16_31;
configport_l = ENABLE_AN0_ANA;//ENABLE_AN0_ANA
 
configscan_h = SCAN_NONE_16_31;
configscan_l = SKIP_SCAN_AN1 & SKIP_SCAN_AN2 & SKIP_SCAN_AN3 & SKIP_SCAN_AN4 & SKIP_SCAN_AN5;
 
OpenADC1(config1,config2,config3,config4,configport_l,configport_h, configscan_h,configscan_l);


}

void InitCmp()
{

ConfigIntCMP(CMP_INT_ENABLE,CMP_INT_PR7);

_C2NEG=0;//C2IN+ 0 = Input is connected to C2IN-
_C2POS=0;//CVREF 0 = Input is connected to CVREFIN
_C2EN=1;

_CVRSS=1;  //0 CVRSRC = AVDD � AVSS, 1 CVRSRC = VREF+ � VREF-
_CVREN=1;

//_CVRR=1;//If CVRR = 1: Voltage Reference = ((CVR<3:0>)/24) x (CVRSRC)
_CVRR=0;//Voltage Reference = (CVRSRC/4) + ((CVR<3:0>)/32) x (CVRSRC)

_CVR0=1;
_CVR1=1;
_CVR2=1;
_CVR3=0;

_CVROE=0;//CVREF voltage level is disconnected from CVREF pin

_C2EVT=0;
_C2OUTEN=0;

//ConfigCMPCVref(CMP_VRef_ENABLE|CMP_VRef_OUTPUT_ENABLE| CMP_VRef_SELECT_24_STEPS|CMP_Vrsrc_VrefPos_VrefNeg| CMP_0p0CVrsrc_OR_0p25CVrsrc);

//ConfigCMP(CMP_IDLE_CON|CMP2_NO_CHANGE|CMP1_NO_CHANGE| CMP2_ENABLE|CMP1_ENABLE|CMP2_OUTPUT_ENABLE| CMP1_OUTPUT_ENABLE|CMP2_INV_OUTPUT|CMP1_INV_OUTPUT| CMP2_NEG_IP_Vin_Neg|CMP2_POS_IP_Vin_Pos| CMP1_NEG_IP_Vin_Neg|CMP1_POS_IP_Vin_Pos);

}

int i2cMasterWriteChar(unsigned char address,unsigned char c) {

 	int x=0;
	IdleI2C1();
    // We append to a buffer
    StartI2C1();
    // Wait till Start sequence is completed
  /* Wait till Start sequence is completed */
  	 while (_SEN)
	{
	__delay_us(1);
	if (x++ > 20) break;
	}

 	/* Clear interrupt flag */
  	_MI2C1IF = 0;

    // Adress
    MasterWriteI2C1(address);
 /* Wait till address is transmitted */
  	while(_TBF);  // 8 clock cycles

	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;

	while(_ACKSTAT);

 //   IdleI2C1();

    // Data
    MasterWriteI2C1(c);
    StopI2C1();
  /* Wait till stop sequence is completed */
  	while(_PEN);
	
	return x;
   
}

unsigned char i2cMasterReadChar(unsigned char address) {
    //	i2cMasterWriteChar(address, I2C_SLAVE_FAKE_WRITE);
    //	delaymSec(50);
     IdleI2C1();

    StartI2C1();
    IdleI2C1();

    // TEST
    /*
MasterWriteI2C(address);
    WaitI2C();
    StopI2C();
    WaitI2C();
    StartI2C();
    WaitI2C();
     */

    // send the address again with read bit
    MasterWriteI2C1(address | 0x01);
     IdleI2C1();

    unsigned char data = MasterReadI2C1();

    StopI2C1();
    return data;
}


unsigned char i2cMasterReadRegisterValue(unsigned char address,unsigned char commandRegister) {
    // Set the register command
	int x = 0;
   	IdleI2C1();
	_ACKDT=0;
    StartI2C1();
   	while (_SEN)
	{ 
		__delay_us(1);
		
		if (x++ > 20) 
			break; 
	}
	x=0;
	_MI2C1IF = 0;
    // send the address
    MasterWriteI2C1(address);
	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;

	while(_ACKSTAT);

   //	IdleI2C1();
    // send the register
    MasterWriteI2C1(commandRegister);
   
	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;

	while(_ACKSTAT);
    // Read the register command
    StartI2C1();
    while (_SEN)
	{
	__delay_us(1);
	if (x++ > 20) break;
	}
	_MI2C1IF = 0;
    // send the address again with read bit
    MasterWriteI2C1(address | 0x01);

	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;
	while(_ACKSTAT);

    IdleI2C1();
    // read the data
    unsigned char data = MasterReadI2C1();
    StopI2C1();
 	while(_PEN);

    return data;
}

unsigned char i2cMasterReadRegisterValues(unsigned char address,unsigned char commandRegister,unsigned char* rdptr,int length) {
    // Set the register command
	int x = 0;
	unsigned int i2c_data_wait=152;
  	IdleI2C1();
	_ACKDT=0;
    StartI2C1();
   	while (_SEN)
	{ 
		__delay_us(1);
		
		if (x++ > 20) 
			break; 
	}
	x=0;
	_MI2C1IF = 0;
    // send the address
    MasterWriteI2C1(address);
	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;

	while(_ACKSTAT);

   //	IdleI2C1();
    // send the register
    MasterWriteI2C1(commandRegister);
   
	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;

	while(_ACKSTAT);
    // Read the register command
    StartI2C1();
    while (_SEN)
	{
	__delay_us(1);
	if (x++ > 20) break;
	}
	_MI2C1IF = 0;
    // send the address again with read bit
    MasterWriteI2C1(address | 0x01);

	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;
	while(_ACKSTAT);

    IdleI2C1();
    // read the data
    MastergetsI2C1(length, rdptr, i2c_data_wait);
    StopI2C1();
 	while(_PEN);

	return 0;
  
}



int i2cMasterWriteRegisterValue(unsigned char address,unsigned char reg,unsigned char data) {

   int x=0;
	IdleI2C1();

	StartI2C1();
  // Wait till Start sequence is completed
	while (_SEN)
	{ 
		__delay_us(1);
		
		if (x++ > 20) 
			break; 
	}
	_MI2C1IF = 0;
  
  
    MasterWriteI2C1(address);
	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;

	while(_ACKSTAT);

  
    MasterWriteI2C1(reg);
	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;

	while(_ACKSTAT);



	_MI2C1IF = 0;
    // command
    MasterWriteI2C1(data);
  	while(_ACKSTAT);

    StopI2C1();
	while(_PEN);

	return x;
}


int i2cMasterWriteRegisterValues(unsigned char address,unsigned char reg,unsigned char* data,int length) {

   int x=0;
	IdleI2C1();

	StartI2C1();
  // Wait till Start sequence is completed
	while (_SEN)
	{ 
		__delay_us(1);
		
		if (x++ > 20) 
			break; 
	}
	_MI2C1IF = 0;
  
  
    MasterWriteI2C1(address);
	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;

	while(_ACKSTAT);

  
    MasterWriteI2C1(reg);
	while(_TBF);  // 8 clock cycles
	while(!_MI2C1IF); // Wait for 9th clock cycle

	_MI2C1IF = 0;

	while(_ACKSTAT);



	_MI2C1IF = 0;

    // command
   for(x=0; x< length; x++)                          //transmit data until null char
    {
        if(MasterputcI2C1(data[x]) == -1)return -3;                          //return with write collison error
		while(_ACKSTAT);
        IdleI2C1();
       
    }


  

    StopI2C1();
	while(_PEN);

	return x;
}


int main(void)
{
unsigned char r;

char command=0;
UINT8_BITS address;
char subaddress;
char nbytes;
char read;
char rmax;
char DataByte=0;
char BytesReceived=0;
_TRISA4=0;
_PLLPRE=0;
_PLLPOST=1;
_PLLDIV=1;
address.Val=0;




//_PCFG5   =    1; 
//_PCFG4   =    1; 
AD1PCFGL = 0xFFFF;

_TRISB8=INPUT; 
_TRISB9=INPUT; 

InitUart(); //FCY=0.5*osc*PLLDIV/(PLLPRE*PLLPOST)
Initi2c();
InitCmp();

DataEEInit();
dataEEFlags.val = 0;
//LoadSTA308FromEEPROM();

/*
firsttime=DataEERead(255);
while(BusyUART1());

printf("firsttime = %d \n\r",firsttime);

readee=DataEERead(254);

if(readee!=62)
{
printf("Read 254 = %d not 62 \n\r",readee);
DataEEWrite(62,254);
}

printf("Read 254 = %d \n\r",DataEERead(254));

*/



i=0;
while(1)
{
ClrWdt();
_RA4=!_RA4;

if(uart_timeout++ >12800)
{
while(DataRdyUART1())temp= ReadUART1();
uart_timeout=0;
i=0;
command=0;
r=0;
nbytes=0;
rmax=0;
read=0;
subaddress=-1;
address.Val=0;
memset(Buf,0,30);
BytesReceived=0;
CloseI2C1();
init_TM1640();
Initi2c();
}



/* check for receive errors */
if(U1STAbits.FERR == 1)
{
continue;
}

if(U1STAbits.OERR == 1)
{
U1STAbits.OERR = 0;
continue;
}




if(_C2EVT==1)
{

_C2EVT=0;
_CMIF = 0;
__delay_ms(50);
printf("Comparitor event=%d %d\n\r",_C2OUT,cmp++);

}





while(DataRdyUART1() )
{
	Buf[r] = ReadUART1();
	if(r==0)command=Buf[0];
	BytesReceived++;
	if(command ==0x53)
	{
		if(r==1)
		{
		address.Val=Buf[1];
		read=address.bits.b0;
		if(read)rmax=1;
		else rmax=2;
		}
		else if(r==2)DataByte=Buf[2];
		
	}
	else if(command==0x54)
	{	
		if(r==1)
		{
		address.Val=Buf[1];
		read=address.bits.b0;
		if(read)rmax=1;
		else rmax=2;
		}
		else if(r==2)
		{
		nbytes=Buf[2];
		}
	}
	else if(command==0x55)
	{
		if(r==1)
		{
		address.Val=Buf[1];
		read=address.bits.b0;
		rmax=3;
		}
		else if(r==2)
		{
		subaddress=Buf[2];
	
		}
		else if(r==3)
		{
		nbytes=Buf[3];
		if(read==0)rmax=rmax+nbytes;
		}
	}
	else if(command==0x13)
	{
		SaveSTA308ToEEPROM();
	}
if(r > rmax)break;
r++;	
} 






if( r>0 && address.Val>0 && rmax>0 && r > rmax )
{

//if(read==0 && r>1 && command==0x53)i2cMasterWriteChar(address.Val, DataByte);
int x=0;
unsigned char datar=0;
if(command==0x55 && nbytes==1 )
{
	if(read==0 )
	{
	DataByte=Buf[4];
	x=i2cMasterWriteRegisterValue(address.Val,subaddress, DataByte);
	}
	else if(read==1)
	{
	address.bits.b0=0;
	datar=i2cMasterReadRegisterValue(address.Val, subaddress);
	putcUART1(datar);
	}
}
else if(command==0x55 && nbytes > 1)
{
	if(read==0 )
	{

	i2cMasterWriteRegisterValues(address.Val,subaddress, Buf+4,nbytes);
/*
 	printf("read=%d rmax=%d command=%x r=%d address=%x subaddess=%x bytes=%d DataByte=%x BytesReceived=%d x=%d datarecieved=%x",read,rmax,command,r,address.Val,subaddress,nbytes, DataByte,BytesReceived,x,datar);
	for ( x=0; x< nbytes; x++)
		{
		while(BusyUART1());
		putcUART1(Buf[4+x]);
		}
*/
	}
	else if(read==1)
	{
	address.bits.b0=0;
	memset(i2cbuffer,0,64);
	i2cMasterReadRegisterValues(address.Val, subaddress,i2cbuffer,nbytes);
		for(x=0; x < nbytes; x++)
		{ 
		while(BusyUART1());
		putcUART1(i2cbuffer[x]);
		}
	}

}


//printf("read=%d rmax=%d command=%x r=%d address=%x subaddess=%x bytes=%d DataByte=%x BytesReceived=%d x=%d datarecieved=%x \n\r",read,rmax,command,r,address.Val,subaddress,nbytes, DataByte,BytesReceived,x,datar);

//putsUART1((unsigned int*)Buf);
while(DataRdyUART1())temp= ReadUART1();
r=0;
i=0;
command=0;
address.Val=0;
rmax=0;
read=0;
nbytes=0;
DataByte=0;
memset(Buf,0,30);
BytesReceived=0;
subaddress=0;
}


i=0;
}//while


}

void InitUart()
{
	//unsigned int U1MODEvalue;
	//unsigned int U1STAvalue;
	PPSUnLock;
    iPPSInput(IN_FN_PPS_U1RX,IN_PIN_PPS_RP3);
	iPPSOutput(OUT_PIN_PPS_RP2,OUT_FN_PPS_U1TX);
	//PPSInput(PPS_U1RX, PPS_RP3);
	//PPSOutput(PPS_U1TX,PPS_RP2);
	PPSLock;

	/*Simplex Mode , 8bit, No Parity, 1 bit de stop, TX,RX enabled, CTS,RTS not*/
	U1BRG = BRGVAL; /* baud rate */
	U1MODE = 0x8000; /* TX & RX interrupt modes */
	U1STA = 0x0000; /* operation settings */

	_U1TXIP = 1; // set UART Tx interrupt priority
    _U1TXIF = 0; // clear UART Tx interrupt flag
	_U1TXIE = 0; // enable UART Tx interrupt
	
	
	_U1RXIP = 4; // set UART Rx interrupt priority	
	_U1RXIF = 0; // clear UART Rx interrupt flag
	_U1RXIE = 1; // enable UART Rx interrupt

   _URXISEL=1; //  char  received interupt

    _UTXEN = 1; // TX enabled
	uart_timeout=0;

//	ConfigIntUART1(UART_RX_INT_EN & UART_RX_INT_PR4 & UART_TX_INT_EN  & UART_TX_INT_PR1);

//	U1MODEvalue = UART_EN & UART_IDLE_CON & UART_IrDA_DISABLE & UART_MODE_SIMPLEX & UART_UEN_00 & UART_DIS_WAKE & UART_DIS_LOOPBACK & UART_EN_ABAUD & UART_BRGH_SIXTEEN & UART_NO_PAR_8BIT & UART_1STOPBIT;

	 
//	U1STAvalue  = UART_INT_TX &  UART_IrDA_POL_INV & UART_TX_ENABLE & UART_INT_RX_3_4_FUL & UART_ADR_DETECT_DIS & UART_RX_OVERRUN_CLEAR ;

//	OpenUART1(U1MODEvalue,U1STAvalue,BRGVAL);



}


void SaveSTA308ToEEPROM()
{
unsigned char datar=0;
unsigned int reg;
unsigned char index;
int eeromindex=0;
int x;
	
for (reg=0; reg < REGMAX; reg++)
{
	datar=i2cMasterReadRegisterValue(0x30, reg);
//	DataEEWrite(datar,eeromindex++);
	while(BusyUART1());
	printf("%x %x \n\r",reg,datar);
	ClrWdt();
}

i2cMasterWriteRegisterValue(0x30,0x2C,0);

for(index=0; index< COEFFMAX; index++)
{
	x=i2cMasterWriteRegisterValue(0x30,0x1C,index);
	while(BusyUART1());
	printf("x=%d \n\r",x);


	datar=i2cMasterReadRegisterValue(0x30,0x1D);
//	DataEEWrite(datar,eeromindex++);
	while(BusyUART1());
	printf("%d %x \n\r",index,datar);

	datar=i2cMasterReadRegisterValue(0x30,0x1E);
//	DataEEWrite(datar,eeromindex++);
	while(BusyUART1());
	printf("%d %x \n\r",index,datar);

	datar=i2cMasterReadRegisterValue(0x30,0x1F);
//	DataEEWrite(datar,eeromindex++);
	while(BusyUART1());
	printf("%d %x \n\r",index,datar);
	ClrWdt();
}

}

void LoadSTA308FromEEPROM()
{
char reg;
unsigned int readee;
int eeromindex=0;
int index;
int len = strlen(regtab);
char *ptr;

readee=DataEERead(eeromindex);

printf("regs to write %d reg 0h=%x\n\r",len,readee);

if(readee!=0x83)return;

for(reg=0; reg < REGMAX; reg++)
{

readee=DataEERead(eeromindex++);

ptr=memchr(regtab, reg, len); 

if(ptr!=NULL)
{
//i2cMasterWriteReisterValue(0x30,reg,readee);
while(BusyUART1());
printf("%x %x \n\r",reg,readee);
}
ClrWdt();
}


for(index=0; index < COEFFMAX; index++)
{


//i2cMasterWriteReisterValue(0x30,0x1C,index);

readee=DataEERead(eeromindex++);
while(BusyUART1());
printf("%d %x \n\r",index,readee);

//i2cMasterWriteReisterValue(0x30,0x1D,readee);
readee=DataEERead(eeromindex++);
while(BusyUART1());
printf("%d %x \n\r",index,readee);

//i2cMasterWriteReisterValue(0x30,0x1E,readee);
readee=DataEERead(eeromindex++);
while(BusyUART1());
printf("%d %x \n\r",index,readee);

//i2cMasterWriteReisterValue(0x30,0x1F,readee);

//i2cMasterWriteReisterValue(0x30,0x2C,1);
ClrWdt();

}


}



void InitSPI()
{
unsigned int config1;
unsigned int config2;
unsigned int config3;

config1 = ENABLE_SDO_PIN &
SPI_MODE16_ON &
SPI_SMP_ON &
SPI_CKE_OFF &
SLAVE_ENABLE_OFF &
CLK_POL_ACTIVE_HIGH &
MASTER_ENABLE_ON &
SEC_PRESCAL_7_1 &
PRI_PRESCAL_64_1;
 
config2 = FRAME_ENABLE_OFF &
FRAME_SYNC_OUTPUT &
FRAME_POL_ACTIVE_HIGH &
FRAME_SYNC_EDGE_COINCIDE;
 
config3 = SPI_ENABLE &
SPI_IDLE_CON &
SPI_RX_OVFLOW_CLR;
 
OpenSPI1(config1, config2, config3);

}








