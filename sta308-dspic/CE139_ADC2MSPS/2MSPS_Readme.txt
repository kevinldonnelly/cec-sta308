
		Readme File for Code Example:
              CExxx - 10-bit ADC Sampling at 2.2MSPS 
             ----------------------------------------

This file contains the following sections:
1. Code Example Description
2. Folder Contents
3. Suggested Development Resources
4. Reconfiguring the project for a different dsPIC33F device
5. Revision History

NOTE: PRESENTLY, THE MAXIMUM RELIABLE CONVERSION CLOCK IS Tad=150nS, so the minimum single 
ADC Conversion Time = 12*Tad = 1.8uS (555.5KHz). This technique will therefore provide 1.1MSPs
throughput (max.)

THE ADC DOES NOT RELIABLY SAMPLE (IN TIME) THE INPUT SIGNAL WITH Tad = 75nS (1.1MSPS), EVEN THOUGH
YOU WILL BE ABLE TO ACQUIRE DATA AND VIEW IT

Please see bitmaps from captured acquisitions of a 1Vp-p 32kHz signal using 2.2MSPS and 1.1MSPS settings.
1_1MSPS Plot.bmp

Please also see several bitmaps from oscilloscope traces of RA6 and RA7 being toggled by ADC1 and ADC2 
interrupt service routines to confirm proper interleaving of the sample instants. Note the missing
samples and inconsistent operation at 2.2MSPS(!)


1. Code Example Description:
----------------------------
This test code is designed to sample channel AN0 at 2.2MSPS (single-shot acquisition) using an 
ADC interleaving technique. This will allow 24H/33F devices with 2 ADC modules to double the 
maximum sampling rate vs. a single ADC.

The method requires 2 sample/hold amplifiers from each ADC module. You configure
both ADC, then you start one ADC and enable and wait N cycles to start the second ADC. 
These N cycles will depend on ADC sampling and converting speed, for example if conversions
last 12 Tad cycles (10-bit mode), then we need to wait 6 Tad before enabling second ADC.

Upon each single conversion on ADC1, DMA ch0 reads this data and stores in DPSRAM @ 0x7800. 
ADC1 Interrupt is enabled to toggle a GPIO pin (RA6) on each conversion.
DMA ch0 is set to take 32 samples from ADC1, store in DSPSRAM, autoincrement the DPSRAM pointer. 
When the transfer is complete, the DMA interrupt occurs, which disables the DMA channel and ADC1. 
The main() routine then reads/stores this data in the even addresses in the final 64-word result buffer.

Upon each single conversion on ADC2, DMA ch1 reads this data and stores in DPSRAM @ 0x7840. 
ADC2 Interupt is enabled to toggle another GPIO pin (RA7) on each conversion
DMA ch1 is set to take 32 samples from ADC2, store in DSPSRAM, autoincrement the DPSRAM pointer. 
When the transfer is complete, the DMA interrupt occurs, which disables the DMA channel and ADC2. 
The main() routine then reads/stores this data in the odd addresses in the final 64-word result buffer.

There is a timing diagram that will show you what the ADCs are doing (file '2 ADC Together.doc').

Once proper synchronization is verified (i.e. equal time observed between edges on the 2 GPIO signals),
then the ADC interrupts can be disabled. The initial delay between starting the two ADCs is provided
by a REPEAT instruction in main().  

Function Descriptions:

void initAdc1(void);
ADC1 CH0 and CH1 S/H is set-up to covert AIN0 in 10-bit mode. ADC is configured to next sample data immediately after the conversion.
So, ADC1 keeps convertion data throgh CH0/CH1 S/H alternatively. Effective conversion rate is 1.1Mhz (555.5kHz)

void initAdc2(void);
ADC2 CH0 and CH1 S/H is set-up to covert AIN0 in 10-bit mode. ADC is configured to next sample data immediately after the conversion.
So, ADC2 keeps convertion data throgh CH0/CH1 S/H alternatively. Effective conversion rate is 1.1Mhz (555.5kHz)

void initDma0(void);
DMA channel 0 is confiured in continuous mode to move the converted data from ADC1 to DMA RAM on every sample/convert sequence. 
It generates interrupt after every 32 sample transfer. It is disabled after the first transfer. 

void initDma1(void);
DMA channel 1 is confiured in continuous mode to move the converted data from ADC2 to DMA RAM on every sample/convert sequence. 
It generates interrupt after every 32 sample transfer. It is disabled after the first transfer. 

void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void);
DMA ch0 interrupt service routine, disables the DMA channel and sets flag to signal main() that ADC1 acquisition is complete

void __attribute__((interrupt, no_auto_psv)) _DMA1Interrupt(void);
DMA ch1 interrupt service routine, disables the DMA channel and sets flag to signal main() that ADC2 acquisition is complete


2. Folder Contents:
-------------------
This folder contains the following sub-folders:
a. C:\Program Files\Microchip\MPLAB C30\support\gld
        This folder will have the device GLD file, it is used for building the project. 
	This file was provided with the MPLAB� C30 toolsuite.

b. C:\Program Files\Microchip\MPLAB C30\support\h
        This folder contains C header files useful in building this
        project. Device register and bit definitions are provided in
        the *.h file that follows the device name. These files were provided
        with the MPLAB� C30 toolsuite.

c. C:\Program Files\Microchip\MPLAB C30\lib
        This folder contains library archive files, which are a
        collection of precompiled object files. The file
        named "libpic30-coff.a" contains the C run-time start-up
        library. These file were provided with the
        MPLAB� C30 toolsuite.

d. hex
        This folder contains three file types - coff, hex and map.
        These are files generated by the MPLAB� C30 toolsuite on build
        operation performed within MPLAB� IDE. The *.map file contains
        details on memory allocation for various variables, constants
        and dsPIC instructions specified in the source and library
        code. The *.hex file contains a binary file that may be
        programmed into the dsPIC device. The *.coff file contains
        a binary file that is used by MPLAB� IDE for simulation.

e. h
        This folder contains include files for the code example.


f. src
        This folder contains all the C and Assembler source files (*.c,
        *.s) used in demonstrating the described example. This folder
        also contains a sub-folder named "obj" that stores compiled
        object files generated when the project is built.


3. Suggested Development Resources:
-----------------------------------
        a. Explorer 16 Demo board (R4) with dsPIC33FJ256GP710 controller

4. Reconfiguring the project for a different dsPIC33F device:
-------------------------------------------------------------
The Project/Workspace can be easily reconfigured for any dsPIC33F device.
Please use the following general guidelines:
        a. Change device selection within MPLAB� IDE to a dsPIC33F device of
        your choice by using the following menu option:
        MPLAB IDE>>Configure>>Select Device

        b. Provide the correct device linker script and header file for your
        device. Device linker scripts and header files are available in your
        MPLAB� C30 installation folder under:
        Device Linker Script-
                YourDrive:>Program Files\Microchip\MPLAB C30\support\gld
        Device C Header file-
                YourDrive:>Program Files\Microchip\MPLAB C30\support\h
        Device ASM Include file-
                YourDrive:>Program Files\Microchip\MPLAB C30\support\inc

        c. Provide the appropriate path to your MPLAB C30 support file locations
        using the menu option:
        MPLAB IDE>>Project>>Build Options>>Project

        d. Chose the development board applicable to your device. Some options
        are provided below:


        e. Re-build the MPLAB� project using the menu option:
        MPLAB IDE>>Project>>Build All

        f. Download the hex file into the device and run.

5. Revision History :
---------------------
        05/06/2006 - Initial Release of the Code Example
