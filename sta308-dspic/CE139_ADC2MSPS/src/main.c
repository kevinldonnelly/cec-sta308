/**********************************************************************
* � 2005 Microchip Technology Inc.
*
* FileName:        main.c
* Dependencies:    Header (.h) files if applicable, see below
* Processor:       dsPIC33Fxxxx/PIC24Hxxxx
* Compiler:        MPLAB� C30 v3.00 or higher
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip's
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP'S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
* REVISION HISTORY:
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Author            Date      Comments on this revision
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Cecic D			05/06/06   Highly modified CE101 for dual ADC mode	
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**
*
* ADDITIONAL NOTES:
* Code Tested on:
* Explorer 16 Demo board (R4) with  dsPIC33FJ256GP710 controller (8MHz XTAL)
*
* The Processor starts with the External Crystal without PLL enabled and then the Clock is switched to PLL Mode.
**********************************************************************/

#if defined(__dsPIC33F__)
#include "p33Fxxxx.h"
#elif defined(__PIC24H__)
#include "p24Hxxxx.h"
#endif

#include "tglPin.h"
#include "adc1Drv.h"
#include "adc2Drv.h"

//Macros for Configuration Fuse Registers:
//Invoke macros to set up  device configuration fuse registers.
//The fuses will select the oscillator source, power-up timers, watch-dog
//timers etc. The macros are defined within the device
//header files. The configuration fuse registers reside in Flash memory.


// External Oscillator
_FOSCSEL(FNOSC_FRC); 
_FOSC(FCKSM_CSECMD & OSCIOFNC_OFF  & POSCMD_XT);  
								// Clock Switching is enabled and Fail Safe Clock Monitor is disabled
								// OSC2 Pin Function: OSC2 is Clock Output
								// Primary Oscillator Mode: XT Crystanl


_FWDT(FWDTEN_OFF);              // Watchdog Timer Enabled/disabled by user software
								// (LPRC can be disabled by clearing SWDTEN bit in RCON register
_FPOR(FPWRT_PWR1);   			// Turn off the power-up timers.
_FGS(GCP_OFF);            		// Disable Code Protection

void dummyWrite(void);			// Function to initialize DMA DPSRAM contents

unsigned int adcBuff[64];		// Buffer for interleaved results

int main (void)
{

int i;
extern int dma0_done, dma1_done;	// Flags used to signal to main() that both DMA channels finished
//extern int _DMA_BASE;				// DMA DPSRAM Base Addr= 0x7800 on the dsPIC33FJ256GP710 - defined in gld file
volatile unsigned int *adcPtr;		// Used as a pointer to DMA DPSRAM DMA Buffers

// Configure Oscillator to operate the device at 40Mhz
// Fosc= Fin*M/(N1*N2), Fcy=Fosc/2
// Fosc= 8M*40/(2*2)=80Mhz for 8M input clock
	PLLFBD=38;					// M=40
	CLKDIVbits.PLLPOST=0;		// N1=2
	CLKDIVbits.PLLPRE=0;		// N2=2
	OSCTUN=0;					// Tune FRC oscillator, if FRC is used

// Disable Watch Dog Timer
	RCONbits.SWDTEN=0;

// Clock switch to incorporate PLL
	__builtin_write_OSCCONH(0x03);		// Initiate Clock Switch to Primary
													// Oscillator with PLL (NOSC=0b011)
	__builtin_write_OSCCONL(0x01);		// Start clock switching
	while (OSCCONbits.COSC != 0b011);	// Wait for Clock switch to occur	


// Wait for PLL to lock
	while(OSCCONbits.LOCK!=1) {};


// Initialize DMA RAM
	dummyWrite();				// Initialize DMA RAM

// Initiallize Results Buffer

	for(i=0;i<64;i++)
	{
		adcBuff[i]=0;					// Reset result buffer
	}

// Peripheral Initialisation
   	tglPinInit();				// Initialize output pins that are toggled by ADC1/2 isr every conversion
								// Used to measure proper initial synchronization between the ADCs
								// 50% overlap on the signals means that proper interleaving is achieved, 
								// and that a proper 2x sampling of the signal is achieved
	initAdc1();             	// Initialize ADC1 to convert Channel 0
	initAdc2();					// Initialize ADC2 to convert Channel 0
	initDma0();					// Initialise DMA Ch0 controller to buffer ADC data in conversion order
	initDma1();					// Initialise DMA Ch1 controller to buffer ADC data in conversion order

    INTCON1bits.NSTDIS = 1;		// Disable interrupt nesting

	IEC0bits.AD1IE = 1;			// Enable ADC1 interrupts
	AD1CON1bits.ADON = 1;		// Turn on ADC1
	asm("repeat #35");
	asm("nop");					// 36Tcyc = 6Tad conversion delay between both ADC modules (@Tad=150nS)
	AD2CON1bits.ADON = 1;		// Turn on ADC2
	IEC1bits.AD2IE = 1;			// Enable ADC2 interrupt
   	IEC0bits.DMA0IE = 1;		// Enable DMA Ch0 interrupts
	IEC0bits.DMA1IE = 1;		// Enable DMA Ch1 interrupts

   	while(1)
	{
		if((dma0_done==1) && (dma1_done==1))
		{
			adcPtr = (unsigned int *)(&_DMA_BASE);		//Point to acquired data from ADC1
	
			for(i=0;i<64;i=i+2) 
			{
    			adcBuff[i]= *adcPtr++;	// Get ADC1 value and interleave into EVEN addresses in result array
			}

			adcPtr = (unsigned int *)(&_DMA_BASE + 0x20); // Point to acquired data from ADC2	
	
			for (i=1;i<64;i=i+2) 
			{
    			adcBuff[i]= *adcPtr++;	// Get ADC2 values and interleave into ODD addresses in result array
			}

			while(1);	// loop forever after acquisition or set breakpoint here to examine data
		}
	}
		
    return 0;
}


void dummyWrite(void)
{	
unsigned int *txMsgBox=(unsigned int *) &_DMA_BASE;
int i=0;
	
	for(i=0;i<1024;i++) {
		txMsgBox[i]=0xDEAD;	
	}

}



