/**********************************************************************
* � 2005 Microchip Technology Inc.
*
* FileName:        adc1Drv.c
* Dependencies:    Header (.h) files if applicable, see below
* Processor:       dsPIC33Fxxxx/PIC24Hxxxx
* Compiler:        MPLAB� C30 v3.00 or higher
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip's
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP'S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
* REVISION HISTORY:
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Author            Date      Comments on this revision
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Cecic D			04/06/06  Highly modified CE101 for dual ADC mode 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**
*
* ADDITIONAL NOTES:
* This file contains three functions - initAdc1(), initDma0() and _DMA0Interrupt().
* Note: continuous mode DMA is used and disabled after first transfer since
* single-shot mode not currently working
**********************************************************************/

#if defined(__dsPIC33F__)
#include "p33Fxxxx.h"
#elif defined(__PIC24H__)
#include "p24Hxxxx.h"
#endif

//Functions:
//initAdc1() is used to configure ADC1 to convert AIN0 using CH0 and CH1 sample/hold in sequencial mode 
//at 1.1MHz throughput rate. ADC clock is configured at 13.3Mhz or Tad=75ns
//several other settings are commented out for lower sample rates (per current silicon errata, 555.5kHz is max)
void initAdc1(void)
{

	//AD1CON1 Register
        AD1CON1bits.FORM = 0;		// Data Output Format: Unsigned Int (Dennis' Mod)
        AD1CON1bits.SSRC = 7;		// Internal Counter (SAMC) ends sampling and starts convertion
        AD1CON1bits.ASAM = 1;		// ADC Sample Control: Sampling begins immediately after conversion
 		AD1CON1bits.SIMSAM=0;		// Sequencial Sampling/conversion
		AD1CON1bits.AD12B =0;		// 10-bit 2/4-channel operation
		AD1CON1bits.ADDMABM=1; 		// DMA buffers are built in conversion order mode

    //AD1CON2 Register
        AD1CON2bits.SMPI=0;			// Increment DMA address every 1 sample/conversion
		AD1CON2bits.BUFM=0;
		AD1CON2bits.CHPS=1;			// Converts CH0/CH1


	//AD1CON3 Register        
		AD1CON3bits.ADRC=0;			// ADC Clock is derived from Systems Clock
		AD1CON3bits.SAMC=0; 		// Auto Sample Time = 0*Tad

//		AD1CON3bits.ADCS=2;		    // ADC Conversion Clock Tad=Tcy*(ADCS+1)= (1/40M)*3 = 75ns (13.3Mhz)
									// ADC Conversion Time for 10-bit Tc=12*Tab =  900ns (1.1MHz)

//		AD1CON3bits.ADCS=4;			// ADC Conversion Clock Tad=Tcy*(ADCS+1)= (1/40M)*5 = 125nS
									// ADC Conversion Time for 10-bit Tc=12*Tad = 1.5uS (666.7kHz)

		AD1CON3bits.ADCS=5;			// ADC Conversion Clock Tad=Tcy*(ADCS+1)= (1/40M)*6 = 150nS
									// ADC Conversion Time for 10-bit Tc=12*Tad = 1.8uS (555.5kHz)

//		AD1CON3bits.ADCS=6;			// ADC Conversion Clock Tad=Tcy*(ADCS+1)= (1/40M)*7 = 175nS
									// ADC Conversion Time for 10-bit Tc=12*Tad = 2.1uS (476.2kHz)
						
	//AD1CON4 Register  
		AD1CON4=0;					// Allocate 1 words of buffer to each analog input
									// This register is not used in conversion order mode
									// This is required only in the scatter/gather mode


  	//AD1CHS0/AD1CHS123: A/D Input Select Register
        AD1CHS0bits.CH0SA=0;		// MUXA +ve input selection (AIN0) for CH0
		AD1CHS0bits.CH0NA=0;		// MUXA -ve input selection (Vref-) for CH0

        AD1CHS123bits.CH123SA=0;	// MUXA +ve input selection (AIN0) for CH1
		AD1CHS123bits.CH123NA=0;	// MUXA -ve input selection (Vref-) for CH1


	//AD1PCFGH/AD1PCFGL: Port Configuration Register
		AD1PCFGL=0xFFFF;
		AD1PCFGH=0xFFFF;
		AD1PCFGLbits.PCFG0 = 0;		// AN0 as Analog Input

 	//AD1CSSH/AD1CSSL: A/D Input Scan Selection Register
        AD1CSSH = 0x0000;
		AD1CSSL = 0x0000;			// Channel Scan is disabled, default state
        

        IFS0bits.AD1IF = 0;			// Clear the A/D interrupt flag bit
        IEC0bits.AD1IE = 0;			// Do Not Enable A/D interrupt 
   	
    
       

}

// DMA0 configuration
// Direction: Read from peripheral address 0x300 (ADC1BUF0) and write to DMA RAM 
// AMODE: Regigster indirect with register increment
// MODE: Continuous, Ping-Pong Mode Disabled
// IRQ: ADC Interrupt

unsigned int dma0_done=0;
void initDma0(void)
{
	DMA0CON=0x0000;					// Configure DMA in Register indirect mode for (AD1CON1bits.ADDMABM=1)
									// Continuous mode (ping-pong disabled)
	DMA0PAD=0x0300;					// ADC1BUF0 address
	DMA0CNT=31;						// 32 DMA requests per block transfer
	DMA0STA=0x0000;					// BUFFER A: DMA_BASE[0] to DMA_BASE[31]		
	DMA0CONbits.CHEN=1;
	DMA0REQ=13;						// IRQ# for ADC1

	IFS0bits.DMA0IF = 0;			//Clear the DMA interrupt flag bit
 

}

/*=============================================================================
_DMA0Interrupt(): ISR name is chosen from the device linker script.
=============================================================================*/

void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void)
{
 	IFS0bits.DMA0IF = 0;		// Clear the DMA interrupt flag bit
	IEC0bits.DMA0IE = 0;		// Disable DMA Ch0 interrupts
	DMA0CONbits.CHEN = 0;		// Disable the DMA channel

	AD1CON1bits.ADON = 0;		// Turn off ADC1
	IFS0bits.AD1IF = 0;			// Clear the A/D interrupt flag bit
	IEC0bits.AD1IE = 0;			// Disable ADC1 interrupts

	dma0_done^=1;				// Toggle variable to signal main()
}

/*================================================================================
_ADC1Interrupt(): ISR name is chosen from the device linker script
=================================================================================*/
void __attribute__((interrupt, no_auto_psv)) _ADC1Interrupt(void)
{
	tglPinRA6();	// toggle RA6 on every conversion until we synchronize the 2ADCs
	IFS0bits.AD1IF = 0;			// Clear the A/D interrupt flag bit
}


