/**********************************************************************
* � 2005 Microchip Technology Inc.
*
* FileName:        adc2Drv.c
* Dependencies:    Header (.h) files if applicable, see below
* Processor:       dsPIC33Fxxxx/PIC24Hxxxx
* Compiler:        MPLAB� C30 v3.00 or higher
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip's
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP'S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
* REVISION HISTORY:
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Author            Date      Comments on this revision
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Cecic D			04/06/06  Highly modified CE101 for dual ADC mode 
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~**
*
* ADDITIONAL NOTES:
* This file contains three functions - initAdc2(), initDma1() and _DMA1Interrupt().
* Note: continuous mode DMA is used and disabled after first transfer since
* single-shot mode not currently working
*
**********************************************************************/

#if defined(__dsPIC33F__)
#include "p33Fxxxx.h"
#elif defined(__PIC24H__)
#include "p24Hxxxx.h"
#endif

//Functions:
//initAdc2() is used to configure ADC2 to convert AIN0 using CH0 and CH1 sample/hold in sequencial mode 
//at 1.1MHz throughput rate. ADC clock is configured at 13.3Mhz or Tad=75ns
//several other settings are commented out for lower sample rates (per current silicon errata, 555.5kHz is max)
void initAdc2(void)
{

	//AD2CON1 Register
        AD2CON1bits.FORM = 0;		// Data Output Format: Unsigned Int (Dennis' Mod)
        AD2CON1bits.SSRC = 7;		// Internal Counter (SAMC) ends sampling and starts convertion
        AD2CON1bits.ASAM = 1;		// ADC Sample Control: Sampling begins immediately after conversion
 		AD2CON1bits.SIMSAM=0;		// Sequencial Sampling/conversion
		AD2CON1bits.AD12B =0;		// 10-bit 2/4-channel operation
		AD2CON1bits.ADDMABM=1; 		// DMA buffers are built in conversion order mode

    //AD2CON2 Register
        AD2CON2bits.SMPI=0;			// Increment DMA address every 1 sample/conversion
		AD2CON2bits.BUFM=0;
		AD2CON2bits.CHPS=1;			// Converts CH0/CH1


	//AD2CON3 Register        
		AD2CON3bits.ADRC=0;			// ADC Clock is derived from Systems Clock
		AD2CON3bits.SAMC=0; 		// Auto Sample Time = 0*Tad

//		AD2CON3bits.ADCS=2;		    // ADC Conversion Clock Tad=Tcy*(ADCS+1)= (1/40M)*3 = 75ns (13.3Mhz)
									// ADC Conversion Time for 10-bit Tc=12*Tab =  900ns (1.1MHz)

//		AD2CON3bits.ADCS=4;			// ADC Conversion Clock Tad=Tcy*(ADCS+1)= (1/40M)*5 = 125nS
									// ADC Conversion Time for 10-bit Tc=12*Tad = 1.5uS (666.7kHz)

		AD2CON3bits.ADCS=5;			// ADC Conversion Clock Tad=Tcy*(ADCS+1)= (1/40M)*6 = 150nS
									// ADC Conversion Time for 10-bit Tc=12*Tad = 1.8uS (555.5kHz)

//		AD2CON3bits.ADCS=6;			// ADC Conversion Clock Tad=Tcy*(ADCS+1)= (1/40M)*7 = 175nS
									// ADC Conversion Time for 10-bit Tc=12*Tad = 2.1uS (476.2kHz)
						
	//AD2CON4 Register  
		AD2CON4=0;					// Allocate 1 words of buffer to each analog input
									// This register is not used in conversion order mode
									// This is required only in the scatter/gather mode


  	//AD2CHS0/AD2CHS123: A/D Input Select Register
        AD2CHS0bits.CH0SA=0;		// MUXA +ve input selection (AIN0) for CH0
		AD2CHS0bits.CH0NA=0;		// MUXA -ve input selection (Vref-) for CH0

        AD2CHS123bits.CH123SA=0;	// MUXA +ve input selection (AIN0) for CH1
		AD2CHS123bits.CH123NA=0;	// MUXA -ve input selection (Vref-) for CH1


	//AD2PCFGH/AD2PCFGL: Port Configuration Register
		AD2PCFGL=0xFFFF;
		AD2PCFGLbits.PCFG0 = 0;		// AN0 as Analog Input

 	//AD2CSSL: A/D Input Scan Selection Register
       	AD2CSSL = 0x0000;			// Channel Scan is disabled, default state
        

        IFS1bits.AD2IF = 0;			// Clear the A/D interrupt flag bit
        IEC1bits.AD2IE = 0;			// Do Not Enable A/D interrupt 
 
}

// DMA1 configuration
// Direction: Read from peripheral address 0x0340 (ADC2BUF0) and write to DMA RAM 
// AMODE: Regigster indirect with register increment
// MODE: Continuous, Ping-Pong Mode Disabled
// IRQ: ADC Interrupt

unsigned int dma1_done=0;
void initDma1(void)
{
	DMA1CON=0x0000;					// Configure DMA in Register indirect mode for (AD2CON1bits.ADDMABM=1)
									// Continuous mode (Ping Pong Disabled)
	DMA1PAD=0x0340;					// ADC2BUF0 address
	DMA1CNT=31;						// 31 DMA transfers until dma1 interrupt
	DMA1STA=0x0040;					// BUFFER C: DMA_BASE[32] to DMA_BASE[63]		
	DMA1CONbits.CHEN=1;
	DMA1REQ=21;						// IRQ# for ADC2

	IFS0bits.DMA1IF = 0;			//Clear the DMA interrupt flag bit
   
}

/*=============================================================================
_DMA1Interrupt(): ISR name is chosen from the device linker script.
=============================================================================*/

void __attribute__((interrupt, no_auto_psv)) _DMA1Interrupt(void)
{
  
	IFS0bits.DMA1IF = 0;		//Clear the DMA interrupt flag bit
	IEC0bits.DMA1IE = 0;		// Disable DMA Ch1 interrupts
	DMA1CONbits.CHEN = 0;		// Disable the DMA channel

	AD2CON1bits.ADON = 0;		// Turn off ADC2
	IFS1bits.AD2IF = 0;			// Clear the A/D interrupt flag bit
	IEC1bits.AD2IE = 0;			// Disable ADC2 interrupt
	
	dma1_done^=1;				// Toggle variable to signal main()
}

/*================================================================================
_ADC2Interrupt(): ISR name is chosen from the device linker script
=================================================================================*/
void __attribute__((interrupt, no_auto_psv)) _ADC2Interrupt(void)
{
	tglPinRA7();	// toggle RA7 on every conversion until we synchronize the 2ADCs
	IFS1bits.AD2IF = 0;			// Clear the A/D interrupt flag bit
}




