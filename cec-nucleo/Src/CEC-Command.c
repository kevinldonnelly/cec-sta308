/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* FreeRTOS+CLI includes. */

#include "FreeRTOS_CLI.h"
#include "hdmi_cec.h"
#include "cmsis_os.h"
/*
 * Implements the run-time-stats command.
 */
static portBASE_TYPE prvSendCECPacket( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvSendCECPacketKey( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvSendCECPacketSysAudioModeReq( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvSendCECPacketSetSystemAudioMode( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );
static portBASE_TYPE prvSendCECPacketInitiateARC( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString );

uint8_t CEC_Transmit_Data[8];
extern osSemaphoreId myBinarySemCECHandle;

#define InitiatorAddress 5

/* Structure that defines the "run-time-stats" command line command.   This
generates a table that shows how much run time each task has */
const CLI_Command_Definition_t prvSendCECPacketCommandDefinition =
{
	( const int8_t * const ) "tx", /* The command string to type. */
	( const int8_t * const ) "tx:\r\n Transmit packet\r\n\r\n",
	prvSendCECPacket, /* The function to run. */
	-1 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSendCECPacketKeyCommandDefinition =
{
	( const int8_t * const ) "key", /* The command string to type. */
	( const int8_t * const ) "key:\r\n Transmit ce key\r\n\r\n",
	prvSendCECPacketKey, /* The function to run. */
	1 /* No parameters are expected. */
};

//40:70:30:00
//message ID : 70 - A device implementing System Audio Control and which has volume control RC buttons (eg TV or STB) requests to use System Audio Mode to the amplifier (Directly addressed)

const CLI_Command_Definition_t prvSendCECPacketSysAudioModeReqCommandDefinition =
{
	( const int8_t * const ) "sysaudiomodereq", /* The command string to type. */
	( const int8_t * const ) "sysaudiomodereq:\r\n  A device implementing System Audio Control\r\n\r\n",
	prvSendCECPacketSysAudioModeReq, /* The function to run. */
	0 /* No parameters are expected. */
};
//Set System Audio Mode 08:72:00

const CLI_Command_Definition_t prvSendCECPacketSetSystemAudioModeCommandDefinition =
{
	( const int8_t * const ) "SetSystemAudioMode", /* The command string to type. */
	( const int8_t * const ) "SetSystemAudioMode:\r\n Set System Audio Mode\r\n\r\n",
	prvSendCECPacketSetSystemAudioMode, /* The function to run. */
	1 /* No parameters are expected. */
};

const CLI_Command_Definition_t prvSendCECPacketInitiateARCCommandDefinition =
{
	( const int8_t * const ) "InitARC", /* The command string to type. */
	( const int8_t * const ) "InitARC:\r\n Initiate/Dinit ARC\r\n\r\n",
	prvSendCECPacketInitiateARC, /* The function to run. */
	1 /* No parameters are expected. */
};

static portBASE_TYPE prvSendCECPacketSysAudioModeReq( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	HAL_StatusTypeDef s=0;
	int timeout=0;
	configASSERT( pcWriteBuffer );
	CEC_Transmit_Data[0]=0x70;
	CEC_Transmit_Data[1]=0x30;
	CEC_Transmit_Data[2]=0x00;

	do
	{
	vTaskDelay(1);
	s=HAL_CEC_Transmit_IT(&hcec,InitiatorAddress,0,CEC_Transmit_Data,3);

	} while(s!=HAL_OK && timeout++ < 10 );


	strcpy( ( char * ) pcWriteBuffer, "ok" );
	return pdFALSE;
}

//Initiate ARC

static portBASE_TYPE prvSendCECPacketInitiateARC( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	HAL_StatusTypeDef s=0;
	int timeout=0;
	int32_t lParameterValue=0;
	int8_t *pcParameterString;

	portBASE_TYPE xParameterStringLength, xReturn;
	configASSERT( pcWriteBuffer );

	pcParameterString = ( int8_t * ) FreeRTOS_CLIGetParameter
											(
												pcCommandString,		/* The command string itself. */
												1,		/* Return the next parameter. */
												&xParameterStringLength	/* Store the parameter string length. */
											);
	lParameterValue = ( int32_t ) atol( ( const char * ) pcParameterString );

	if(lParameterValue==1)CEC_Transmit_Data[0]=0xc0;
	else CEC_Transmit_Data[0]=0xc5;

	CEC_Transmit_Data[1]=0x00;

do{

	vTaskDelay(1);
	s=HAL_CEC_Transmit_IT(&hcec,InitiatorAddress,0,CEC_Transmit_Data,1);

}while(s!=HAL_OK && timeout++ < 10 );

//xSemaphoreTake(myBinarySemCECHandle, portMAX_DELAY);

sprintf( ( char * ) pcWriteBuffer, "H=%d t=%d", s,timeout );
osSemaphoreWait(myBinarySemCECHandle, 1000);

return pdFALSE;

}


//08:72:00
static portBASE_TYPE prvSendCECPacketSetSystemAudioMode( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{


	HAL_StatusTypeDef s=0;
		int8_t *pcParameterString;
		static portBASE_TYPE xParameterNumber = 0;
		portBASE_TYPE xParameterStringLength, xReturn;
		configASSERT( pcWriteBuffer );
		uint8_t ret;
		char Param[2];

		pcParameterString = ( int8_t * ) FreeRTOS_CLIGetParameter
										(
											pcCommandString,		/* The command string itself. */
											xParameterNumber,		/* Return the next parameter. */
											&xParameterStringLength	/* Store the parameter string length. */
										);

		if(xParameterStringLength==0){

						xParameterNumber++;
						return pdTRUE;
					}

		strncpy(Param,pcParameterString,2);
		ret=(uint8_t)strtol(Param,NULL,16);
		CEC_Transmit_Data[0]=0x72;
		CEC_Transmit_Data[1]=ret;

		s=HAL_CEC_Transmit_IT(&hcec,InitiatorAddress,0x0,CEC_Transmit_Data,2);

	//	 xSemaphoreTake(myBinarySemCECHandle, portMAX_DELAY);
	strcpy( ( char * ) pcWriteBuffer, "ok" );
	return pdFALSE;
}

static portBASE_TYPE prvSendCECPacketKey( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
	HAL_StatusTypeDef s=0;
	int timeout=0;
	int8_t *pcParameterString;

	portBASE_TYPE xParameterStringLength, xReturn;
	configASSERT( pcWriteBuffer );
	uint8_t ret;
	char Param[2];

	pcParameterString = ( int8_t * ) FreeRTOS_CLIGetParameter
									(
										pcCommandString,		/* The command string itself. */
										1,		/* Return the next parameter. */
										&xParameterStringLength	/* Store the parameter string length. */
									);



	strncpy(Param,pcParameterString,2);
	ret=(uint8_t)strtol(Param,NULL,16);
	CEC_Transmit_Data[0]=0x44;
	CEC_Transmit_Data[1]=ret;

	do
		{
		vTaskDelay(100);
		s=HAL_CEC_Transmit_IT(&hcec,InitiatorAddress,0,CEC_Transmit_Data,2);

	}while(s!=HAL_OK && timeout++ < 10);

	osSemaphoreWait(myBinarySemCECHandle, 1000);
	vTaskDelay(10);
	sprintf( ( char * ) pcWriteBuffer, "H=%d t=%d", s,timeout );

	if(s!=HAL_OK)return pdFALSE;



	CEC_Transmit_Data[0]=0x45;
	CEC_Transmit_Data[1]=0;
	timeout=0;



	do{
		s=HAL_CEC_Transmit_IT(&hcec,InitiatorAddress,0,CEC_Transmit_Data,1);

	}while(s!=HAL_OK && timeout++ < 10);

	osSemaphoreWait(myBinarySemCECHandle, 1000);




	return pdFALSE;
}

static portBASE_TYPE prvSendCECPacket( int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString )
{
int8_t *pcParameterString;
portBASE_TYPE xParameterStringLength, xReturn;
static portBASE_TYPE xParameterNumber = 0;

uint8_t ret;
char Param[2];
	/* Remove compile time warnings about unused parameters, and check the
	write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	write buffer length is adequate, so does not check for buffer overflows. */
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );
	memset(CEC_Transmit_Data,0,16);
	int index=0;
	HAL_StatusTypeDef s=0;
		/* Obtain the parameter string. */
	while(xParameterNumber<16)
	{
		pcParameterString = ( int8_t * ) FreeRTOS_CLIGetParameter
									(
										pcCommandString,		/* The command string itself. */
										xParameterNumber,		/* Return the next parameter. */
										&xParameterStringLength	/* Store the parameter string length. */
									);



		if(pcParameterString==NULL){

			if(xParameterNumber>0){

				s=HAL_CEC_Transmit_IT(&hcec,InitiatorAddress,0,CEC_Transmit_Data,index-1);
			//	xSemaphoreTake(myBinarySemCECHandle, portMAX_DELAY);

				break;
			}

		}
		if(xParameterStringLength==0){

				xParameterNumber++;
				continue;
			}

		strncpy(Param,pcParameterString,2);
		ret=(uint8_t)strtol(Param,NULL,16);
		CEC_Transmit_Data[index]=ret;
		index++;
		xParameterNumber++;
	}
	strcpy( ( char * ) pcWriteBuffer, "ok" );
	return pdFALSE;
}
//If the semaphore is a binary semaphore then 1 is returned if the semaphore is available,
//and 0 is returned if the semaphore is not available. 0 if taken

void HAL_CEC_TxCpltCallback(CEC_HandleTypeDef *hcec)
{
	  int txStatus = 0;
	  BaseType_t xHigherPriorityTaskWoken;
	  printf(">> \n");

	txStatus = xSemaphoreGiveFromISR(myBinarySemCECHandle, &xHigherPriorityTaskWoken);


	    if (pdPASS == txStatus) {
	      portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	    }



}


void HAL_CEC_RxCpltCallback(CEC_HandleTypeDef *hcec, uint32_t RxFrameSize)
  {
		uint8_t k = 0;


    	//printf("<< \n");
		//printf("<< %02x\n",hcec->Init.RxBuffer[k]);
    	//return;
       for (k = 0; k <RxFrameSize; k++)
       {


               if(k==1 && hcec->Init.RxBuffer[1]==0x83)
               {

            	   CEC_Transmit_Data[0]=0x84;
            	   CEC_Transmit_Data[1]=0x30;
            	   CEC_Transmit_Data[2]=0x00;
            	   CEC_Transmit_Data[3]=0x05;
            	//  HAL_CEC_Transmit_IT(hcec,InitiatorAddress,0xf,CEC_Transmit_Data,4);
               }
               else if(k==1 && hcec->Init.RxBuffer[1]==0xc3)
               {
            	   CEC_Transmit_Data[0]=0xc0;
            	   CEC_Transmit_Data[1]=0x00;
            	   CEC_Transmit_Data[2]=0x00;
            	   CEC_Transmit_Data[3]=0x00;
            	 //  HAL_CEC_Transmit_IT(hcec,InitiatorAddress,0x0,CEC_Transmit_Data,1);
               }
               printf("%02x:",hcec->Init.RxBuffer[k]);
       }
       printf("\r\n");

  }
