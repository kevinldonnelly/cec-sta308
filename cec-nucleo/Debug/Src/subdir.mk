################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/CEC-Command.c \
../Src/CLI-commands.c \
../Src/CS8416-Commands.c \
../Src/FreeRTOS_CLI.c \
../Src/I2C-Commands.c \
../Src/UART-poll-driven-command-console.c \
../Src/config.c \
../Src/dma.c \
../Src/flash.c \
../Src/freertos.c \
../Src/gpio.c \
../Src/hdmi_cec.c \
../Src/i2c.c \
../Src/i2s.c \
../Src/main.c \
../Src/stm32f0xx_hal_msp.c \
../Src/stm32f0xx_hal_timebase_TIM.c \
../Src/stm32f0xx_it.c \
../Src/syscalls.c \
../Src/system_stm32f0xx.c \
../Src/usart.c 

OBJS += \
./Src/CEC-Command.o \
./Src/CLI-commands.o \
./Src/CS8416-Commands.o \
./Src/FreeRTOS_CLI.o \
./Src/I2C-Commands.o \
./Src/UART-poll-driven-command-console.o \
./Src/config.o \
./Src/dma.o \
./Src/flash.o \
./Src/freertos.o \
./Src/gpio.o \
./Src/hdmi_cec.o \
./Src/i2c.o \
./Src/i2s.o \
./Src/main.o \
./Src/stm32f0xx_hal_msp.o \
./Src/stm32f0xx_hal_timebase_TIM.o \
./Src/stm32f0xx_it.o \
./Src/syscalls.o \
./Src/system_stm32f0xx.o \
./Src/usart.o 

C_DEPS += \
./Src/CEC-Command.d \
./Src/CLI-commands.d \
./Src/CS8416-Commands.d \
./Src/FreeRTOS_CLI.d \
./Src/I2C-Commands.d \
./Src/UART-poll-driven-command-console.d \
./Src/config.d \
./Src/dma.d \
./Src/flash.d \
./Src/freertos.d \
./Src/gpio.d \
./Src/hdmi_cec.d \
./Src/i2c.d \
./Src/i2s.d \
./Src/main.d \
./Src/stm32f0xx_hal_msp.d \
./Src/stm32f0xx_hal_timebase_TIM.d \
./Src/stm32f0xx_it.d \
./Src/syscalls.d \
./Src/system_stm32f0xx.d \
./Src/usart.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F042x6 -DUSE_FULL_LL_DRIVER -I"D:/workspace/cec-sta308/cec-nucleo/Inc" -I"D:/workspace/cec-sta308/cec-nucleo/Drivers/STM32F0xx_HAL_Driver/Inc" -I"D:/workspace/cec-sta308/cec-nucleo/Drivers/STM32F0xx_HAL_Driver/Inc/Legacy" -I"D:/workspace/cec-sta308/cec-nucleo/Drivers/CMSIS/Device/ST/STM32F0xx/Include" -I"D:/workspace/cec-sta308/cec-nucleo/Drivers/CMSIS/Include" -I"D:/workspace/cec-sta308/cec-nucleo/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM0" -I"D:/workspace/cec-sta308/cec-nucleo/Middlewares/Third_Party/FreeRTOS/Source/include" -I"D:/workspace/cec-sta308/cec-nucleo/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"  -O2 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


